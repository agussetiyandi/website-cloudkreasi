<div id="wdes-fullscreen-search-wrapper">
	<form method="get" id="wdes-fullscreen-searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input type="text" name="s" placeholder="<?php esc_attr_e('Search...', 'phox'); ?>" id="wdes-fullscreen-search-input">
		<button type="submit">
			<i class="fas fa-search fullscreen-search-icon"></i>
		</button>
	</form>
</div>
