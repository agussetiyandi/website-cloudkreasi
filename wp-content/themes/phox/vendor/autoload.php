<?php

/**
 * Register the autoloader.
 *
 * Based off the official PSR-0 autoloader example found here:
 * @see https://www.php-fig.org/psr/psr-0/
 *
 * @param string $class The fully-qualified class name
 *
 * @return void
 */

spl_autoload_register(function ($class) {

    $prefix = 'Phox\\';

    $base_dir =  WDES_THEME_DIR . '/includes/';

    $len = strlen($prefix);
    if (0 !== strncmp($prefix, $class, $len)) {

        return;
    }

    $relative_class = substr($class, $len);

    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    if (file_exists($file)) {
        require $file;
    }
});

