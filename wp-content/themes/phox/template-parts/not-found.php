<div class="error-p">
	<div class="title-error">

		<h2 ><?php echo esc_html__('Oops! That page can’t be found.', 'phox'); ?></h2>

		<?php if( is_home() && current_user_can('publish_posts' ) ): ?>

			<p><?php echo esc_html__('it seems we can\'t find what you\'re looking for. perhaps searching can help', 'phox'); ?></p>
			<?php get_search_form(); ?>

		<?php elseif(is_search()): ?>

			<p><?php echo esc_html__('Sorry, but nothing matched your search terms. Please try again with some different keywords', 'phox'); ?></p>
			<?php get_search_form(); ?>

		<?php else: ?>

		<p><?php echo esc_html__('it seems we can\'t find what you\'re looking for. perhaps searching can help', 'phox'); ?></p>
		<?php get_search_form(); ?>

		<?php endif; ?>
	</div>
</div>