<?php
/**
 * Footer Payments Template
 *
 * @Since 1.0
 * @Author WHMCSdes
 * @link https://whmcsdes.com
 */

#Display Payment
$display_payment = wdes_opts_get('footer-payments');

#Payment Data
$payments_data = wdes_opts_get( 'choose-payment' );

$payment_class = ( !is_null ( wdes_opts_get('footer-payment-class') ) ) ? wdes_opts_get('footer-payment-class') : '' ;

#Callback function to array_map
function payment_templ ($val){

    echo '<span class="fab fa-cc-'. esc_attr($val) .'"></span>';

}

?>
<?php if( !is_null( wdes_opts_get('footer-payments') ) && !is_null($payments_data)  ) : ?>
<div class="wdes-partners-company <?php echo esc_attr( $payment_class ); ?>">
	<div class="container">
		<div class="wdes-blocks-partner">
            <?php array_map( 'payment_templ' , $payments_data); ?>
		</div>
	</div>
</div>
<?php endif; ?>