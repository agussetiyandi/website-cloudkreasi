<?php
    $sidebar_count = 0;
    $sidebar_items = [];
    for($i = 1; $i <= 5; $i++){
        if(is_active_sidebar('footer-widget-'.$i)){
	        $sidebar_items []= 'footer-widget-'.$i;
            $sidebar_count++;
        }
    }
?>
<?php if($sidebar_count > 0) :
    $footer_col_lg = 12 / $sidebar_count ;
	$footer_col_md = ($footer_col_lg === 12) ? '12' : $footer_col_lg + 1;
?>
    <div class="widgets_footer_area">
        <div class="container">
            <div class="row">
                <?php
                foreach ( $sidebar_items as $item ){
	                if (is_active_sidebar($item)) {
                        echo '<div class="col-lg-'.$footer_col_lg.' col-sm-12 col-md-'.$footer_col_md.'">';
                            dynamic_sidebar($item);
                        echo '</div>';
	                }
                }
                ?>
            </div>
        </div>
    </div>
<?php endif; ?>