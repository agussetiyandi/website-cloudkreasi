<?php
/**
 * The Template for displaying all single products
 *
 * @version 2.0.1
 */

defined( 'ABSPATH' ) || exit;

get_header();

/**
 * Hook: wdes_rs_before_main_content.
 * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::wrappers
 */
do_action( 'wdes_rs_before_main_content' );

while ( have_posts() ) : the_post();

    x_wdes()->wdes_get_tp( 'reseller_product/content', 'single-product' );

endwhile;


/**
 * Hook: wdes_rs_after_main_content.
 * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::wrappers()
 */
do_action( 'wdes_rs_after_main_content' );

get_footer();
