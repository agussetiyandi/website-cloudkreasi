<?php
/**
 * The template for displaying product content within loops
 *
 * @version 2.0.1
 */

defined( 'ABSPATH' ) || exit;

global $post;

// Ensure visibility.
if ( empty( $post ) || ! $post->post_status == 'publish' ) {
    return;
}
print ( '<li class="wdes-rs-product-item product">' );

    /**
     * Hook: wdes_rs_before_shop_loop_item.
     *
     * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::template_loop_product_link
     */
    do_action( 'wdes_rs_before_shop_loop_item' );

    /**
     * Hook: wdes_rs_before_shop_loop_item_title.
     *
     * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::shop_loop_item_title
     */
    do_action( 'wdes_rs_before_shop_loop_item_title' );

    /**
     * Hook: wdes_rs_shop_loop_item_title.
     *
     * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::shop_loop_item_title
     */
    do_action( 'wdes_rs_shop_loop_item_title' );


    /**
     * Hook: wdes_rs_after_shop_loop_item_title.
     *
     * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::shop_loop_item_title
     */
    do_action( 'wdes_rs_after_shop_loop_item_title' );



    /**
     * Hook: wdes_rs_after_shop_loop_item.
     *
     * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::template_loop_product_link
     */
    do_action( 'wdes_rs_after_shop_loop_item' );

print( '</li>' );


