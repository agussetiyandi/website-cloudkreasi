<?php
/**
 * The Template for displaying all single products
 *
 * @version 2.0.1
 */

defined( 'ABSPATH' ) || exit;

global $post;

/**
 * Hook: wdes_rs_before_single_product.
 */
do_action( 'wdes_rs_before_single_product' );

if ( post_password_required() ) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}

printf('<div id="product-%1$d" %2$s>', $post->ID, 'class="' . esc_attr( implode( ' ', get_post_class() ) ) . ' wide-posts-block custom-width-post body-article item-art inner-item-art "' );

    /**
     * Hook: wdes_rs_before_single_product_summary.
     * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::single_product_summary_wrappers
     */
    do_action( 'wdes_rs_before_single_product_summary' );


    print('<div class="summary entry-summary">');

        /**
         * Hook: wdes_rs_single_product_summary.
         * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::single_product_summary
         */
        do_action( 'wdes_rs_single_product_summary' );

    print('</div>');

    /**
     * Hook: wdes_rs_after_single_product.
     */
    do_action( 'wdes_rs_after_single_product_summary' );

print('</div>');