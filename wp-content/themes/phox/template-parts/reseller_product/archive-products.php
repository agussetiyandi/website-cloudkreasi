<?php
/**
 * The Template for displaying product archives
 *
 * @version 2.0.1
 */

defined( 'ABSPATH' ) || exit;

get_header();

/**
 * Hook: wdes_rs_before_main_content.
 * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::wrappers
 */
do_action( 'wdes_rs_before_main_content' );

if ( have_posts() ) :

    /**
     * Hook: wdes_rs_before_shop_loop.
     * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::shop_loop
     */
    do_action( 'wdes_rs_before_shop_loop' );

    while ( have_posts() ) : the_post();

        x_wdes()->wdes_get_tp( 'reseller_product/content', 'product' );

    endwhile;

    /**
     * Hook: wdes_rs_pagination.
     * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::pagination
     */
    do_action( 'wdes_rs_pagination' );

    /**
     * Hook: wdes_rs_after_shop_loop.
     * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::shop_loop
     */
    do_action( 'wdes_rs_after_shop_loop' );

endif;



/**
 * Hook: wdes_rs_after_main_content.
 * @see \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::wrappers()
 */
do_action( 'wdes_rs_after_main_content' );

get_footer();



