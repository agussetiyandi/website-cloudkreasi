<?php $top_header_layout = wdes_opts_get( 'top-header-layout' ); ?>

<div class="parent-top-first-lvl <?php echo esc_attr( $top_header_layout ); ?>">
    <div class="wdes-sub-items">
        <?php
            echo wdes_header()->menu_icons();
            x_wdes()->wdes_get_tp('header/header', 'social');
        ?>
    </div>
    <?php do_action('wdes_top_company_box'); ?>
</div>
