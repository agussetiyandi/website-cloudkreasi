<div class="container">
    <div class="row">
        <div class="parent-top-lvl">
            <!-- Logo & Menu -->
            <div class="l-area">

                <!-- Logo -->
                <?php x_wdes()->wdes_get_tp('header/header', 'logo'); ?>

            </div>

            <!-- Menu -->
            <?php x_wdes()->wdes_get_tp( 'header/header', 'menu' ); ?>
        </div>
    </div>
</div>