<hr class="custom-art-hr single-hr-custom">
<div class="text-area">
    <div class="body-article">
        <?php the_content(); ?>
        <!-- Tags -->
        <?php if(is_null(wdes_opts_get('single-post-tag') ) ): ?>

            <?php if(has_tag()) {
                echo '<div class="clearfix"></div>';
            }
            ?>

            <?php echo x_wdes()->wdes_get_tags(get_the_tag_list('', ''), 'art-block-sub tags-block-area'); ?>

        <?php endif; ?>
    </div>
    <div class="clearfix"></div>
    <?php x_wdes()->wdes_post_multi_pages(); ?>
</div>