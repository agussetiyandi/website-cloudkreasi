<?php if(have_posts()): ?>

    <!-- Item -->
    <?php while (have_posts()) : the_post(); ?>
        <div id="post-<?php the_ID() ?>" class="item-art inner-item-art <?php echo esc_attr( ( ! wdes_archive()->blog_title_background()? 'single-no-feature-img': '' ) ) ?>">
            <?php x_wdes()->wdes_get_tp('posts/entry', 'header'); ?>
            <?php x_wdes()->wdes_get_tp('posts/single', 'title'); ?>
            <?php x_wdes()->wdes_get_tp('posts/single', 'content'); ?>

    <?php endwhile; ?>

        <?php

            $is_author_active = wdes_opts_get('single-post-author');
            if(!is_page()){
                if( is_null($is_author_active) && !empty(get_the_author_meta('description')) ){
                    x_wdes()->wdes_get_tp('posts/single','author-box');
                }
            }

            #Share Button
            if( ! is_page() ) {

                wdes_archive()->single_share_button();

            }


        ?>
        </div>
<?php endif; ?>


<!-- Previous & Next -->
<?php
if(!is_page()){
	x_wdes()->wdes_content_nav();
}
?>
<div class="clearfix"></div>

<!-- comment -->
<?php if(is_null(wdes_opts_get('single-post-comments'))): ?>

        <?php comments_template(); ?>

<?php endif; ?>
