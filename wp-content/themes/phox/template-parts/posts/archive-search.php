<div class="row">
    <!-- Item -->
    <?php
        while ( have_posts() ) :the_post();
    ?>
        <div class="<?php echo wdes_archive()->posts_layout(); ?>">

            ﻿<?php

                $layout= 'classic';

                if($layout){

                    $class_classic = 'classic-post-view';

                }else{

                    $class_classic = '';

                }

            ?>
            
            <div id="post-<?php the_ID(); ?>" <?php post_class( array('item-art', $class_classic) ) ?>  >
                <!-- IMG -->
                <?php x_wdes()->wdes_get_tp( 'posts/entry', 'header' ); ?>
                <?php x_wdes()->wdes_get_tp( 'posts/entry', 'content' ); ?>
            </div>
        </div>
    <?php endwhile; ?>
</div>
<?php x_wdes()->wdes_posts_pagination(); ?>
