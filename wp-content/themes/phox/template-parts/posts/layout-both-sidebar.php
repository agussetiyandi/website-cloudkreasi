<?php

    do_action('wdes_before_blog');


    $sidebar_name = wdes_header()->sidebar_name('both');

    if(is_single()) {

        $type = 'single';

    }else{

        $type = 'archive';

    }
?>

<div class="blog-content <?php echo (is_single()) ?'article-page' : ''?>">
    <div class="container">
        <div class="row">

            <?php if(is_active_sidebar('sidebar-'.$sidebar_name[1])) { ?>
                <?php get_sidebar($sidebar_name[1]); ?>
            <?php } ?>

            <div class="<?php echo x_wdes()->wdes_sys_col_grid($type); ?>">
                <?php
                if(is_single()|| is_page()){
                    x_wdes()->wdes_get_tp('posts/single');
                } elseif (is_archive() || is_search()){
                    x_wdes()->wdes_get_tp('posts/archive');
                }else{
                    x_wdes()->wdes_get_tp('posts/archive');
                }
                ?>
            </div>

            <?php if(is_active_sidebar('sidebar-'.$sidebar_name[0])) { ?>
                <?php get_sidebar($sidebar_name[0]); ?>
            <?php } ?>

        </div>
    </div>
</div>

