<?php

    $layout = 'classic';

    if ( empty(get_the_post_thumbnail()) && $layout ) {
        $class_content = 'cat-content-width-x';
    } else {
        $class_content = '';
    }
?>
<div class="text-area <?php echo esc_attr($class_content); ?>">

    <div class="art-info-sub">

    <!-- Date -->
    <?php wdes_archive()->blog_post_content(); ?>

    </div>
    <div class="func-hr2 custom-post-padding"></div>
    <!-- Title -->
    <a class="title-art-sub <?php if( empty(get_the_post_thumbnail_url()) ) { echo 'custom-title-art-sub-width'; } ?>" href="<?php esc_url( the_permalink() ); ?>">
        <?php if(!empty(get_the_title())){
            echo get_the_title();
        }else{
            echo esc_html_e('No Title', 'phox');
        }  ?>
    </a>

    <div class="info-content-hp">
        <?php x_wdes()->wdes_the_excerpt(); ?>
    </div>
    <!-- Continue Reading -->
    <a class="read-more-btn" href="<?php echo esc_url( get_the_permalink() );  ?>"><?php esc_html_e('Continue reading','phox'); ?></a>
</div>