<div class="author-blog">
    <!-- IMG -->
    <?php  echo get_avatar(get_the_author_meta('user_email', get_the_author_meta('ID')), '180'); ?>

    <div class="details-author">
        <!-- User -->
        <a href="<?php echo esc_url( get_author_posts_url(get_the_author_meta('ID')) ); ?>"><?php the_author_meta('nickname'); ?></a>
        <!-- Information -->
        <p><?php the_author_meta('description');  ?></p>

        <!-- SocialMedia -->
        <?php
        if( class_exists('Phox_Host\Author_Profile' ) ) {
	        $author_social = \Phox_Host\Author_Profile::wdes_author_social_array();

	        echo '<ul class="socialmedia-author">';

	        foreach ( $author_social as $key => $value ) {
		        if ( get_the_author_meta( $key ) ) {
			        $icon = empty ( $value['icon'] ) ? $key : $value['icon'];

			        echo '<li>';
			        echo '<a class="fab fa-' . $icon . '" href="' . esc_url( get_the_author_meta( $key ) ) . '"></a>';
			        echo '</li>';
		        }

	        }
	        echo '</ul>';
        }

        ?>
    </div>
</div>