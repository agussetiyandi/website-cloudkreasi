<?php

    do_action('wdes_before_blog');



    if(is_single()) {

        $type = 'single';

    }else{

        $type = 'archive';

    }
?>

<div class="blog-content <?php echo (is_single()) ?'article-page' : ''?>">
    <div class="container">
        <div class="row">
            <div class="<?php echo x_wdes()->wdes_sys_col_grid($type); ?>">
                <?php
                if(is_single()|| is_page()){
                    x_wdes()->wdes_get_tp('posts/single');
                } elseif (is_archive() || is_search()){
                    x_wdes()->wdes_get_tp('posts/archive');
                }else{
                    x_wdes()->wdes_get_tp('posts/archive');
                }
                ?>
            </div>
        </div>
    </div>
</div>

