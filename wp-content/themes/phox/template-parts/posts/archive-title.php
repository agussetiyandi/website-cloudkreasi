<div class=parent-img-post-inner>
    <div <?php x_wdes()->wdes_get_text( wdes_archive()->blog_class_style() ); ?>>
    </div>
    <div class="parent-title-archive">
        <div class="container">
            <div class="row">
                <div class="custom-width-area-content">
                <!-- Title -->
                <div class="main-page-title-in">
                    <h2>
                        <?php
                        if( is_category() ) :
                            printf(esc_html__('Category Archives: %s', 'phox'), '<span>'. single_cat_title('', false) .'</span>');

                        elseif (is_tag()) :
                            printf(esc_html__('Tag Archives: %s', 'phox'), '<span>'. single_tag_title('', false) .'</span>');

                        elseif ( is_search() ) :
                            printf(esc_html__('search Results for: %s', 'phox'), '<span>'. get_search_query() .'</span>');

                        elseif (is_author()) :
                            the_post();
                            printf(esc_html__('Author Archives: %s', 'phox'), '<span><a class="" href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '" title=" ' . esc_attr(get_the_author()) .' " >'. get_the_author() .'</a></span>');
                            rewind_posts();

                        elseif ( is_day() ) :
                            printf(esc_html__('Daily Archives: %s', 'phox'), '<span>'. get_the_date() .'</span>');

                        elseif ( is_month() ) :
                            printf(esc_html__('Monthly Archives: %s', 'phox'), '<span>'. get_the_date('F Y') .'</span>');

                        elseif ( is_year() ) :
                            printf(esc_html__('Yearly Archives: %s', 'phox'), '<span>'. get_the_date('Y') .'</span>');

                        elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
                            esc_html_e( 'Asides', 'phox' );

                        elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
                            esc_html_e( 'Images', 'phox');

                        elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
                            esc_html_e( 'Videos', 'phox' );

                        elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
                            esc_html_e( 'Quotes', 'phox' );

                        elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
                            esc_html_e( 'Links', 'phox' );

                        elseif ( is_single() || is_page() ) :

                            if(!empty(get_the_title())){
                                echo get_the_title();
                            }else{
                                echo esc_html__('No Title', 'phox');
                            }

                        else :
                            //get the blog title and description from blog options
                             wdes_archive()->blog_title();

                        endif;
                        ?>
                    </h2>
                    <?php if( is_home() && ! empty( wdes_opts_get( 'blog-page-description' )  ) ) : ?>

                        <p> <?php esc_html( wdes_archive()->blog_description() ); ?> </p>

                    <?php endif; ?>
                </div>

                <?php
                    if ( is_category() ) :
                        // show an optional category description
                        $category_description = category_description();
                        if ( ! empty( $category_description ) ) :
                                echo apply_filters( 'category_archive_meta', $category_description );
                        endif;

                    elseif ( is_tag() ) :
                        // show an optional tag description
                        $tag_description = tag_description();
                        if ( ! empty( $tag_description ) ) :
                            echo apply_filters( 'tag_archive_meta',  $tag_description );
                        endif;

                    endif;
                ?>

                <!-- Breadcrumb -->
                <?php x_wdes()->wdes_breadcrumb(); ?>

                </div>
            </div>
        </div>
    </div>
</div>