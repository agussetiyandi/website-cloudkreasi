<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php esc_attr(bloginfo('charset')) ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php do_action('wdes_seo'); ?>

    <?php echo wdes_header()->favicon(); ?>

    <!-- wp-head -->
    <?php wp_head() ?>
</head>
<body>
    <div class="coming">
        <div class="container">
            <!-- Logo -->
            <?php if( !empty( wdes_opts_get('logo') ) ): ?>
            <img class="logo-coming" src="<?php esc_url( wdes_opts_show('logo') );  ?>" alt="<?php esc_attr_e('Phox', 'phox'); ?>">
            <?php endif; ?>
            <!-- Title -->
            <div class="com-title">
                <h1><?php wdes_opts_show('comingsoon-text', esc_html__('Coming Soon', 'phox') ) ?></h1>
                <p><?php wdes_opts_show('comingsoon-description', esc_html__('We\'re currently working on creating something fantastic.', 'phox') ) ?></p>
            </div>
            <!-- Counter -->
            <div id="defaultCountdown"></div>
            <div class="clearfix"></div>
        </div>
    </div>

<?php wp_footer(); ?>
</body>
</html>