<?php
/**
 * The template for displaying comments.
 *
 */

if(post_password_required()){

	return;
}

function wdes_comment($comment,$args, $depth){

    $GLOBALS['comment'] = $comment;
    switch ($comment->comment_type) :
        case 'pingback'  :
        case 'trackback' :

?>
    <li class="post pingback" />
        <p><?php  esc_html_e('Pingback', 'phox');?> <?php comment_author_link(); ?><?php edit_comment_link(esc_html__('Edit', 'phox'), '<span class="edit-link">', '</span>') ?></p>
    </li>
    <?php break; ?>
       <?php default : ?>
            <li <?php comment_class('comment-area'); ?>  id="comment-<?php comment_ID(); ?>">

                <div id="li-comments-<?php comment_ID(); ?>"  class="comment-area">
                    <?php echo get_avatar($comment, 100); ?>
                    <div class="reply-area">
                        <a class="user-rep"><?php echo get_comment_author(); ?></a>
                        <?php edit_comment_link(esc_html__('Edit', 'phox')); ?>
                        <?php
                        comment_reply_link( array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );
                        ?>
                        <a class="date-rep"><?php printf( _x('%1$s - %2$s', '1: date, 2: time', 'phox'), get_comment_date(), get_comment_time() ); ?></a>
                        <p><?php comment_text(); ?></p>
                    </div>
                </div>
            </li>
            <?php
            break;
    endswitch;
}
?>
<?php if( have_comments() ): ?>
    <!-- Comments -->
    <div class="comments-blog">
        <div class="wid-title">
            <h2>
                <?php
                    $num_comments = get_comments_number(); // get_comments_number returns only a numeric value
                    if ( comments_open() ) {
                        if ( $num_comments == 0 ) {
                            $comments_m = esc_html__('No Comments', 'phox');
                        } elseif ( $num_comments > 1 ) {
                            $comments_m = $num_comments . esc_html__(' Comments', 'phox');
                        } else {
                            $comments_m = esc_html__('1 Comment', 'phox');
                        }
                        $write_comments = $comments_m ;
                    } else {
                        $write_comments =  esc_html__('Comments are off for this post.', 'phox');
                    }
                    echo esc_html($write_comments);
                ?>
            </h2>
            <div class="func-hr"></div>
            <div class="func-hr2"></div>
        </div>
        <ol class="comment-list">
            <?php wp_list_comments( array('callback' => 'wdes_comment' ) ); ?>
        </ol>

        ﻿<?php if ( get_comment_pages_count() > 1): ?>
            <nav id="comment-nav">
                <div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'phox' ) ); ?></div>
                <div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments ', 'phox' ) ); ?></div>
            </nav>
        <?php endif; ?>
    </div>
<?php endif; ?>

<div class="clearfix"></div>
<?php
    $args = array(
        'comment_notes_before' => '',
        'logged_in_as'  => '<p class="logged-in-as">' . sprintf(
            /* translators: 1: edit user link, 2: accessibility text, 3: user name, 4: logout URL */
             '<a href="%1$s" aria-label="%2$s">'.esc_html__('Logged in as %3$s', 'phox').'</a> <a href="%4$s">'.esc_html__('Log out?', 'phox').'</a>',
            get_edit_user_link(),
            /* translators: %s: user name */
            esc_attr( sprintf( esc_html__( 'Logged in as %s. Edit your profile.', 'phox' ), $user_identity ) ),
            $user_identity,
            wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) )
        ) . '</p>'
    );
?>
<?php comment_form($args); ?>
