<?php get_header(); ?>

<?php if ( have_posts() && trim( $_GET['s'] ) ) : ?>

<?php $sidebar_name = wdes_header()->sidebar_name('side'); ?>

<?php do_action( 'wdes_before_blog' ); ?>
    <div class="blog-content">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <?php
                            x_wdes()->wdes_get_tp( 'posts/archive', 'search' );
                        ?>
                    </div>

                    <?php if(is_active_sidebar('sidebar-'.$sidebar_name)) { ?>
                        <?php get_sidebar($sidebar_name); ?>
                    <?php } ?>

                </div>
            </div>
    </div>
<?php else : ?>

	<?php x_wdes()->wdes_get_tp('not-found'); ?>

<?php endif; ?>


<?php get_footer(); ?>
