<?php
/**
 * Carousels
 */

namespace Elementor;

if(! defined('ABSPATH') ) exit; //Exit if accessed directly

class WDES_Elementor_Carousels extends Widget_Base
{
    public function get_name(){
        return 'wdes-carousels-widget';
    }

    public function get_title(){
        return esc_html__('Carousels', 'phox');
    }

    public function get_icon(){
        return 'eicon-testimonial-carousel';
    }

    public function get_categories(){
        return ['phox-elements'];
    }

	public function show_in_panel(){
		return false;
	}

    protected function register_controls(){


				/*-- Layouts --*/

				$this->start_controls_section(
						'the_carousel_layout',
						[
								'label'	=> esc_html__('Layouts', 'phox'),
								'tab'		=> Controls_Manager::TAB_LAYOUT,
						]
				);

				$this->add_control('the_carousels_type',
						[
								'label' => esc_html__('Type', 'phox'),
								'type'  => Controls_Manager::SELECT2,
								'default'   => 'testimonial',
								'options'   => [
										'testimonial' => esc_html__('Testimonial', 'phox'),
										'team' => esc_html__('Team', 'phox'),
								],
								'multiple'  => false
						]
				);

				$this->end_controls_section();

				//Display

				$this->start_controls_section(
						'the_carousels_display',
						[
							'label'	=> esc_html__('Display', 'phox'),
							'tab'		=> Controls_Manager::TAB_LAYOUT,
						]
				);

				$this->add_control(
						'the_display_subtitle',
						[
							'label' => esc_html__('Subtitle Section', 'phox'),
							'type'      => Controls_Manager::SWITCHER,
							'default'   => 'yes',
						]
				);

				$this->end_controls_section();

				/*-- Content --*/

        $this->start_controls_section(
            'the_carousels_controls',
            ['label' => esc_html__('Carousels', 'phox'), ]
        );

	    $users_list = new Repeater();

	    $users_list->add_control(
		    'user_name',
		    [
			    'name'  => 'user_name',
			    'label' => esc_html__('Title', 'phox'),
			    'type'  => Controls_Manager::TEXT,
			    'default'   => esc_html__('User Name', 'phox')

		    ]
	    );

	    $users_list->add_control(
		    'user_website',
		    [
			    'label' => esc_html__('Subtitle', 'phox'),
			    'type'  => Controls_Manager::TEXT,

		    ]
	    );

	    $users_list->add_control(
		    'user_pic',
		    [
			    'label' => esc_html__('Image', 'phox'),
			    'type'  => Controls_Manager::MEDIA,
			    'default' => ['url' => 'https://placehold.it/50x50'],

		    ]
	    );

	    $users_list->add_control(
		    'user_comment',
		    [
			    'label' => esc_html__('Content', 'phox'),
			    'type'  => Controls_Manager::TEXTAREA,

		    ]
	    );

        $this->add_control(
            'the_users_list',
            [
                'label'  => esc_html__('Users Testimonials', 'phox'),
                'type'   => Controls_Manager::REPEATER,
                'fields' => $users_list->get_controls(),
                'title_field' => '{{{user_name}}}',
                'condition' => [
                    'the_carousels_type' => ['testimonial']
                ]
            ]
        );

        $this->end_controls_section();

				/*-- Style Tab --*/

				$this->start_controls_section(
						'the_carousel_style',
						[
								'label'	=> esc_html__('Carousel Style', 'phox'),
								'tab'		=> Controls_Manager::TAB_STYLE,
						]
				);

				/* Tap Image  */
				$this->add_control(
						'the_carousels_head',
						[
								'label' => esc_html__('Carousels Head', 'phox'),
								'type'      => Controls_Manager::SELECT,
								'default'   => 'content',
								'options'   => [
									'image' => esc_html__('Image', 'phox'),
									'content' => esc_html__('Content', 'phox')
								],
								'multiple'  => false

						]
				);

				$this->end_controls_section();


    }

    protected function render ($instance = []){

        x_wdes()->wdes_get_widget_block('carousels',[
            'carousels_type' => $this->get_settings('the_carousels_type'),
            'display_subtitle' => $this->get_settings('the_display_subtitle'),
            'users_list' => $this->get_settings('the_users_list'),
						'carousels_head' => $this->get_settings('the_carousels_head'),
        ]);
    }

    protected function content_template(){}
    public function render_plain_content(){}
}

Plugin::instance()->widgets_manager->register_widget_type(new WDES_Elementor_Carousels);
