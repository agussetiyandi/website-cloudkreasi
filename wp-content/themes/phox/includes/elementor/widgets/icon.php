<?php
/**
 * Icon
 */

namespace Elementor;

if(! defined('ABSPATH') ) exit; //Exit if accessed directly

class WDES_Elementor_ICON extends Widget_Base
{
    public function get_name(){
        return 'wdes-icon-widget';
    }

    public function get_title(){
        return esc_html__('Icons', 'phox');
    }

    public function get_icon(){
        return 'eicon-parallax';
    }

    public function get_categories(){
        return ['phox-elements'];
    }

	public function show_in_panel(){
		return false;
	}

    protected function register_controls(){

        $this->start_controls_section(
            'the_icon_controls',
            ['label' => esc_html__('Icons', 'phox'), ]
        );


        $this->add_control(
            'the_layouts',
            [
                'label' => esc_html__('Layouts', 'phox'),
                'type'  => Controls_Manager::SELECT2,
                'default'   => 'square',
                'options'   => [
                    'square' => esc_html__('Square', 'phox'),
                    'circle' => esc_html__('Circle', 'phox'),
                    'radius' => esc_html__('Radius', 'phox'),
                    'border' => esc_html__('Border', 'phox'),
                ],
                'multiple'  => false,
            ]
        );

        $this->add_control(
            'the_icon_anime',
            [
                'label' => esc_html__('Icon  Animation', 'phox'),
                'type'  => Controls_Manager::SWITCHER,
            ]
        );

        $this->add_control(
            'the_icon',
            [
                'label' => esc_html__('Icon', 'phox'),
                'type'  => Controls_Manager::ICONS,
                'default'   => [
                	'value' =>'fas fa-star'
                ],

            ]
        );

        $this->end_controls_section();

    }

    protected function render ($instance = []){

        x_wdes()->wdes_get_widget_block('icon',[
            'layouts' => $this->get_settings('the_layouts'),
            'icon_anime' => $this->get_settings('the_icon_anime'),
            'icon' => $this->get_settings('the_icon'),
        ]);
    }

    protected function content_template(){}
    public function render_plain_content(){}
}

Plugin::instance()->widgets_manager->register_widget_type(new WDES_Elementor_ICON);
