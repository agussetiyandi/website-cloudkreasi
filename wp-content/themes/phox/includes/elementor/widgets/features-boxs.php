<?php
/**
 * Features
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} //Exit if accessed directly

class WDES_Elementor_Features_Boxs extends Widget_Base {
	public function get_name() {
		return 'wdes-features-boxs-widget';
	}

	public function get_title() {
		return esc_html__( 'Features', 'phox' );
	}

	public function get_icon() {
		return 'eicon-favorite';
	}

	public function get_categories() {
		return [ 'phox-elements' ];
	}

	public function show_in_panel() {
		return false;
	}

	public function render_plain_content() {
	}

	protected function register_controls() {

		$this->start_controls_section(
			'the_features_boxs_controls',
			[ 'label' => esc_html__( 'Features', 'phox' ), ]
		);

		$this->add_control( 'the_info_layouts',
			[
				'label'    => esc_html__( 'Layouts', 'phox' ),
				'type'     => Controls_Manager::SELECT2,
				'default'  => 'l-1',
				'options'  => [
					'l-1' => esc_html__( 'Layout 1', 'phox' ),
					'l-2' => esc_html__( 'Layout 2', 'phox' ),
					'l-3' => esc_html__( 'Layout 3', 'phox' ),
					'l-4' => esc_html__( 'Layout 4', 'phox' ),
				],
				'multiple' => false
			]
		);

		//Layout 1

		$boxs_list = new Repeater();

		$boxs_list->add_control(
			'box_title',
			[
				'label'   => esc_html__( 'Box Title', 'phox' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'Box Title', 'phox' )

			]
		);

		$boxs_list->add_control(
			'box_icon',
			[
				'label' => esc_html__( 'Box Icon', 'phox' ),
				'type'  => Controls_Manager::ICONS,

			]
		);

		$boxs_list->add_control(
			'box_button',
			[
				'label' => esc_html__( 'Button Title', 'phox' ),
				'type'  => Controls_Manager::TEXT,

			]
		);

		$boxs_list->add_control(
			'box_url',
			[
				'label' => esc_html__( 'Button URL', 'phox' ),
				'type'  => Controls_Manager::URL,

			]
		);

		$this->add_control(
			'the_boxs_list',
			[
				'label'       => esc_html__( 'Box', 'phox' ),
				'type'        => Controls_Manager::REPEATER,
				'fields'      => $boxs_list->get_controls(),
				'title_field' => '{{{box_title}}}',
				'condition'   => [
					'the_info_layouts' => [ 'l-1' ]
				]
			]
		);

		//Layout 2

		$info_box_list = new Repeater();

		$info_box_list->add_control(
			'info_title',
			[
				'label'   => esc_html__( 'Title', 'phox' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'Info Title', 'phox' )

			]
		);

		$info_box_list->add_control(
			'info_desc',
			[
				'label' => esc_html__( 'Description', 'phox' ),
				'type'  => Controls_Manager::TEXTAREA,

			]
		);

		$this->add_control(
			'the_info_box_list',
			[
				'label'       => esc_html__( 'Info Box', 'phox' ),
				'type'        => Controls_Manager::REPEATER,
				'fields'      => $info_box_list->get_controls(),
				'title_field' => '{{{info_title}}}',
				'condition'   => [
					'the_info_layouts' => [ 'l-2' ]
				]
			]
		);

		//Layout 3

		$info_box_list_icon = new Repeater();

		$info_box_list_icon->add_control(
			'info_box_title',
			[
				'label'   => esc_html__( 'Title', 'phox' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'Info Title', 'phox' )

			]
		);

		$info_box_list_icon->add_control(
			'info_box_desc',
			[
				'label' => esc_html__( 'Description', 'phox' ),
				'type'  => Controls_Manager::TEXTAREA,

			]
		);

		$info_box_list_icon->add_control(
			'info_box_icon_layouts',
			[
				'label'    => esc_html__( 'Layouts', 'phox' ),
				'type'     => Controls_Manager::SELECT2,
				'default'  => 'square',
				'options'  => [
					'square' => esc_html__( 'Square', 'phox' ),
					'circle' => esc_html__( 'Circle', 'phox' ),
					'radius' => esc_html__( 'Radius', 'phox' ),
				],
				'multiple' => false,

			]
		);

		$info_box_list_icon->add_control(
			'info_box_icon_bg',
			[
				'label' => esc_html__( 'Icon Background ', 'phox' ),
				'type'  => Controls_Manager::SWITCHER,

			]
		);

		$info_box_list_icon->add_control(
			'info_box_icon',
			[
				'label'   => esc_html__( 'Icon', 'phox' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-rocket'
				]

			]
		);

		$this->add_control(
			'the_info_box_list_icon',
			[
				'label'       => esc_html__( 'Info Box', 'phox' ),
				'type'        => Controls_Manager::REPEATER,
				'fields'      => $info_box_list_icon->get_controls(),
				'title_field' => '{{{info_box_title}}}',
				'condition'   => [
					'the_info_layouts' => [ 'l-3' ]
				]
			]
		);

		//Layout 4

		$this->add_control( 'the_side_feature_title',
			[
				'label'     => esc_html__( 'Title', 'phox' ),
				'type'      => Controls_Manager::TEXT,
				'default'   => esc_html__( 'Title', 'phox' ),
				'condition' => [
					'the_info_layouts' => [ 'l-4' ]
				]
			]
		);

		$this->add_control( 'the_side_feature_img',
			[
				'label'     => esc_html__( 'Image', 'phox' ),
				'type'      => Controls_Manager::MEDIA,
				'condition' => [
					'the_info_layouts' => [ 'l-4' ]
				]
			]
		);

		$side_feature_item = new Repeater();

		$side_feature_item->add_control(
			'item_content',
			[
				'label'   => esc_html__( 'Item Content', 'phox' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'Item Content', 'phox' )

			]
		);

		$this->add_control( 'the_side_feature_item',
			[
				'label'       => esc_html__( 'Item', 'phox' ),
				'type'        => Controls_Manager::REPEATER,
				'fields'      => $side_feature_item->get_controls(),
				'title_field' => '{{{item_content}}}',
				'condition'   => [
					'the_info_layouts' => [ 'l-4' ]
				]
			]
		);

		$this->add_control( 'the_side_feature_button_show',
			[
				'label'     => esc_html__( 'Show Button', 'phox' ),
				'type'      => Controls_Manager::SWITCHER,
				'condition' => [
					'the_info_layouts' => [ 'l-4' ]
				]
			]
		);

		$this->add_control( 'the_side_feature_button_title',
			[
				'label'     => esc_html__( 'Button Title', 'phox' ),
				'type'      => Controls_Manager::TEXT,
				'default'   => esc_html__( 'Button Title', 'phox' ),
				'condition' => [
					'the_info_layouts' => [ 'l-4' ]
				]
			]
		);

		$this->add_control( 'the_side_feature_button_url',
			[
				'label'     => esc_html__( 'Button URL', 'phox' ),
				'type'      => Controls_Manager::URL,
				'condition' => [
					'the_info_layouts' => [ 'l-4' ]
				]
			]
		);


		$this->add_control(
			'position',
			[
				'label'        => esc_html__( 'Text Position', 'phox' ),
				'type'         => Controls_Manager::CHOOSE,
				'default'      => 'center',
				'options'      => [
					'left'   => [
						'title' => esc_html__( 'Left', 'phox' ),
						'icon'  => 'fas fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'phox' ),
						'icon'  => 'fas fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'phox' ),
						'icon'  => 'fas fa-align-right',
					],
				],
				'prefix_class' => 'block-info-alignment-',
				'toggle'       => false,
				'condition'    => [
					'the_info_layouts' => [ 'l-2', 'l-3' ]
				]
			]
		);

		$this->end_controls_section();

	}

	protected function render( $instance = [] ) {

		x_wdes()->wdes_get_widget_block( 'features-boxs', [
			'info_layouts'              => $this->get_settings( 'the_info_layouts' ),
			'box_list'                  => $this->get_settings( 'the_boxs_list' ),
			'info_box_list'             => $this->get_settings( 'the_info_box_list' ),
			'box_list_icon'             => $this->get_settings( 'the_info_box_list_icon' ),
			'side_feature_title'        => $this->get_settings( 'the_side_feature_title' ),
			'side_feature_img'          => $this->get_settings( 'the_side_feature_img' ),
			'side_feature_item'         => $this->get_settings( 'the_side_feature_item' ),
			'side_feature_button_show'  => $this->get_settings( 'the_side_feature_button_show' ),
			'side_feature_button_title' => $this->get_settings( 'the_side_feature_button_title' ),
			'side_feature_button_url'   => $this->get_settings( 'the_side_feature_button_url' ),
		] );
	}

	protected function content_template() {
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new WDES_Elementor_Features_Boxs );
