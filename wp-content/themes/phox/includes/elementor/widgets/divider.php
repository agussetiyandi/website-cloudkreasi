<?php
/**
 * Divider
 */

namespace Elementor;

if(! defined('ABSPATH') ) exit; //Exit if accessed directly

class WDES_Elementor_Divider extends Widget_Base
{
    public function get_name(){
        return 'wdes-divider-widget';
    }

    public function get_title(){
        return esc_html__('Dividers', 'phox');
    }

    public function get_icon(){
        return 'eicon-divider-shape';
    }

    public function get_categories(){
        return ['phox-elements'];
    }

	public function show_in_panel(){
		return false;
	}

    protected  function register_controls(){

        $this->start_controls_section(
            'the_divider_controls',
            ['label' => esc_html__('Divider', 'phox'), ]
        );


        $this->add_control(
            'left_color',
            [
                'label' => esc_html__( 'Left Color', 'phox' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#66c23a',
                'selectors' => [
                    '{{WRAPPER}} .l-border' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'right_color',
            [
                'label' => esc_html__( 'Right Color', 'phox' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#656d78',
                'selectors' => [
                    '{{WRAPPER}} .r-border' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'space',
            [
                'label' => esc_html__( 'Space', 'phox' ),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 15,
                ],
                'range' => [
                    'px' => [
                        'min' => 2,
                        'max' => 50,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .xdata-divider' => 'padding-top: {{SIZE}}{{UNIT}}; padding-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );



        $this->end_controls_section();

    }

    protected function render ($instance = []){
        x_wdes()->wdes_get_widget_block('divider');
    }

    protected function _content_template(){}
    public function render_plain_content(){}
}

Plugin::instance()->widgets_manager->register_widget_type(new WDES_Elementor_Divider);
