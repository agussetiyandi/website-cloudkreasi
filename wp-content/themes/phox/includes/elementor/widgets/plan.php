<?php
/**
 * Pricing
 */

namespace Elementor;

if(! defined('ABSPATH') ) exit; //Exit if accessed directly

class WDES_Elementor_Plan extends Widget_Base
{
    public function get_name(){
        return 'wdes-plan-widget';
    }

    public function get_title(){
        return esc_html__('Pricing', 'phox');
    }

    public function get_icon(){
        return 'eicon-price-table';
    }

    public function get_categories(){
        return ['phox-elements'];
    }

	public function show_in_panel(){
		return false;
	}

    protected  function register_controls(){

        $this->start_controls_section(
            'the_plan_controls',
            ['label' => esc_html__('Plan Content', 'phox'), ]
        );

	    $plans = new Repeater();

	    $plans->add_control(
		    'title',
		    [
			    'label' => esc_html__('Title', 'phox'),
			    'type'  => Controls_Manager::TEXT,
			    'default'   => esc_html__('Plan Title', 'phox')
		    ]
	    );

	    $plans->add_control(
		    'price',
		    [
			    'label' => esc_html__('Price', 'phox'),
			    'type'  => Controls_Manager::NUMBER,
		    ]
	    );

	    $plans->add_control(
		    'currency_symbol',
		    [
			    'label' => esc_html__( 'Currency Symbol', 'phox' ),
			    'type' => Controls_Manager::SELECT,
			    'options' => [
				    '' => __( 'None', 'phox' ),
				    'dollar' => '&#36; ' . esc_html__( 'Dollar', 'phox' ),
				    'euro' => '&#128; ' . esc_html__( 'Euro', 'phox' ),
				    'baht' => '&#3647; ' . esc_html__( 'Baht', 'phox' ),
				    'franc' => '&#8355; ' . esc_html__( 'Franc', 'phox' ),
				    'guilder' => '&fnof; ' . esc_html__( 'Guilder', 'phox' ),
				    'krona' => 'kr ' . esc_html__( 'Krona', 'phox' ),
				    'lira' => '&#8356; ' . esc_html__( 'Lira', 'phox' ),
				    'peseta' => '&#8359 ' . esc_html__( 'Peseta', 'phox' ),
				    'peso' => '&#8369; ' . esc_html__( 'Peso', 'phox' ),
				    'pound' => '&#163; ' . esc_html__( 'Pound Sterling', 'phox' ),
				    'real' => 'R$ ' . esc_html__( 'Real', 'phox' ),
				    'ruble' => '&#8381; ' . esc_html__( 'Ruble', 'phox' ),
				    'rupee' => '&#8360; ' . esc_html__( 'Rupee', 'phox' ),
				    'indian_rupee' => '&#8377; ' . esc_html__( 'Rupee (Indian)', 'phox' ),
				    'shekel' => '&#8362; ' . esc_html__( 'Shekel', 'phox' ),
				    'yen' => '&#165; ' . esc_html__( 'Yen/Yuan', 'phox' ),
				    'won' => '&#8361; ' . esc_html__( 'Won', 'phox' ),
				    'custom' => esc_html__( 'Custom', 'phox' ),
			    ],
			    'default' => 'dollar',
		    ]
	    );

	    $plans->add_control(
		    'currency_symbol_custom',
		    [
			    'label' => esc_html__( 'Custom Symbol', 'phox' ),
			    'type' => Controls_Manager::TEXT,
			    'condition' => [
				    'currency_symbol' => 'custom',
			    ],
		    ]
	    );

	    $plans->add_control(
		    'currency_symbol_location',
		    [
			    'label' => esc_html__( 'Symbol Location', 'phox' ),
			    'type' => Controls_Manager::SELECT,
			    'options'	=> [
				    'left' 	=> 'Left',
				    'right' => 'Right'
			    ],
			    'default'	=> 'left'
		    ]
	    );

	    $plans->add_control(
		    'payment_type',
		    [
			    'label' => esc_html__('Payment Type', 'phox'),
			    'type'  => Controls_Manager::SELECT2,
			    'default'   => 'month',
			    'options'   => [
				    'one-time' => esc_html__('One Time', 'phox'),
				    'year' 	=> esc_html__('Annually', 'phox'),
				    'month' => esc_html__('Monthly', 'phox'),
				    'custom' => esc_html__('Custom', 'phox'),
			    ],
			    'multiple'  => false
		    ]
	    );

	    $plans->add_control(
		    'payment_type_custom',
		    [
			    'label' => esc_html__( 'Custom Payment Type', 'phox' ),
			    'type' => Controls_Manager::TEXT,
			    'condition' => [
				    'payment_type' => 'custom',
			    ],
		    ]
	    );

	    $plans->add_control(
		    'icon',
		    [
			    'label' => esc_html__('Icon', 'phox'),
			    'type'  => Controls_Manager::MEDIA
		    ]
	    );

	    $plans->add_control(
		    'features_items',
		    [
			    'label' => esc_html__('Features Items', 'phox'),
			    'type'  => Controls_Manager::TEXTAREA
		    ]
	    );

	    $plans->add_control(
		    'popular',
		    [
			    'label' => esc_html__('Popular Tag', 'phox'),
			    'type'  => Controls_Manager::SWITCHER,
		    ]
	    );

	    $plans->add_control(
		    'order_button',
		    [
			    'label' => esc_html__('Order Button Text', 'phox'),
			    'type'  => Controls_Manager::TEXT,
			    'default' => esc_html__( 'Order Plan', 'phox' )
		    ]
	    );

	    $plans->add_control(
		    'order_button_url',
		    [
			    'label' => esc_html__('Order Button URL', 'phox'),
			    'type'  => Controls_Manager::URL
		    ]
	    );


        $this->add_control(
            'the_plans',
            [
                'label'  => esc_html__('Plans', 'phox'),
                'type'   => Controls_Manager::REPEATER,
                'fields' => $plans->get_controls(),
                'title_field' => '{{{title}}}',
                'condition' => [
                    'the_layouts' => ['l-1', 'l-2']
                ]
            ]
        );

	    $plans_3 = new Repeater();

	    $plans_3->add_control(
		    'title',
		    [
			    'label' => esc_html__('Title', 'phox'),
			    'type'  => Controls_Manager::TEXT,
			    'default'   => esc_html__('Plan Title', 'phox')
		    ]
	    );

	    $plans_3->add_control(
		    'price',
		    [
			    'label' => esc_html__('Price', 'phox'),
			    'type'  => Controls_Manager::NUMBER,
		    ]
	    );

	    $plans_3->add_control(
		    'currency_symbol',
		    [
			    'label' => esc_html__( 'Currency Symbol', 'phox' ),
			    'type' => Controls_Manager::SELECT,
			    'options' => [
				    '' => __( 'None', 'phox' ),
				    'dollar' => '&#36; ' . esc_html__( 'Dollar', 'phox' ),
				    'euro' => '&#128; ' . esc_html__( 'Euro', 'phox' ),
				    'baht' => '&#3647; ' . esc_html__( 'Baht', 'phox' ),
				    'franc' => '&#8355; ' . esc_html__( 'Franc', 'phox' ),
				    'guilder' => '&fnof; ' . esc_html__( 'Guilder', 'phox' ),
				    'krona' => 'kr ' . esc_html__( 'Krona', 'phox' ),
				    'lira' => '&#8356; ' . esc_html__( 'Lira', 'phox' ),
				    'peseta' => '&#8359 ' . esc_html__( 'Peseta', 'phox' ),
				    'peso' => '&#8369; ' . esc_html__( 'Peso', 'phox' ),
				    'pound' => '&#163; ' . esc_html__( 'Pound Sterling', 'phox' ),
				    'real' => 'R$ ' . esc_html__( 'Real', 'phox' ),
				    'ruble' => '&#8381; ' . esc_html__( 'Ruble', 'phox' ),
				    'rupee' => '&#8360; ' . esc_html__( 'Rupee', 'phox' ),
				    'indian_rupee' => '&#8377; ' . esc_html__( 'Rupee (Indian)', 'phox' ),
				    'shekel' => '&#8362; ' . esc_html__( 'Shekel', 'phox' ),
				    'yen' => '&#165; ' . esc_html__( 'Yen/Yuan', 'phox' ),
				    'won' => '&#8361; ' . esc_html__( 'Won', 'phox' ),
				    'custom' => esc_html__( 'Custom', 'phox' ),
			    ],
			    'default' => 'dollar',
		    ]
	    );

	    $plans_3->add_control(
		    'currency_symbol_custom',
		    [
			    'label' => esc_html__( 'Custom Symbol', 'phox' ),
			    'type' => Controls_Manager::TEXT,
			    'condition' => [
				    'currency_symbol' => 'custom',
			    ],
		    ]
	    );

	    $plans_3->add_control(
		    'currency_symbol_location',
		    [
			    'label' => esc_html__( 'Symbol Location', 'phox' ),
			    'type' => Controls_Manager::SELECT,
			    'options'	=> [
				    'left' 	=> 'Left',
				    'right' => 'Right'
			    ],
			    'default'	=> 'left'
		    ]
	    );

	    $plans_3->add_control(
		    'payment_type',
		    [
			    'label' => esc_html__('Payment Type', 'phox'),
			    'type'  => Controls_Manager::SELECT2,
			    'default'   => 'month',
			    'options'   => [
				    'one-time' => esc_html__('One Time', 'phox'),
				    'year' 	=> esc_html__('Annually', 'phox'),
				    'month' => esc_html__('Monthly', 'phox'),
				    'custom' => esc_html__('Custom', 'phox'),
			    ],
			    'multiple'  => false
		    ]
	    );

	    $plans_3->add_control(
		    'payment_type_custom',
		    [
			    'label' => esc_html__( 'Custom Payment Type', 'phox' ),
			    'type' => Controls_Manager::TEXT,
			    'condition' => [
				    'payment_type' => 'custom',
			    ],
		    ]
	    );

	    $plans_3->add_control(
		    'icon',
		    [
			    'label' => esc_html__('Icon', 'phox'),
			    'type'  => Controls_Manager::MEDIA
		    ]
	    );

	    $plans_3->add_control(
		    'features_items',
		    [
			    'label' => esc_html__('Features Items', 'phox'),
			    'type'  => Controls_Manager::TEXTAREA
		    ]
	    );

	    $plans_3->add_control(
		    'popular',
		    [
			    'label' => esc_html__('Popular Tag', 'phox'),
			    'type'  => Controls_Manager::SWITCHER,
		    ]
	    );

	    $plans_3->add_control(
		    'order_button',
		    [
			    'label' => esc_html__('Order Button Text', 'phox'),
			    'type'  => Controls_Manager::TEXT,
			    'default' => esc_html__( 'Order Plan', 'phox' )
		    ]
	    );

	    $plans_3->add_control(
		    'order_button_url',
		    [
			    'label' => esc_html__('Order Button URL', 'phox'),
			    'type'  => Controls_Manager::URL
		    ]
	    );

        $this->add_control(
            'the_plans_3',
            [
                'label'  => esc_html__('Plans', 'phox'),
                'type'   => Controls_Manager::REPEATER,
                'fields' => $plans_3->get_controls(),
                'title_field' => '{{{title}}}',
                'condition' => [
                    'the_layouts' => ['l-3']
                ]
            ]
        );

	    $plans_4_head = new Repeater();

	    $plans_4_head->add_control(
		    'head_content',
		    [
			    'name'  => 'head_content',
			    'label' => esc_html__('Content', 'phox'),
			    'type'  => Controls_Manager::TEXT
		    ]
	    );

	    $plans_4_head->add_control(
		    'popular_head',
		    [
			    'label' => esc_html__('Popular Item', 'phox'),
			    'type'  => Controls_Manager::SWITCHER
		    ]
	    );

        $this->add_control(
            'the_plans_4_head',
            [
                'label'  => esc_html__('Plans Table Head', 'phox'),
                'type'   => Controls_Manager::REPEATER,
                'fields' => $plans_4_head->get_controls(),
                'condition' => [
                    'the_layouts' => ['l-4']
                ],
				'default'	=>	[
					[
						'head_content'			=> esc_html__('Feature', 'phox'),
						'popular_head'			=> 'yes',

					],
					[
						'head_content'			=> esc_html__('Company 1', 'phox'),
						'popular_head'			=> 'no',

					]

				]
            ]
        );

	    $plans_4_body = new Repeater();

	    $plans_4_body->add_control(
		    'title',
		    [
			    'label' => esc_html__('Title', 'phox'),
			    'type'  => Controls_Manager::TEXT,
			    'default'   => esc_html__('Title', 'phox')
		    ]
	    );

	    $plans_4_body->add_control(
		    'row_head',
		    [
			    'name'  => 'row_head',
			    'label' => esc_html__('Row Head', 'phox'),
			    'type'  => Controls_Manager::TEXT
		    ]
	    );

	    $plans_4_body->add_control(
		    'price',
		    [
			    'label' => esc_html__('Price', 'phox'),
			    'type'  => Controls_Manager::SWITCHER
		    ]
	    );

	    $plans_4_body->add_control(
		    'body_content_button',
		    [
			    'label' => esc_html__('Add Button', 'phox'),
			    'type'  => Controls_Manager::SWITCHER
		    ]
	    );

	    $plans_4_body->add_control(
		    'body_content',
		    [
			    'label' => esc_html__('Content', 'phox'),
			    'type'  => Controls_Manager::TEXTAREA,
		    ]
	    );

	    $plans_4_body->add_control(
		    'button_title',
		    [
			    'label' => esc_html__('Button Title', 'phox'),
			    'type'  => Controls_Manager::TEXT,
			    'condition'	=> [
				    'body_content_button'	=> 'yes'
			    ]
		    ]
	    );

	    $plans_4_body->add_control(
		    'button_url',
		    [
			    'label' => esc_html__('Button Url', 'phox'),
			    'type'  => Controls_Manager::URL,
			    'condition'	=> [
				    'body_content_button'	=> 'yes'
			    ]
		    ]
	    );

        $this->add_control(
            'the_plans_4_body',
            [
                'label'  => esc_html__('Plans Table Body', 'phox'),
                'type'   => Controls_Manager::REPEATER,
                'fields' => $plans_4_body->get_controls(),
                'condition' => [
                    'the_layouts' => ['l-4']
                ],
				'default'	=>	[
					[
						'row_head'						=> esc_html__('Max Cores', 'phox'),
						'price'								=> 'no',
						'body_content_button'	=> 'no',
						'body_content'				=> esc_html__('12', 'phox'),
						'button_title'				=> esc_html(''),
						'button_url'				  => esc_url(''),

					]

				]
            ]
        );

	    $plans_4_footer = new Repeater();

	    $plans_4_footer->add_control(
		    'content_type',
		    [
			    'label' => esc_html__('Content Type', 'phox'),
			    'type'  => Controls_Manager::SELECT2,
			    'default'   => 'row-head',
			    'options'   => [
				    'row-head' => esc_html__('Row Head', 'phox'),
				    'content' => esc_html__('Content', 'phox')
			    ],
			    'multiple'  => false
		    ]
	    );

	    $plans_4_footer->add_control(
		    'footer_content',
		    [
			    'label' => esc_html__('Content', 'phox'),
			    'type'  => Controls_Manager::TEXT,
			    'condition' => [
				    'content_type' => ['row-head']
			    ]
		    ]
	    );

	    $plans_4_footer->add_control(
		    'footer_content_url',
		    [
			    'label' => esc_html__('Content', 'phox'),
			    'type'  => Controls_Manager::URL,
			    'condition' => [
				    'content_type' => ['content']
			    ]
		    ]
	    );

        $this->add_control(
            'the_plans_4_footer',
            [
                'label'  => esc_html__('Plans Table Footer', 'phox'),
                'type'   => Controls_Manager::REPEATER,
                'fields' => $plans_4_footer->get_controls(),
                'condition' => [
                    'the_layouts' => ['l-4'],
					'the_footer_display' => ['yes']

                ],
				'default'	=>	[
					[
						'content_type'		=> 'row-head',
						'footer_content' 	=> esc_html__('Select', 'phox')
					],
					[
						'content_type'		=> 'content',
						'footer_content_url' 	=> ['url' => esc_url('http://phox.whmcsdes.com')]
					]

				]
            ]
        );

        $this->end_controls_section();

				/*-- Layouts --*/

				$this->start_controls_section(
					'the_plan_there_layout_item',
					[
						'label'	=> esc_html__('Layouts', 'phox'),
						'tab'		=> Controls_Manager::TAB_LAYOUT,
					]
				);

				$this->add_control(
						'the_layouts',
						[
								'label' => esc_html__('Layouts', 'phox'),
								'type'      => Controls_Manager::SELECT2,
								'default'   => 'l-1',
								'options'   => [
									'l-1' => esc_html__('Layout 1', 'phox'),
									'l-2' => esc_html__('Layout 2', 'phox'),
									'l-3' => esc_html__('Layout 3', 'phox'),
									'l-4' => esc_html__('Layout 4', 'phox'),
								],
								'multiple'  => false,
								'separator' => 'after'

						]
				);

				/* Layout 4 */
				$this->add_control(
						'the_heading_layout_four_items',
						[
								'label'	=> esc_html__('Layout 4', 'phox'),
								'type'	=> Controls_Manager::HEADING,
								'condition'	=> [
									'the_layouts'	=> 'l-4'
								]
						]

				);

				$this->add_control(
						'the_footer_display',
						[
								'label' => esc_html__('Show The Footer', 'phox'),
								'type'      => Controls_Manager::SWITCHER,
								'default'   => 'yes',
								'condition' => [
									'the_layouts' => ['l-4']
								]
						]

				);

				$this->end_controls_section();


        /*-- Style Tab --*/

				/* Layout One */
        $this->start_controls_section(
						'the_plan_layout_one_style',
						[
								'label'	=> esc_html__('Layout', 'phox'),
								'tab'		=> Controls_Manager::TAB_STYLE,
								'condition'	=>	[
									'the_layouts'	=>	'l-1',
								]
						]
				);

				//Popular
				$this->add_control(
						'the_heading_popular_plan_layone',
						[
							'label'	=> esc_html__('Popular', 'phox'),
							'type'	=> Controls_Manager::HEADING,
						]
				);

				$this->add_control(
						'the_popular_border_width_plan_layone',
						[
								'label'	=> esc_html__('Popular Border Width', 'phox'),
								'type'	=> Controls_Manager::SLIDER,
								'default'	=> [
									'size'	=> 2
								],
								'range'	=>	[
										'px'	=>	[
											'min'	=> 0,
											'max'	=> 10,
										]
								],
								'selectors'	=>	[
									'{{WRAPPER}} .plan-hp.plan-popular'	=>	'border-width: {{SIZE}}{{UNIT}};'
								]
						]
				);

				$this->add_control(
						'the_popular_border_color_plan_layone',
						[
								'label'	=> esc_html__('Popular Border Color', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .plan-hp.plan-popular'	=>	'border-color: {{VALUE}};'
								]
						]
				);

				$this->add_control(
						'the_popular_button_color_plan_layone',
						[
								'label'	=> esc_html__('Popular Button Color', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .plan-hp.plan-popular a.order-plan'	=>	'background: {{VALUE}};'
								]
						]
				);

				//Price
				$this->add_control(
						'the_heading_price_plan_layone',
						[
							'label'	=> esc_html__('Price', 'phox'),
							'type'	=> Controls_Manager::HEADING,
						]
				);

				$this->add_control(
						'the_price_color_plan_layone',
						[
								'label'	=> esc_html__('Color', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .price'	=>	'color: {{VALUE}};'
								]
						]
				);

				//Button
				$this->add_control(
					'the_heading_button_plan_layone',
					[
						'label'	=> esc_html__('Button', 'phox'),
						'type'	=> Controls_Manager::HEADING,
					]
				);

				$this->add_control(
					'the_button_color_plan_layone',
					[
						'label'	=> esc_html__('Background', 'phox'),
						'type'	=> Controls_Manager::COLOR,
						'selectors'	=>	[
							'{{WRAPPER}} .plan-hp a.order-plan'	=>	'background: {{VALUE}};'
						]
					]
				);


        $this->end_controls_section();

				/* Layout Two */
				$this->start_controls_section(
						'the_plan_layout_two_style',
						[
								'label'	=> esc_html__('Layout', 'phox'),
								'tab'		=> Controls_Manager::TAB_STYLE,
								'condition'	=>	[
									'the_layouts'	=>	'l-2',
								]
						]
				);

				//Popular
				$this->add_control(
						'the_heading_popular_plan_laytwo',
						[
								'label'	=> esc_html__('Popular', 'phox'),
								'type'	=> Controls_Manager::HEADING,

						]
				);

				$this->add_control(
						'the_popular_border_width_plan_laytwo',
						[
								'label'	=> esc_html__('Popular Border Width', 'phox'),
								'type'	=> Controls_Manager::SLIDER,
								'default'	=> [
										'size'	=> 3
								],
								'range'	=>	[
										'px'	=>	[
												'min'	=> 0,
												'max'	=> 10,
										]
								],
								'selectors'	=>	[
									'{{WRAPPER}} .active-st2'	=>	'border-width: {{SIZE}}{{UNIT}};'
								]
						]
				);

				$this->add_control(
						'the_popular_border_color_plan_laytwo',
						[
								'label'	=> esc_html__('Popular Border Color', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
										'{{WRAPPER}} .active-st2'	=>	'border-color: {{VALUE}};'
								]
						]
				);

				$this->add_control(
						'the_popular_button_order_colo_plan_laytwo',
						[
								'label'	=> esc_html__('Button Order Color', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .active-st2 a.order-st2'	=>	'background-color: {{VALUE}} !important;'
								]
						]
				);

				$this->add_control(
						'the_popular_button_order_colo_hover_plan_laytwo',
						[
								'label'	=> esc_html__('Button Order Color Hover', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .active-st2 a.order-st2:hover'	=>	'background: {{VALUE}} !important;'
								]
						]
				);

				//Normal
				$this->add_control(
						'the_heading_normal_plan_laytwo',
						[
								'label'	=> esc_html__('Normal', 'phox'),
								'type'	=> Controls_Manager::HEADING,
								'separator'	=> 'before'

						]
				);

				$this->add_control(
					'the_button_order_colo_hover_plan_laytwo',
					[
						'label'	=> esc_html__('Button Order Color Hover', 'phox'),
						'type'	=> Controls_Manager::COLOR,
						'selectors'	=>	[
							'{{WRAPPER}} .plan-st2 a.order-st2:hover'	=>	'background: {{VALUE}};'
						]
					]
				);

				$this->add_control(
						'the_price_plan_laytwo',
						[
								'label'	=> esc_html__('Price', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .plan-st2 .price'	=>	'color: {{VALUE}};'
								]
						]
				);

				$this->end_controls_section();

				/* Layout There */
				$this->start_controls_section(
						'the_plan_layout_there_style',
						[
								'label'	=>	esc_html__('Layout', 'phox'),
								'tab'		=>	Controls_Manager::TAB_STYLE,
								'condition'	=>	[
									'the_layouts'	=>	'l-3',
								]
						]
				);

				//Popular
				$this->add_control(
					'the_heading_popular_plan_laythere',
					[
						'label'	=> esc_html__('Popular', 'phox'),
						'type'	=> Controls_Manager::HEADING,

					]
				);

				$this->add_control(
						'the_popular_border_width_plan_laythere',
						[
								'label'	=> esc_html__('Border Width', 'phox'),
								'type'	=> Controls_Manager::SLIDER,
								'default'	=> [
									'size'	=> 4
								],
								'range'	=>	[
										'px'	=>	[
											'min'	=> 0,
											'max'	=> 10,
										]
								],
								'selectors'	=>	[
									'{{WRAPPER}} .active-st3'	=>	'border-top-width: {{SIZE}}{{UNIT}} !important;'
								]
						]
				);

				$this->add_control(
						'the_popular_border_color_plan_laythere',
						[
								'label'	=> esc_html__('Border Color', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .active-st3'	=>	'border-top-color: {{VALUE}} !important;'
								]
						]
				);

				$this->add_control(
						'the_popular_button_color_plan_laythere',
						[
								'label'	=> esc_html__('Button Color', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .active-st3 a.order-st3'	=>	'background: {{VALUE}} !important;'
								]
						]
				);

				$this->add_control(
					'the_popular_title_color_plan_laythere',
					[
						'label'	=> esc_html__('Title Color', 'phox'),
						'type'	=> Controls_Manager::COLOR,
						'selectors'	=>	[
							'{{WRAPPER}} .active-st3 h3'	=>	'color: {{VALUE}} '
						]
					]
				);

				//Price
				$this->add_control(
					'the_heading_price_plan_laythere',
					[
						'label'	=> esc_html__('Price', 'phox'),
						'type'	=> Controls_Manager::HEADING,
						'separator'	=> 'before'
					]
				);

				$this->add_control(
						'the_price_color_plan_laythere',
						[
								'label'	=> esc_html__(' Color', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .plan-st3 .price-num'	=>	'color: {{VALUE}} ;'
								]
						]
				);

				//Button
				$this->add_control(
						'the_heading_button_plan_laythere',
						[
							'label'	=> esc_html__('Button','phox'),
							'type'	=> Controls_Manager::HEADING,
							'separator'	=> 'before'
						]
				);

				$this->add_control(
						'the_button_color_plan_laythere',
						[
								'label'	=> esc_html__('Color', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .plan-st3 a.order-st3'	=>	'color: {{VALUE}};'
								]
						]
				);

				$this->add_control(
						'the_button_background_plan_laythere',
						[
								'label'	=> esc_html__('Background', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .plan-st3 a.order-st3'	=>	'background: {{VALUE}};'
								]
						]
				);

				$this->add_control(
						'the_button_color_hover_plan_laythere',
						[
								'label'	=> esc_html__('Color Hover', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .plan-st3 a.order-st3:hover'	=>	'color: {{VALUE}};'
								]
						]
				);

				$this->add_control(
						'the_button_background_hover_plan_laythere',
						[
								'label'	=> esc_html__('Background Hover', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .plan-st3 a.order-st3:hover'	=>	'background: {{VALUE}};'
								]
						]
				);

				$this->end_controls_section();

				/* Layout Four : Head */
				$this->start_controls_section(
						'the_plan_layout_four_head_style',
						[
								'label'	=>	esc_html__('Head', 'phox'),
								'tab'		=>	Controls_Manager::TAB_STYLE,
								'condition'	=>	[
									'the_layouts'	=>	'l-4',
								]
						]
				);

				$this->add_control(
						'the_table_head_background_plan_layfour',
						[
								'label'	=> esc_html__('Background', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .wdes-table-ly4 .table-head tr '	=>	'background: {{VALUE}};'
								]
						]
				);

				$this->add_control(
					'the_popular_table_head_background_plan_layfour',
					[
						'label'	=> esc_html__('Popular Background', 'phox'),
						'type'	=> Controls_Manager::COLOR,
						'selectors'	=>	[
							'{{WRAPPER}} .plan-layout4-modern-active-btn '	=>	'background: {{VALUE}};'
						]
					]
				);

				$this->end_controls_section();

				/* Layout Four : Body */
				$this->start_controls_section(
						'the_plan_layout_four_body_style',
						[
								'label'	=>	esc_html__('Body', 'phox'),
								'tab'		=>	Controls_Manager::TAB_STYLE,
								'condition'	=>	[
									'the_layouts'	=>	'l-4',
								]
						]
				);

				$this->add_control(
					'the_heading_button_plan_layfour',
					[
						'label'	=> esc_html__('Button','phox'),
						'type'	=> Controls_Manager::HEADING,

					]
				);

				$this->add_control(
						'the_order_button_background_plan_layfour',
						[
								'label'	=> esc_html__('Background', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} a.ordernow-plan-modern '	=>	'background: {{VALUE}};'
								]
						]
				);

				$this->add_control(
						'the_order_button_color_plan_layfour',
						[
								'label'	=> esc_html__('Color', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} a.ordernow-plan-modern '	=>	'color: {{VALUE}};'
								]
						]
				);

				$this->end_controls_section();

				/* Layout Four : Footer */
				$this->start_controls_section(
						'the_plan_layout_four_footer_style',
						[
								'label'	=>	esc_html__('Footer', 'phox'),
								'tab'		=>	Controls_Manager::TAB_STYLE,
								'condition'	=>	[
									'the_layouts'	=>	'l-4',
								]
						]
				);

				$this->add_control(
						'the_shopping_cart_color_plan_layfour',
						[
								'label'	=> esc_html__('Color', 'phox'),
								'type'	=> Controls_Manager::COLOR,
								'selectors'	=>	[
									'{{WRAPPER}} .wdes-table-ly4 tfoot .order-table td a'	=>	'color: {{VALUE}};'
								]
						]
				);

				$this->end_controls_section();

    }

    protected function render ($instance = []){
        x_wdes()->wdes_get_widget_block('plan',[
            'layouts' => $this->get_settings('the_layouts'),
            'plans' => $this->get_settings('the_plans'),
            'plans_3' => $this->get_settings('the_plans_3'),
            'footer_display' => $this->get_settings('the_footer_display'),
            'plans_4_head' => $this->get_settings('the_plans_4_head'),
            'plans_4_body' => $this->get_settings('the_plans_4_body'),
            'plans_4_footer' => $this->get_settings('the_plans_4_footer')
        ]);
    }

    protected function content_template(){}
    public function render_plain_content(){}
}

Plugin::instance()->widgets_manager->register_widget_type(new WDES_Elementor_Plan);
