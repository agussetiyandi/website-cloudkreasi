<?php
/**
 * Features Tabs
 */

namespace Elementor;

if(! defined('ABSPATH') ) exit; //Exit if accessed directly

class WDES_Elementor_SOCIAL extends Widget_Base
{
    public function get_name(){
        return 'wdes-social-widget';
    }

    public function get_title(){
        return esc_html__('Social', 'phox');
    }

    public function get_icon(){
        return 'eicon-social-icons';
    }

    public function get_categories(){
        return ['phox-elements'];
    }

	public function show_in_panel(){
		return false;
	}

    protected  function register_controls(){

        $this->start_controls_section(
            'the_social_controls',
            ['label' => esc_html__('Social', 'phox'), ]
        );

	    $social_icon = new Repeater();

	    $social_icon->add_control(
		    'icon',
		    [
			    'label' => esc_html__('Icon', 'phox'),
			    'type'  => Controls_Manager::ICONS,
			    'default' => [ 'value' => 'fab fa-facebook'],
			    'include' => [
				    'fab fa-facebook',
				    'fab fa-twitter',
				    'fab fa-google-plus',
				    'fab fa-behance',
				    'fab fa-youtube',
				    'fab fa-instagram',
				    'fab fa-youtube',
			    ]

		    ]
	    );

	    $social_icon->add_control(
		    'link',
		    [
			    'label' => esc_html__('URL', 'phox'),
			    'type'  => Controls_Manager::URL,

		    ]
	    );

        $this->add_control(
            'the_social_icon',
            [
                'label'  => esc_html__('Social Icon', 'phox'),
                'type'   => Controls_Manager::REPEATER,
                'fields' => $social_icon->get_controls(),
                'title_field' => '{{{icon}}}',
            ]
        );


        $this->end_controls_section();

    }

    protected function render ($instance = []){

        x_wdes()->wdes_get_widget_block('social',[
            'social_icon' => $this->get_settings('the_social_icon')
        ]);
    }

    protected function content_template(){}
    public function render_plain_content(){}
}

Plugin::instance()->widgets_manager->register_widget_type(new WDES_Elementor_SOCIAL);
