<?php
/**
 * Accordion
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} //Exit if accessed directly

class WDES_Elementor_Faq extends Widget_Base {
	public function get_name() {
		return 'wdes-faq-widget';
	}

	public function get_title() {
		return esc_html__( 'Accordion', 'phox' );
	}

	public function get_icon() {
		return 'eicon-download-button';
	}

	public function get_categories() {
		return [ 'phox-elements' ];
	}

	public function show_in_panel() {
		return false;
	}

	public function render_plain_content() {
	}

	protected function register_controls() {

		$this->start_controls_section(
			'the_faq_controls',
			[ 'label' => esc_html__( 'Accordion', 'phox' ), ]
		);

		$this->add_control(
			'the_layouts',
			[
				'label'    => esc_html__( 'Layouts', 'phox' ),
				'type'     => Controls_Manager::SELECT2,
				'default'  => 'l-1',
				'options'  => [
					'l-1' => esc_html__( 'Layout 1', 'phox' ),
					'l-2' => esc_html__( 'Layout 2', 'phox' ),
				],
				'multiple' => false

			]
		);

		$faqs = new Repeater();

		$faqs->add_control(
			'question',
			[
				'label'   => esc_html__( 'Question', 'phox' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'Question', 'phox' )
			]
		);

		$faqs->add_control(
			'answer',
			[
				'label' => esc_html__( 'Answer', 'phox' ),
				'type'  => Controls_Manager::TEXTAREA,
			]
		);

		$this->add_control(
			'the_faqs',
			[
				'label'       => esc_html__( 'FAQs', 'phox' ),
				'type'        => Controls_Manager::REPEATER,
				'fields'      => $faqs->get_controls(),
				'title_field' => '{{{question}}}',
			]
		);

		$this->end_controls_section();

	}

	protected function render( $instance = [] ) {
		x_wdes()->wdes_get_widget_block( 'faq', [
			'layouts' => $this->get_settings( 'the_layouts' ),
			'faqs'    => $this->get_settings( 'the_faqs' ),
		] );
	}

	protected function content_template() {
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new WDES_Elementor_Faq );
