<?php
/**
 * Alert
 */

namespace Elementor;

if(! defined('ABSPATH') ) exit; //Exit if accessed directly

class WDES_Elementor_ALERT extends Widget_Base
{
    public function get_name(){
        return 'wdes-alert-widget';
    }

    public function get_title(){
        return esc_html__('Alerts', 'phox');
    }

    public function get_icon(){
        return 'eicon-alert';
    }

    public function get_categories(){
        return ['phox-elements'];
    }

	public function show_in_panel(){
		return false;
	}

    protected function register_controls(){

        $this->start_controls_section(
            'the_alert_controls',
            ['label' => esc_html__('Alert', 'phox'), ]
        );


        $this->add_control(
            'the_layouts',
            [
                'label' => esc_html__('Layouts', 'phox'),
                'type'      => Controls_Manager::SELECT2,
                'default'   => 'l-1',
                'options'   => [
                    'l-1' => esc_html__('Layout 1', 'phox'),
                    'l-2' => esc_html__('Layout 2', 'phox'),
                    'l-3' => esc_html__('Layout 3', 'phox'),
                    'l-4' => esc_html__('Layout 4', 'phox'),
                    'l-5' => esc_html__('Layout 5', 'phox'),
                    'l-6' => esc_html__('Layout 6', 'phox'),
                ],
                'multiple'  => false

            ]
        );

        $this->add_control(
            'the_alert_title',
            [
                'label' => esc_html__('Alert', 'phox'),
                'type'  => Controls_Manager::TEXTAREA,
                'default'   => esc_html__('Alert', 'phox')
            ]
        );

        $this->add_control(
            'the_icon',
            [
                'label' => esc_html__('Icon', 'phox'),
                'type'  => Controls_Manager::ICONS,
                'default'   => [
                	'value' => 'fas fa-star'
                ],
                'condition' => [
                    'the_layouts' => ['l-1', 'l-2']
                ]
            ]
        );

        $this->end_controls_section();

    }

    protected function render ($instance = []){

        x_wdes()->wdes_get_widget_block('alert',[
            'layouts' => $this->get_settings('the_layouts'),
            'alert_title' => $this->get_settings('the_alert_title'),
            'icon' => $this->get_settings('the_icon'),
        ]);
    }

    protected function content_template(){}
    public function render_plain_content(){}
}

Plugin::instance()->widgets_manager->register_widget_type(new WDES_Elementor_ALERT);
