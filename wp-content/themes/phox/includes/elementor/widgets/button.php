<?php
/**
 * Button
 */

namespace Elementor;

if(! defined('ABSPATH') ) exit; //Exit if accessed directly

class WDES_Elementor_BUTTON extends Widget_Base
{
    public function get_name(){
        return 'wdes-button-widget';
    }

    public function get_title(){
        return esc_html__('Buttons', 'phox');
    }

    public function get_icon(){
        return 'eicon-dual-button';
    }

    public function get_categories(){
        return ['phox-elements'];
    }

	public function show_in_panel(){
		return false;
	}

    protected function register_controls(){

        $this->start_controls_section(
            'the_button_controls',
            ['label' => esc_html__('Button', 'phox'), ]
        );


        $this->add_control(
            'the_layouts',
            [
                'label' => esc_html__('Layouts', 'phox'),
                'type'      => Controls_Manager::SELECT2,
                'default'   => 'l-1',
                'options'   => [
                    'l-1' => esc_html__('Layout 1', 'phox'),
                    'l-2' => esc_html__('Layout 2', 'phox'),
                    'l-3' => esc_html__('Layout 3', 'phox'),
                    'l-4' => esc_html__('Layout 4', 'phox'),
                    'l-5' => esc_html__('Layout 5', 'phox'),
                    'l-6' => esc_html__('Layout 6', 'phox'),
                    'l-7' => esc_html__('Layout 7', 'phox'),
                    'l-8' => esc_html__('Layout 8', 'phox'),
                ],
                'multiple'  => false

            ]
        );

        $this->add_control(
            'the_size',
            [
                'label' => esc_html__('Size', 'phox'),
                'type'      => Controls_Manager::SELECT2,
                'default'   => 'small',
                'options'   => [
                    'small' => esc_html__('Small', 'phox'),
                    'medium' => esc_html__('Medium', 'phox'),
                    'large' => esc_html__('Large', 'phox'),
                ],
                'multiple'  => false

            ]
        );

        $this->add_control(
            'the_button_title',
            [
                'label' => esc_html__('Button Title', 'phox'),
                'type'  => Controls_Manager::TEXT,
                'default'   => esc_html__('Button', 'phox')
            ]
        );

        $this->add_control(
            'the_button_url',
            [
                'label' => esc_html__('Button URL', 'phox'),
                'type'  => Controls_Manager::URL,
            ]
        );

        $this->add_control(
            'the_icon',
            [
                'label' => esc_html__('Icon', 'phox'),
                'type'  => Controls_Manager::ICONS,
                'condition' => [
                    'the_layouts' => ['l-4', 'l-5', 'l-6']
                ]
            ]
        );

        $this->add_control(
            'position',
            [
                'label' => esc_html__( 'Button Position', 'phox' ),
                'type' => Controls_Manager::CHOOSE,
                'default' => 'center',
                'options' => [
                    'left' => [
                        'title' => esc_html__( 'Left', 'phox' ),
                        'icon' => 'fas fa-align-left',
                    ],
                    'center' => [
                        'title' => esc_html__( 'Center', 'phox' ),
                        'icon' => 'fas fa-align-center',
                    ],
                    'right' => [
                        'title' => esc_html__( 'Right', 'phox' ),
                        'icon' => 'fas fa-align-right',
                    ],
                ],
                'prefix_class' => 'button-alignment-',
                'toggle' => false,
            ]
        );



        $this->end_controls_section();

    }

    protected function render ($instance = []){

        x_wdes()->wdes_get_widget_block('button',[
            'layouts' => $this->get_settings('the_layouts'),
            'size' => $this->get_settings('the_size'),
            'button_title' => $this->get_settings('the_button_title'),
            'button_url' => $this->get_settings('the_button_url'),
            'icon' => $this->get_settings('the_icon'),

        ]);
    }

    protected function content_template(){}
    public function render_plain_content(){}
}

Plugin::instance()->widgets_manager->register_widget_type(new WDES_Elementor_BUTTON);
