<?php
/**
 * Features Tabs
 */

namespace Elementor;

if(! defined('ABSPATH') ) exit; //Exit if accessed directly

class WDES_Elementor_Partners extends Widget_Base
{
    public function get_name(){
        return 'wdes-partner-widget';
    }

    public function get_title(){
        return esc_html__('Partners', 'phox');
    }

    public function get_icon(){
        return 'eicon-gallery-grid';
    }

    public function get_categories(){
        return ['phox-elements'];
    }

	public function show_in_panel(){
		return false;
	}

    protected  function register_controls(){

        $this->start_controls_section(
            'the_partner_controls',
            ['label' => esc_html__('Partner', 'phox'), ]
        );

	    $partners_list = new Repeater();

	    $partners_list->add_control(
		    'partner_name',
		    [
			    'label' => esc_html__('Partner Name', 'phox'),
			    'type'  => Controls_Manager::TEXT,
			    'default'   => esc_html__('Partner Name', 'phox')

		    ]
	    );

	    $partners_list->add_control(
		    'partner_logo',
		    [
			    'label' => esc_html__('Partner Logo', 'phox'),
			    'type'  => Controls_Manager::MEDIA,

		    ]
	    );

        $this->add_control(
            'the_partners_list',
            [
                'label'  => esc_html__('Partners', 'phox'),
                'type'   => Controls_Manager::REPEATER,
                'fields' => $partners_list->get_controls(),
                'title_field' => '{{{partner_name}}}',
            ]
        );


        $this->end_controls_section();

    }

    protected function render ($instance = []){

        x_wdes()->wdes_get_widget_block('partners',[
            'partners_list' => $this->get_settings('the_partners_list')
        ]);
    }

    protected function content_template(){}
    public function render_plain_content(){}
}

Plugin::instance()->widgets_manager->register_widget_type(new WDES_Elementor_Partners);
