<?php
/**
 * Map
 */

namespace Elementor;

if(! defined('ABSPATH') ) exit; //Exit if accessed directly

class WDES_Elementor_MAP extends Widget_Base
{
    public function get_name(){
        return 'wdes-map-widget';
    }

    public function get_title(){
        return esc_html__('Maps', 'phox');
    }

    public function get_icon(){
        return 'eicon-google-maps';
    }

    public function get_categories(){
        return ['phox-elements'];
    }

    public function get_script_depends(){
        return ['wdes-map'];
    }

	public function show_in_panel(){
		return false;
	}

    protected function register_controls(){

        $this->start_controls_section(
            'the_map_controls',
            ['label' => esc_html__('Map', 'phox'), ]
        );

        $this->add_control('the_map_layouts',
            [
                'label' => esc_html__('Layouts', 'phox'),
                'type'  => Controls_Manager::SELECT2,
                'default'   => 'map',
                'options'   => [
                    'map' => esc_html__('Map', 'phox'),
                    'world' => esc_html__('World Map', 'phox'),
                ],
                'multiple'  => false
            ]
        );


        $this->add_control(
            'the_lat',
            [
                'label' => esc_html__('Latitude', 'phox'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('40.67', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['map']
                ]
            ]
        );

        $this->add_control(
            'the_long',
            [
                'label' => esc_html__('Longitude', 'phox'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('-73.94', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['map']
                ]
            ]
        );

        $this->add_control(
            'the_zoom',
            [
                'label' => esc_html__('Zoom', 'phox'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('11', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['map']
                ]
            ]
        );

        // World Map

        $this->add_control(
            'the_world_name_1',
            [
                'label' => esc_html__('Country Name 1', 'phox'),
                'type'  => Controls_Manager::TEXT,
                'default'   => esc_html__('Paris', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );

        $this->add_control(
            'the_world_lat_1',
            [
                'label' => esc_html__('Latitude 1', 'phox'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('48.8567', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );

        $this->add_control(
            'the_world_long_1',
            [
                'label' => esc_html__('Longitude 1', 'phox'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('2.3510', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );

        $this->add_control(
            'the_world_name_2',
            [
                'label' => esc_html__('Country Name 2', 'phox'),
                'type'  => Controls_Manager::TEXT,
                'default'   => esc_html__('Toronto', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );

        $this->add_control(
            'the_world_lat_2',
            [
                'label' => esc_html__('Latitude 2', 'phox'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('43.8163', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );

        $this->add_control(
            'the_world_long_2',
            [
                'label' => esc_html__('Longitude 2', 'phox'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('-79.4287', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );

        $this->add_control(
            'the_world_name_3',
            [
                'label' => esc_html__('Country Name 3', 'phox'),
                'type'  => Controls_Manager::TEXT,
                'default'   => esc_html__('Los Angeles', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );

        $this->add_control(
            'the_world_lat_3',
            [
                'label' => esc_html__('Latitude 3', 'phox'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('34.3', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );

        $this->add_control(
            'the_world_long_3',
            [
                'label' => esc_html__('Longitude 3', 'phox'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('-118.15', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );

        $this->add_control(
            'the_world_name_4',
            [
                'label' => esc_html__('Country Name 4', 'phox'),
                'type'  => Controls_Manager::TEXT,
                'default'   => esc_html__('Havana', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );

        $this->add_control(
            'the_world_lat_4',
            [
                'label' => esc_html__('Latitude 4', 'phox'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('23', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );

        $this->add_control(
            'the_world_long_4',
            [
                'label' => esc_html__('Longitude 4', 'phox'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('-82', 'phox'),
                'condition' => [
                    'the_map_layouts' => ['world']
                ]
            ]

        );


        $this->end_controls_section();

    }

    protected function render ($instance = []){

        x_wdes()->wdes_get_widget_block('map',[
            'map_layouts' => $this->get_settings('the_map_layouts'),
            'lat' => $this->get_settings('the_lat'),
            'long' => $this->get_settings('the_long'),
            'zoom' => $this->get_settings('the_zoom'),
            'world_name_1' => $this->get_settings('the_world_name_1'),
            'world_lat_1' => $this->get_settings('the_world_lat_1'),
            'world_long_1' => $this->get_settings('the_world_long_1'),
            'world_name_2' => $this->get_settings('the_world_name_2'),
            'world_lat_2' => $this->get_settings('the_world_lat_2'),
            'world_long_2' => $this->get_settings('the_world_long_2'),
            'world_name_3' => $this->get_settings('the_world_name_3'),
            'world_lat_3' => $this->get_settings('the_world_lat_3'),
            'world_long_3' => $this->get_settings('the_world_long_3'),
            'world_name_4' => $this->get_settings('the_world_name_4'),
            'world_lat_4' => $this->get_settings('the_world_lat_4'),
            'world_long_4' => $this->get_settings('the_world_long_4'),
        ]);
    }

    protected function content_template(){}
    public function render_plain_content(){}
}

Plugin::instance()->widgets_manager->register_widget_type(new WDES_Elementor_Map);
