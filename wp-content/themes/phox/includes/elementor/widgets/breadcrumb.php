<?php
/**
 * Features Tabs
 */

namespace Elementor;

if(! defined('ABSPATH') ) exit; //Exit if accessed directly

class WDES_Elementor_Breadcrumb extends Widget_Base
{
    public function get_name(){
        return 'wdes-breadcrumb-widget';
    }

    public function get_title(){
        return esc_html__('Breadcrumbs', 'phox');
    }

    public function get_icon(){
        return 'eicon-product-breadcrumbs';
    }

    public function get_categories(){
        return ['phox-elements'];
    }

	public function show_in_panel(){
		return false;
	}

    protected  function register_controls(){

        $this->start_controls_section(
            'the_breadcrumb_controls',
            ['label' => esc_html__('Breadcrumb', 'phox'), ]
        );


        $this->add_control(
            'the_bread_title',
            [
                'label' => esc_html__('Breadcrumb Title', 'phox'),
                'type'  => Controls_Manager::TEXT,
                'default'   => esc_html__('Breadcrumb Title', 'phox')
            ]
        );

        $this->add_control(
            'the_bread_desc',
            [
                'label' => esc_html__('Breadcrumb Description', 'phox'),
                'type'  => Controls_Manager::TEXTAREA,
            ]
        );


        $this->add_control(
            'show_categories_bar',
            [
                'label'     => esc_html__('Show Categories Bar', 'phox'),
                'type'      => Controls_Manager::SWITCHER,

            ]
        );

	    $categories = new Repeater();

	    $categories->add_control(
		    'title',
		    [
			    'label' => esc_html__('Title', 'phox'),
			    'type'  => Controls_Manager::TEXT,
			    'default'   => esc_html__('Title', 'phox')
		    ]
	    );

	    $categories->add_control(
		    'slug',
		    [
			    'label' => esc_html__('Slug', 'phox'),
			    'type'  => Controls_Manager::TEXT,
		    ]
	    );

	    $this->add_control(
            'the_categories',
            [
                'label'     => esc_html__('Categories', 'phox'),
                'type'      => Controls_Manager::REPEATER,
                'fields'    => $categories->get_controls(),
                'title_field' => '{{{title}}}',
                'condition' => [
                    'show_categories_bar' => 'yes'
                ]
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_title_style',
            [
                'label' => esc_html__( 'Title and Description', 'phox' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'Title Color', 'phox' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#333',
                'scheme' => [
                    'type' => Core\Schemes\Color::get_type(),
                    'value' => Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    // Stronger selector to avoid section style from overwriting
                    '{{WRAPPER}} .bread-title h2' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'label' => esc_html__( 'Title Typography', 'phox' ),
                'name' => 'typography_title',
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .breadcrumb h2',
                'fields_options' => [
                    'font_weight' => [
                        'default' => '500',
                    ],
                    'font_family' => [
                        'default' => 'Poppins',
                    ]
                ],
            ]
        );

        $this->add_control(
            'break_color',
            [
                'label' => esc_html__( 'Break Color', 'phox' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#e7e7e7',
                'scheme' => [
                    'type' => Core\Schemes\Color::get_type(),
                    'value' => Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    // Stronger selector to avoid section style from overwriting
                    '{{WRAPPER}} .b-brd' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'desc_color',
            [
                'label' => esc_html__( 'Description Color', 'phox' ),
                'type' => Controls_Manager::COLOR,
                'default' => '#7e7e7e7e',
                'scheme' => [
                    'type' => Core\Schemes\Color::get_type(),
                    'value' => Core\Schemes\Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .breadcrumb p' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'label' => esc_html__( 'Description Typography', 'phox' ),
                'name' => 'typography_description',
                'scheme' => Core\Schemes\Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .breadcrumb p',
                'fields_options' => [
                    'font_weight' => [
                        'default' => '500',
                    ],
                    'font_family' => [
                        'default' => 'Poppins',
                    ]
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render ($instance = []){

        x_wdes()->wdes_get_widget_block('breadcrumb',[
            'bread_title' => $this->get_settings('the_bread_title'),
            'bread_desc' => $this->get_settings('the_bread_desc'),
            'show_categories' => $this->get_settings('show_categories_bar'),
            'categories' => $this->get_settings('the_categories'),
        ]);
    }

    protected function content_template(){}
    public function render_plain_content(){}
}

Plugin::instance()->widgets_manager->register_widget_type(new WDES_Elementor_Breadcrumb);
