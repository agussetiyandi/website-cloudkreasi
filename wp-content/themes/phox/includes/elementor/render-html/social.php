<?php
use Phox\core\shortcode\Wdes_SC_Social;

$data = x_wdes()->wdes_array_merge([
    'social_icon' => [],
],$data);

?>

<div class="social-media">
	<ul>

		<?php foreach ($data['social_icon'] as $item): ?>
			<li>
				<a target="_blank" class="<?php echo esc_attr (($item['icon'])? esc_attr($item['icon']) : '') ; ?> so-m" href="<?php echo esc_url(($item['link']['url'])? ($item['link']['url']) : '') ; ?>"></a>
			</li>
		<?php endforeach; ?>

	</ul>
</div>
