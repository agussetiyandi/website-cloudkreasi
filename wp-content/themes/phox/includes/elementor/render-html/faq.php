<?php

$data = x_wdes()->wdes_array_merge([
    'layouts' => '',
    'faqs' => [],
],$data);

$content_count = 0;

?>

<div class="container-fluid">
	<div class="row">
		<div class="col">

			<?php if ( $data['layouts'] === 'l-1'): ?>
				<div class="panel-group custom">
					<?php foreach ($data['faqs'] as $item): ?>
						<?php $accordion_id = 'wdes-'.$item['_id'];?>
						<div class="panel custom">
							<?php
							if($content_count === 0){
								$active =  'show';
							}else{
								$active =  '';
							}
							?>
							<div class="panel-heading custom">
								<h4 class="panel-title custom">
									<a role="button" data-toggle="collapse" data-parent="#<?php echo esc_attr($accordion_id)?>" href="#<?php echo esc_attr($accordion_id)?>" aria-expanded="true" aria-controls="<?php echo esc_attr($accordion_id)?>">
										<?php x_wdes()->wdes_get_text (($item['question']) ? esc_html($item['question']) : '') ;?>
										<span class="fas fa-plus coll-a"></span>
									</a>
								</h4>
							</div>


							<div id="<?php echo esc_attr($accordion_id)?>" class="panel-collapse collapse <?php echo esc_attr($active); ?> custom" aria-labelledby="<?php echo esc_attr($accordion_id)?>" data-parent="#<?php echo esc_attr($accordion_id)?>">
								<div class="panel-body custom">
									<?php x_wdes()->wdes_get_text (($item['answer']) ? $item['answer'] : '') ;?>
								</div>
							</div>
						</div>
						<?php $content_count++; ?>

					<?php endforeach; ?>
				</div>

			<?php elseif ( $data['layouts'] === 'l-2'): ?>

				<div class="block-q" >
					<?php foreach ($data['faqs'] as $item): ?>

						<div class="faq-b">
							<div class="title-faq-lv2">
								<span class="fas fa-question-circle custom-qn"></span>
								<span class="title-faq-q"><?php x_wdes()->wdes_get_text (($item['question']) ? esc_html($item['question']) : '') ;?></span>
							</div>
							<p><?php x_wdes()->wdes_get_text (($item['answer']) ? $item['answer'] : '') ; ?></p>
						</div>

					<?php endforeach; ?>
				</div>

			<?php endif; ?>

		</div>
	</div>
</div>

