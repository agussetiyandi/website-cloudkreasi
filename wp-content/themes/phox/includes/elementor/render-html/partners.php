<?php

$data = x_wdes()->wdes_array_merge([
    'partners_list' => [],
],$data);

?>

<div class="partners">
	<div class="container-fluid">
		<div class="row">

			<?php foreach ($data['partners_list'] as $item) : ?>
				<div class="col">
					<?php if (!empty($item['partner_logo']['url']) ) : ?>
						<img  src="<?php echo esc_url($item['partner_logo']['url']) ?>" alt="<?php echo esc_attr($item['partner_name']); ?>">
					<?php endif; ?>
				</div>
			<?php endforeach; ?>

		</div>
	</div>
</div>



