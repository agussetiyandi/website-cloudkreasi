<?php

$data = x_wdes()->wdes_array_merge([
    'info_layouts'                  => '',
    'box_list'                      => [],
    'info_box_list'                 => [],
    'box_list_icon'                 => [],
    'side_feature_title'            => '',
    'side_feature_img'              => [],
    'side_feature_item'             => [],
    'side_feature_button_show'      => '',
    'side_feature_button_title' => '',
    'side_feature_button_url'       => []
],$data);

if($data['info_layouts'] === 'l-4'): ?>
	<div class="container-fluid">
		<div class="row">
			<div class="best-feature">

				<div class="l-item-f">
					<?php if(!empty($data['side_feature_img']['url'])): ?>
						<img src="<?php echo esc_url($data['side_feature_img']['url']); ?>" alt="<?php esc_attr_e('best feature', 'phox'); ?>">
					<?php endif; ?>
				</div>

				<div class="r-item-f">
					<div class="item-c-center">
						<div class="head">
							<h2> <?php x_wdes()->wdes_get_text (($data['side_feature_title']) ? esc_html($data['side_feature_title']) : '') ; ?></h2>
						</div>
						<div class="content">
							<ul>
								<?php if(!empty($data['side_feature_item'])):?>
									<?php foreach ($data['side_feature_item'] as $item): ?>
										<li><?php x_wdes()->wdes_get_text (($item['item_content']) ? esc_html($item['item_content']) : '') ; ?></li>
									<?php endforeach; ?>
								<?php endif; ?>
							</ul>
							<?php if( $data['side_feature_button_show'] === 'yes'): ?>
								<a href="<?php x_wdes()->wdes_get_text (($data['side_feature_button_url']['url'])? esc_url($data['side_feature_button_url']['url']) : '') ; ?>"><?php x_wdes()->wdes_get_text (($data['side_feature_button_title'])? esc_html($data['side_feature_button_title']) : '') ; ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php else: ?>
	<section class="integrations">
		<div class="container-fluid">
			<div class="row">
				<?php if( $data['info_layouts'] === 'l-1' ): ?>
					<?php foreach ($data['box_list'] as $item) : ?>
						<div class="col">
							<div class="block-inte">
								<i class="fa <?php x_wdes()->wdes_get_text (($item['box_icon'])?esc_attr($item['box_icon']): '') ; ?> serv"></i>
								<h3><?php x_wdes()->wdes_get_text (($item['box_title']) ? esc_html($item['box_title']) : '') ; ?></h3>
								<?php if($item['box_url']['url']): ?>
									<a href="<?php echo esc_url($item['box_url']['url'])?>"><?php x_wdes()->wdes_get_text (($item['box_title']) ? esc_html($item['box_button']) : '') ; ?></a>
								<?php endif; ?>
							</div>
						</div>
					<?php endforeach; ?>
				<?php elseif ( $data['info_layouts'] === 'l-2'): ?>
					<?php foreach ($data['info_box_list'] as $item) : ?>
						<div class="col">
							<div class="block-comp">
								<span><?php x_wdes()->wdes_get_text (($item['info_title']) ? esc_html($item['info_title']) : ''); ?></span>
								<p><?php x_wdes()->wdes_get_text (($item['info_desc']) ? esc_html($item['info_desc']) : '') ; ?></p>
							</div>
						</div>
					<?php endforeach; ?>
				<?php elseif ( $data['info_layouts'] === 'l-3' ): ?>
					<?php foreach ($data['box_list_icon'] as $item) : ?>
						<?php
						$bg = '';
						if($item['info_box_icon_layouts'] === 'square'){
							$icon_class = 'short-sq';
						}elseif ($item['info_box_icon_layouts'] === 'circle'){
							$icon_class = 'short-circ';
						}else{
							$icon_class = 'short-rad';
						}

						if($item['info_box_icon_bg'] === 'yes'){
							$bg = 'bgz';
						}
						?>

						<div class="col t-sty-a blocks-ti">
							<i class="<?php x_wdes()->wdes_get_text (($item['info_box_icon']) ? esc_attr($item['info_box_icon']) : '') ; ?> <?php echo esc_attr($icon_class) .' '. esc_attr($bg)?>"></i>
							<h2><?php x_wdes()->wdes_get_text (($item['info_box_title']) ? esc_html($item['info_box_title']) : '') ; ?></h2>
							<p><?php x_wdes()->wdes_get_text (($item['info_box_desc']) ? esc_html($item['info_box_desc']) : '') ; ?></p>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
