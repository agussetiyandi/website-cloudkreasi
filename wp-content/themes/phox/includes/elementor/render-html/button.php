<?php

$data = x_wdes()->wdes_array_merge([
    'layouts' => '',
    'size'  => '',
    'button_title' => '',
    'button_url'   => [],
    'icon'         => ''
],$data);

switch ($data['layouts']) {
	case "l-1":
		$layout_class = 'bg-btn';
	break;
	case "l-2":
		$layout_class = 'bg-btn-o-brd';
	break;
	case "l-3":
		$layout_class = 'bg-btn-b-brd';
	break;
	case "l-4":
		$layout_class = 'bg-btn-icon';
	break;
	case "l-5":
		$layout_class = 'bg-btn-o-brd';
	break;
	case "l-6":
		$layout_class = 'bg-btn-b-brd';
	break;
	case "l-7":
		$layout_class = 'bg-btn-cys';
	break;
	default:
		$layout_class = 'bg-btn-cys-brd';
	break;
}


switch ($data['size']) {
	case "medium":
		$size_class = 'md';
	break;
	case "large":
		$size_class = 'lg';
	break;
	default :
		$size_class = '';
	break;
}

?>

<a class="<?php echo esc_attr($layout_class).' '.esc_attr($size_class) ; ?>" href="<?php echo esc_url($data['button_url']['url']) ?>">

	<?php if ( $data['layouts'] === 'l-4' || $data['layouts'] === 'l-5' || $data['layouts'] === 'l-6' ): ?>

	<span class="<?php echo esc_attr($data['icon'])?> s-btn-c"></span><?php x_wdes()->wdes_get_text (($data['button_title']) ?esc_html($data['button_title']) : ''); ?>

	<?php else: ?>

		<?php echo esc_html($data['button_title']) ?>

	<?php endif; ?>

</a>




