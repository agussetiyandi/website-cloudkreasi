<?php

$data = x_wdes()->wdes_array_merge([
    'map_layouts' => '',
    'lat' => '',
    'long' => '',
    'zoom' => '',
    'world_name_1' => '',
    'world_lat_1' => '',
    'world_long_1' => '',
    'world_name_2' => '',
    'world_lat_2' => '',
    'world_long_2' => '',
    'world_name_3' => '',
    'world_lat_3' => '',
    'world_long_3' => '',
    'world_name_4' => '',
    'world_lat_4' => '',
    'world_long_4' => '',

],$data);

?>

<div class="wdes-container-map" data-layout ="<?php echo esc_attr($data['map_layouts']);?>">
	<?php if( $data['map_layouts'] === 'map'): ?>
		<div id="map" data-lat="<?php echo esc_html($data['lat']); ?>" data-ing="<?php echo esc_html($data['long']); ?>" data-zoom="<?php echo esc_html($data['zoom']); ?>" style="width:100%; height:474px"></div>
	<?php else: ?>
		<div class="datacenter-map" data-name1="<?php echo esc_html($data['world_name_1']); ?>" data-lat1="<?php echo esc_html($data['world_lat_1']); ?>" data-long1="<?php echo esc_html($data['world_long_1']); ?>" data-name2="<?php echo esc_html($data['world_name_2']); ?>" data-lat2="<?php echo esc_html($data['world_lat_2']); ?>" data-long2="<?php echo esc_html($data['world_long_2']); ?>" data-name3="<?php echo esc_html($data['world_name_3']); ?>" data-lat3="<?php echo esc_html($data['world_lat_3']); ?>" data-long3="<?php echo esc_html($data['world_long_3']); ?>" data-name4="<?php echo esc_html($data['world_name_4']); ?>" data-lat4="<?php echo esc_html($data['world_lat_4']); ?>" data-long4="<?php echo esc_html($data['world_long_4']); ?>" >
			<div id="chartdiv"></div>
		</div>
	<?php endif; ?>
</div>
