<?php

$data = x_wdes()->wdes_array_merge([
    'layouts' => '',
    'alert_title' => '',
    'icon'         => ''
],$data);

switch ($data['layouts']) {
	case "l-1":
		$layout_class = 'phox-alert';
	break;
	case "l-2":
		$layout_class = 'phox-alert bg-col';
	break;
	case "l-3":
		$layout_class = 'alert-warning';
	break;
	case "l-4":
		$layout_class = 'alert-danger';
	break;
	case "l-5":
		$layout_class = 'alert-success';
	break;
	default:
		$layout_class = 'alert-info';
	break;
}
?>

<div class="col">
	 <?php if ( $data['layouts'] === 'l-1' ) { ?>
	<div class="<?php echo esc_attr($layout_class); ?>" role="alert">
		<span class="<?php echo esc_attr($data['icon']); ?> x-icon"></span> <p> <?php echo esc_html($data['alert_title']); ?> </p>
	</div>
	<?php }elseif( $data['layouts'] === 'l-2'){ ?>
	<div class="<?php echo esc_attr($layout_class) ?>" role="alert">
		<span class="<?php echo esc_attr($data['icon']); ?> bg-col x-icon"></span> <p> <?php echo esc_html( $data['alert_title'] ) ; ?> </p>
	</div>
	<?php }else{ ?>
	<div class="alert <?php echo esc_attr( $layout_class ) ?>" role="alert">
		<?php echo esc_html( $data['alert_title'] ); ?>
	</div>
	<?php } ?>
</div>