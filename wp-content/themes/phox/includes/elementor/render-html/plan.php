<?php
use Phox\core\shortcode\Wdes_SC_Plan;

$data = x_wdes()->wdes_array_merge([
    'layouts' => '',
    'plans' => [],
    'plans_3' => [],
    'footer_display' => '',
    'plans_4_head' => [],
    'plans_4_body' => [],
    'plans_4_footer' => []

],$data);
?>
<div class="plans-area-xdata">
	<?php if( $data['layouts'] === 'l-1' || $data['layouts'] === 'l-2' || $data['layouts'] === 'l-3' ): ?>

	<?php if( $data['layouts'] === 'l-1'): ?>
		<?php foreach ($data['plans'] as $plan): ?>
			<div class="plan-hp <?php echo esc_attr(($plan['popular'] === 'yes') ? 'plan-popular' : ''); ?>">

				<?php if($plan['icon']['url']) : ?>
					<img src="<?php echo esc_url($plan['icon']['url']); ?>" alt="<?php echo esc_attr($plan['title']) ?>">
				<?php endif; ?>

				<?php if($plan['title']): ?>
					<h1><?php echo esc_html($plan['title']) ?></h1>
				<?php endif; ?>

				<div class="line-pp">

					<?php if($plan['price']): ?>
						<span class="price">
							<?php

								if ( $plan['currency_symbol_location'] === 'right' ) {

									echo esc_html($plan['price']);

								}

								if( $plan['currency_symbol'] == 'custom' ){

									echo esc_html( $plan['currency_symbol_custom'] );

								}else {

									echo esc_html( x_wdes()->wdes_get_currency_symbol( $plan['currency_symbol'] ) );

								};


								if ( $plan['currency_symbol_location'] === 'left' ) {

									echo esc_html($plan['price']);

								}
							?>
						</span>
					<?php endif; ?>

					<?php if ($plan['payment_type'] === 'one-time'): ?>

						<span class="time">/<?php  esc_html_e('one time', 'phox'); ?></span>

					<?php elseif ($plan['payment_type'] === 'year'): ?>

						<span class="time">/<?php esc_html_e('per year', 'phox'); ?></span>

					<?php elseif ($plan['payment_type'] === 'custom'): ?>

						<span class="time">/ <?php echo  esc_html( $plan['payment_type_custom'] ); ?></span>

					<?php else: ?>

						<span class="time">/<?php esc_html_e('per month', 'phox'); ?></span>

					<?php endif; ?>
				</div>

				<?php $items = explode(PHP_EOL, $plan['features_items']); ?>
				<ul class="features-plans">
					<?php foreach ($items as $item) : ?>
						<li>
							<a> <?php echo esc_html($item) ?></a>
						</li>
					<?php endforeach; ?>
				</ul>

				<a href="<?php x_wdes()->wdes_get_text (($plan['order_button_url']['url']) ? esc_url($plan['order_button_url']['url']) : '') ; ?>" class="order-plan"><?php x_wdes()->wdes_get_text (($plan['order_button']) ? esc_html($plan['order_button']) : '') ; ?></a>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if( $data['layouts'] === 'l-2'): ?>
		<?php foreach ($data['plans'] as $plan): ?>
			<?php $pop = ''; ?>

			<?php

			if($plan['popular'] === 'yes'){
				$pop = 'active-st2';
			}
			?>

			<div class="plan-st2 <?php echo esc_attr($pop); ?>">

				<div class="info-plan-details">

					<?php if($plan['icon']['url']) : ?>
						<img src="<?php echo esc_url($plan['icon']['url']); ?>" alt="<?php echo esc_attr($plan['title']) ?>">
					<?php endif; ?>

					<?php if($plan['title']): ?>
						<h3><?php echo esc_html($plan['title']) ?></h3>
					<?php endif; ?>

					<?php if($plan['price']): ?>
						<span class="price">
							<?php

								if ( $plan['currency_symbol_location'] === 'right' ) {

									echo esc_html($plan['price']);

								}

								if( $plan['currency_symbol'] == 'custom' ){
									echo esc_html( $plan['currency_symbol_custom'] );
								}else {
									echo esc_html( x_wdes()->wdes_get_currency_symbol( $plan['currency_symbol'] ) );
								};


								if ( $plan['currency_symbol_location'] === 'left' ) {

									echo esc_html($plan['price']);

								}

							?>
						</span>

						<span class="duration">
							<?php if ($plan['payment_type'] === 'one-time'): ?>

								/<?php  esc_html_e('one time', 'phox') ?>

							<?php elseif ($plan['payment_type'] === 'year'): ?>

								/ <?php esc_html_e('per year', 'phox'); ?>

							<?php elseif ($plan['payment_type'] === 'custom'): ?>

								/ <?php echo  esc_html( $plan['payment_type_custom'] ); ?>

							<?php else : ?>

								/ <?php esc_html_e('per month', 'phox'); ?>

							<?php endif; ?>
						</span>
					<?php endif; ?>

				</div>

				<div class="clearfix"></div>

				<?php $items = explode(PHP_EOL, $plan['features_items']); ?>
				<ul>
					<?php foreach ($items as $item) : ?>
						<li>
							<a><?php x_wdes()->wdes_get_text ($item); ?></a>
						</li>
					<?php endforeach; ?>
				</ul>

				<a href="<?php x_wdes()->wdes_get_text (($plan['order_button_url']['url']) ? esc_url($plan['order_button_url']['url']) : '') ; ?>" class="order-st2"><?php x_wdes()->wdes_get_text (($plan['order_button']) ? esc_html($plan['order_button']) : ''); ?></a>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if( $data['layouts'] === 'l-3'): ?>
		<?php foreach ($data['plans_3'] as $plan): ?>
			<?php $pop = ''; ?>

			<?php

			if($plan['popular'] === 'yes'){
				$pop = 'active-st3';
			}
			?>
			<div class="plan-st3 <?php echo esc_attr($pop); ?>">

				<?php if($plan['icon']['url']) : ?>
					<img src="<?php echo esc_url($plan['icon']['url']); ?>" alt="<?php echo esc_attr($plan['title']) ?>">
				<?php endif; ?>

				<?php if($plan['title']): ?>
					<h3><?php echo esc_html($plan['title']); ?></h3>
				<?php endif; ?>

				<?php if($plan['price']): ?>
					<div class="price-box-area">
						<span class="price-num">
							<?php

								if ( $plan['currency_symbol_location'] === 'right' ) {

									echo esc_html($plan['price']);

								}

								if( $plan['currency_symbol'] == 'custom' ){

									echo esc_html( $plan['currency_symbol_custom'] );

								}else {

									echo esc_html( x_wdes()->wdes_get_currency_symbol( $plan['currency_symbol'] ) );

								};

								if ( $plan['currency_symbol_location'] === 'left' ) {

									echo esc_html($plan['price']);

								}

							?>
						</span>
						<span class="price-period">
							<?php if ($plan['payment_type'] === 'one-time'): ?>

								/ <?php  esc_html_e('one time', 'phox') ?>

							<?php elseif ($plan['payment_type'] === 'year'): ?>

								/ <?php esc_html_e('per year', 'phox'); ?>

							<?php elseif ($plan['payment_type'] === 'custom'): ?>

								/ <?php echo  esc_html( $plan['payment_type_custom'] ); ?>

							<?php else : ?>

								/ <?php esc_html_e('per month', 'phox'); ?>

							<?php endif; ?>
						</span>
					</div>
				<?php endif; ?>
				<hr class="plan3-hr-list-f">
				<?php $items = explode(PHP_EOL, $plan['features_items']); ?>
				<ul>
					<?php foreach ($items as $item) : ?>
						<li>
							<a><?php x_wdes()->wdes_get_text ($item); ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
				<a href="<?php x_wdes()->wdes_get_text (($plan['order_button_url']['url']) ? esc_url($plan['order_button_url']['url']) : '') ; ?>" class="order-st3"><?php x_wdes()->wdes_get_text (($plan['order_button']) ? esc_html($plan['order_button']) : '') ; ?></a>
			</div>

		<?php endforeach; ?>
	<?php endif; ?>

<?php elseif ( $data['layouts'] === 'l-4'): ?>
	<?php if( !empty($data['plans_4_head']) && !empty($data['plans_4_body']) ): ?>

		<div class="table-responsive wdes-table-ly4">
			<table class="table">
				<thead class="table-head">
				<tr>
					<?php foreach ($data['plans_4_head'] as $plan): ?>

						<?php
						$popular_head_class = '';
						if($plan['popular_head'] === 'yes'){
							$popular_head_class = 'plan-layout4-modern-active-btn';
						}
						?>

						<th class="<?php echo esc_attr( $popular_head_class )?>"><?php x_wdes()->wdes_get_text (($plan['head_content'])? $plan['head_content'] : '') ;?></th>
					<?php endforeach; ?>
				</tr>
				</thead>
				<tbody class="table-body">
				<?php foreach ($data['plans_4_body'] as $plan): ?>

					<?php
					$price_class = '';
					if($plan['price'] === 'yes'){
						$price_class = 'price-table';
					}
					?>

					<tr class="<?php echo esc_attr($price_class); ?>">
						<th class="table-l-head" scope="row"><?php x_wdes()->wdes_get_text (($plan['row_head'])? $plan['row_head'] : '') ;?></th>

						<?php $items = explode(PHP_EOL, $plan['body_content']); ?>

						<?php foreach ($items as $item) : ?>

							<td><?php x_wdes()->wdes_get_text($item); ?></td>

						<?php endforeach; ?>

						<?php if( $plan['body_content_button'] === 'yes' ): ?>

							<td>
								<a href="<?php echo esc_url($plan['button_url']['url']); ?>" class="ordernow-plan-modern"> <?php echo esc_html($plan['button_title']); ?> </a>
							</td>

						<?php endif; ?>
					</tr>
				<?php endforeach; ?>
				</tbody>
				<?php if( $data['footer_display'] === 'yes'): ?>
					<tfoot>
					<tr class="order-table">
						<?php foreach ($data['plans_4_footer'] as $plan): ?>
							<?php if($plan['content_type'] === 'row-head'): ?>
								<th class="table-l-head" scope="row"><?php x_wdes()->wdes_get_text (($plan['footer_content']) ? $plan['footer_content'] : '') ; ?></th>
							<?php else: ?>
								<td>
									<a class="fas fa-shopping-cart" href="<?php x_wdes()->wdes_get_text (($plan['footer_content_url']['url']) ? esc_url($plan['footer_content_url']['url']) : '') ; ?>"></a>
								</td>
							<?php endif; ?>
						<?php endforeach; ?>
					</tr>
					</tfoot>
				<?php endif; ?>
			</table>
		</div>
	<?php endif; ?>
<?php endif; ?>
</div>
<div class="clearfix"></div>