<?php

$data = x_wdes()->wdes_array_merge([
    'layouts' => '',
    'icon_anime' => '',
    'icon'         => ''
],$data);

switch ($data['layouts']) {
	case "circle":
		$layout_class = 'circle-icon';
	break;
	case "square":
		$layout_class = 'square-icon';
	break;
	case "border":
		$layout_class = 'border-icon';
	break;
	default:
		$layout_class = 'rotate-icon';
	break;
}

$anime = '';
if( $data['icon_anime'] === 'yes'){
	$anime = 'fa-spin';
}

?>

<div class="col">
	<span class="<?php echo esc_attr($data['icon']) ; ?> <?php echo esc_attr($layout_class) ; ?> <?php echo esc_attr($anime) ; ?>"></span>
</div>

