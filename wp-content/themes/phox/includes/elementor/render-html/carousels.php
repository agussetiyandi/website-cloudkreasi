<?php

$data = x_wdes()->wdes_array_merge([
    'carousels_type' => '',
    'display_subtitle' => '',
    'users_list'     => [],
	'carousels_head'  => ''
],$data);

if ( ! function_exists( 'wdes_testimonial_content' ) ){
	function wdes_testimonial_content($item) {

		echo '<div class="f-back">'. esc_html (($item['user_comment'])? $item['user_comment'] : '') .'</div>';

	}
}

if ( ! function_exists( 'wdes_testimonial_image' ) ){
	function wdes_testimonial_image($item) {

		if(!empty($item['user_pic']['url'])) {

			echo '<div class="img-testimonial-wrap"><img src="'. esc_url($item['user_pic']['url']) .'" alt="'. esc_attr( ($item['user_name'])? $item['user_name'] : '' ) .'" /></div>';

		}

	}
}

if( ! function_exists( 'wdes_testimonial_title' ) ){
	function wdes_testimonial_title($item) {

		echo '<h4>'. esc_html (($item['user_name'])? $item['user_name'] : '') .'</h4>';

	}
}

if( ! function_exists( 'wdes_testimonial_subtitle' ) ){
	function wdes_testimonial_subtitle($item) {

		echo '<span>'. esc_html (($item['user_website'])? $item['user_website'] : '') .'</span>';

	}
}

if ( ! function_exists( 'wdes_testimonial_head' ) ){
	function wdes_testimonial_head ($testimonial_item, $attribute){

		if ($attribute['carousels_head'] === 'image'){

			wdes_testimonial_image($testimonial_item);

		}elseif ($attribute['carousels_head'] === 'content') {

			wdes_testimonial_content($testimonial_item);

		}


	}
}
if( ! function_exists( 'wdes_testimonial_box' ) ){
	function wdes_testimonial_box($testimonial_item, $attribute){

		if ($attribute['carousels_head'] === 'image'){

			wdes_testimonial_content($testimonial_item);

		}elseif ($attribute['carousels_head'] === 'content') {

			wdes_testimonial_image($testimonial_item);

		}

		echo x_wdes()->wdes_get_text('<div class="info-bio">');
		wdes_testimonial_title($testimonial_item);
		if ($attribute['display_subtitle'] === 'yes'){
			wdes_testimonial_subtitle($testimonial_item);
		}
		echo x_wdes()->wdes_get_text('</div>');

	}
}

?>

<?php if( $data['carousels_type'] === 'testimonial' || $data['carousels_type'] === 'team' ): ?>
	<section class="integrations">
		<div class="container-fluid">
			<div class="row">
				<?php if( $data['carousels_type'] === 'testimonial'): ?>
					<div class="col">
						<div class="testimonials">
							<div class="owl-carousel owl-theme wdes-testimonials-pos">
								<?php foreach ($data['users_list'] as $item): ?>

									<div class="client-block">
										<span class="fas fa-quote-left icon-quote"></span>

										<?php wdes_testimonial_head($item, $data); ?>

										<div class="wrap-item-carousel-in">

											<?php wdes_testimonial_box($item, $data); ?>

										</div>

									</div>

								<?php endforeach; ?>
							</div>
						</div>
					</div>
				<?php elseif( $data['carousels_type'] === 'team') : ?>
					<?php
					$slider_count = 0;
					$id = uniqid('wdes-');
					?>
					<div class="teamwork-sec">
						<div class="col">
							<div id="<?php echo esc_attr($id) ?>" class="carousel slide" data-ride="carousel">
								<div class="carousel-inner">
									<?php foreach ($data['users_list'] as $item): ?>
										<?php
										if($slider_count === 0){
											$active =  'active';
										}else{
											$active =  '';
										}
										?>
										<div class="carousel-item <?php x_wdes()->wdes_get_text ($active); ?>">
											<div class="team-item">
												<p><?php x_wdes()->wdes_get_text (($item['user_comment']) ? esc_html($item['user_comment']) : '') ; ?></p>
												<?php if(!empty($item['user_name']) && $item['user_website']  ): ?>
													<span class="mail-team"><?php echo esc_html($item['user_name']).'@'.  esc_html($item['user_website'] ); ?></span>
												<?php endif; ?>
												<?php if(!empty($item['user_pic']['url'])): ?>
													<img class="user-p" src="<?php echo esc_url($item['user_pic']['url']); ?>" alt="<?php echo esc_attr($item['user_name']); ?>">
												<?php endif; ?>

											</div>
										</div>
										<?php $slider_count++; ?>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>

					<a class="carousel-control-prev" href="#<?php x_wdes()->wdes_get_text ($id); ?>" role="button" data-slide="prev">
						<span class="fas fa-angle-left carousel-con" aria-hidden="true"></span>
						<span class="sr-only"><?php esc_html_e('Previous', 'phox'); ?></span>
					</a>
					<a class="carousel-control-next" href="#<?php x_wdes()->wdes_get_text ($id); ?>" role="button" data-slide="next">
						<span class="fas fa-angle-right carousel-con" aria-hidden="true"></span>
						<span class="sr-only"><?php esc_html_e('Next', 'phox'); ?></span>
					</a>

				<?php endif; ?>

			</div>
		</div>
	</section>
<?php endif; ?>

