<?php

$data = x_wdes()->wdes_array_merge([
    'bread_title' => '',
    'bread_desc'  => '',
    'show_categories' => '',
    'categories'    => []
],$data);

?>

<div class="breadcrumb">
	<div class="container-fluid">
		<div class="bread-title">
			<?php if(!empty($data['bread_title'])): ?>
				<h2>
					<?php echo esc_html($data['bread_title']); ?>
				</h2>
			<?php endif; ?>
		</div>
		<div class="b-brd"></div>
		<?php if(!empty($data['bread_desc'])): ?>
			<p>
				<?php echo esc_html($data['bread_desc']); ?>
			</p>
		<?php endif; ?>
	</div>
</div>
<?php if( $data['show_categories'] === 'yes'): ?>

	<?php if(!empty($data['categories'])): ?>

		<div class="cat">
			<ul>
				<?php foreach ($data['categories'] as $item) :?>
					<li>
						<a class="g-q" href="#<?php echo (!empty($item['slug'])) ? esc_attr($item['slug']) : ''; ?>"><?php echo (!empty($item['title'])) ? esc_html($item['title']) : '' ;?></a>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>

<?php endif; ?>


