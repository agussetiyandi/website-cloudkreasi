<?php
/**
 * Elementor
 *
 * @package Phox
 */

namespace Elementor;

use Phox\core as Root;

class WDES_Elementor_Builder
{

    protected static $_instance = null;
    
    public $widgets;

	private  $custom_fonts = [];

	/**
	 * The Phox font name.
	 */
	const PHOXFONTS = 'phoxfonts';

    public static function instance()
    {
        if ( is_null( self::$_instance ) )
        {
            self::$_instance = new self;
        }

        return self::$_instance;
    }


    public function __construct()
    {
        $this->widgets = [
            'plan',
            'faq',
            'divider',
            'partners',
            'breadcrumb',
            'features-boxs',
            'map',
            'soical',
            'button',
            'icon',
            'alert'
        ];


        //register widgets & controls & Elementor Custom style file & locations
        add_action('elementor/elements/categories_registered', array( $this, 'wdes_category_register') );
        add_action('elementor/widgets/widgets_registered', array( $this, 'wdes_widgets_register'), 11 );
        add_action( 'elementor/frontend/after_enqueue_styles', array($this, 'wdes_custom_styles'));
        add_action( 'elementor/editor/wp_head', function(){

            $wdes_google_api = wdes_opts_get('google_maps_api_key');
            if(isset($wdes_google_api) && !empty($wdes_google_api)){
                wp_enqueue_script('wdes-map', esc_url( set_url_scheme('https://maps.googleapis.com/maps/api/js?key='.$wdes_google_api ) ), null, null );
            }

            wp_enqueue_script('popper',WDES_ASSETS_URI .'/js/popper.min.js', array('jquery'), WDES_THEME_VERSION, true );
            wp_enqueue_script('bootstrap',WDES_ASSETS_URI .'/js/bootstrap.min.js', array('jquery'), WDES_THEME_VERSION, true );
            wp_enqueue_script('wdes-custom-script',WDES_ASSETS_URI .'/js/custom-script.js', array('jquery'), WDES_THEME_VERSION, true );

        });

        add_action( 'elementor/theme/register_locations', array( $this, 'register_elementor_locations' ) );
        add_filter( 'after_setup_theme', array( $this, 'add_support_header_footer_elementor' ) );

	    // custom uploaded font
	    $f_custom = wdes_opts_get('font-custom');
	    $f_custom2 = wdes_opts_get('font-custom-2');

	    if( $f_custom || $f_custom2 ){

		    add_filter( 'elementor/fonts/groups', [ $this , 'add_custom_font_group_elemenntor' ] );

		    if( $f_custom ){

			    $this->custom_fonts [] = $f_custom;

		    }
		    if( $f_custom2 ){

			    $this->custom_fonts [] = $f_custom2;

		    }

		    add_filter( 'elementor/fonts/additional_fonts', [$this, 'add_custom_font_elementor'] );

	    }

    }

    /**
     * Widget Register
     *
     * @return bool
     */
    public function wdes_widgets_register(){

        if (!defined('ELEMENTOR_PATH') || !class_exists('Elementor\Widget_Base') || ! class_exists('Elementor\Plugin')){
            return false;
        }

        foreach ($this->widgets as $widget){
            $template_file = WDES_INC_DIR . "/elementor/widgets/{$widget}.php";
            if(file_exists($template_file)){
                require_once $template_file;
            }
        }

    }

    /**
     * Category Register
     */

    public function wdes_category_register($categories_manager){

	    $categories_manager->add_category('phox-site-builder',
		    [
			    'title' => esc_html__('Phox Site Builder', 'phox'),
		    ]

	    );

        $categories_manager->add_category('phox-elements',
            [
                'title' => esc_html__('Phox Elements', 'phox'),
            ]

        );

    }


    public function wdes_custom_styles(){

		wp_enqueue_style('wdes-elementor-custom', WDES_ASSETS_URI .'/css/elementor-custom.css', false , WDES_THEME_VERSION);

    }

	/**
	 * Register Elementor Locations
	 *
	 * Use for Elementor Pro only
	 *
	 * @since 1.4.2
	 * @param $elementor_theme_manager object store all location
	 */
    public function register_elementor_locations($elementor_theme_manager) {

		//Header Location
		$elementor_theme_manager->register_location(
			'header',
			array(
					'hook'					=> 'wdes_header_after_body_open',
					'remove_hooks'	=> array( 'top_header_section', 'main_header_section' )
			)
		);

		//Footer Location
		$elementor_theme_manager->register_location(
			'footer',
			array(
					'hook'						=> 'wdes_the_footer',
					'multiple'				=> false,
					'edit_in_content'	=> false,
					'remove_hooks'	=> array( array( Root\Wdes_Footer::instance() ,'site_footer' ) )
			)
		);

		$elementor_theme_manager->register_location( 'single' );

		//Remove the theme sections hooks (reason to use this way because the remove from elementor must be without priority)
		if( $elementor_theme_manager->location_exits( 'header', true ) ){

			$instance_header = Root\Wdes_Header::instance();

			remove_action('wdes_header_after_body_open', array( $instance_header, 'top_header_section' ), 4);
			remove_action('wdes_header_after_body_open', array( $instance_header,'main_header_section' ), 4);

		}


	}

	/**
	 * Add Support Header and Footer For Elementor
	 *
	 * @since 1.4.2
	 */
	public function add_support_header_footer_elementor() {
		add_theme_support( 'header-footer-elementor' );
	}

	/**
	 * Font groups.
	 *
	 * add fonts groups used by Elementor.
	 *
	 * @since 1.8.1
	 * @param array $font_groups Font groups.
	 * @return array $font_groups Font groups.
	 */
	public function add_custom_font_group_elemenntor($font_groups){

		$font_groups[self::PHOXFONTS] = 'Phox Custom Fonts';

		return $font_groups;

	}

	/**
	 * Additional fonts.
	 *
	 * Add the fonts used by Elementor to add additional fonts.
	 *
	 * @since 1.8.1
	 * @param array $additional_fonts Additional Elementor fonts.
	 * @return array $additional_fonts Additional Elementor fonts.
	 *
	 */
	public function add_custom_font_elementor($additional_fonts){

		foreach ( $this->custom_fonts as $custom_font ){

			$additional_fonts [$custom_font] = self::PHOXFONTS;

		}

		return $additional_fonts;

	}


}

WDES_Elementor_Builder::instance();