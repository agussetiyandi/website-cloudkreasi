<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_number extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   */
   function render(){

    $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';
    $name  = $this->args['opt_name'].'['.$this->field['id'].']';


		$value = $this->value;

		 if( ! is_array($value) ){

			 $value = $this->value;

		 }else{

			 $value = isset( $this->field['def'] ) ? $this->field['def'] : '';

		 }
    echo '<div class="right-block-c">';
        echo '<input  name="'.$name.'" type="number" value="'.$value.'" step="any" class="input-c">';
        echo '<span class="size-in-pixel">'.esc_html__('px', 'phox').'</span>';
    echo '</div>';
   }
 }
