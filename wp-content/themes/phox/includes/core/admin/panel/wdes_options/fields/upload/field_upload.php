<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_upload extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   * Div:
   * @var class   : test if find class with the field
   * @var name    : it is the name property in html
   * @var upload  : to run the style property in the upload button
   * @var remove  : to run the style property in the remove button
   * @see https://developer.wordpress.org/reference/functions/wp_localize_script
   * @see https://developer.wordpress.org/reference/functions/wp_enqueue_media
   */
   function render($meta= false){

    /** Class **/
    $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';

    /** media type **/
    $media_type = (isset($this->field['media']))? $this->field['media'] : 'image';

    /** name **/
    if($meta){
        $name  = 'name="'.$this->field['id'].'"';
    }else{
        $name  = 'name="'.$this->args['opt_name'].'['.$this->field['id'].']"';
    }

    if($this->value == ''){
      $upload = '';
      $remove = 'style="display:none;"';
    }else{
      $upload = 'style="display:none;"';
      $remove = '';
    }

    echo'<div class="right-block-c">';

      echo '<div class="wdes-upload-btn">';

        echo '<div class="form-function" >';

          echo '<input type="hidden"'.$name.' value="'.$this->value.'" class="'.$media_type.'" />';
          echo '<a href="javascript:void(0);"  class="fileUpload func-up wdes-upload" data-choose="Choose a File" data-update="Select File"'. $upload .' >'. esc_html__('upload', 'phox') .'</a>';
          echo '<a href="javascript:void(0);"class="fileUpload func-up wdes-remove"'. $remove .' >'. esc_html__('remove', 'phox'). '</a>';
		  if( $media_type === 'image' ){
		  	echo '<img class="img-flr wdes-screenshot" style="max-width:350px; display:block"  src="'. esc_url($this->value) .'"' ;
		  }elseif ($media_type === 'font'){
		  	$value = (!empty( $this->value )) ? esc_url(WDES_OPTIONS_URI.'fields/upload/font.png') : '';
		  	echo '<img class="img-flr wdes-screenshot" style="max-width:350px; display:block"  src="'.$value.'"' ;
		  }

        echo '</div>';

      echo '</div>';

    echo '</div>';

    echo '</div>';


   }

  function enqueue(){
    wp_enqueue_media();
    wp_enqueue_script('wdes-upload', WDES_OPTIONS_URI . 'fields/upload/upload.js', array('jquery'), time(), true);
    wp_localize_script('wdes-upload','wdes_upload', array('url'=> $this->url .'fields/upload/blank.png', 'fontImg' => esc_url(WDES_OPTIONS_URI.'fields/upload/font.png')));

  }

 }
