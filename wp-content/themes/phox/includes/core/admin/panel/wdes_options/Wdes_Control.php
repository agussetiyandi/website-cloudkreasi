<?php

namespace Phox\core\admin\panel\wdes_options;

if (! defined('ABSPATH')) {
    exit; //exit if accessed directly
}


/**
 * Admin Class
 * Div: it use to register the admin page
 */

class Wdes_Control{

    public $dir                 = WDES_OPTIONS_DIR;
    public $url                 = WDES_OPTIONS_URI;
    public $page                = array();
    public $args                = array();
    public $sections            = array();
    public $extra_tabs          = array();
    private static $options     = array();

    public $menu        = array();

    function __construct($menu = array(), $sections = array()){

        $this->menu = apply_filters('wdes-opts-menu',$menu);

        $defaults = array();

        $defaults['opt_name'] 		= 'phox';

        $defaults['menu_icon'] 		= WDES_OPTIONS_URI.'/img/menu_icon.png';
        $defaults['menu_title'] 	= esc_html__('Theme Options', 'phox');
        $defaults['page_title'] 	= esc_html__('Theme Options', 'phox');
        $defaults['page_slug'] 		= 'phox_options';
        $defaults['page_cap'] 		= 'edit_theme_options';
        $defaults['page_type'] 		= 'menu';
        $defaults['page_parent'] 	= '';
        $defaults['page_position'] 	= 100;


        //get args
        $this->args = $defaults;

        //get sections
        $this->sections = apply_filters('wdes_opts_sections', $sections);
        $this->sections = apply_filters('wdes_opts_sections'.$this->args['opt_name'], $this->sections);

        //defaults options
        add_action('wp_loaded', array($this, 'set_default_options'), 999 );

        //register setting
        add_action('admin_init', array(&$this, '_register_setting'));

        //options page
        add_action('admin_menu', array(&$this, '_options_page'));

        //download the export settings
        add_action( "wp_ajax_download_options-" . $this->args['opt_name'], array($this,"download_options") );
		add_action( "wp_ajax_nopriv_download_options-" . $this->args['opt_name'], array($this,"download_options") );

        //get the options for use later on
        self::$options = get_option($this->args['opt_name']);

    }

    public static function get($opt_name, $default = null){

        if( ( ! is_array(self::$options) ) || ( ! key_exists($opt_name, self::$options) ) ) return $default;

        return ( ( ! empty(self::$options[$opt_name])) || (self::$options[$opt_name]==='0') ) ? self::$options[$opt_name] : $default;


    }

    public function set($opt_name, $value) {
        self::$options[$opt_name] = $value;
        update_option($this->args['opt_name'], self::$options);
    }

    public static function show($opt_name, $default = null){
        $option = self::get($opt_name, $default);
        if(!is_array($option)){
			x_wdes()->wdes_get_text($option);
        }
    }

    public function _options_page(){
        $this->page = add_submenu_page(
            'phox',
            $this->args['page_title'],
            $this->args['menu_title'],
            $this->args['page_cap'],
            $this->args['page_slug'],
            array( &$this, '_options_page_html' )
        );

        //Add theme admin style and script to dashboard
        add_action('admin_print_styles-'.$this->page, array(&$this, '_enqueue'));
    }

    public function set_default_options(){

        if(!get_option($this->args['opt_name'])){
            add_option($this->args['opt_name'], $this->default_values());
        }
        self::$options = get_option($this->args['opt_name']);

    }

    function _enqueue(){

        //Fix Bootstrap
        if(get_admin_page_parent() === 'phox'){
            wp_enqueue_style('bootstrap-style',$this->url . 'css/bootstrap.min.css', false, time(),'all');
            wp_enqueue_script('bootstrap',$this->url . 'js/bootstrap.min.js', array('jquery'), time(),true );
        }

        wp_enqueue_style('ie10-viewport-bug-workaround',$this->url . 'css/ie10-viewport-bug-workaround.css', false, time(),'all');
        wp_enqueue_style('wdes-style',$this->url . 'css/style.css', false, time(),'all');
        wp_enqueue_style('wdes-responsive',$this->url . 'css/responsive.css', false, time(),'all');

        //font Awesome
        wp_enqueue_style('font-awesome',$this->url .'/css/fontawesome.min.css',false,time(),'all');


        wp_enqueue_script('wdes-ie10-viewport-bug-workaround',$this->url . 'js/ie10-viewport-bug-workaround.js', array('jquery'), time(),true );
        wp_enqueue_script('wdes-plugins',$this->url . 'js/plugins.js', array('jquery'), time(),true );
        wp_enqueue_script('wdes-icons',$this->url . 'js/icons.js', array('jquery'), time(),true );
        wp_localize_script('wdes-icons','wdes', array('url'=> WDES_ADMIN_URI));
        wp_enqueue_script('wdes-script',$this->url . 'js/script.js', array('jquery'), time(),true );

        // Modal
        wp_enqueue_script('modal',$this->url . 'js/jquery.modal.min.js', array('jquery'), time(),true );

        // Tabs
        wp_enqueue_script('tabs',$this->url . 'js/jquery-ui.js', array('jquery'), time(),true );

        do_action('wdes-opts-enqueue-'.$this->args['opt_name']);

        //Load the field class and enqueue field style/js
        foreach ($this->sections as $key => $section) {

            if(isset($section['fields'])){

                foreach ($section['fields'] as $key => $field) {

                    if(isset($field['type']) || ! empty($field['package'])){

                        if(isset($field['package'])){
                            foreach ($field['package-options'] as $key => $pack_fields){
                                $field_class = 'WDES_Admin_' .$pack_fields['type'];

                                if(!class_exists($field_class)){
                                    require_once($this->dir.'/fields/'.$pack_fields['type'].'/field_'.$pack_fields['type'] .'.php');
                                }
                            }
                        }else{

                            $field_class = 'WDES_Admin_' .$field['type'];

                            if(!class_exists($field_class)){
                                require_once($this->dir.'fields/'.$field['type'].'/field_'.$field['type'] .'.php');
                            }
                        }

                        if(class_exists($field_class) && method_exists($field_class, 'enqueue')){
                            $enqueue = new $field_class('','',$this);
                            $enqueue->enqueue();
                        }
                    }

                }

            }

        }

    }
    public function default_values(){
        $defaults = array();

        foreach($this->sections as $k => $section){
            if(isset($section['fields'])){
                foreach($section['fields'] as $fieldp){
                    foreach ($fieldp as $fieldk ){
                        if(is_array($fieldk)){
                            foreach ($fieldk as $key => $field){

                                if(!isset($field['def'])){
                                    $field['def'] = '';
                                }
                                $defaults[$field['id']] = $field['def'];
                            }
                        }
                    }
                }
            }
        }

        return $defaults;
    }
    function _register_setting(){

        register_setting($this->args['opt_name'].'_group', $this->args['opt_name'], array($this, 'validation_options') );

        foreach ($this->sections as $key => $section) {
            add_settings_section($key.'_section', $section['title'], array(&$this,'_section_desc'),$key.'_section_group');

            if(isset($section['fields'])){

                foreach ($section['fields'] as $fields_key => $field) {

                    if(isset($field['package'])){

                        $th = array();
                        foreach ( $field['package-options'] as $pack_field_key => $pack_field) {

                            $th[] = (isset($pack_field['desc'])) ? '<div class="left-block-c"><h2>' . esc_html( $pack_field['title'] ) . '</h2> <p>' . $pack_field['desc'] . '</p></div>' : $pack_field['title'] . '</div>';

                        }
                        add_settings_field($fields_key.'_field', $th, array(&$this, '_field_input'), $key . '_section_group', $key . '_section', $field);

                    }else{
                        if (isset($field['title'])) {

                            $th = (isset($field['desc']))?'<div class="left-block-c"><h2>'. esc_html( $field['title'] ).'</h2> <p>'.$field['desc'].'</p></div>': $field['title'].'</div>';
                        }else {

                            $th = '';

                        }
                        add_settings_field($fields_key.'_field', $th, array(&$this, '_field_input'), $key . '_section_group', $key .'_section',$field);
                    }

                }
            }
        }
    }

    function wdes_do_settings_sections( $page ) {
        global $wp_settings_sections, $wp_settings_fields;

        if ( ! isset( $wp_settings_sections[$page] ) )
            return;

        foreach ( (array) $wp_settings_sections[$page] as $section ) {
            if ( $section['title'] )
                echo "<h2>{$section['title']}</h2>\n";

            if ( $section['callback'] )
                call_user_func( $section['callback'], $section );

            if ( ! isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section['id']] ) )
                continue;

            echo '<div class="form-table">';
            $this->wdes_do_settings_fields( $page, $section['id'] );
            echo '</div>';
        }
    }

    public function validation_options($theme_options)
    {

        set_transient('wdes_opts_saved', '1', 1000);

        //option / Import
        if( ! empty( $theme_options['import'] ) ){

            if(! empty( $theme_options['import_code'] )){

                $import = $theme_options['import_code'];

            }

            $import_options = json_decode( trim( $import, '##' ), true);

            if( is_array( $import_options ) ){
                $import_options['imported'] = 1;
                $import_options['last_tab'] = 'global';

                return $this->_sanitize_values($import_options);
            }

        }
        if( isset( $theme_options['reset'] ) ){

            $theme_options = $this->default_values();
            return $theme_options;

        }

        $theme_options = $this->_sanitize_values($theme_options);

        do_action('wdes-opts-options-validate',$theme_options,self::$options);
        do_action('wdes-opts-options-validate-'.$this->args['opt_name'],$theme_options,self::$options);

        unset($theme_options['import_code']);

        return $theme_options;

    }

	/**
	 * Sanitize Values
	 *
	 * Clean data before add it to database
	 *
	 * @since 1.4.9
	 * @param $theme_options
	 * @return array $theme_options
	 */
    public function _sanitize_values($theme_options){

    	foreach ( $this->sections as $k_section => $section ){
    		foreach ($section['fields'] as $k_package => $package ){
			    foreach ( $package["package-options"] as $k_field => $field ){
			    	if ( $field['type'] === 'text' ) {
					    $theme_options[ $field['id'] ] = sanitize_text_field($theme_options[ $field['id'] ]);
				    }
				    elseif ( $field['type'] === 'upload' ) {
			    		if( ! empty( $theme_options[ $field['id'] ] ) ){
						    $file_dir = explode('wp-content/uploads/', $theme_options[ $field['id'] ] );
						    $theme_options[ $field['id'] ] = esc_url( site_url() . '/wp-content/uploads/' . $file_dir[1] );
					    }
				    }
				    elseif ( $field['type'] === 'radio' && $field['type'] === 'select'  ) {
						if( isset( $theme_options[ $field['id'] ] ) ){
							if ( ! in_array( $theme_options[ $field['id'] ], array_keys($field['options']) ) ){
								$theme_options[ $field['id'] ] = $field['def'] ;
							}else {
								$theme_options[ $field['id'] ] = esc_attr( $theme_options[ $field['id'] ] ) ;
							}
						}
				    }
				    elseif( $field['type'] === 'typography' ){

						$default_weight = [ '100', '200', '300', '400', '500', '600', '700', '800', '900' ];

					    if( ! is_string( $theme_options[ $field['id'] ]['fonts'] ) ) {
						    $theme_options[ $field['id'] ]['fonts'] = $field['def']['fonts'] ;
					    }

					    if( ! in_array( $theme_options[ $field['id'] ]['weight-font'] , $default_weight ) ){
						    $theme_options[ $field['id'] ]['weight-font'] = $field['def']['weight-font'] ;
					    }

					    if( empty( (int) $theme_options[ $field['id'] ]['size'] ) ) {
						    $theme_options[ $field['id'] ]['size'] = $field['def']['size'] ;
					    }

				    }
			    }
		    }
	    }

	    return $theme_options ;

    }

    function wdes_do_settings_fields($page, $section) {
        global $wp_settings_fields;

        if ( ! isset( $wp_settings_fields[$page][$section] ) )return;

        foreach ( (array) $wp_settings_fields[$page][$section] as $field ) {
            $class = '';

            if(! empty( $field['args']['package'])){

                //to find the title
                $field_title_key = 0;
                $title = (isset($field['args']['package'])) ? $field['args']['package'] : '';
								$id = (isset($field['args']['package_id'])) ? $field['args']['package_id'] : '';

                echo '<div id="'.$id.'" class="wdes-admin-package">';
                    echo '<h1>'.esc_html($title).'</h1>';
                    foreach ( (array) $field['args']['package-options'] as $package_field){


                        if ( ! empty( $package_field['class'] ) ) {
                            $class = ' class="' . esc_attr( $package_field['class'] ) . '"';
                        }


                        echo "<div{$class}>";

                        // Left Section
                        echo '<div scope="row">' . $field['title'][$field_title_key] . '</div>';

                            echo '<div>';
                                // Right Section
                                call_user_func($field['callback'], $package_field);

                            echo '</div>';

                            //add +1 to the key
                            $field_title_key++;

                        echo '</div>';
                    }
                echo '</div>';
                }else{

                    if ( ! empty( $field['args']['class'] ) ) {
                        $class = ' class="' . esc_attr( $field['args']['class'] ) . '"';
                    }

                    echo "<div {$class}>";
                    if ( ! empty( $field['args']['label_for'] ) ) {
                        echo '<div scope="row"><label for="' . esc_attr( $field['args']['label_for'] ) . '">' . $field['title'] . '</label></div>';
                    } else {
                        echo '<div scope="row">' . $field['title'] . '</div>';
                    }

                    echo '<div>';
                    call_user_func($field['callback'], $field['args']);
                    echo '</div>';
                echo '</div>';
            }

        }
    }

    function _options_page_html(){
        //Container
        echo '<div class="container-bg">';
            //navbar
            echo '<nav class="navbar navbar-inverse wh-nav">';
                echo '<div class="container-fluid">';
                $theme_logo_png = WDES_OPTIONS_URI .'img/logo.png' ;
                    echo '<a class="navbar-brand" href="#"><img src="'. esc_url($theme_logo_png) .'" alt="'.esc_attr__('Phox', 'phox').'"></a>';
                    echo '<span class="version-wh">V '. WDES_THEME_VERSION .'</span>';
                echo '</div>';
            echo '</nav>';
            //start Form
            echo '<form method="post" action="./options.php" enctype="multipart/form-data" id="wdes-form">';
                settings_fields($this->args['opt_name'].'_group');

                self::$options['last_tab'] = (isset( self::$options['last_tab'] )) ? self::$options['last_tab'] : 'global' ;
                echo '<input type="hidden" id="last_tab" name="'. $this->args['opt_name'] .'[last_tab]" value="'. esc_attr(self::$options['last_tab']) .'"  />';

                // start panel container
                echo '<div class="container-fluid">';
                // start panel row
                    echo '<div class="row">';
                    // Fix-hright
                        echo '<div class="fixed-bg-layout">';
                        //start sidebar
                            echo '<div class="col-sm-3 col-md-2 sidebar-wh">';
                            //start ul
                                echo '<ul class="nav nav-sidebar-wh" role="tablist">';
                                foreach ($this->menu as $key => $menu_item) {

	                                //page case & typography case
	                                if($key === 'page'){
		                                if('blog' === self::$options['last_tab'] || 'single' === self::$options['last_tab'] || 'page404' === self::$options['last_tab'] || 'comingsoon' === self::$options['last_tab']  ){
			                                $class_ul_active = 'active'.' '. ' in';
			                                $class_page_active = 'active';
		                                }
	                                }elseif($key === 'typography'){
		                                if ('custom-fonts' === self::$options['last_tab'] || 'gen-typo' === self::$options['last_tab']){
			                                $class_ul_active = 'active'.' '. ' in';
			                                $class_page_active = 'active';
		                                }
	                                }else{
		                                $class_ul_active  = $class_page_active = '';
	                                }

                                    //check global  && page active
                                    if($key === self::$options['last_tab']){
                                        echo' <li '.'class="active collapsed"  data-target="'.$key.'"'.' role="presentation">';
                                    }elseif($key === 'page' || $key === 'typography'){
                                        echo '<li data-toggle="tab" data-target="#'.$key.'"class="collapsed '.$class_page_active.'" rel="tab" role="presentation">';
                                    }else{
                                        echo'<li '.'class=""'.' role="presentation">';
                                    }
                                    // change Link
                                    if($key === 'page' || $key === 'typography'){
                                        echo  '<a href="#'. $key .'" aria-controls="'. $key .'" role="tab" data-toggle="collapse"><span class="'.$menu_item['icon'].'"></span>'.$menu_item['title'].'<span class="fas fa-angle-down custom-down-arrow"></span></a>';
                                    }else{
                                        echo  '<a href="#'. $key .'" aria-controls="'. $key .'" role="tab" data-toggle="tab"><span class="'.$menu_item['icon'].'"></span>'.$menu_item['title'].'</a>';
                                    }
                                    echo '</li>';
                                    if( array_key_exists('sections', $menu_item) ){
                                        echo '<ul class="sub-menu collapse '.$class_ul_active.'" id="'.$key.'" role="presentation">';
                                            foreach ($menu_item['sections'] as $icon => $sub_item) {
	                                            $active_sub_menu = (self::$options['last_tab'] === $sub_item['id']) ? 'class="active"' : '';
	                                            echo '<li href="#'.$sub_item['id'].'"  aria-expanded="true" '.$active_sub_menu.' " role="tab" aria-controls="'. $sub_item['id'] .'" data-toggle="tab"><a><span class="'.$icon.'"></span>'.$sub_item['title'].'</a></li>';
                                            }
                                        echo '</ul>';
                                    }
                                }
                                //end ul
                                echo '</ul>';
                            //end sidebar
                            echo '</div>';

                            //start Content
                            echo '<div class="col-sm-9 col-md-10 main">';
                            //save button
                            echo'<div class="btns-cloud">';
                                echo'<button type="submit" name="submit" class="save-btns-c"><span class="fas fa-save wdes-admin-saveBtn"></span>' .esc_html__( 'Save','phox'). '</button>';
                            echo '</div>';
                                //start tab content
                                echo '<div class="tab-content">';
                                foreach ($this->sections as $key => $section) {

                                    //start tab panel
                                    self::$options['last_tab'] = (isset( self::$options['last_tab'] )) ? self::$options['last_tab'] : 'global' ;
                                    if($key === self::$options['last_tab']){echo '<div id="'. $key .'" class="tab-pane fade in active" role="tabpanel" >';}else{echo '<div id="'. $key .'" class="tab-pane fade" role="tabpanel" >';}
                                    // start content block
                                    //  Class : block-function remove
                                        echo '<div class="">';
                                            $this->wdes_do_settings_sections($key.'_section_group');
                                            // end content block
                                        echo '</div>';
                                    //end tab panel
                                    echo '</div>';
                                }

                                //Import & Export
                                echo '<div id="import-export" class="tab-pane '.( isset(self::$options['last_tab']) && 'import-export' === self::$options['last_tab']? 'in active' : '' ).' " role="tabpanel" >';
                                    echo '<h2 class="tab-title-head"><span class="fas fa-power-off"></span>'. esc_html__('Import, Export & Reset', 'phox') .'</h2>';
                                    echo '<div class="form-table">';

                                        // Export
                                        echo '<div class="wdes-admin-package">';
                                            echo '<h1>'.esc_html__('Export', 'phox').'</h1>';
                                            echo '<div class="block-function-c">';
                                                echo '<div scope="row">';
                                                    echo '<div class="left-block-c">';
                                                        echo '<h2>'. esc_html__('Export Settings', 'phox') .'</h2>';
                                                        echo '<p>'.esc_html__('Here you can download your themes current option settings.', 'phox').'</p>';
                                                    echo '</div>';
                                                echo '</div>';
                                                echo '<div>';
                                                    echo '<div class="right-block-c">';
                                                        echo '<a href="'. esc_url(  admin_url( 'admin-ajax.php?action=download_options-'.$this->args['opt_name'] .'&secret='. md5( AUTH_KEY.SECURE_AUTH_KEY) )) .' " class="func-up custom-func-up-ex" >'. esc_html__('Download', 'phox') .'</a>';
                                                    echo '</div>';
                                                echo '</div>';
                                            echo '</div>';
                                        echo '</div>';

                                        // Import
                                        echo '<div class="wdes-admin-package">';
                                            echo '<h1>'.esc_html__('Import', 'phox').'</h1>';
                                            echo '<div class="block-function-c">';
                                                echo '<div scope="row">';
                                                    echo '<div class="left-block-c">';
                                                        echo '<h2>'. esc_html__('Import Settings', 'phox') .'</h2>';
                                                        echo '<p>'.esc_html__('WARNING! This will overwrite all existing options, please proceed with caution!', 'phox').'</p>';
                                                    echo '</div>';
                                                echo '</div>';
                                                echo '<div>';
                                                    echo '<div class="right-block-c">';
                                                    echo '<textarea class="textarea-c" name="'.$this->args['opt_name'].'[import_code]" rows="8" ></textarea>';
                                                    echo '<input type="submit"  name="'. $this->args['opt_name'] .'[import]" class="import-btn-bg" value="'. esc_attr__('Import', 'phox') .'" >';
                                                    echo '</div>';
                                                echo '</div>';
                                            echo '</div>';
                                        echo '</div>';

                                        // Reset
                                        echo '<div class="wdes-admin-package">';
                                            echo '<h1>'.esc_html__('Reset', 'phox').'</h1>';
                                            echo '<div class="block-function-c">';
                                                echo '<div scope="row">';
                                                    echo '<div class="left-block-c">';
                                                        echo '<h2>'. esc_html__('Reset Settings', 'phox') .'</h2>';
                                                        echo '<p>'.esc_html__('WARNING! This will overwrite all existing options, please proceed with caution!', 'phox').'</p>';
                                                    echo '</div>';
                                                echo '</div>';
                                                echo '<div>';
                                                    echo '<div class="right-block-c">';
                                                    echo '<input type="button" data-toggle="modal" data-target="#resetOptions" class="reset-btn-bg" value="'. esc_attr__('Reset', 'phox') .'" >';
                                                    //Start Model
                                                    echo '<div class="modal fade" id="resetOptions" tabindex="-1" role="dialog" aria-labelledby="resetOptionsz" >';
                                                        echo '<div class="modal-dialog wdes-moda-control-admin" role="document">';
                                                            echo '<div class="modal-content">';
                                                                echo '<div class="modal-body">';
                                                                    esc_html_e('WARNING! This will overwrite all existing options, please proceed with caution!', 'phox');
                                                                echo '</div>';

                                                                echo '<div class="modal-footer">';
                                                                    echo '<button type="button" class="cancel-button" data-dismiss="modal">'.esc_html__('Close', 'phox').'</button>';
                                                                    echo '<input type="submit"  name="'. $this->args['opt_name'] .'[reset]" class="submit-button-foot" value="'. esc_attr__('Reset', 'phox') .'" >';
                                                                echo '</div>';

                                                            echo '</div>';
                                                        echo '</div>';
                                                    echo '</div>';
                                                    //End Model
                                                    echo '</div>';
                                                echo '</div>';
                                            echo '</div>';
                                        echo '</div>';

                                    echo '</div>';

                                //End Import &Export
                                echo '</div>';
                            //end tab contnet
                            echo '</div>';
                        //end Content
                        echo '</div>';
                    echo '</div>';

                    // end panel row
                    echo '</div>';
                // end Panel container
                echo '</div>';
            //end Form
            echo '</form>';
        //end container
        echo '</div>';
    }

    function _section_desc($section){

    }

    function _field_input($field){
        if (isset($field['type'])) {
            $field_class = 'WDES_Admin_'.$field['type'];

            if(class_exists($field_class)){
                require_once($this->dir . '/fields/'.$field['type']. '/field_'.$field['type'].'.php');
            }

            if(class_exists($field_class)){
                $value = (isset(self::$options[$field['id']]))?self::$options[$field['id']]:'';

                $render = '';
                $render = new $field_class($field, $value, $this);
                $render->render();
            }
        }
    }

    public function download_options()
    {

        if ( ! isset( $_GET['secret'] ) || $_GET['secret'] != md5(AUTH_KEY.SECURE_AUTH_KEY) ){
            wp_die('Invalid Secret Key');
            exit;
        }

        $backup_options = get_option($this->args['opt_name']);
        $backup_options['wdes-opts-backup'] = '1';

        if(isset( $_GET['action']) && $_GET['action'] == 'download_options-'.$this->args['opt_name'] ){

            header('Content-Description:FileTransfer');
            header('Content-type:application/json');
            header('Content-Disposition:attachment;filename="'.str_replace('wdes-opts-','',$this->args['opt_name']).'_options_'.date('d-m-Y').'.json"');
            header('Content-Transfer-Encoding:binary');
            header('Expires:0');
            header('Cache-Control:must-revalidate');
            header('Pragma:public');
		  	x_wdes()->wdes_get_text('##'.json_encode($backup_options).'##');

            exit;

        }

    }

    /**
     * Theme Fonts
    */

    public function wdes_fonts($type = false)
    {
        $fonts = array();

        // local font
        $fonts['local'] = array(
            'Arial',
            'Georgia',
            'Tahoma',
            'Times',
            'Trebuchet',
            'Verdana',
        );

        // google font
        $fonts['google'] = array(
            'ABeeZee',
            'Abel',
            'Abhaya Libre',
            'Abril Fatface',
            'Aclonica',
            'Acme',
            'Actor',
            'Adamina',
            'Advent Pro',
            'Aguafina Script',
            'Akronim',
            'Aladin',
            'Aldrich',
            'Alef',
            'Alegreya',
            'Alegreya SC',
            'Alegreya Sans',
            'Alegreya Sans SC',
            'Alex Brush',
            'Alfa Slab One',
            'Alice',
            'Alike',
            'Alike Angular',
            'Allan',
            'Allerta',
            'Allerta Stencil',
            'Allura',
            'Almendra',
            'Almendra Display',
            'Almendra SC',
            'Amarante',
            'Amaranth',
            'Amatic SC',
            'Amatica SC',
            'Amethysta',
            'Amiko',
            'Amiri',
            'Amita',
            'Anaheim',
            'Andada',
            'Andika',
            'Angkor',
            'Annie Use Your Telescope',
            'Anonymous Pro',
            'Antic',
            'Antic Didone',
            'Antic Slab',
            'Anton',
            'Arapey',
            'Arbutus',
            'Arbutus Slab',
            'Architects Daughter',
            'Archivo Black',
            'Archivo Narrow',
            'Aref Ruqaa',
            'Arima Madurai',
            'Arimo',
            'Arizonia',
            'Armata',
            'Artifika',
            'Arvo',
            'Arya',
            'Asap',
            'Asar',
            'Asset',
            'Assistant',
            'Astloch',
            'Asul',
            'Athiti',
            'Atma',
            'Atomic Age',
            'Aubrey',
            'Audiowide',
            'Autour One',
            'Average',
            'Average Sans',
            'Averia Gruesa Libre',
            'Averia Libre',
            'Averia Sans Libre',
            'Averia Serif Libre',
            'Bad Script',
            'Baloo',
            'Baloo Bhai',
            'Baloo Bhaina',
            'Baloo Chettan',
            'Baloo Da',
            'Baloo Paaji',
            'Baloo Tamma',
            'Baloo Thambi',
            'Balthazar',
            'Bangers',
            'Basic',
            'Battambang',
            'Baumans',
            'Bayon',
            'Belgrano',
            'Belleza',
            'BenchNine',
            'Bentham',
            'Berkshire Swash',
            'Bevan',
            'Bigelow Rules',
            'Bigshot One',
            'Bilbo',
            'Bilbo Swash Caps',
            'BioRhyme',
            'BioRhyme Expanded',
            'Biryani',
            'Bitter',
            'Black Ops One',
            'Bokor',
            'Bonbon',
            'Boogaloo',
            'Bowlby One',
            'Bowlby One SC',
            'Brawler',
            'Bree Serif',
            'Bubblegum Sans',
            'Bubbler One',
            'Buda',
            'Buenard',
            'Bungee',
            'Bungee Hairline',
            'Bungee Inline',
            'Bungee Outline',
            'Bungee Shade',
            'Butcherman',
            'Butterfly Kids',
            'Cabin',
            'Cabin Condensed',
            'Cabin Sketch',
            'Caesar Dressing',
            'Cagliostro',
            'Cairo',
            'Calligraffitti',
            'Cambay',
            'Cambo',
            'Candal',
            'Cantarell',
            'Cantata One',
            'Cantora One',
            'Capriola',
            'Cardo',
            'Carme',
            'Carrois Gothic',
            'Carrois Gothic SC',
            'Carter One',
            'Catamaran',
            'Caudex',
            'Caveat',
            'Caveat Brush',
            'Cedarville Cursive',
            'Ceviche One',
            'Changa',
            'Changa One',
            'Chango',
            'Chathura',
            'Chau Philomene One',
            'Chela One',
            'Chelsea Market',
            'Chenla',
            'Cherry Cream Soda',
            'Cherry Swash',
            'Chewy',
            'Chicle',
            'Chivo',
            'Chonburi',
            'Cinzel',
            'Cinzel Decorative',
            'Clicker Script',
            'Coda',
            'Coda Caption',
            'Codystar',
            'Coiny',
            'Combo',
            'Comfortaa',
            'Coming Soon',
            'Concert One',
            'Condiment',
            'Content',
            'Contrail One',
            'Convergence',
            'Cookie',
            'Copse',
            'Corben',
            'Cormorant',
            'Cormorant Garamond',
            'Cormorant Infant',
            'Cormorant SC',
            'Cormorant Unicase',
            'Cormorant Upright',
            'Courgette',
            'Cousine',
            'Coustard',
            'Covered By Your Grace',
            'Crafty Girls',
            'Creepster',
            'Crete Round',
            'Crimson Text',
            'Croissant One',
            'Crushed',
            'Cuprum',
            'Cutive',
            'Cutive Mono',
            'Damion',
            'Dancing Script',
            'Dangrek',
            'David Libre',
            'Dawning of a New Day',
            'Days One',
            'Dekko',
            'Delius',
            'Delius Swash Caps',
            'Delius Unicase',
            'Della Respira',
            'Denk One',
            'Devonshire',
            'Dhurjati',
            'Didact Gothic',
            'Diplomata',
            'Diplomata SC',
            'Domine',
            'Donegal One',
            'Doppio One',
            'Dorsa',
            'Dosis',
            'Dr Sugiyama',
            'Droid Sans',
            'Droid Sans Mono',
            'Droid Serif',
            'Duru Sans',
            'Dynalight',
            'EB Garamond',
            'Eagle Lake',
            'Eater',
            'Economica',
            'Eczar',
            'Ek Mukta',
            'El Messiri',
            'Electrolize',
            'Elsie',
            'Elsie Swash Caps',
            'Emblema One',
            'Emilys Candy',
            'Engagement',
            'Englebert',
            'Enriqueta',
            'Erica One',
            'Esteban',
            'Euphoria Script',
            'Ewert',
            'Exo',
            'Exo 2',
            'Expletus Sans',
            'Fanwood Text',
            'Farsan',
            'Fascinate',
            'Fascinate Inline',
            'Faster One',
            'Fasthand',
            'Fauna One',
            'Federant',
            'Federo',
            'Felipa',
            'Fenix',
            'Finger Paint',
            'Fira Mono',
            'Fira Sans',
            'Fjalla One',
            'Fjord One',
            'Flamenco',
            'Flavors',
            'Fondamento',
            'Fontdiner Swanky',
            'Forum',
            'Francois One',
            'Frank Ruhl Libre',
            'Freckle Face',
            'Fredericka the Great',
            'Fredoka One',
            'Freehand',
            'Fresca',
            'Frijole',
            'Fruktur',
            'Fugaz One',
            'GFS Didot',
            'GFS Neohellenic',
            'Gabriela',
            'Gafata',
            'Galada',
            'Galdeano',
            'Galindo',
            'Gentium Basic',
            'Gentium Book Basic',
            'Geo',
            'Geostar',
            'Geostar Fill',
            'Germania One',
            'Gidugu',
            'Gilda Display',
            'Give You Glory',
            'Glass Antiqua',
            'Glegoo',
            'Gloria Hallelujah',
            'Goblin One',
            'Gochi Hand',
            'Gorditas',
            'Goudy Bookletter 1911',
            'Graduate',
            'Grand Hotel',
            'Gravitas One',
            'Great Vibes',
            'Griffy',
            'Gruppo',
            'Gudea',
            'Gurajada',
            'Habibi',
            'Halant',
            'Hammersmith One',
            'Hanalei',
            'Hanalei Fill',
            'Handlee',
            'Hanuman',
            'Happy Monkey',
            'Harmattan',
            'Headland One',
            'Heebo',
            'Henny Penny',
            'Herr Von Muellerhoff',
            'Hind',
            'Hind Guntur',
            'Hind Madurai',
            'Hind Siliguri',
            'Hind Vadodara',
            'Holtwood One SC',
            'Homemade Apple',
            'Homenaje',
            'IM Fell DW Pica',
            'IM Fell DW Pica SC',
            'IM Fell Double Pica',
            'IM Fell Double Pica SC',
            'IM Fell English',
            'IM Fell English SC',
            'IM Fell French Canon',
            'IM Fell French Canon SC',
            'IM Fell Great Primer',
            'IM Fell Great Primer SC',
            'Iceberg',
            'Iceland',
            'Imprima',
            'Inconsolata',
            'Inder',
            'Indie Flower',
            'Inika',
            'Inknut Antiqua',
            'Irish Grover',
            'Istok Web',
            'Italiana',
            'Italianno',
            'Itim',
            'Jacques Francois',
            'Jacques Francois Shadow',
            'Jaldi',
            'Jim Nightshade',
            'Jockey One',
            'Jolly Lodger',
            'Jomhuria',
            'Josefin Sans',
            'Josefin Slab',
            'Joti One',
            'Judson',
            'Julee',
            'Julius Sans One',
            'Junge',
            'Jura',
            'Just Another Hand',
            'Just Me Again Down Here',
            'Kadwa',
            'Kalam',
            'Kameron',
            'Kanit',
            'Kantumruy',
            'Karla',
            'Karma',
            'Katibeh',
            'Kaushan Script',
            'Kavivanar',
            'Kavoon',
            'Kdam Thmor',
            'Keania One',
            'Kelly Slab',
            'Kenia',
            'Khand',
            'Khmer',
            'Khula',
            'Kite One',
            'Knewave',
            'Kotta One',
            'Koulen',
            'Kranky',
            'Kreon',
            'Kristi',
            'Krona One',
            'Kumar One',
            'Kumar One Outline',
            'Kurale',
            'La Belle Aurore',
            'Laila',
            'Lakki Reddy',
            'Lalezar',
            'Lancelot',
            'Lateef',
            'Lato',
            'League Script',
            'Leckerli One',
            'Ledger',
            'Lekton',
            'Lemon',
            'Lemonada',
            'Libre Baskerville',
            'Libre Franklin',
            'Life Savers',
            'Lilita One',
            'Lily Script One',
            'Limelight',
            'Linden Hill',
            'Lobster',
            'Lobster Two',
            'Londrina Outline',
            'Londrina Shadow',
            'Londrina Sketch',
            'Londrina Solid',
            'Lora',
            'Love Ya Like A Sister',
            'Loved by the King',
            'Lovers Quarrel',
            'Luckiest Guy',
            'Lusitana',
            'Lustria',
            'Macondo',
            'Macondo Swash Caps',
            'Mada',
            'Magra',
            'Maiden Orange',
            'Maitree',
            'Mako',
            'Mallanna',
            'Mandali',
            'Marcellus',
            'Marcellus SC',
            'Marck Script',
            'Margarine',
            'Marko One',
            'Marmelad',
            'Martel',
            'Martel Sans',
            'Marvel',
            'Mate',
            'Mate SC',
            'Maven Pro',
            'McLaren',
            'Meddon',
            'MedievalSharp',
            'Medula One',
            'Meera Inimai',
            'Megrim',
            'Meie Script',
            'Merienda',
            'Merienda One',
            'Merriweather',
            'Merriweather Sans',
            'Metal',
            'Metal Mania',
            'Metamorphous',
            'Metrophobic',
            'Michroma',
            'Milonga',
            'Miltonian',
            'Miltonian Tattoo',
            'Miniver',
            'Miriam Libre',
            'Mirza',
            'Miss Fajardose',
            'Mitr',
            'Modak',
            'Modern Antiqua',
            'Mogra',
            'Molengo',
            'Molle',
            'Monda',
            'Monofett',
            'Monoton',
            'Monsieur La Doulaise',
            'Montaga',
            'Montez',
            'Montserrat',
            'Montserrat Alternates',
            'Montserrat Subrayada',
            'Moul',
            'Moulpali',
            'Mountains of Christmas',
            'Mouse Memoirs',
            'Mr Bedfort',
            'Mr Dafoe',
            'Mr De Haviland',
            'Mrs Saint Delafield',
            'Mrs Sheppards',
            'Mukta Vaani',
            'Muli',
            'Mystery Quest',
            'NTR',
            'Neucha',
            'Neuton',
            'New Rocker',
            'News Cycle',
            'Niconne',
            'Nixie One',
            'Nobile',
            'Nokora',
            'Norican',
            'Nosifer',
            'Nothing You Could Do',
            'Noticia Text',
            'Noto Sans',
            'Noto Serif',
            'Nova Cut',
            'Nova Flat',
            'Nova Mono',
            'Nova Oval',
            'Nova Round',
            'Nova Script',
            'Nova Slim',
            'Nova Square',
            'Numans',
            'Nunito',
            'Nunito Sans',
            'Odor Mean Chey',
            'Offside',
            'Old Standard TT',
            'Oldenburg',
            'Oleo Script',
            'Oleo Script Swash Caps',
            'Open Sans',
            'Open Sans Condensed',
            'Oranienbaum',
            'Orbitron',
            'Oregano',
            'Orienta',
            'Original Surfer',
            'Oswald',
            'Over the Rainbow',
            'Overlock',
            'Overlock SC',
            'Ovo',
            'Oxygen',
            'Oxygen Mono',
            'PT Mono',
            'PT Sans',
            'PT Sans Caption',
            'PT Sans Narrow',
            'PT Serif',
            'PT Serif Caption',
            'Pacifico',
            'Palanquin',
            'Palanquin Dark',
            'Paprika',
            'Parisienne',
            'Passero One',
            'Passion One',
            'Pathway Gothic One',
            'Patrick Hand',
            'Patrick Hand SC',
            'Pattaya',
            'Patua One',
            'Pavanam',
            'Paytone One',
            'Peddana',
            'Peralta',
            'Permanent Marker',
            'Petit Formal Script',
            'Petrona',
            'Philosopher',
            'Piedra',
            'Pinyon Script',
            'Pirata One',
            'Plaster',
            'Play',
            'Playball',
            'Playfair Display',
            'Playfair Display SC',
            'Podkova',
            'Poiret One',
            'Poller One',
            'Poly',
            'Pompiere',
            'Pontano Sans',
            'Poppins',
            'Port Lligat Sans',
            'Port Lligat Slab',
            'Pragati Narrow',
            'Prata',
            'Preahvihear',
            'Press Start 2P',
            'Pridi',
            'Princess Sofia',
            'Prociono',
            'Prompt',
            'Prosto One',
            'Proza Libre',
            'Puritan',
            'Purple Purse',
            'Quando',
            'Quantico',
            'Quattrocento',
            'Quattrocento Sans',
            'Questrial',
            'Quicksand',
            'Quintessential',
            'Qwigley',
            'Racing Sans One',
            'Radley',
            'Rajdhani',
            'Rakkas',
            'Raleway',
            'Raleway Dots',
            'Ramabhadra',
            'Ramaraja',
            'Rambla',
            'Rammetto One',
            'Ranchers',
            'Rancho',
            'Ranga',
            'Rasa',
            'Rationale',
            'Ravi Prakash',
            'Redressed',
            'Reem Kufi',
            'Reenie Beanie',
            'Revalia',
            'Rhodium Libre',
            'Ribeye',
            'Ribeye Marrow',
            'Righteous',
            'Risque',
            'Roboto',
            'Roboto Condensed',
            'Roboto Mono',
            'Roboto Slab',
            'Rochester',
            'Rock Salt',
            'Rokkitt',
            'Romanesco',
            'Ropa Sans',
            'Rosario',
            'Rosarivo',
            'Rouge Script',
            'Rozha One',
            'Rubik',
            'Rubik Mono One',
            'Rubik One',
            'Ruda',
            'Rufina',
            'Ruge Boogie',
            'Ruluko',
            'Rum Raisin',
            'Ruslan Display',
            'Russo One',
            'Ruthie',
            'Rye',
            'Sacramento',
            'Sahitya',
            'Sail',
            'Salsa',
            'Sanchez',
            'Sancreek',
            'Sansita One',
            'Sarala',
            'Sarina',
            'Sarpanch',
            'Satisfy',
            'Scada',
            'Scheherazade',
            'Schoolbell',
            'Scope One',
            'Seaweed Script',
            'Secular One',
            'Sevillana',
            'Seymour One',
            'Shadows Into Light',
            'Shadows Into Light Two',
            'Shanti',
            'Share',
            'Share Tech',
            'Share Tech Mono',
            'Shojumaru',
            'Short Stack',
            'Shrikhand',
            'Siemreap',
            'Sigmar One',
            'Signika',
            'Signika Negative',
            'Simonetta',
            'Sintony',
            'Sirin Stencil',
            'Six Caps',
            'Skranji',
            'Slabo 13px',
            'Slabo 27px',
            'Slackey',
            'Smokum',
            'Smythe',
            'Sniglet',
            'Snippet',
            'Snowburst One',
            'Sofadi One',
            'Sofia',
            'Sonsie One',
            'Sorts Mill Goudy',
            'Source Code Pro',
            'Source Sans Pro',
            'Source Serif Pro',
            'Space Mono',
            'Special Elite',
            'Spicy Rice',
            'Spinnaker',
            'Spirax',
            'Squada One',
            'Sree Krushnadevaraya',
            'Sriracha',
            'Stalemate',
            'Stalinist One',
            'Stardos Stencil',
            'Stint Ultra Condensed',
            'Stint Ultra Expanded',
            'Stoke',
            'Strait',
            'Sue Ellen Francisco',
            'Suez One',
            'Sumana',
            'Sunshiney',
            'Supermercado One',
            'Sura',
            'Suranna',
            'Suravaram',
            'Suwannaphum',
            'Swanky and Moo Moo',
            'Syncopate',
            'Tangerine',
            'Taprom',
            'Tauri',
            'Taviraj',
            'Teko',
            'Telex',
            'Tenali Ramakrishna',
            'Tenor Sans',
            'Text Me One',
            'The Girl Next Door',
            'Tienne',
            'Tillana',
            'Timmana',
            'Tinos',
            'Titan One',
            'Titillium Web',
            'Trade Winds',
            'Trirong',
            'Trocchi',
            'Trochut',
            'Trykker',
            'Tulpen One',
            'Ubuntu',
            'Ubuntu Condensed',
            'Ubuntu Mono',
            'Ultra',
            'Uncial Antiqua',
            'Underdog',
            'Unica One',
            'UnifrakturCook',
            'UnifrakturMaguntia',
            'Unkempt',
            'Unlock',
            'Unna',
            'VT323',
            'Vampiro One',
            'Varela',
            'Varela Round',
            'Vast Shadow',
            'Vesper Libre',
            'Vibur',
            'Vidaloka',
            'Viga',
            'Voces',
            'Volkhov',
            'Vollkorn',
            'Voltaire',
            'Waiting for the Sunrise',
            'Wallpoet',
            'Walter Turncoat',
            'Warnes',
            'Wellfleet',
            'Wendy One',
            'Wire One',
            'Work Sans',
            'Yanone Kaffeesatz',
            'Yantramanav',
            'Yatra One',
            'Yellowtail',
            'Yeseva One',
            'Yesteryear',
            'Yrsa',
            'Zeyada',
        );

	    // custom uploaded font

	    $font_custom = wdes_opts_get('font-custom');
	    $font_custom2 = wdes_opts_get('font-custom-2');

	    if( $font_custom || $font_custom2 ){
		    $fonts['custom'] = [];

		    if( $font_custom ){
			    $fonts['custom'][] = $font_custom;
		    }
		    if( $font_custom2 ){
			    $fonts['custom'][] = $font_custom2;
		    }
	    }

	    //add new font to custom
	    $fonts = apply_filters( 'wdes_add_google_font', $fonts );

        if($type){
            return $fonts[$type];
        }else{
            return $fonts;
        }


    }


}