<?php

namespace Phox\core\admin\panel\wdes_options;

if (! defined('ABSPATH')) {
    exit; //exit if accessed directly
}


class Wdes_Options{

    public $menu =array();
    public $sections = array();
    public $images_icons = array(
    	'layout_1'  				=> WDES_OPTIONS_URI .'img/layout/wide.svg',
    	'layout_2'    				=> WDES_OPTIONS_URI .'img/layout/boxed.svg',
	    'topheader_layout_1'  		=> WDES_OPTIONS_URI .'img/layout/top-heaeder-layout-1.png',
	    'topheader_layout_2'    	=> WDES_OPTIONS_URI .'img/layout/top-heaeder-layout-2.png',
		'background_patterns_1' 	=> WDES_OPTIONS_URI .'img/layout/patterns/1.png',
		'background_patterns_2' 	=> WDES_OPTIONS_URI .'img/layout/patterns/2.png',
		'background_patterns_3' 	=> WDES_OPTIONS_URI .'img/layout/patterns/3.png',
		'background_patterns_4' 	=> WDES_OPTIONS_URI .'img/layout/patterns/4.png',
		'background_patterns_5' 	=> WDES_OPTIONS_URI .'img/layout/patterns/5.png',
		'background_patterns_6' 	=> WDES_OPTIONS_URI .'img/layout/patterns/6.png',
		'background_patterns_7' 	=> WDES_OPTIONS_URI .'img/layout/patterns/7.png',
		'background_patterns_8' 	=> WDES_OPTIONS_URI .'img/layout/patterns/8.png',
		'background_patterns_9' 	=> WDES_OPTIONS_URI .'img/layout/patterns/9.png',
		'background_patterns_10' 	=> WDES_OPTIONS_URI .'img/layout/patterns/10.png',
		'sidebar-page-layout_1'		=> WDES_OPTIONS_URI .'img/layout/wide.svg',
		'sidebar-page-layout_2'		=> WDES_OPTIONS_URI .'img/layout/l-sidebar.svg',
		'sidebar-page-layout_3'		=> WDES_OPTIONS_URI .'img/layout/r-sidebar.svg',
		'blog-pin-post_1' 			=> WDES_OPTIONS_URI .'/img/layout/pin-left.svg',
		'blog-pin-post_2' 			=> WDES_OPTIONS_URI .'/img/layout/pin-right.svg',
	    'backtotop_layout_1' 		=> WDES_OPTIONS_URI .'/img/layout/back-top-right.png',
	    'backtotop_layout_2' 		=> WDES_OPTIONS_URI .'/img/layout/back-top-center.png',
	    'backtotop_layout_3' 		=> WDES_OPTIONS_URI .'/img/layout/back-top-left.png',

		);


    function fire()
    {

        $this->wdes_opts_setup();
        $WDES_Admin = new Wdes_Control($this->menu, $this->sections );

    }

	public function get_builder_items ( $type = '' ) {

		$list = [
			'default' => 'Default'
		];

		$args = [
			'post_type' => 'wdes-theme-builder',
			'tax_query' => [
				[
					'taxonomy' => 'wdes_library_type',
					'field'    => 'slug',
					'terms'    => [$type],
					'operator' => 'IN',
					'include_children' => true
				]
			]
		];


		$query = new \WP_Query( $args );

		$posts = $query->posts;

		if( ! empty( $posts ) ){

			foreach ( $posts as $item ){

				$list[$item->ID] = $item->post_title;

			}

		}

		return $list;

	}

    public function wdes_opts_setup()
    {
        $this->menu = array(

            //General *********************************
            'global' => array(
                'title'    => esc_html__('General', 'phox'),
                'icon'     =>'wh-icons fas fa-tachometer-alt'
            ),

            //Header *******************************
            'header' => array(
                'title'    =>esc_html__('Header', 'phox'),
                'icon'     =>'wh-icons fas fa-desktop'
            ),


            //sidebar *******************************
            'sidebar' => array(
                'title'    =>esc_html__('Sidebar', 'phox'),
                'icon'     =>'wh-icons fas fa-window-restore'
            ),

            //Footer *******************************
            'footer' => array(
                'title'    =>esc_html__('Footer', 'phox'),
                'icon'     =>'wh-icons fas fa-file'
            ),

            //seo *******************************
            'seo' => array(
                'title'    =>esc_html__('SEO', 'phox'),
                'icon'     =>'wh-icons fas fa-chart-pie',
            ),

	        //page *******************************
            'page' => [
	            'title'    =>esc_html__('Pages', 'phox'),
	            'icon'     =>esc_html__('wh-icons far fa-file','phox'),
	            'sections'  => [
		            'wh-icons fas fa-columns'=>[
			            'id' => 'blog',
			            'title' => esc_html__('Blog', 'phox'),
		            ],
		            'wh-icons fas fa-file'=>[
			            'id' => 'single',
			            'title' => esc_html__('Single', 'phox'),
		            ],
		            'wh-icons fas fa-bug'=>[
			            'id' => 'page404',
			            'title' => esc_html__('Page 404', 'phox'),
		            ],
		            'wh-icons fas fa-cubes'=>[
			            'id' => 'comingsoon',
			            'title' => esc_html__('Coming Soon', 'phox'),
		            ]
	            ]

            ],

            //Social Media *******************************
            'social-media' => array(
                'title'    =>esc_html__('Social Media', 'phox'),
                'icon'     =>'wh-icons fas fa-share-alt'
            ),

            //color *******************************
            'color' => array(
                'title'    =>esc_html__('Color', 'phox'),
                'icon'     =>'wh-icons fas fa-paint-brush'
            ),

	        //typography *******************************
            'typography' => [
	            'title'    =>esc_html__('Typography', 'phox'),
	            'icon'     =>'wh-icons fas fa-text-width',
	            'sections'  => [
		            'wh-icons fas fa-file'=>[
			            'id' => 'gen-typo',
			            'title' => esc_html__('General Typo', 'phox')
		            ],
		            'wh-icons fas fa-bug'=>[
			            'id' => 'custom-fonts',
			            'title' => esc_html__('Custom Fonts', 'phox')
		            ],
	            ]
            ],

	        //Elementor Widgets Settings *******************************
            'elementor-widgets' => [
	            'title'    =>esc_html__('Elementor Widgets', 'phox'),
	            'icon'     =>'wh-icons fab fa-elementor'
            ],

		    //Chat Platforms *******************************
			'chat-platforms' => array(
				'title'    =>esc_html__('Chat Platforms', 'phox'),
				'icon'     =>'wh-icons fas fa-comment'
			),

            //Custom style & script *******************************
            'custom-css-js' => array(
                'title'    =>esc_html__('Custom Style & Script', 'phox'),
                'icon'     =>'wh-icons fas fa-file'
            ),

            //Import / Export *******************************
            'import-export' => array(
                'title'    =>esc_html__('Import , Export & Reset', 'phox'),
                'icon'     =>'wh-icons fas fa-upload'
            ),

        );

        // ---------------------------- General -----------------------------------
        $this->sections['global'] = array(
            'title'     => '<div class="tab-title-head"><span class="fas fa-cogs"></span>'. esc_html__('General Settings', 'phox'). '</div>',
            'fields'    => array(
                // Layout
                array(
                    'package'              => esc_html__('Layout', 'phox'),
					'package_id'					 => 'general-layout',
                    'package-options'      =>array(
                        array(
                            'id'              => 'layout',
                            'type'            => 'radio',
                            'title'           => esc_html__('Layout','phox'),
                            'desc'            => esc_html__('Choose Layout', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c custom-block-function-option',
                            'options'         => array(
                                'wide'      => '<img class="layout-style" src="'. esc_url( $this->images_icons['layout_1'] ) .'" alt="'.esc_attr('Wide', 'phox').'"> <span class="wdes-option-span-sub">'. esc_html__('Wide', 'phox') .'</span>',
                                'boxed'     => '<img class="layout-style" src="'. esc_url( $this->images_icons['layout_2'] ) .'" alt="'.esc_attr('Boxed', 'phox').'"> <span class="wdes-option-span-sub">'. esc_html__('Boxed', 'phox') .'</span>'
                            ),
                            'def'               => 'wide'
                        ),
                        array(
                            'id'              => 'background_patterns',
                            'type'            => 'radio',
                            'title'           => esc_html__('Background Patterns','phox'),
                            'desc'            => esc_html__('Patterns works only in Boxed Layout', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c custom-block-function-option custom2-block-function-option',
                            'options'         => array(
                                'pattern1'      => '<img class="layout-style custom-new-layout-style" src="'. esc_url($this->images_icons['background_patterns_1']).'" alt="'.esc_attr__('Pattern 1', 'phox').'">',
                                'pattern2'      => '<img class="layout-style custom-new-layout-style" src="'. esc_url($this->images_icons['background_patterns_2']) .'" alt="'.esc_attr__('Pattern 2', 'phox').'">',
                                'pattern3'      => '<img class="layout-style custom-new-layout-style" src="'. esc_url($this->images_icons['background_patterns_3']) .'" alt="'.esc_attr__('Pattern 3', 'phox').'">',
                                'pattern4'      => '<img class="layout-style custom-new-layout-style" src="'. esc_url($this->images_icons['background_patterns_4']) .'" alt="'.esc_attr__('Pattern 4', 'phox').'">',
                                'pattern5'      => '<img class="layout-style custom-new-layout-style" src="'. esc_url($this->images_icons['background_patterns_5']) .'" alt="'.esc_attr__('Pattern 5', 'phox').'">',
                                'pattern6'      => '<img class="layout-style custom-new-layout-style" src="'. esc_url($this->images_icons['background_patterns_6']) .'" alt="'.esc_attr__('Pattern 6', 'phox').'">',
                                'pattern7'      => '<img class="layout-style custom-new-layout-style" src="'. esc_url($this->images_icons['background_patterns_7']) .'" alt="'.esc_attr__('Pattern 7', 'phox').'">',
                                'pattern8'      => '<img class="layout-style custom-new-layout-style" src="'. esc_url($this->images_icons['background_patterns_8']) .'" alt="'.esc_attr__('Pattern 8', 'phox').'">',
                                'pattern9'      => '<img class="layout-style custom-new-layout-style" src="'. esc_url($this->images_icons['background_patterns_9']) .'" alt="'.esc_attr__('Pattern 9', 'phox').'">',
                                'pattern10'      => '<img class="layout-style custom-new-layout-style" src="'. esc_url($this->images_icons['background_patterns_10']) .'" alt="'.esc_attr__('Pattern 10', 'phox').'">'
                            ),
                        ),
                        array(
                            'id'              => 'preloader',
                            'type'            => 'switch',
                            'title'           => esc_html__('Display Loading Screen','phox'),
                            'desc'            => esc_html__('Turn on to display loading screen', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                    )
                ),
                // Logo
                array(
                    'package'              => esc_html__('Logo', 'phox'),
					'package_id'					 => 'general-logo',
                    'package-options'      =>array(
                        array(
                            'id'              => 'logo',
                            'type'            => 'upload',
                            'title'           => esc_html__('Upload Logo','phox'),
                            'desc'            => esc_html__('Image Logo', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'retinal-logo',
                            'type'            => 'upload',
                            'title'           => esc_html__('Retina Logo','phox'),
                            'desc'            => esc_html__('( optional ) Retina Logo should be 2x larger than Custom Logo', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'height-logo',
                            'type'            => 'number',
                            'title'           => esc_html__('Height','phox'),
                            'desc'            => esc_html__('( optional ) Add custom height to the logo', 'phox'),
                            'note'            => esc_html__('default:31px', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '40'
                        ),
                        array(
                            'id'              => 'text-logo',
                            'type'            => 'text',
                            'title'           => esc_html__('Text Logo','phox'),
                            'desc'            => esc_html__('( optional ) Type your Company name', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'text-logo-size',
                            'type'            => 'number',
                            'title'           => esc_html__('Logo size','phox'),
                            'desc'            => esc_html__('( optional ) Add logo size ', 'phox'),
                            'note'            => esc_html__('default:20px', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '20'
                        ),
                        array(
                            'id'              => 'favicon',
                            'type'            => 'upload',
                            'title'           => esc_html__('Favicon','phox'),
                            'desc'            => esc_html__('image that get displayed in the address bar of every browser.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),

                    )
                ),
                // Google Maps
                array(
                    'package'              => esc_html__('Google Maps', 'phox'),
					'package_id'					 => 'general-google-maps',
                    'package-options'      =>array(

                        array(
                            'id'              => 'google_maps_api_key',
                            'type'            => 'text',
                            'title'           => esc_html__('Google Maps API ','phox'),
                            'desc'            => esc_html__('If you are generating a large volume of queries to the Google Maps API please ', 'phox').'<a target="_blank" href="https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key">'.esc_html__('Get an API key', 'phox').'</a>',
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                    ),
                ),

            )
        );

        // ---------------------------- Header -----------------------------------
        $this->sections['header'] =array(
            'title'     => '<div class="tab-title-head"><span class="fas fa-file"></span> '. esc_html__('Header', 'phox').'</div>',
            'fields'    => array(
                // Header Layout
                array(
                    'package'              => esc_html__('General', 'phox'),
					'package_id'		   => 'header-general',
                    'package-options'      =>array(
	                    array(
		                    'id'              => 'website_header',
		                    'type'            => 'select',
		                    'title'           => esc_html__('Website Header','phox'),
		                    'desc'            => esc_html__('Select your custom header', 'phox'),
		                    'note'            => esc_html__('', 'phox'),
		                    'options'         => $this->get_builder_items('wdes_header'),
		                    'class'           => 'block-function-c',
	                    ),
                        array(
                            'id'              => 'topbar-login',
                            'type'            => 'switch',
                            'title'           => esc_html__('Display Login Button','phox'),
                            'desc'            => esc_html__('Turn on to display login button on header', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'topbar-login-url',
                            'type'            => 'text',
                            'title'           => esc_html__('Login Url','phox'),
                            'desc'            => esc_html__('Set Login Url', 'phox'),
                            'note'            => esc_html__('You should display login before that', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'topbar-cart',
                            'type'            => 'switch',
                            'title'           => esc_html__('Display cart Button','phox'),
                            'desc'            => esc_html__('Turn on to display cart button on header', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'topbar-cart-url',
                            'type'            => 'text',
                            'title'           => esc_html__('Cart Url','phox'),
                            'desc'            => esc_html__('Set Cart Url', 'phox'),
                            'note'            => esc_html__('You should display cart before that', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'topbar-search',
                            'type'            => 'switch',
                            'title'           => esc_html__('Display Search Button','phox'),
                            'desc'            => esc_html__('Turn on to display search button on header', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
	                    array(
		                    'id'              => 'sticky-header',
		                    'type'            => 'switch',
		                    'title'           => esc_html__('Sticky Header','phox'),
		                    'desc'            => esc_html__('Turn on to display sticky header', 'phox'),
		                    'note'            => esc_html__('', 'phox'),
		                    'class'           => 'block-function-c',
	                    ),
                    ),
                ),

	            array(
		            'package'              => esc_html__('Top Header', 'phox'),
		            'package_id'		   => 'top-header',
		            'package-options'      =>array(
			            array(
				            'id'              => 'show-topheader',
				            'type'            => 'switch',
				            'title'           => esc_html__('Display Top Header bar','phox'),
				            'desc'            => esc_html__('Enable its to display top header bar', 'phox'),
				            'note'            => esc_html__('', 'phox'),
				            'class'           => 'block-function-c',
				            'def'             => 1
			            ),
			            array(
				            'id'              => 'top-header-layout',
				            'type'            => 'radio',
				            'title'           => esc_html__('Layout','phox'),
				            'desc'            => esc_html__('Choose Layout for top header bar', 'phox'),
				            'note'            => esc_html__('', 'phox'),
				            'class'           => 'block-function-c top-header-options',
				            'options'         => array(
					            'top-header-layout-1'      => '<img class="layout-view-xl" src="'. esc_url( $this->images_icons['topheader_layout_1'] ) .'" alt="'.esc_attr('Layout 1', 'phox').'">',
					            'top-header-layout-2'      => '<img class="layout-view-xl" src="'. esc_url( $this->images_icons['topheader_layout_2'] ) .'" alt="'.esc_attr('Layout 2', 'phox').'">'
				            ),
				            'def'              => 'top-header-layout-1'
			            ),

		            ),
	            ),

            ),
        );


        // ---------------------------- SideBar -----------------------------------
        $this->sections['sidebar'] =array(
            'title'     => '<div class="tab-title-head"><span class="fas fa-file"></span>'. esc_html__('Sidebar', 'phox').'</div>',
            'fields'    => array(
                // Page Sidebar
                array(
                    'package'              => esc_html__('Archive Sidebar', 'phox'),
										'package_id'					 => 'page-sidebar',
                    'package-options'      =>array(
                        array(
                            'id'              => 'sidebar-page-layout',
                            'type'            => 'radio',
                            'title'           => esc_html__('Layout','phox'),
                            'desc'            => esc_html__('Choose Archive Sidebar Layout', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c custom-block-function-option',
                            'options'         => array(
                                'no'        => '<img class="layout-style" src="'. esc_url($this->images_icons['sidebar-page-layout_1']) .'" alt="'.esc_attr__('Full', 'phox').'"> <span class="wdes-option-span-sub">'. esc_html__('Full width', 'phox') .'</span>',
                                'left'      => '<img class="layout-style" src="'. esc_url($this->images_icons['sidebar-page-layout_2']) .'" alt="'.esc_attr__('Left', 'phox').'"> <span class="wdes-option-span-sub">'. esc_html__('Left Sidebar', 'phox') .'</span>',
                                'right'     => '<img class="layout-style" src="'. esc_url($this->images_icons['sidebar-page-layout_3']) .'" alt="'.esc_attr__('Right', 'phox').'"> <span class="wdes-option-span-sub">'. esc_html__('Right Sidebar', 'phox') .'</span>'

                            ),
                            'def'              => 'right'
                        ),
                        array(
                            'id'              => 'sidebar-page-sidebar-one',
                            'type'            => 'select',
                            'title'           => esc_html__('Sidebar','phox'),
                            'desc'            => esc_html__('Use this option when use one sidebar option', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'options'         =>array(
                                'main'   => esc_html__('Main Sidebar', 'phox'),
                                'left'   => esc_html__('Left Sidebar', 'phox'),
                                'right'  => esc_html__('Right Sidebar', 'phox'),

                            ),
                            'def'              => 'main'
                        ),

                    ),
                ),

                // Single Sidebar
                array(
                    'package'              => esc_html__('Single Sidebar', 'phox'),
										'package_id'					 => 'single-sidebar',
                    'package-options'      =>array(
                        array(
                            'id'              => 'sidebar-single-layout',
                            'type'            => 'radio',
                            'title'           => esc_html__('Layout','phox'),
                            'desc'            => esc_html__('Choose Single Sidebar Layout', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c custom-block-function-option',
                            'options'         => array(
                                'no'        => '<img class="layout-style" src="'. esc_url($this->images_icons['sidebar-page-layout_1']) .'" alt="'.esc_attr__('Full', 'phox').'"> <span class="wdes-option-span-sub">'. esc_html__('Full width', 'phox') .'</span>',
                                'left'      => '<img class="layout-style" src="'. esc_url($this->images_icons['sidebar-page-layout_2']) .'" alt="'.esc_attr__('Left', 'phox').'"> <span class="wdes-option-span-sub">'. esc_html__('Left Sidebar', 'phox') .'</span>',
                                'right'     => '<img class="layout-style" src="'. esc_url($this->images_icons['sidebar-page-layout_3']) .'" alt="'.esc_attr__('Right', 'phox').'"> <span class="wdes-option-span-sub">'. esc_html__('Right Sidebar', 'phox') .'</span>'

                            ),
                            'def'              => 'no'
                        ),
                        array(
                            'id'              => 'sidebar-single-sidebar-one',
                            'type'            => 'select',
                            'title'           => esc_html__('Sidebar','phox'),
                            'desc'            => esc_html__('Use this option when use one sidebar option', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'options'         =>array(
                                'main'   => esc_html__('Main Sidebar', 'phox'),
                                'left'   => esc_html__('Left Sidebar', 'phox'),
                                'right'  => esc_html__('Right Sidebar', 'phox'),

                            ),
                            'def'              => 'main'
                        ),

                    ),
                ),

            ),
        );
        // ---------------------------- Footer -----------------------------------
        $this->sections['footer'] =array(
            'title'     => '<div class="tab-title-head"><span class="fas fa-file"></span>'. esc_html__('Footer', 'phox').'</div>',
            'fields'    => array(
	            //general
	            array(
		            'package'              => esc_html__('General', 'phox'),
		            'package_id'					 => 'footer-general',
		            'package-options'      =>array(
			            array(
				            'id'              => 'website_footer',
				            'type'            => 'select',
				            'title'           => esc_html__('Website Footer','phox'),
				            'desc'            => esc_html__('Select your custom header', 'phox'),
				            'note'            => esc_html__('', 'phox'),
				            'options'         => $this->get_builder_items('wdes_footer'),
				            'class'           => 'block-function-c',
			            ),
		            ),
	            ),
                // Layout
                array(
                    'package'              => esc_html__('BackGround', 'phox'),
					'package_id'					 => 'footer-background',
                    'package-options'      =>array(
                        array(
                            'id'              => 'footer-bg-img',
                            'type'            => 'upload',
                            'title'           => esc_html__('Image','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'footer-bg-img-position',
                            'type'            => 'select',
                            'title'           => esc_html__('Position','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'options'         =>array(
                                'no-repeat;left top;;' => esc_html__( 'Left Top | no-repeat', 'phox' ),
                                'repeat;left top;;' => esc_html__( 'Left Top | repeat', 'phox' ),
                                'no-repeat;left center;;' => esc_html__( 'Left Center | no-repeat', 'phox' ),
                                'repeat;left center;;' => esc_html__( 'Left Center | repeat', 'phox' ),
                                'no-repeat;left bottom;;' => esc_html__( 'Left Bottom | no-repeat', 'phox' ),
                                'repeat;left bottom;;' => esc_html__( 'Left Bottom | repeat', 'phox' ),

                                'no-repeat;center top;;' => esc_html__( 'Center Top | no-repeat', 'phox' ),
                                'repeat;center top;;' => esc_html__( 'Center Top | repeat', 'phox' ),
                                'repeat-x;center top;;' => esc_html__( 'Center Top | repeat-x', 'phox' ),
                                'no-repeat;center;;' => esc_html__( 'Center Center | no-repeat', 'phox' ),
                                'repeat;center;;' => esc_html__( 'Center Center | repeat', 'phox' ),
                                'no-repeat;center bottom;;' => esc_html__( 'Center Bottom | no-repeat', 'phox' ),
                                'repeat;center bottom;;' => esc_html__( 'Center Bottom | repeat', 'phox' ),
                                'repeat-x;center bottom;;' => esc_html__( 'Center Bottom | repeat-x', 'phox' ),

                                'no-repeat;right top;;' => esc_html__( 'Right Top | no-repeat', 'phox' ),
                                'repeat;right top;;' => esc_html__( 'Right Top | repeat', 'phox' ),
                                'no-repeat;right center;;' => esc_html__( 'Right Center | no-repeat', 'phox' ),
                                'repeat;right center;;' => esc_html__( 'Right Center | repeat', 'phox' ),
                                'no-repeat;right bottom;;' => esc_html__( 'Right Bottom | no-repeat', 'phox' ),
                                'repeat;right bottom;;' => esc_html__( 'Right Bottom | repeat', 'phox' ),
                            )
                        ),
                        array(
                            'id'              => 'footer-bg-img-size',
                            'type'            => 'select',
                            'title'           => esc_html__('Size','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'options'         =>array(
                                'auto' => esc_html__('Auto', 'phox'),
                                'contain' => esc_html__('Contain', 'phox'),
                                'cover' => esc_html__('Cover', 'phox'),
                                'cover-ultrawide'	=> esc_html__('Cover, on ultrawide screens only > 1920px', 'phox'),
                            )
                        ),
                    ),
                ),
                // CopyRights & Payments
                array(
                    'package'              => esc_html__('Copyrights & Payments', 'phox'),
					'package_id'					 => 'copyrights-payments',
                    'package-options'      =>array(
                        array(
                            'id'              => 'footer-payments',
                            'type'            => 'switch',
                            'title'           => esc_html__('Footer Payments Methods','phox'),
                            'desc'            => esc_html__('Control the visibility and concealment of payment methods', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
	                    array(
		                    'id'              => 'choose-payment',
		                    'type'            => 'multi_select',
		                    'title'           => esc_html__('Payments Methods','phox'),
		                    'desc'            => esc_html__('Multiselect is available', 'phox'),
		                    'note'            => esc_html__('', 'phox'),
		                    'class'           => 'block-function-c',
		                    'options'         => [
			                    'visa' => 'Visa',
			                    'mastercard' => 'Mastercard',
			                    'paypal' => 'Paypal',
			                    'stripe' => 'Stripe',
			                    'discover' => 'Discover',
			                    'amex' => 'Amex',
			                    'apple-pay' => 'Apple',
			                    'amazon-pay' => 'Amazon',
			                    'diners-club' => 'Diners Club',
			                    'jcb' => 'Jcb',
		                    ]
	                    ),
	                    array(
		                    'id'              => 'footer-payment-class',
		                    'type'            => 'text',
		                    'title'           => esc_html__('Payment Container class','phox'),
		                    'desc'            => esc_html__('Add custom class to payment container', 'phox'),
		                    'note'            => esc_html__('', 'phox'),
		                    'class'           => 'block-function-c',
	                    ),
	                    array(
		                    'id'              => 'footer-display',
		                    'type'            => 'switch',
		                    'title'           => esc_html__('Hide Footer','phox'),
		                    'desc'            => esc_html__('', 'phox'),
		                    'note'            => esc_html__('', 'phox'),
		                    'class'           => 'block-function-c',
	                    ),
                        array(
                            'id'              => 'footer-copyright',
                            'type'            => 'textarea',
                            'title'           => esc_html__('Footer Text Copyrights','phox'),
                            'desc'            => esc_html__('Use {{Y}} to set year & {{sitename}} to sit site name', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        )
                    )
                ),
	            // Go to Top
	            array(
		            'package'              => esc_html__('Back To Top Button', 'phox'),
		            'package_id'					 => 'backto-top-btn',
		            'package-options'      =>array(
			            array(
				            'id'              => 'backto-btn-display',
				            'type'            => 'switch',
				            'title'           => esc_html__('Display Back to top button','phox'),
				            'desc'            => esc_html__('Enable it to display Back to top button', 'phox'),
				            'note'            => esc_html__('', 'phox'),
				            'class'           => 'block-function-c',
				            'def'             => 'on'
			            ),
			            array(
				            'id'              => 'backto-btn-position',
				            'type'            => 'radio',
				            'title'           => esc_html__('Back to top button position','phox'),
				            'desc'            => esc_html__('Specifies the position of back to top button ', 'phox'),
				            'note'            => esc_html__('', 'phox'),
				            'class'           => 'block-function-c',
				            'options'         => [
					            'right'      => '<img class="layout-view-xl layout-more-height" src="'. esc_url( $this->images_icons['backtotop_layout_1'] ) .'" alt="'.esc_attr('Right', 'phox').'">',
					            'center'      => '<img class="layout-view-xl layout-more-height" src="'. esc_url( $this->images_icons['backtotop_layout_2'] ) .'" alt="'.esc_attr('Center', 'phox').'">',
					            'left'      => '<img class="layout-view-xl layout-more-height" src="'. esc_url( $this->images_icons['backtotop_layout_3'] ) .'" alt="'.esc_attr('Left', 'phox').'">'
				            ],
				            'def'             => 'right'
			            )
		            )
	            ),
            ),
        );

        // ---------------------------- SEO -----------------------------------
        $this->sections['seo'] =array(
            'title'     => '<div class="tab-title-head"><span class="fas fa-chart-pie"></span>'. esc_html__('SEO', 'phox').'</div>',
            'fields'    => array(
                // SEO FIELDS
                array(
                    'package'              => esc_html__('SEO Fields', 'phox'),
										'package_id'					 => 'seo-fields',
                    'package-options'      =>array(
                        array(
                            'id'              => 'use-seo-fields',
                            'type'            => 'switch',
                            'title'           => esc_html__('Use theme SEO fields','phox'),
                            'desc'            => esc_html__('Turn it OFF if you want to use external SEO plugin', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'meta-description',
                            'type'            => 'text',
                            'title'           => esc_html__('Meta Description','phox'),
                            'desc'            => esc_html__('These setting may be overridden for single posts & pages', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'meta-keyword',
                            'type'            => 'text',
                            'title'           => esc_html__('Meta Keywords','phox'),
                            'desc'            => esc_html__('These setting may be overridden for single posts & pages', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                    ),
                ),
            )
        );

        // ---------------------------- Blog -----------------------------------
        $this->sections['blog'] =array(
            'title'     => '<div class="tab-title-head"><span class="fas fa-file"></span>'. esc_html__('Blog', 'phox').'</div>',
            'fields'    => array(
                //Option
                array(
                    'package'              => esc_html__('Blog Options', 'phox'),
										'package_id'					 => 'blog-options',
                    'package-options'      =>array(
                        array(
                            'id'              => 'blog-page-title',
                            'type'            => 'text',
                            'title'           => esc_html__('Blog Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>  'Blog'
                        ),
						array(
							'id'              => 'blog-page-description',
							'type'            => 'textarea',
							'title'           => esc_html__('Blog Description','phox'),
							'desc'            => esc_html__('', 'phox'),
							'note'            => esc_html__('', 'phox'),
							'class'           => 'block-function-c',
							'def'             =>  ''
						),
                        array(
                            'id'              => 'blog-page-excerpt',
                            'type'            => 'text',
                            'title'           => esc_html__('Excerpt Length','phox'),
                            'desc'            => esc_html__('Number of words', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>  30
                        ),
                        array(
                            'id'              => 'blog-feature-img',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Featured Image on Blog','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c custom-block-function-option',
                        ),
                        array(
                            'id'              => 'blog-time',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Post Date','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('To hide the time of a post', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'blog-author',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Author ','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('To hide the author of a post', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                    ),
                ),
                //Pin Posts
                array(
                    'package'              => esc_html__('Pin Posts', 'phox'),
										'package_id'					 => 'blog-pin-posts',
                    'package-options'      =>array(
                        array(
                            'id'              => 'blog-pin-post-show',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Pin Posts','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c custom-block-function-option',
                        ),
                        array(
                            'id'              => 'blog-pin-post',
                            'type'            => 'radio',
                            'title'           => esc_html__('Pin Posts Layout','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c custom-block-function-option',
                            'options'         => array(
                                'left'       => '<img class="layout-style custom-layout-height" src="'. esc_url($this->images_icons['blog-pin-post_1']) .'" alt="'.esc_attr__('Left', 'phox').'"> <span class="wdes-option-span-sub">'. esc_html__('Left', 'phox') .'</span>',
                                'right'      => '<img class="layout-style custom-layout-height" src="'. esc_url($this->images_icons['blog-pin-post_2']) .'" alt="'.esc_attr__('Right', 'phox').'"> <span class="wdes-option-span-sub">'. esc_html__('Right', 'phox') .'</span>',
                            ),
                            'def'              => 'left'
                        ),
                    ),
                ),
            ),
        );

        // ---------------------------- Single Post Page -----------------------------------
        $this->sections['single'] =array(
            'title'     => '<div class="tab-title-head"><span class="fas fa-file"></span> '. esc_html__('Single Post Page', 'phox').'</div>',
            'fields'    => array(
                // Options
                array(
                    'package'              => esc_html__('General Settings', 'phox'),
										'package_id'					 => 'single-general-settings',
                    'package-options'      =>array(
                        array(
                            'id'              => 'single-post-sidebars',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Sidebar ','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('To hide the Sidebar', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'single-post-time',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Post Date','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('To hide the time', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'single-post-author',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Post Author Box ','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('To hide the author box', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'single-post-image',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Featured Image ','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('To hide the image post', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'single-post-category',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Category ','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('To hide the category', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'single-post-tag',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Tag ','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('To hide the tag', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'single-post-comments',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Comments','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('To hide Comments', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'single-socailshare',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Social Share Links','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('To Show the social share', 'phox'),
                            'class'           => 'block-function-c',
                        )
                    ),
                ),
            ),
        );
        // ---------------------------- 404 Page ---------------------------------------
        $this->sections['page404'] =array(
            'title'     => '<div class="tab-title-head"><span class="fas fa-bug"></span>'. esc_html__('404 Page', 'phox').'</div>',
            'fields'    => array(
                // Layout
                array(
                    'package'              => esc_html__('Layout', 'phox'),
										'package_id'					 => '404-page-layout',
                    'package-options'      =>array(
                        array(
                            'id'              => 'page404-header',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Header','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'page404-footer',
                            'type'            => 'switch',
                            'title'           => esc_html__('Hide Footer','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'page404-background',
                            'type'            => 'color',
                            'title'           => esc_html__('Background Color','phox'),
                            'desc'            => esc_html__('Choose Background color, Then Notice: that you should change entry color', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'page404-background-img',
                            'type'            => 'upload',
                            'title'           => esc_html__('Background Image','phox'),
                            'desc'            => esc_html__('Choose Background image, Then Notice: that you should change entry color', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'page404-main',
                            'type'            => 'color',
                            'title'           => esc_html__('Main Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'

                        ),
                        array(
                            'id'              => 'page404-desc',
                            'type'            => 'color',
                            'title'           => esc_html__('Description Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#7e7e7e'

                        )
                    ),
                ),
                // Options
                array(
                    'package'              => esc_html__('404 Page Options', 'phox'),
										'package_id'					 => '404-page-options',
                    'package-options'      =>array(
                        array(
                            'id'              => 'page404-main-text',
                            'type'            => 'text',
                            'title'           => esc_html__('Main Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>  '404'
                        ),
                        array(
                            'id'              => 'page404-desc-text',
                            'type'            => 'text',
                            'title'           => esc_html__('Description Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => 'OOPS! PAGE NOT FOUND'
                        ),
                    ),
                ),
            )
        );

        // ---------------------------- Social Media -----------------------------------
        $this->sections['social-media'] =array(
            'title'     => '<div class="tab-title-head"><span class="fas fa-share-alt"></span>'. esc_html__('Social Media', 'phox').'</div>',
            'fields'    => array(
                // Social Accounts
                array(
                    'package'              => esc_html__('Social Networking Accounts', 'phox'),
										'package_id'					 => 'social-networking-accounts',
                    'package-options'      =>array(
                        array(
                            'id'              => 'social-media-facebook',
                            'type'            => 'text',
                            'title'           => esc_html__('Facebook','phox'),
                            'desc'            => esc_html__('Link to the Company profile page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-twitter',
                            'type'            => 'text',
                            'title'           => esc_html__('Twitter','phox'),
                            'desc'            => esc_html__('Link to the Company profile page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-google+',
                            'type'            => 'text',
                            'title'           => esc_html__('Google +','phox'),
                            'desc'            => esc_html__('Link to the Company profile page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-skype',
                            'type'            => 'text',
                            'title'           => esc_html__('Skype','phox'),
                            'desc'            => esc_html__('Skype login. You can use callto: or skype: prefix', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-youtube',
                            'type'            => 'text',
                            'title'           => esc_html__('Youtube','phox'),
                            'desc'            => esc_html__('Link to the Company Channel page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-vimeo',
                            'type'            => 'text',
                            'title'           => esc_html__('Vimeo','phox'),
                            'desc'            => esc_html__('Link to the Company Channel page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-instagram',
                            'type'            => 'text',
                            'title'           => esc_html__('Instagram','phox'),
                            'desc'            => esc_html__('Link to the Company profile page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-pinterest',
                            'type'            => 'text',
                            'title'           => esc_html__('Pinterest','phox'),
                            'desc'            => esc_html__('Link to the Company profile page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-dribbble',
                            'type'            => 'text',
                            'title'           => esc_html__('Dribbble','phox'),
                            'desc'            => esc_html__('Link to the Company profile page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-behance',
                            'type'            => 'text',
                            'title'           => esc_html__('Behance','phox'),
                            'desc'            => esc_html__('Link to the Company profile page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-flickr',
                            'type'            => 'text',
                            'title'           => esc_html__('Flickr','phox'),
                            'desc'            => esc_html__('Link to the Company profile page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-tumblr',
                            'type'            => 'text',
                            'title'           => esc_html__('Tumblr','phox'),
                            'desc'            => esc_html__('Link to the Company profile page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                        array(
                            'id'              => 'social-media-linkedin',
                            'type'            => 'text',
                            'title'           => esc_html__('LinkedIn','phox'),
                            'desc'            => esc_html__('Link to the Company profile page', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                    ),
                ),
            )
        );

        // ---------------------------- Elements Color -----------------------------------
        $this->sections['color'] =array(
            'title'     =>'<div class="tab-title-head"><span class="fas fa-paint-brush"></span>'. esc_html__('Elements Color', 'phox').'</div>',
            'fields'    => array(
                // Top Header
                array(
                    'package'              => esc_html__('Top Header', 'phox'),
					'package_id'					 => 'top-header',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-header-sub-bg',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#112835',
                                'color-two'     => '#112835'
                            )
                        ),
                        array(
                            'id'              => 'color-header-top-bar-border-b',
                            'type'            => 'color',
                            'title'           => esc_html__('Bottom Border Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#112835'
                        ),
                        array(
                            'id'              => 'color-header-top-header-link-clr',
                            'type'            => 'color',
                            'title'           => esc_html__('Menu | Link Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-header-top-header-link-clr-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Menu | Hover Link Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-header-top-bar-r-clr',
                            'type'            => 'color',
                            'title'           => esc_html__('Social Media | Icon Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-header-top-bar-r-clr-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Social Media | Icon Hover Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#FAE474'
                        ),
                        array(
                            'id'              => 'color-header-top-bar-shortcuts-icon-clr',
                            'type'            => 'color',
                            'title'           => esc_html__('Shortcuts | Icon Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-header-top-bar-shortcuts-icon-clr-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Shortcuts | Icon Hover Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-header-bg-shortcut-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Shortcuts | Left Border','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#20394a'
                        ),
                    ),
                ),
                // Header
                array(
                    'package'              => esc_html__('Header', 'phox'),
					'package_id'					 => 'header-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-header-bg',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Background','phox'),
                            'desc'            => esc_html__('Choose Left and Right Background Gradient Colors', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#122d3e',
                                'color-two'     => '#274961'
                            )

                        ),
                        array(
                            'id'              => 'color-header-bg-border-bottom',
                            'type'            => 'color',
                            'title'           => esc_html__('Bottom Border Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#182f3c'
                        ),
                        array(
                            'id'              => 'color-header-sticky-background',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Sticky Header | Background Color','phox'),
                            'desc'            => esc_html__('Choose Left and Right Background Gradient Colors', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#122d3e',
                                'color-two'     => '#274961'
                            )

                        ),
                        array(
                            'id'              => 'color-header-sticky-border-bottom',
                            'type'            => 'color',
                            'title'           => esc_html__('Sticky Header | Bottom Border Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#182f3c'
                        ),
                    ),
                ),
                // Menu
                array(
                    'package'              => esc_html__('Menu', 'phox'),
										'package_id'					 => 'menu-colors',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-header-menu-clr',
                            'type'            => 'color',
                            'title'           => esc_html__('Link Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ced4d9'
                        ),
                        array(
                            'id'              => 'color-header-menu-clr-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Hover Link Color ','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-header-menu-clr-active',
                            'type'            => 'color',
                            'title'           => esc_html__('Active Link Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                    )
                ),
                // SubMenu
                array(
                    'package'              => esc_html__('SubMenu', 'phox'),
										'package_id'					 => 'submenu-colors',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-header-sub-menu-clr',
                            'type'            => 'color',
                            'title'           => esc_html__('Link Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#7f7f7f'
                        ),
                        array(
                            'id'              => 'color-header-sub-menu-clr-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Hover Link Color ','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#333333'
                        ),
                        array(
                            'id'              => 'color-header-sub-menu-clr-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Border Color ','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#eeeeee'
                        ),
                    )
                ),
                // Megamenu
                array(
                    'package'              => esc_html__('Mega Menu', 'phox'),
										'package_id'					 => 'mega-menu-colors',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-header-menu-mega-heading',
                            'type'            => 'color',
                            'title'           => esc_html__('Heading Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#423f4f'
                        ),
                        array(
                            'id'              => 'color-header-menu-mega-heading-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Heading Color Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#f36e46'
                        ),
                        array(
                            'id'              => 'color-header-menu-mega-heading-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Heading Color Bottom Border','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#eeeeee'
                        ),
                    )
                ),
                // Responsive Menu
                array(
                    'package'              => esc_html__('Responsive Menu', 'phox'),
										'package_id'					 => 'responsive-menu-colors',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-header-menu-switch-btn',
                            'type'            => 'color',
                            'title'           => esc_html__('Switch Button','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-header-menu-switch-btn-inside',
                            'type'            => 'color',
                            'title'           => esc_html__('Switch Button Inside Overlay Menu','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-header-dropdown-menu',
                            'type'            => 'color',
                            'title'           => esc_html__('Overlay Menu Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#191919'
                        ),
                        array(
                            'id'              => 'color-header-overlay-menu-link',
                            'type'            => 'color',
                            'title'           => esc_html__('Overlay Menu Link','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-header-overlay-menu-link-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Overlay Menu Hover Link','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#f36e46'
                        ),
                        array(
                            'id'              => 'color-header-overlay-menu-link-active',
                            'type'            => 'color',
                            'title'           => esc_html__('Overlay Menu Active Link','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#f36e46'
                        ),
                        array(
                            'id'              => 'color-header-overlay-menu-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Border Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#232323'
                        ),
                        array(
                            'id'              => 'color-header-overlay-sub-menu-link',
                            'type'            => 'color',
                            'title'           => esc_html__('Sub Menu Link Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-header-overlay-sub-menu-link-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Sub Menu Hover Link Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                    )
                ),
                // Headings
                array(
                    'package'              => esc_html__('Headings', 'phox'),
										'package_id'					 => 'headings-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-heading-h1',
                            'type'            => 'color',
                            'title'           => esc_html__('Heading H1 color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-heading-h2',
                            'type'            => 'color',
                            'title'           => esc_html__('Heading H2 color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-heading-h3',
                            'type'            => 'color',
                            'title'           => esc_html__('Heading H3 color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-heading-h4',
                            'type'            => 'color',
                            'title'           => esc_html__('Heading H4 color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-heading-h5',
                            'type'            => 'color',
                            'title'           => esc_html__('Heading H5 color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-heading-h6',
                            'type'            => 'color',
                            'title'           => esc_html__('Heading H6 color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                    )
                ),
                // Shortcodes
                array(
                    'package'              => esc_html__('Shortcodes', 'phox'),
										'package_id'					 => 'shortcodes-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-shortcodes-loading-bg',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Loading Screen Background','phox'),
                            'desc'            => esc_html__('Choose Left and Right Background Gradient Colors', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#122d3e',
                                'color-two'     => '#204056'
                            ),
                        ),
                        array(
                            'id'              => 'color-shortcodes-best-f-bg',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Best Features Section Gradient','phox'),
                            'desc'            => esc_html__('Choose Left and Right Background Gradient Colors', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#C51E3A',
                                'color-two'     => '#FAE474'
                            ),
                        ),
                        array(
                            'id'              => 'color-shortcodes-best-f-bg-r',
                            'type'            => 'color',
                            'title'           => esc_html__('Best Features Section Text Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#f8f8f8'
                        ),
                        array(
                            'id'              => 'color-shortcodes-info-box',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Info Box Title Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#C51E3A',
                                'color-two'     => '#FAE474'
                            ),
                        ),
                        array(
                            'id'              => 'color-shortcodes-text-general',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('General Elements Text Gradient','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#C51E3A',
                                'color-two'     => '#FAE474'
                            ),
                        ),
                        array(
                            'id'              => 'color-shortcodes-bg-general',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('General Elements Background Gradient','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#C51E3A',
                                'color-two'     => '#FAE474'
                            ),
                        ),
                        array(
                            'id'              => 'color-shortcodes-faq_bg',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('FAQ Head Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#C51E3A',
                                'color-two'     => '#FAE474'
                            ),
                        ),
                    ),
                ),
                // Contact Form7
                array(
                    'package'              => esc_html__('Contact Form7', 'phox'),
										'package_id'					 => 'contact-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-shortcode-cf7-input-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Contact Form7 Inputs Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#122d3e'
                        ),
                        array(
                            'id'              => 'color-shortcode-cf7-send-btn-bg',
                            'type'            => 'color',
                            'title'           => esc_html__('Contact Form7 Send Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-shortcode-cf7-send-btn-bg-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Contact Form7 Send Button Background Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#C51E3A'
                        ),
                    )
                ),
                // WHMCS Bridge
                array(
                    'package'              => esc_html__('WHMCS Bridge', 'phox'),
                    'package_id'					 => 'bridge-color',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-bridge-bg',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Header Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#122d3e',
                                'color-two'     => '#274961'
                            ),
                        ),
                        array(
                            'id'              => 'color-bridge-search',
                            'type'            => 'color',
                            'title'           => esc_html__('Search Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#cc2241'
                        ),
                        array(
                            'id'              => 'color-bridge-transfer',
                            'type'            => 'color',
                            'title'           => esc_html__('Transfer Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#2b516b'
                        ),
                        array(
                            'id'              => 'color-bridge-home-bg',
                            'type'            => 'color',
                            'title'           => esc_html__('Home Shortcuts Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e39'
                        ),
                        array(
                            'id'              => 'color-bridge-home-sc-c',
                            'type'            => 'color',
                            'title'           => esc_html__('Home Shortcuts Border Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#b62c46'
                        ),
                        array(
                            'id'              => 'color-bridge-twitter-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Tweets Border Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e39'
                        ),
                        array(
                            'id'              => 'color-bridge-sidebar-head',
                            'type'            => 'color',
                            'title'           => esc_html__('Sidebar Head Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#2c526b'
                        ),
                        array(
                            'id'              => 'color-bridge-sidebar-active-tab',
                            'type'            => 'color',
                            'title'           => esc_html__('Sidebar Active Tab Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#C51E39'
                        ),
                        array(
                            'id'              => 'color-bridge-btn-bg',
                            'type'            => 'color',
                            'title'           => esc_html__('Buttons Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#C51E39'
                        ),
                    )
                ),
                // WooCommerce
                array(
                    'package'              => esc_html__('WooCommerce', 'phox'),
										'package_id'					 => 'woocommerce',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-woocommerce-btn-count',
                            'type'            => 'color',
                            'title'           => esc_html__('Cart Button Count Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e39'
                        ),
                        array(
                            'id'              => 'color-woocommerce-btn-view-cart',
                            'type'            => 'color',
                            'title'           => esc_html__('View Cart Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#244960'
                        ),
                        array(
                            'id'              => 'color-woocommerce-btn-view-cart-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('View Cart Hover Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e39'
                        ),
                        array(
                            'id'              => 'color-woocommerce-btn-checkout',
                            'type'            => 'color',
                            'title'           => esc_html__('Checkout Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e39'
                        ),
                        array(
                            'id'              => 'color-woocommerce-btn-checkout-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Checkout Hover Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#244960'
                        ),
                        array(
                            'id'              => 'color-woocommerce-bread_bg',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Breadcrumb Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#122d3e',
                                'color-two'     => '#274961'
                            ),
                        ),
                        array(
                            'id'              => 'color-woocommerce-product-h-brd',
                            'type'            => 'color',
                            'title'           => esc_html__('Product Border Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e39'
                        ),
                        array(
                            'id'              => 'color-woocommerce-product-btn',
                            'type'            => 'color',
                            'title'           => esc_html__('Product Cart Button','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#cc2241'
                        ),
                        array(
                            'id'              => 'color-woocommerce-product-sale',
                            'type'            => 'color',
                            'title'           => esc_html__('Product Sale Button','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e39'
                        ),
                        array(
                            'id'              => 'color-woocommerce-product-title',
                            'type'            => 'color',
                            'title'           => esc_html__('Product Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-woocommerce-widget-title',
                            'type'            => 'color',
                            'title'           => esc_html__('Widgets Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-woocommerce-widget-cart',
                            'type'            => 'color',
                            'title'           => esc_html__('Widgets Cart Button','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#244960'
                        ),
                        array(
                            'id'              => 'color-woocommerce-widget-cart-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Widgets Cart Button Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#cc2241'
                        ),
                        array(
                            'id'              => 'color-woocommerce-widget-filter',
                            'type'            => 'color',
                            'title'           => esc_html__('Widgets Filter Range Bar','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#d2274a'
                        ),
                        array(
                            'id'              => 'color-woocommerce-product-page-title',
                            'type'            => 'color',
                            'title'           => esc_html__('Product Page Titles','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-woocommerce-product-page-feature',
                            'type'            => 'color',
                            'title'           => esc_html__('Product Page Features','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e39'
                        ),
                        array(
                            'id'              => 'color-woocommerce-product-page-btn',
                            'type'            => 'color',
                            'title'           => esc_html__('Product Page Cart Button','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e39'
                        ),
                        array(
                            'id'              => 'color-woocommerce-product-tabs',
                            'type'            => 'color',
                            'title'           => esc_html__('Product Page Tabs Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e39'
                        ),
                        array(
                            'id'              => 'color-woocommerce-product-order-bg',
                            'type'            => 'color',
                            'title'           => esc_html__('Checkout Order Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#244960'
                        ),
                        array(
                            'id'              => 'color-woocommerce-search-bg',
                            'type'            => 'color',
                            'title'           => esc_html__('Search Widget Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-woocommerce-profile-link',
                            'type'            => 'color',
                            'title'           => esc_html__('Profile Links Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e3a'
                        ),
                        array(
                            'id'              => 'color-woocommerce-pagination-bg',
                            'type'            => 'color',
                            'title'           => esc_html__('Pagination Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#f3f5f9'
                        ),
                        array(
                            'id'              => 'color-woocommerce-pagination-color',
                            'type'            => 'color',
                            'title'           => esc_html__('Pagination Button Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-woocommerce-pagination-bg-active',
                            'type'            => 'color',
                            'title'           => esc_html__('Pagination Active Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-woocommerce-pagination-color-active',
                            'type'            => 'color',
                            'title'           => esc_html__('Pagination Active Button Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                    )
                ),
                // Blog
                array(
                    'package'              => esc_html__('Blog', 'phox'),
										'package_id'					 => 'blog-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-blog-breadcrumb',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Blog Breadcrumb','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#122d3e',
                                'color-two'     => '#274961'
                            ),
                        ),
                        array(
                            'id'              => 'color-blog-blog-breadcrumb-title',
                            'type'            => 'color',
                            'title'           => esc_html__('Blog Breadcrumb Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-blog-blog-breadcrumb-description',
                            'type'            => 'color',
                            'title'           => esc_html__('Blog Breadcrumb Description','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-blog-blog-breadcrumb-link',
                            'type'            => 'color',
                            'title'           => esc_html__('Blog Breadcrumb Link','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-blog-blog-breadcrumb-link-active',
                            'type'            => 'color',
                            'title'           => esc_html__('Blog Breadcrumb Active Link','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-blog-blog-breadcrumb-link-separator',
                            'type'            => 'color',
                            'title'           => esc_html__('Blog Breadcrumb Link Separator','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-blog-post-title',
                            'type'            => 'color',
                            'title'           => esc_html__('Post Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-post-title-hover',
                            'type'            => 'color',
                            'title'           => esc_html__('Post Title Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-post-more-btn',
                            'type'            => 'color',
                            'title'           => esc_html__('Post Read More Button Text','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-post-more-btn-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Post Read More Button Border','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-post-more-btn-bg-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Post Read More Button Background Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-post-more-btn-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Post Read More Button Text Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-blog-post-more-btn-border-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Post Read More Button Border Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-widget-title',
                            'type'            => 'color',
                            'title'           => esc_html__('Sidebar Widgets Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-widget-link',
                            'type'            => 'color',
                            'title'           => esc_html__('Sidebar Widgets Link Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#C51E3A'
                        ),
                        array(
                            'id'              => 'color-blog-search-widget-send',
                            'type'            => 'color',
                            'title'           => esc_html__('Search Widget Search Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-widget-posts-title-clr',
                            'type'            => 'color',
                            'title'           => esc_html__('Recent Posts Widget Post Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-widget-posts-title-clr-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Recent Posts Widget Post Title Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#C51E3A'
                        ),
                        array(
                            'id'              => 'color-blog-post-heading-main',
                            'type'            => 'color',
                            'title'           => esc_html__('Single Headings','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-tags-widget-tag',
                            'type'            => 'color',
                            'title'           => esc_html__('Single Tags Background Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#C51E3A'
                        ),
                        array(
                            'id'              => 'color-blog-related-posts-title',
                            'type'            => 'color',
                            'title'           => esc_html__('Related Posts Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-related-posts-title-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Related Posts Title Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e3a'
                        ),
                        array(
                            'id'              => 'color-blog-tags-widget-tag-item',
                            'type'            => 'color',
                            'title'           => esc_html__('Tags Widget Background Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#C51E3A'
                        ),
                        array(
                            'id'              => 'color-blog-comments-title',
                            'type'            => 'color',
                            'title'           => esc_html__('Comments Area Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-comment-reply-link',
                            'type'            => 'color',
                            'title'           => esc_html__('Comments Reply Link','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-comment-reply-link-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Comments Reply Link Border','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-comment-user',
                            'type'            => 'color',
                            'title'           => esc_html__('Comments User','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-reply-title',
                            'type'            => 'color',
                            'title'           => esc_html__('Add Reply Title','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-reply-logged',
                            'type'            => 'color',
                            'title'           => esc_html__('Add Reply Logged in user','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#234961'
                        ),
                        array(
                            'id'              => 'color-blog-comment-input-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Add Comment Inputs Border Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-comment-input-send',
                            'type'            => 'color',
                            'title'           => esc_html__('Add Comment Send Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#204056'
                        ),
                        array(
                            'id'              => 'color-blog-comment-input-send-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Add Comment Send Button Hover Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#c51e3a'
                        ),
                    )
                ),
                // Footer
                array(
                    'package'              => esc_html__('Footer', 'phox'),
										'package_id'					 => 'footer-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-footer-bg',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Footer Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#122d3e',
                                'color-two'     => '#274961'
                            )
                        ),
                    ),
                ),
                // Footer General Widgets
                array(
                    'package'              => esc_html__('Footer | General Widgets', 'phox'),
										'package_id'					 => 'footer-genral-widgets',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-footer-heading',
                            'type'            => 'color',
                            'title'           => esc_html__('Heading color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-footer-heading-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Heading Bottom Border color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#d8dde6'
                        ),
                        array(
                            'id'              => 'color-footer-link-clr',
                            'type'            => 'color',
                            'title'           => esc_html__('Link Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-footer-link-clr-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Link Hover Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#FAE474'
                        ),
                        array(
                            'id'              => 'color-footer-text-clr',
                            'type'            => 'color',
                            'title'           => esc_html__('Text Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-blog-footer-widget-input',
                            'type'            => 'color',
                            'title'           => esc_html__('Inputs Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#132a39'
                        ),
                        array(
                            'id'              => 'color-blog-footer-widget-input-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Inputs Border Colors','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#082333'
                        ),
                    )
                ),
                // Footer About Us Widget
                array(
                    'package'              => esc_html__('Footer | About Us Widget', 'phox'),
										'package_id'					 => 'footer-aboutus-widgets',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-footer-aboutus-widget',
                            'type'            => 'color',
                            'title'           => esc_html__('Text Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-footer-aboutus-widget-icons',
                            'type'            => 'color',
                            'title'           => esc_html__('Icons Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                    )
                ),
                // Footer Calendar Widget
                array(
                    'package'              => esc_html__('Footer | Calendar Widget', 'phox'),
										'package_id'					 => 'footer-calendar-widgets',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-blog-footer-widget-calendar-head',
                            'type'            => 'color',
                            'title'           => esc_html__('Head Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#132a39'
                        ),
                        array(
                            'id'              => 'color-blog-footer-widget-calendar-body',
                            'type'            => 'color',
                            'title'           => esc_html__('Body Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#132a39'
                        ),
                        array(
                            'id'              => 'color-blog-footer-widget-calendar-body-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Border Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#082333'
                        ),
                        array(
                            'id'              => 'color-blog-footer-widget-calendar-body-text',
                            'type'            => 'color',
                            'title'           => esc_html__('Text Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-blog-footer-widget-calendar-highlighted-bg',
                            'type'            => 'color',
                            'title'           => esc_html__('Highlighted Day Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#C51E3A'
                        ),
                        array(
                            'id'              => 'color-blog-footer-widget-calendar-highlighted-text',
                            'type'            => 'color',
                            'title'           => esc_html__('Highlighted Day Text Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                    )
                ),
                // Footer Search Widget
                array(
                    'package'              => esc_html__('Footer | Search Widget', 'phox'),
										'package_id'					 => 'footer-search-widgets',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-blog-footer-widget-search-btn',
                            'type'            => 'color',
                            'title'           => esc_html__('Search Input Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#132a39'
                        ),
                        array(
                            'id'              => 'color-blog-footer-widget-search-btn-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Search Input Border','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#082333'
                        ),
                        array(
                            'id'              => 'color-footer-search-bg-btn',
                            'type'            => 'color',
                            'title'           => esc_html__('Search Button Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#132a39'
                        ),
                        array(
                            'id'              => 'color-footer-search-clr-btn',
                            'type'            => 'color',
                            'title'           => esc_html__('Search Button Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                    )
                ),
                // Footer Newsletter Widget
                array(
                    'package'              => esc_html__('Footer | Newsletter Widget', 'phox'),
										'package_id'					 => 'footer-newsletter-widgets',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-footer-new-letter-inp',
                            'type'            => 'color',
                            'title'           => esc_html__('Text Input Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#132a39'
                        ),
                        array(
                            'id'              => 'color-footer-new-letter-inp-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Text Input Border','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#132a39'
                        ),
                        array(
                            'id'              => 'color-footer-new-letter-sub',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Submit Input Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#FAE474',
                                'color-two'     => '#C51E3A'
                            )
                        ),
                    )
                ),
                // Footer Tags Widget
                array(
                    'package'              => esc_html__('Footer | Tags Widget', 'phox'),
										'package_id'					 => 'footer-tags-widgets',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-blog-footer-widget-tags',
                            'type'            => 'color',
                            'title'           => esc_html__('Tag Background','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#132a39'
                        ),
                        array(
                            'id'              => 'color-tags-text',
                            'type'            => 'color',
                            'title'           => esc_html__('Tag Text Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-blog-footer-widget-tags-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Tag Background Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#C51E3A'
                        ),
                        array(
                            'id'              => 'color-tags-text-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Tag Text Color Hover','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                    )
                ),
                // Footer Payment Methods
                array(
                    'package'              => esc_html__('Footer | Payment Methods', 'phox'),
										'package_id'					 => 'footer-payment-methods',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-footer-copyright-border-top',
                            'type'            => 'color',
                            'title'           => esc_html__('Payment Bar Top Border','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#0f2836'
                        ),
                        array(
                            'id'              => 'color-footer-copyright-border-bottom',
                            'type'            => 'color',
                            'title'           => esc_html__('Payment Bar Bottom Border','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#0f2836'
                        ),
                        array(
                            'id'              => 'color-footer-payment-icon-color',
                            'type'            => 'color',
                            'title'           => esc_html__('Icon Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-footer-payment-icon-color-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Icon Hover Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                    )
                ),
                // Footer Copyrights Bar
                array(
                    'package'              => esc_html__('Footer | Copyrights Bar', 'phox'),
										'package_id'					 => 'footer-copyrights-bar',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-footer-copyright-top-border',
                            'type'            => 'color_gradient',
                            'title'           => esc_html__('Background Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             =>array(
                                'color-one'     => '#112835',
                                'color-two'     => '#112835'
                            )
                        ),
                        array(
                            'id'              => 'color-footer-copyright-text',
                            'type'            => 'color',
                            'title'           => esc_html__('Text Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-footer-copyright-link-clr',
                            'type'            => 'color',
                            'title'           => esc_html__('Link Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                        array(
                            'id'              => 'color-footer-copyright-link-clr-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Link Hover Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                    )
                ),
                // Footer Back To Top Button
                array(
                    'package'              => esc_html__('Footer | Back To Top Button', 'phox'),
                                        'package_id'					 => 'footer-backtotop-btn',
                    'package-options'      =>array(
                        array(
                            'id'              => 'color-footer-backtotop-bg',
                            'type'            => 'color',
                            'title'           => esc_html__('Button Background Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#274961'
                        ),
                        array(
                            'id'              => 'color-footer-backtotop-bg-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Button Hover Background Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#CC2241'
                        ),
                        array(
                            'id'              => 'color-footer-backtotop-border',
                            'type'            => 'color',
                            'title'           => esc_html__('Button Border Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#274961'
                        ),
                        array(
                            'id'              => 'color-footer-backtotop-border-hvr',
                            'type'            => 'color',
                            'title'           => esc_html__('Button Hover Border Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#CC2241'
                        ),
                        array(
                            'id'              => 'color-footer-backtotop-icon-clr',
                            'type'            => 'color',
                            'title'           => esc_html__('Button Icon Color','phox'),
                            'desc'            => esc_html__('', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => '#ffffff'
                        ),
                    )
                ),
            )
        );

        // ---------------------------- Typography -----------------------------------
        $this->sections['gen-typo'] =array(
            'title'     => '<div class="tab-title-head"><span class="fas fa-text-width"></span> '.esc_html__('Typography', 'phox').'</div>',
            'fields'    => array(
                // Menu Typography
                array(
                    'package'              => esc_html__('Menu Typography', 'phox'),
										'package_id'					 => 'menu-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'typography-main-menu',
                            'type'            => 'typography',
                            'title'           => esc_html__('Link Typography','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Menus.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '14',
                                'fonts'       => 'Montserrat',
                                'weight-font' => '600'
                            ),
                        ),
                    ),
                ),
                // SubMenu Typography
                array(
                    'package'              => esc_html__('SubMenu Typography', 'phox'),
										'package_id'					 => 'submenu-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'typography-sub-menu',
                            'type'            => 'typography',
                            'title'           => esc_html__('Link Typography','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Menus.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '13',
                                'fonts'       => 'Montserrat',
                                'weight-font' => '400'
                            ),
                        ),
                    ),
                ),
                // Mega Menu Typography
                array(
                    'package'              => esc_html__('Mega Menu Typography', 'phox'),
										'package_id'					 => 'mega-menu-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'typography-mega-menu',
                            'type'            => 'typography',
                            'title'           => esc_html__('Heading Typography','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Menus.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '16',
                                'fonts'       => 'Montserrat',
                                'weight-font' => '600'
                            ),
                        ),
                    ),
                ),
                // Blog Typography
                array(
                    'package'              => esc_html__('Blog Typography', 'phox'),
										'package_id'					 => 'blog-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'typo_blog_bread_title',
                            'type'            => 'typography',
                            'title'           => esc_html__('Breadcrumb Title Typography In Blog Page','phox'),
                            'desc'            => esc_html__('These settings control the typography for Breadcrumb Title Typography.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '50',
                                'fonts'       => 'Poppins',
                                'weight-font' => '600'
                            ),
                        ),
                        array(
                            'id'              => 'typography-blog-t',
                            'type'            => 'typography',
                            'title'           => esc_html__('Post Title Typography In Blog Page','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Post Title.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '18',
                                'fonts'       => 'Poppins',
                                'weight-font' => '700'
                            ),
                        ),
                        array(
                            'id'              => 'typography-blog-t-s',
                            'type'            => 'typography',
                            'title'           => esc_html__('Post Title Typography In Single Page','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Post Title.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '36',
                                'fonts'       => 'Poppins',
                                'weight-font' => '600'
                            ),
                        ),
                    ),
                ),
                // Body Typography
                array(
                    'package'              => esc_html__('Body Typography', 'phox'),
										'package_id'					 => 'body-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'typography-body-p',
                            'type'            => 'typography',
                            'title'           => esc_html__('Paragraph Typography','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Paragraph.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '14',
                                'fonts'       => 'Roboto',
                                'weight-font' => '400'
                            ),
                        ),
                        array(
                            'id'              => 'typography-body-blockquote',
                            'type'            => 'typography',
                            'title'           => esc_html__('Blockquote Typography','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Blockquotes.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '16',
                                'fonts'       => 'Roboto',
                                'weight-font' => '600'
                            ),
                        ),
                        array(
                            'id'              => 'typography-body-links',
                            'type'            => 'typography',
                            'title'           => esc_html__('Links Typography','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Links.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '17',
                                'fonts'       => 'Roboto',
                                'weight-font' => '400'
                            ),
                        ),
                    ),
                ),
                 // Headings Typography
                 array(
                    'package'              => esc_html__('Headings Typography', 'phox'),
										'package_id'					 => 'headings-typography',
                    'package-options'      =>array(
                        array(
                            'id'              => 'heading-h1',
                            'type'            => 'typography',
                            'title'           => esc_html__('H1','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Headings.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '32',
                                'fonts'       => 'Poppins',
                                'weight-font' => '700'
                            ),
                        ),
                        array(
                            'id'              => 'heading-h2',
                            'type'            => 'typography',
                            'title'           => esc_html__('H2','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Headings.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '24',
                                'fonts'       => 'Poppins',
                                'weight-font' => '700'
                            ),
                        ),
                        array(
                            'id'              => 'heading-h3',
                            'type'            => 'typography',
                            'title'           => esc_html__('H3','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Headings.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '20.8',
                                'fonts'       => 'Poppins',
                                'weight-font' => '700'
                            ),
                        ),
                        array(
                            'id'              => 'heading-h4',
                            'type'            => 'typography',
                            'title'           => esc_html__('H4','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Headings.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '16',
                                'fonts'       => 'Poppins',
                                'weight-font' => '700'
                            ),
                        ),
                        array(
                            'id'              => 'heading-h5',
                            'type'            => 'typography',
                            'title'           => esc_html__('H5','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Headings.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '12.8',
                                'fonts'       => 'Poppins',
                                'weight-font' => '700'
                            ),
                        ),
                        array(
                            'id'              => 'heading-h6',
                            'type'            => 'typography',
                            'title'           => esc_html__('H6','phox'),
                            'desc'            => esc_html__('These settings control the typography for all Headings.', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                            'def'             => array(
                                'size'        => '11.2',
                                'fonts'       => 'Poppins',
                                'weight-font' => '700'
                            ),
                        ),
                    ),
                ),
            )
        );

	    // ---------------------------- Custom Fonts -----------------------------------
	    $this->sections['custom-fonts'] = [
		    'title'     => '<div class="tab-title-head"><span class="fas fa-text-width"></span> '.esc_html__('Custom Fonts', 'phox').'</div>',
		    'fields'    => [
			    [
				    'package'              => esc_html__('Fonts 1', 'phox'),
				    'package_id'					 => 'custom-fonts-one',
				    'package-options' => [
					    [
						    'id'              => 'font-custom',
						    'type'            => 'text',
						    'title'           => esc_html__('Name','phox'),
						    'desc'            => esc_html__('Please use only letters or spaces, eg. Custom Font 1', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
					    ],
					    [
						    'id'              => 'font-custom-woff2',
						    'type'            => 'upload',
						    'title'           => esc_html__('WOFF2','phox'),
						    'desc'            => esc_html__('recommended', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'media'           => 'font'
					    ],
					    [
						    'id'              => 'font-custom-woff',
						    'type'            => 'upload',
						    'title'           => esc_html__('WOFF','phox'),
						    'desc'            => esc_html__('optional', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'media'           => 'font'
					    ],
					    [
						    'id'              => 'font-custom-ttf',
						    'type'            => 'upload',
						    'title'           => esc_html__('TTF','phox'),
						    'desc'            => esc_html__('optional', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'media'           => 'font'
					    ],
					    [
						    'id'              => 'font-custom-otf',
						    'type'            => 'upload',
						    'title'           => esc_html__('OTF','phox'),
						    'desc'            => esc_html__('optional', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'media'           => 'font'
					    ],
					    [
						    'id'              => 'font-custom-svg',
						    'type'            => 'upload',
						    'title'           => esc_html__('SVG','phox'),
						    'desc'            => esc_html__('optional', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'media'           => 'font'
					    ]
				    ]
			    ],
			    [
				    'package'              => esc_html__('Fonts 2', 'phox'),
				    'package_id'					 => 'custom-fonts-two',
				    'package-options' => [
					    [
						    'id'              => 'font-custom-2',
						    'type'            => 'text',
						    'title'           => esc_html__('Name','phox'),
						    'desc'            => esc_html__('Please use only letters or spaces, eg. Custom Font 2', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
					    ],
					    [
						    'id'              => 'font-custom-2-woff2',
						    'type'            => 'upload',
						    'title'           => esc_html__('WOFF2','phox'),
						    'desc'            => esc_html__('recommended', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'media'           => 'font'
					    ],
					    [
						    'id'              => 'font-custom-2-woff',
						    'type'            => 'upload',
						    'title'           => esc_html__('WOFF','phox'),
						    'desc'            => esc_html__('optional', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'media'           => 'font'
					    ],
					    [
						    'id'              => 'font-custom-2-ttf',
						    'type'            => 'upload',
						    'title'           => esc_html__('TTF','phox'),
						    'desc'            => esc_html__('optional', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'media'           => 'font'
					    ],
					    [
						    'id'              => 'font-custom-2-otf',
						    'type'            => 'upload',
						    'title'           => esc_html__('OTF','phox'),
						    'desc'            => esc_html__('optional', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'media'           => 'font'
					    ],
					    [
						    'id'              => 'font-custom-2-svg',
						    'type'            => 'upload',
						    'title'           => esc_html__('SVG','phox'),
						    'desc'            => esc_html__('optional', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'media'           => 'font'
					    ]
				    ]
			    ]
		    ],
	    ];

	    // ---------------------------- Elementor Widgets -----------------------------------
	    $this->sections['elementor-widgets'] =array(
		    'title'     => '<div class="tab-title-head"><span class="fab fa-elementor"></span>'. esc_html__('Elementor Widgets', 'phox').'</div>',
		    'fields'    => array(
			    // GoDaddy API
			    array(
				    'package'              => esc_html__('Domain Search', 'phox'),
				    'package_id'					 => 'domain_search',
				    'package-options'      =>array(
					    array(
						    'id'              => 'godaddy_api_key',
						    'type'            => 'text',
						    'title'           => esc_html__('GoDaddy Api Key','phox'),
						    'desc'            => esc_html__('Put the GoDaddy api key', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'def'              => ''
					    ),
					    array(
						    'id'              => 'godaddy_api_secret',
						    'type'            => 'text',
						    'title'           => esc_html__('GoDaddy Api Secret','phox'),
						    'desc'            => esc_html__('Put the GoDaddy api secret', 'phox'),
						    'note'            => esc_html__('', 'phox'),
						    'class'           => 'block-function-c',
						    'def'              => ''
					    ),
				    ),
			    ),
		    ),
	    );

		// ---------------------------- Chat Platform -----------------------------------
		$this->sections['chat-platforms'] =array(
			'title'     => '<div class="tab-title-head"><span class="fas fa-file"></span>'. esc_html__('Chat Platforms', 'phox').'</div>',
			'fields'    => array(
				// platform
				array(
					'package'              => esc_html__('Platforms', 'phox'),
					'package_id'					 => 'platforms',
					'package-options'      =>array(
						array(
							'id'              => 'available-platform',
							'type'            => 'select',
							'title'           => esc_html__('Choose the platform','phox'),
							'desc'            => esc_html__('When select the platform, the box will appear', 'phox'),
							'note'            => esc_html__('', 'phox'),
							'class'           => 'block-function-c',
							'options'         =>array(
								'none'   => esc_html__('None', 'phox'),
								'tawk'   => esc_html__('Tawk', 'phox'),
								'intercom'  => esc_html__('Intercom', 'phox'),
							),
							'def'              => 'none'
						),
					),
				),
				// platform Api
				array(
					'package'              => esc_html__('Platforms API', 'phox'),
					'package_id'					 => 'platforms-api',
					'package-options'      =>array(
						array(
							'id'              => 'platform-api-tawk',
							'type'            => 'text',
							'title'           => esc_html__('Tawk Chat Link','phox'),
							'desc'            => esc_html__('Put the chat link example https://tawk.to/chat/apikey/default', 'phox'),
							'note'            => esc_html__('', 'phox'),
							'class'           => 'block-function-c api-tawk',
						),
						array(
							'id'              => 'platform-api-intercom',
							'type'            => 'text',
							'title'           => esc_html__('Intercom API','phox'),
							'desc'            => esc_html__('Put the api code for Intercom', 'phox'),
							'note'            => esc_html__('', 'phox'),
							'class'           => 'block-function-c api-intercom',
						),
					),
				),
			),
		);

        // ---------------------------- Custom Style & Script -----------------------------------
        $this->sections['custom-css-js'] =array(
            'title'     => '<div class="tab-title-head"><span class="fas fa-file"></span>'. esc_html__('Custom Style & Script', 'phox').'</div>',
            'fields'    => array(
                // Custom Css
                array(
                    'package'              => esc_html__('Custom Style', 'phox'),
										'package_id'					 => 'custom-style',
                    'package-options'      =>array(
                        array(
                            'id'              => 'custom-css',
                            'type'            => 'textarea',
                            'title'           => esc_html__('Custom Css','phox'),
                            'desc'            => esc_html__('Paste your custom CSS code here', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                    ),
                ),
                // Custom Js
                array(
                    'package'              => esc_html__('Custom Script', 'phox'),
										'package_id'					 => 'custom-script',
                    'package-options'      =>array(
                        array(
                            'id'              => 'custom-js',
                            'type'            => 'textarea',
                            'title'           => esc_html__('Custom JS','phox'),
                            'desc'            => esc_html__('Past your custom Script code here  * To use jQuery code wrap it into  jQuery(function($){ ... }); </b>', 'phox'),
                            'note'            => esc_html__('', 'phox'),
                            'class'           => 'block-function-c',
                        ),
                    ),
                ),
            )
        );
        
        //filter
	    $this->menu = apply_filters('wdes_add_menu_item_admin_panel', $this->menu);
	    $this->sections = apply_filters('wdes_add_section_item_admin_panel', $this->sections);
    }
}

