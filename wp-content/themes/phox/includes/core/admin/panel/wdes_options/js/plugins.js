(function ($) {
"use strict"

	// Orders 
	$(function() {
		var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
		var randomColorFactor = function() {
			return Math.round(Math.random() * 255);
		};
		var randomColor = function(opacity) {
			return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
		};

		var config = {
			data: {
				datasets: [{
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
					],
					backgroundColor: [
						"#F7464A",
						"#46BFBD",
						"#FDB45C",
						"#949FB1",
						"#4D5360",
					],
					label: 'My dataset' // for legend
				}],
				labels: [
					"Pending",
					"Active",
					"Canceled",
					"Suspend",
					"Waiting Reply"
				]
			},
			options: {
				responsive: true,
				legend: {
					position: 'top',
				},
				scale: {
				  ticks: {
					beginAtZero: true
				  },
				  reverse: false
				},
				animateRotate:false
			}
		};


		$('#randomizeData').on('click', function() {
			$.each(config.data.datasets, function(i, piece) {
				$.each(piece.data, function(j, value) {
					config.data.datasets[i].data[j] = randomScalingFactor();
					config.data.datasets[i].backgroundColor[j] = randomColor();
				});
			});
			window.myPolarArea.update();
		});

		$('#addData').on('click',function() {
			if (config.data.datasets.length > 0) {
				config.data.labels.push('dataset #' + config.data.labels.length);

				$.each(config.data.datasets, function(i, dataset) {
					dataset.backgroundColor.push(randomColor());
					dataset.data.push(randomScalingFactor());
				});

				window.myPolarArea.update();
			}
		});

		$('#removeData').on('click',function() {
			config.data.labels.pop(); // remove the label first

			$.each(config.data.datasets, function(i, dataset) {
				dataset.backgroundColor.pop();
				dataset.data.pop();
			});

			window.myPolarArea.update();
		});
	});

})(jQuery);