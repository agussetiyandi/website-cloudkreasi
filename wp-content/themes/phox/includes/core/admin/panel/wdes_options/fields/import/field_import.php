<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_import extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   * Div:
   * @see https://developer.wordpress.org/reference/functions/selected
   */
   function render(){

    $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';
    $name  = $this->args['opt_name'].'['.$this->field['id'].']';
    $fonts = wdes_fonts();

    $value = $this->value;

    if (! is_array($value)) {
      $value = array(
        'size'       => '',

      );
    }
    echo '<div class="right-block-c">';
        echo '<input  name="'.$name.'" type="submit" value="'. esc_attr__('Import', 'phox') .'" class="import-btn-wdes">';
    echo '</div>';
   }
 }
