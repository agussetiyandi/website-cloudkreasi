function WDESUpload(){
	(function($) {

		jQuery( 'img[src=""]' ).attr( 'src', wdes_upload.url );

        jQuery(".wdes-upload" ).on("click", function(event){

        	event.preventDefault();

        	var activeFileUploadContext = jQuery( this ).parent();
        	var type = jQuery( 'input', activeFileUploadContext ).attr( 'class' );


        	custom_file_frame = null;

            // Create the media frame.
            custom_file_frame = wp.media.frames.customHeader = wp.media({
            	title: jQuery(this).data( 'choose' ),
            	library: {
            		type: type
            	},
                button: {
                    text: jQuery(this).data( 'update' )
                }
            });

            custom_file_frame.on( "select", function() {
            	var attachment = custom_file_frame.state().get( "selection" ).first();

				//get media type
				var scrValue = (type === 'image') ? attachment.attributes.url : wdes_upload.fontImg;

                // Update value of the targetfield input with the attachment url.
                jQuery( '.wdes-screenshot', activeFileUploadContext ).attr( 'src', scrValue );
                jQuery( 'input', activeFileUploadContext )
            		.val( attachment.attributes.url )
            		.trigger( 'change' );

                jQuery( '.wdes-upload', activeFileUploadContext ).hide();
                jQuery( '.wdes-screenshot', activeFileUploadContext ).show();
                jQuery( '.wdes-remove', activeFileUploadContext ).show();
            });

            custom_file_frame.open();
        });

        jQuery(document).on("click", ".wdes-remove" , function(event){
	    	event.preventDefault();

	        var activeFileUploadContext = jQuery( this ).parent();

			jQuery( 'input', activeFileUploadContext ).val('');

	        jQuery( this ).prev().fadeIn( 'slow' );
	        jQuery( '.wdes-screenshot', activeFileUploadContext ).fadeOut( 'slow' );
	        jQuery( this ).fadeOut( 'slow' );
	    });

	})(jQuery);
}



