<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_checkbox extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   */
  function render(){

    $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';

    /** name **/
    $name  = $this->args['opt_name'].'['.$this->field['id'].']';

    echo '<div class="right-block-c">';

        if(is_array($this->field['options'])){
          if (! isset($this->value)) {
            $this->value = array();
          }
          if (! is_array($this->value)) {
            $this->value = array();
          }
          foreach ($this->field['options'] as $key => $value) {
            if (! key_exists($key, $this->value)) $this->value[$key] = '' ;
            echo '<label class="checkbox-inline" >';
              echo '<input type="checkbox" name = "'. $name .'['.$key.']" value="'.$key.'" '.checked($this->value[$key], $key, false).' > '.$value.'';
            echo '</label>';
          }
       }
    echo '</div>';



  }

}
