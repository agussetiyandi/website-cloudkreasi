<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_range extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) {
        parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    }
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   */
  function render($meta= false){

       /** name **/
      $name  = 'name="'.$this->args['opt_name'].'['.$this->field['id'].']"';

      //parameters

      if(isset($this->field['param'])){
          $param=$this->field['param'];

      }else{
          $param=false;
      }

      $min=isset($param['min'])?$param['min']:1;
      $max=isset($param['max'])?$param['max']:100;


      echo'<div class="right-block-c">';
        echo '<input id="'.$this->field['id'].'" '.$name.' type="range" min="'.$param['min'].'" max="'.$param['max'].'" value="'.esc_attr($this->value).'" class="range-input-c">';
      echo '</div>';

  }
}
