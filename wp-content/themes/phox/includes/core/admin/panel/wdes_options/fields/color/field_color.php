<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_color extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   */
  function render($meta= false){

      /** name **/
      $name  = $this->args['opt_name'].'['.$this->field['id'].']';


      if( $this->value ){

          $value = $this->value;

      }else{

          $value = isset( $this->field['def'] ) ? $this->field['def'] : '';

      }


    echo '<div class="right-block-c">';
      echo '<input type="text" id="'.$this->field['id'].'" name="'.$name.'" class="wdes-color-selector" value="'.$value.'">';
    echo '</div>';
  }

  function enqueue() {

  	wp_enqueue_style('wp-color-picker');
  	wp_enqueue_script( 'wdes-color-picker', WDES_OPTIONS_URI . 'fields/color/color-picker.js', array('jquery', 'jquery-ui-sortable', 'jquery-ui-draggable', 'wp-color-picker'), false, true );

	}

}
