<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_select extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   * Div:
   * @see https://developer.wordpress.org/reference/functions/selected
   */
  function render($meta= false){

    $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';

    /** name **/

    $name  = 'name="'.$this->args['opt_name'].'['.$this->field['id'].']"';
		$id    = 'id="'.$this->field['id'].'"';

    if( $this->value ){

          $value_data = $this->value;

    }else{

        $value_data = isset( $this->field['def'] ) ? $this->field['def'] : '';

    }

    echo '<div class="right-block-c">';
      echo '<select '. $id .' '. $name .' class="option-c">';
      if(is_array($this->field['options'])){
        foreach ($this->field['options'] as $key => $value) {
          echo'<option value="'.$key.'"'.selected($value_data, $key, false).'>'. $value .'</option>';
        }
      }
      echo '</select>';
    echo '</div>';

  }
}
