<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_text extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) {
        parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    }
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   */
  function render($meta= false){

       /** name **/
      $name  = 'name="'.$this->args['opt_name'].'['.$this->field['id'].']"';


      if( $this->value ){

          $value = $this->value;

      }else{

          $value = isset( $this->field['def'] ) ? $this->field['def'] : '';

      }

    echo'<div class="right-block-c">';
      echo '<input id="'.$this->field['id'].'" '.$name.' type="text" value="'.esc_attr($value).'" class="input-c">';
    echo '</div>';

  }
}
