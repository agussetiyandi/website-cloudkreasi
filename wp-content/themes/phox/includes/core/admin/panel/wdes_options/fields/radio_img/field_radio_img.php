<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_radio_img extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   * Div:
   * @see https://developer.wordpress.org/reference/functions/checked
   */
   function render($meta= false){

    $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';
    $name  = 'name="'.$this->args['opt_name'].'['.$this->field['id'].']"';


    echo '<div class="right-block-c">';
    
    foreach ($this->field['options'] as $key => $value) {
        echo '<div class="field-c-radio">';
          echo '<input type="radio" id="'.$this->field['id'].'_'.array_search($key,array_keys($this->field['options'])).'" value="'.$key.'" '.$name.' class="input-hidden" '.checked($this->value, $key, false).'>';
          echo '<label for="'.$this->field['id'].'_'.array_search($key,array_keys($this->field['options'])).'">';
              echo '<img src="'. esc_url($value['img']) .'" alt="'. esc_attr($value['title']) .'">';
          echo '</label>';
        echo '</div>';
    }

    echo '</div>';

   }
 }
