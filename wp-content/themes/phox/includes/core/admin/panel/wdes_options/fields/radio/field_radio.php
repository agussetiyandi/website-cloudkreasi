<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_radio extends Wdes_Control {

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   */
  function render(){

    $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';

      /** name **/
      $name  = 'name="'.$this->args['opt_name'].'['.$this->field['id'].']"';

      if( $this->value ){

          $value_data = $this->value;

      }else{

          $value_data = isset( $this->field['def'] ) ? $this->field['def'] : '';

      }

    echo '<div class="right-block-c">';
      echo '<div class="btn-group" data-toggle="buttons">';
        if(is_array($this->field['options'])){
          foreach ($this->field['options'] as $key => $value) {
            if(checked($value_data, $key, false) == true){$active = 'active';}else{$active = '';}
            echo '<label for="'.$this->field['id'].'_'.$key.'" class="btn radio-btn-custom '.$active.'" >';
              echo '<input type="radio" '.$name.' id="'.$this->field['id'].'_'.$key.'" value="'.$key.'" autocomplete="off" '.checked($value_data , $key, false).' > '.$value.'';
            echo '</label>';
          }
       }
      echo '</div>';
    echo '</div>';

  }

}
