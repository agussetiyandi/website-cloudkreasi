<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_info extends Wdes_Control{
  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }
  /**
   * Field Render Function.
   */
  function render(){
    if(key_exists('title', $this->field)){
        echo'<p class="wdes-subblock-title"></p>';
    }
  }
}
