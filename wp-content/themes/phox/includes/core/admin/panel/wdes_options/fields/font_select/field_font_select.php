<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_font_select extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   */
  function render($meta= false){

    $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';
    $name  = 'name="'.$this->args['opt_name'].'['.$this->field['id'].']"';

    $fonts = wdes_fonts();

    echo '<div class="right-block-c">';
      echo '<select '.$name.' >';
        // select from local font
        echo '<optgroup label="'.esc_html__('Local Fonts', 'phox').'">';
          foreach ($fonts['local'] as $font) {
            echo '<option value = "'.$font.'" '.selected($this->value, $font, fales).' >'.$font.'</option>';
          }
        echo '</optgroup>';
        // select from google fonts
        echo '<optgroup label="'.esc_html__('Google Fonts', 'phox').'">';
          foreach ($fonts['google'] as $font) {
            echo '<option value = "'.$font.'" '.selected($this->value, $font, fales).' >'.$font.'</option>';
          }
        echo '</optgroup>';
      echo '</select>';
    echo '</div>';
  }
}
