/* COLOR PICKER */
if( jQuery().wpColorPicker ){
    wdes_color_picker();
}

/* Custom Color Picker
 ------------------------------------------------------------------------------------------ */
function wdes_color_picker(){
    Color.prototype.toString = function(remove_alpha) {
        if (remove_alpha == 'no-alpha') {
            return this.toCSS('rgba', '1').replace(/\s+/g, '');
        }
        if (this._alpha < 1) {
            return this.toCSS('rgba', this._alpha).replace(/\s+/g, '');
        }
        var hex = parseInt(this._color, 10).toString(16);
        if (this.error) return '';
        if (hex.length < 6) {
            for (var i = 6 - hex.length - 1; i >= 0; i--) {
                hex = '0' + hex;
            }
        }
        return '#' + hex;
    };

    jQuery('.wdes-color-selector').each(function() {

        var $control = jQuery(this),
            value    = $control.val().replace(/\s+/g, ''),
            palette_input = $control.attr('data-palette');

        if (palette_input == 'false' || palette_input == false) {
            var palette = false;
        }
        else if (palette_input == 'true' || palette_input == true) {
            var palette = true;
        }


        $control.wpColorPicker({ // change some things with the color picker
            clear: function(event, ui) {
                // TODO reset Alpha Slider to 100
            },
            change: function(event, ui) {
                var $transparency = $control.parents('.wp-picker-container:first').find('.transparency');
                $transparency.css('backgroundColor', ui.color.toString('no-alpha'));
            },
            palettes: palette
        });

        jQuery('<div class="wdes-alpha-container"><div class="slider-alpha"></div><div class="transparency"></div></div>').appendTo($control.parents('.wp-picker-container'));
        var $alpha_slider = $control.parents('.wp-picker-container:first').find('.slider-alpha');
        if (value.match(/rgba\(\d+\,\d+\,\d+\,([^\)]+)\)/)) {
            var alpha_val = parseFloat(value.match(/rgba\(\d+\,\d+\,\d+\,([^\)]+)\)/)[1]) * 100;
            var alpha_val = parseInt(alpha_val);
        }
        else {
            var alpha_val = 100;
        }

        $alpha_slider.slider({
            slide: function(event, ui) {
                jQuery(this).find('.ui-slider-handle').text(ui.value); // show value on slider handle
            },
            create: function(event, ui) {
                var v = jQuery(this).slider('value');
                jQuery(this).find('.ui-slider-handle').text(v);
            },
            value: alpha_val,
            range: 'max',
            step: 1,
            min: 1,
            max: 100
        });

        $alpha_slider.slider().on('slidechange', function(event, ui) {
            var new_alpha_val = parseFloat(ui.value),
                iris = $control.data('a8cIris'),
                color_picker = $control.data('wpWpColorPicker');

            iris._color._alpha = new_alpha_val / 100.0;

            $control.val(iris._color.toString());
            color_picker.toggler.css({
                backgroundColor: $control.val()
            });

            var get_val = $control.val();
            jQuery($control).wpColorPicker('color', get_val);
        });
    });
}