if(!window.wdes) var wdes = {};
(function($){

    function wdesIconsPicker(){
        wdes.modelLoop = $('.wdes-icons').find('.modal');
        wdes.tabItems       = ['General','Hosting','Design', 'Develope', 'SEO', 'Support'];
        wdes.iconsItems     = [17,13,8,7,12,11];

        for (var m = 0; m < wdes.modelLoop.length; m++ ){

            wdes.modelName = $(wdes.modelLoop[m]).attr('id');
            wdes.model = $('#'+wdes.modelName);
            wdes.modelTap = wdes.model.find('#tabs');

            if(wdes.modelTap.length == 1 && wdes.modelTap.find('ul').length == 0){
                //find the model then tab div
                var tab              =  wdes.modelTap,
                    model            =  wdes.model,
                    tapData          =  model.data('input'),
                    input            =  $('input#result-'+tapData),
                    display          =  $('a#display-'+tapData).find('img');

                // create ul element
                $('<ul></ul>').appendTo(tab);
                var tapBar = wdes.modelTap.find('ul');

                //append to it the tab item and create icon div
                var tabItems    = wdes.tabItems;
                var iconItems   = wdes.iconsItems;

                for(var i = 0; i < tabItems.length; i++ ){

                    var lowcaseTabList = tabItems[i].toLowerCase();
                    var createTabList  = '<li><a href="#' + lowcaseTabList + '">' + tabItems[i] + '</a></li>';
                    var createIconDiv  = '<div id="' + lowcaseTabList + '"></div>';

                    $(createTabList).appendTo(tapBar);
                    $(createIconDiv).appendTo(tab);

                    for(var n = 0 ; n < iconItems[i]; n++){
                        if(n !==  0){

                            var iconDiv    = wdes.modelTap.find('div#' + lowcaseTabList + '');
                            var createIcon = '<a class="wdes-icon-builder"><img src="'+wdes.url+'img/icons/wdes-icons/'+ lowcaseTabList +'/' + n + '.svg" alt="General icon"></a>';

                            $(createIcon).appendTo(iconDiv);
                        }

                    }
                }

            }

        }

        //when click on icon take the image scr and put in input
        $('.modal').find('a.wdes-icon-builder').on('click', function (e){

            e.preventDefault();
            var $this       =  $(this),
                value       =  $this.find('img').attr('src') ,
                elClosest   =  $this.find('img').closest('.modal'),
                $elClosest  =  $(elClosest),
                elName      =  $elClosest.data('input'),
                elDisplay   =  $('#display-'+elName),
                $elDisplay  =  $(elDisplay),
                elParent    =  $elDisplay.parent(),
                $elParent   =  $(elParent),
                elRemove    =  $elParent.find('a.wdes-icons-remove'),
                elChoose    =  $elParent.find('.modal-wdes-icons-choose');

            //set the value in input
            $elParent.find('input').val(value);

            //display the icon
            if($elDisplay.css('display') === 'none'){
                $elDisplay.css('display', 'block');
            }
            $elDisplay.find('img').attr('src',value);
            $elDisplay.css('display', 'block');

            //close the model after select icon
            $('#'+ elName).css('display','none').appendTo('body');
            $('.jquery-modal').remove();
            $('body').css('overflow', '');

            //show remove hide choose button
            hide(elChoose);
            show(elRemove);

        });

        //remove the icon
        $('.wdes-icons').find('a.wdes-icons-remove').on('click', function (e){
            var $this       =  $(this),
                elParent    =  $this.parent(),
                $elParent   =  $(elParent),
                elChoose    =  $elParent.find('.modal-wdes-icons-choose'),
                elmodal     =  $(elChoose).attr('href'),
                elName      =  elmodal.split("#"),
                elInput     =  $('input#result-'+elName[1]),
                elImg       =  $('a#display-'+elName[1]);


            //remove input & Image Value
            elInput.val('');
            elImg.css('display', 'none')
            elImg.find('img').removeAttr('src');

            //hide remove show choose button
            show(elChoose);
            $this.css('display', 'none');


        });

    };

    function show(arg){
        arg.css('display', 'block');
    }

    function hide(arg){
        arg.css('display', 'none');
    }


    $( document ).ready(function() {
        var wdesIcons = wdesIconsPicker();
    });

})(window.jQuery);

