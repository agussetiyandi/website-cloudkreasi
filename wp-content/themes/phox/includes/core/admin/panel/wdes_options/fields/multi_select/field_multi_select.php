<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_multi_select extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   * Div:
   * @see https://developer.wordpress.org/reference/functions/selected
   */
	function render($meta= false){

		/** name **/

		$name  = 'name="'.$this->args['opt_name'].'['.$this->field['id'].'][]"';
		$id    = 'id="'.$this->field['id'].'"';


		echo '<div class="right-block-c">';

			echo '<select '. $id .' '. $name .' class="option-multi-select" multiple>';
				if(is_array($this->field['options'])){
					if (! isset($this->value)) {
						$this->value = array();
					}
					if (! is_array($this->value)) {
						$this->value = array();
					}
					foreach ($this->field['options'] as $key => $value) {
						if (! key_exists($key, $this->value)) $this->value[$key] = '' ;
						$selected = ( in_array($key,$this->value) ) ? 'selected="selected"' :'';
						echo'<option  value="'.$key.'"'. $selected .'>'. $value .'</option>';
					}
				}
			echo '</select>';
		echo '</div>';

	}
}
