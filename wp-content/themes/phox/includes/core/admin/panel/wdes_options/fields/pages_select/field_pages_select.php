<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_pages_select extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   * Div:
   * @see https://developer.wordpress.org/reference/functions/get_pages/
   */
   function render($meta= false){

    $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';
    $name  = 'name="'.$this->args['opt_name'].'['.$this->field['id'].']"';

    $pages = get_pages('sort_column=post_title&hierarchical=0');
    echo '<div class="right-block-c">';
      echo '<select '. $name .' class="option-c">';
        echo '<option value=""> '.esc_html__('-- Select The Page --', 'phox').' </option>';
        if(is_array($this->field['options'])){
          foreach ($pages as $page) {
            echo'<option value="'.$page->ID.'"'.selected($this->value, $page->ID, fales).'>'.$page->post_title.'</option>';
          }
        }
      echo '</select>';
    echo '</div>';



   }
 }
