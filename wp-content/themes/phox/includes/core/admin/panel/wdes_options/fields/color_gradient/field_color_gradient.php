<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_color_gradient extends Wdes_Control{

    /**
     * Constructor
     */

    function __construct($field = array(), $value='',$parent = NULL){
        if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * Render
     */
    function render($meta= false){

        $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';
        /** name **/
        $name  = $this->args['opt_name'].'['.$this->field['id'].']';


        $value = $this->value;

        if( ! $value ){

            $value = $this->field['def'];

        }
        elseif(! is_array($value) ){

            $value = array(

                'color-one'       => $this->field['def']['color-one'],
                'color-two'       => $this->field['def']['color-two']

            );

        }

        echo '<div class="right-block-c">';
            echo '<input type="text" id="'.$this->field['id'].'" name="'.$name.'[color-one]" class="wdes-color-selector" value="'.$value['color-one'].'" class="color-gradient">';
            echo '<input type="text" id="'.$this->field['id'].'" name="'.$name.'[color-two]" class="wdes-color-selector" value="'.$value['color-two'].'" class="color-gradient">';
        echo '</div>';
    }

}
