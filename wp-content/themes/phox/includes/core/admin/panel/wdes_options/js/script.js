(function ($) {
  "use strict";
  //FadeIn
  function wdesFadeIn(target) {
    $(target).fadeIn(300);
  }
  //FadeOut
  function wdesFadeOut(target) {
    $(target).fadeOut(300);
  }

  //check the radio in out
  function checkRadio(el, target) {
    for (var i = 0; i < el.length; i++) {
      var radio = $(el[i]).prop("checked");
      wdesFadeOut(target[i]);
      if (radio == true) {
        wdesFadeIn(target[i]);
      }
    }
  }

  //Slider Taxonomy js
  function sliderOptions() {
    var img = "#slider_type_image",
      vid = "#slider_type_video",
      imgType = $(img),
      videoType = $(vid),
      vidOption = "#slider-video-options",
      imgOption = "#slider-image-options",
      Types = [img, vid],
      options = [imgOption, vidOption];

    imgType.on("click", function () {
      wdesFadeIn(imgOption);
      wdesFadeOut(vidOption);
      $(this).parent().addClass("active");
      $(vid).parent().removeClass("active");
    });

    videoType.on("click", function () {
      wdesFadeOut(imgOption);
      wdesFadeIn(vidOption);
      $(this).parent().addClass("active");
      $(img).parent().removeClass("active");
    });

    checkRadio(Types, options);
  }

  //Last tab
  function lastTab() {
    $("[role=presentation]").on("click", function () {
      var tabActive = $(this).find("a").attr("href");

      if (tabActive != null) {
        var tabName = tabActive.replace("#", "");

        if (tabName === "page") {
          tabName = "blog";
        }

        if (tabName === "typography") {
          tabName = "gen-typo";
        }

        $("#last_tab").val(tabName);
      }
    });

    $("#page li").on("click", function () {
      var tabActiveUl = $(this).attr("href");
      var tabName = tabActiveUl.replace("#", "");

      $("#last_tab").val(tabName);
    });
    
    $("#typography li").on("click", function () {
      var tabActiveUl = $(this).attr("href");
      var tabName = tabActiveUl.replace("#", "");

      $("#last_tab").val(tabName);
    });
  }

  function topHeaderLoad() {
    var topHeaderButton = jQuery("#show-topheader"),
      topHeaderCheck = topHeaderButton.prop("checked"),
      topHeaderLayout = jQuery(".top-header-options");

    if (topHeaderCheck) {
      topHeaderLayout.fadeIn();
    } else {
      topHeaderLayout.fadeOut();
    }

    topHeaderButton.on("change", function () {
      if (jQuery(this).prop("checked")) {
        topHeaderLayout.fadeIn();
      } else {
        topHeaderLayout.fadeOut();
      }
    });
  }

  function chatShowFieldsLoad() {
    var selectElement = jQuery("#available-platform"),
      platformSelected = jQuery("#available-platform :selected").val();

    chatCases(platformSelected);

    selectElement.on("change", function () {
      var optionClicked = jQuery(this).val();
      chatCases(optionClicked);
    });
  }

  function chatCases(platformSelected) {
    var tawkField = jQuery(".api-tawk"),
      intercomField = jQuery(".api-intercom"),
      platformSection = jQuery("#platforms-api");

    if (platformSelected === "tawk") {
      platformSection.show();
      tawkField.show();
      intercomField.hide();
    } else if (platformSelected === "intercom") {
      platformSection.show();
      tawkField.hide();
      intercomField.show();
    } else {
      platformSection.hide();
      tawkField.hide();
      intercomField.hide();
    }
  }

  $(document).ready(function ($) {
    // Media Upload
    var wdes_upload = new WDESUpload();

    //init tap
    $(".tabs").tabs();

    //JS Fun
    sliderOptions();
    lastTab();
    chatShowFieldsLoad();
    topHeaderLoad();
  });
})(jQuery);
