<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_switch extends Wdes_Control{

    /**
     * Constructor
     */

    function __construct($field = array(), $value='',$parent = NULL){
        if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * Render
     * Div:
     * @see https://developer.wordpress.org/reference/functions/checked
     */
    function render($meta= false){

        $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';

        /** name **/
        if($meta){
            $name  = 'name="'.$this->field['id'].'"';
        }else{
            $name  = 'name="'.$this->args['opt_name'].'['.$this->field['id'].']"';
        }

        /** fix for value **/
	    if($this->value === 'on') $this->value = isset( $this->field['def'] ) ? 1 : 0;
        if(! $this->value) $this->value = 0;

        echo '<div class="right-block-c">';
            echo '<label class="switch">';
                echo '<input type="checkbox" id="'.$this->field['id'].'" '.$name.' '.$class.' value="1" ' .checked($this->value, true, false).' />';
                echo '<div class="slider"></div>';
            echo'</label>';
        echo '</div>';

    }

}
