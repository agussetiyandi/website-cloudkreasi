<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_typography extends Wdes_Control{

  /**
   * Constructor
   */

  function __construct($field = array(), $value='',$parent = NULL){
    if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
    $this->field = $field;
    $this->value = $value;
  }

  /**
   * Render
   * Div:
   * @see https://developer.wordpress.org/reference/functions/selected
   */
   function render(){

    $class = (isset($this->field['class']))? 'class="'.$this->field['class'].'" ': '';
    $name  = $this->args['opt_name'].'['.$this->field['id'].']';
    $fonts = $this->wdes_fonts();

       $value = $this->value;

       if( ! $value ){

           $value = $this->field['def'];

       }
       elseif(! is_array($value) ){

           $value = array(

               'size'       => $this->field['def']['size'],
               'fonts'       => $this->field['def']['fonts'],
               'weight-font'       => $this->field['def']['weight-font'],


           );

       }

    echo '<div class="right-block-c">';
      // fonts
      echo '<select name="'.$name.'[fonts]" class="option-c">';
        // select from local font
        echo '<optgroup label="'.esc_html__('Local Fonts', 'phox').'">';
          foreach ($fonts['local'] as $font) {
            echo '<option value = "'.$font.'" '.selected($value['fonts'], $font, false).' >'.$font.'</option>';
          }
        echo '</optgroup>';
	   // select from custom font
		if(isset($fonts['custom'])){
		   echo '<optgroup label="'.esc_html__('Custom Fonts', 'phox').'">';
			  foreach ($fonts['custom'] as $font) {
				  echo '<option value = "'.$font.'" '.selected($value['fonts'], $font, false).' >'.$font.'</option>';
			  }
		   echo '</optgroup>';
		}
        // select from google fonts
        echo '<optgroup label="'.esc_html__('Google Fonts', 'phox').'">';
          foreach ($fonts['google'] as $font) {
            echo '<option value = "'.$font.'" '.selected($value['fonts'], $font, false).' >'.$font.'</option>';
          }
        echo '</optgroup>';
      echo '</select>';

      // Font Weight
      echo '<select name="'.$name.'[weight-font]" class="option-c">';
        for ($i=1; $i <10 ; $i++) {
          echo '<option value="'.$i.'00"'.selected($value['weight-font'],$i .'00' , false).'>'.$i.'00</option>';
        }
      echo '</select>';

      //Font size
      echo '<input  name="'.$name.'[size]" type="number" value="'.$value['size'].'" step="any" class="input-c">';
      
    echo '</div>';

   }
 }
