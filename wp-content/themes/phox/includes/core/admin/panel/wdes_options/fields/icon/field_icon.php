<?php

use \Phox\core\admin\panel\wdes_options\Wdes_Control;

class WDES_Admin_icon extends Wdes_Control{


    /**
     * Constructor
     */

    function __construct($field = array(), $value='',$parent = NULL){
        if(is_object($parent)) parent::__construct($parent->sections, $parent->args, $parent->extra_tabs);
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * Render
     * Div:
     * @see https://developer.wordpress.org/reference/functions/selected
     */
    function render(){

        /** name **/
        $name  = 'name="'.$this->args['opt_name'].'['.$this->field['id'].']"';


        if(!empty($this->value)){
            $style_remove = 'display:block';
            $style_choose =  'display:none';
        }else{
            $style_remove = 'display:none';
            $style_choose =  'display:block';
        }

        echo '<div class="right-block-c wdes-icons">';
            echo '<div id="'.$this->field['id'].'" class="modal wdes-modal-block" data-input="'.$this->field['id'].'">';
                echo '<div id="tabs" class="tabs">';

                    //generate the model content

                echo '</div>';
            echo '</div>';
            echo '<p><a class="modal-wdes-icons-choose" href="#'.$this->field['id'].'" rel="modal:open" style="'.$style_choose.'">'. esc_html__('Choose', 'phox').'</a></p>';
            echo '<a href="javascript:void(0);"class="fileUpload func-up wdes-icons-remove" style="'.$style_remove.'">'. esc_html__('Remove', 'phox').'</a>';
            echo '<a id="display-'.$this->field['id'].'" class="wdes-icon-builder" style="'.$style_remove.'"><img src="'. esc_url($this->value) .'"></a>';
            echo '<input id="result-'.$this->field['id'].'" '.$name.' type="hidden" value="'.esc_attr($this->value).'" class="input-c">';
        echo '</div>';

    }
}
