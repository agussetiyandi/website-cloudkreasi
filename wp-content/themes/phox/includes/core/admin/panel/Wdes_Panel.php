<?php

namespace Phox\core\admin\panel;


if( ! defined( 'ABSPATH' ) ){
    exit; //Exit if accessed directly
}


class Wdes_Panel {

    protected static $_instance = null;

    public static function instance()
    {

        if (is_null( self::$_instance) ) {
            self::$_instance = new self ;
        }

        return self::$_instance;

    }

    function __construct()
    {

        add_action('after_switch_theme', array($this, 'after_switch_theme'));

        add_action('admin_menu', array($this, 'init'));
    }

    public function after_switch_theme(){

        wp_safe_redirect(admin_url('admin.php?page=phox'));

    }

    public function init()
    {

        $title = array(
          'phox'  => 'Phox',
          'home'    => 'Home',
          'options' => esc_html__('Wdes Options', 'phox')
        );

        $icon =  WDES_ADMIN_ASSETS_URI .'/images/phox.png';

        $this->page = add_menu_page(
            $title['phox'],
            $title['phox'],
            'edit_theme_options',
            'phox',
            array( $this, 'template_part'),
            $icon,
            3
        );

        add_submenu_page(
          'phox',
          $title['home'],
          $title['home'],
          'edit_theme_options',
          'phox',
          array( $this, 'template_part' )
        );

        add_action('admin_print_styles', array( $this, 'enqueue' ) );

        $this->set_status();

    }

    public function template_part()
    {

        include_once WDES_ADMIN_DIR . '/templates-parts/home.php';

    }

	/**
	 * Fonts
	 */
	public function wdes_admin_g_fonts() {

		$fonts_url = '';

		/* Translators: If there are characters in your language that are not
		 * supported by Montserrat, translate this to 'off'. Do not translate
		 * into your own language.
		 */
		$montserrat = _x( 'on', 'Montserrat font: on or off', 'phox' );

		/* Translators: If there are characters in your language that are not
		 * supported by Rubik, translate this to 'off'. Do not translate
		 * into your own language.
		 */
		$rubik = _x( 'on', 'Rubik font: on or off', 'phox' );


		/* Translators: If there are characters in your language that are not
			* supported by Open Sans, translate this to 'off'. Do not translate
			* into your own language.
			*/
		$open_sans = _x( 'on', 'Open Sans font: on or off', 'phox' );

		/* Translators: If there are characters in your language that are not
		 * supported by Poppins, translate this to 'off'. Do not translate
		 * into your own language.
		 */
		$poppins = _x( 'on', 'Poppins font: on or off', 'phox' );

		/* Translators: If there are characters in your language that are not
		 * supported by Karla, translate this to 'off'. Do not translate
		 * into your own language.
		 */
		$karla = _x( 'on', 'Karla font: on or off', 'phox' );

		if ( 'off' !== $montserrat || 'off' !== $rubik || 'off' !== $open_sans || 'off' !== $poppins ) {

			$font_families = array();

			if ( 'off' !== $montserrat ) {
				$font_families[] = 'Montserrat:400,600,700';
			}

			if ( 'off' !== $rubik ) {
				$font_families[] = 'Rubik:400,500';
			}

			if ( 'off' !== $open_sans ) {
				$font_families[] = 'Open Sans:400,600,700';
			}

			if ( 'off' !== $poppins ) {
				$font_families[] = 'Poppins:400,600,700';
			}

			if ( 'off' !== $karla ) {
				$font_families[] = 'Karla:400,500,600';
			}

			$query_args = array(
				'family' => urlencode( implode( '|', $font_families ) ),
				'subset' => urlencode( 'latin,latin-ext' ),
			);

			$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );


		}

		wp_enqueue_style( 'admin-google-fonts', esc_url_raw( $fonts_url ), array(), null);

	}

    public function enqueue()
    {
	    // Google Fonts
	    $this->wdes_admin_g_fonts();

	    // Style
	    wp_enqueue_style('wdes-dashboard', WDES_ADMIN_ASSETS_URI. '/css/style.css', array(), WDES_THEME_VERSION);

	    //Fix Bootstrap
	    if(get_admin_page_title() === 'Home' || get_admin_page_title() === 'Theme Options'){

		    wp_enqueue_style('bootstrap-style',WDES_OPTIONS_URI . 'css/bootstrap.min.css', false, time(),'all');
		    wp_enqueue_script('bootstrap',WDES_OPTIONS_URI . 'js/bootstrap.min.js', array('jquery'), time(),true );

		    // Font Awesome
		    wp_enqueue_style('font-awesome',WDES_OPTIONS_URI. '/css/fontawesome.min.css',false,time(),'all');
	    }

    }

	/**
	 * Server Software
	 * It check if the server run Apache or nginx
	 * This is the alternative way to use global server with flag server_software
	 *
	 * @return string
	 */
    public function serv_software(){

    	#wordpress global variables
			global $is_apache,$is_nginx;

			$default = '';

			if ($is_apache) {

				$default = 'Apache';

			}elseif( $is_nginx ){

				$default = 'nginx';

			}

			return $default;

	}

	/**
	 * Check Server Whois
	 *
	 * We are use iana server to check the domain so it need to check if site is work and the site is support.
	 *
	 * @since 1.7.5
	 * @return bool
	 */
	public function whois_server(){

    	$host = 'whois.iana.org';
    	$ip   = gethostbyname($host);

    	$check_connection = fsockopen( $ip, '43', $errno, $errstr, 30 );

    	if( $check_connection ){
    		$result = true;
	    }else{
		    $result = false;
	    }

    	return $result;
	}

    public function set_status() {
        global $wpdb,$is_apache,$is_nginx;

        $data 	= array(
            'server'					    => $this->serv_software() ,
            'php'									=> phpversion(),
            'mysql'						    => $wpdb->db_version(),
            'memory_limit' 		    => wp_convert_hr_to_bytes( @ini_get( 'memory_limit' ) ),
            'time_limit' 			    => ini_get( 'max_execution_time' ),
            'max_input_vars' 	    => ini_get( 'max_input_vars' ),
            'upload_max_filesize'	=> wp_convert_hr_to_bytes (ini_get( 'upload_max_filesize' ) ),
            'post_max_size'				=> wp_convert_hr_to_bytes( ini_get( 'post_max_size' ) ),
            'home'						    => esc_url(home_url('/')),
            'siteurl'					    => esc_url(get_option( 'siteurl' )),
            'wp_version'		      => get_bloginfo( 'version' ),
            'language'				    => get_locale(),
            'rtl'							    => is_rtl() ? 'RTL' : 'LTR',
        );

        $status = array(
            'php'						=> version_compare( PHP_VERSION, '7.0' ) >= 0,
            'suhosin'					=> extension_loaded( 'suhosin' ),
            'memory_limit'				=> $data['memory_limit'] >= 268435456,
            'time_limit'				=> ( ( $data['time_limit'] >= 180 ) || ( $data['time_limit'] == 0 ) ),
            'max_input_vars'			=> $data['max_input_vars'] >= 5000,
            'curl'						=> extension_loaded( 'curl' ),
            'dom'						=> class_exists( 'DOMDocument' ),
			'upload_max_filesize'       => $data['upload_max_filesize'] >= 8000000,
			'post_max_size' 			=> $data['post_max_size'] >= 8000000,
            'siteurl'					=> false,
            'wp_version'				=> version_compare( get_bloginfo( 'version' ), '4.9' ) >= 0,
            'multisite'					=> is_multisite(),
            'debug'						=> defined( 'WP_DEBUG' ) && WP_DEBUG,
            'fsockopen'       		    => (function_exists('fsockopen') || function_exists('curl_init')),
	        'whois_server'              => $this->whois_server(),
	        'icu_version'               => defined('INTL_ICU_VERSION') && version_compare(INTL_ICU_VERSION, '4.6', '>=')
        );

        $parse = array(
            'home' 		=> parse_url( $data['home'] ),
            'siteurl' => parse_url( $data['siteurl'] ),
        );

        if( isset( $parse['home']['host'] ) && isset( $parse['siteurl']['host'] ) ){
            if( $parse['home']['host'] == $parse['siteurl']['host'] ){
                $status['siteurl'] = true;
            }
        } elseif( isset( $parse['home']['path'] ) && isset( $parse['siteurl']['path'] ) ){
            if( $parse['home']['path'] == $parse['siteurl']['path'] ){
                $status['siteurl'] = true;
            }
        }

        $this->data		= $data;
        $this->status = $status;

    }

}