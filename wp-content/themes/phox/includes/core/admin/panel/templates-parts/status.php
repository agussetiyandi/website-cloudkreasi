<?php
if( ! defined( 'ABSPATH' ) ){
    exit; //Exit if accessed directly
}

?>
<div class="tab-pane fade in active" id="pills-system" role="tabpanel" aria-labelledby="pills-system-tab">
    <div class="content-bg-tab-x">
        <div class="title-in">
            <h2>
                <span class="fas fa-exclamation-circle"></span><?php esc_html_e(' System Status', 'phox') ?></h2>
            <hr>
            <p><?php esc_html_e('WordPress Environment Info', 'phox'); ?>.</p>
        </div>
        <div class="row">
            <div class="col-md-6">
                <!-- Server Environment -->
                <table class="table table-responsive wdes-table-welcome-page">
                    <thead>
                    <tr>
                        <th><?php esc_html_e('Server Environment', 'phox') ?></th>
                        <th><?php esc_html_e('Value', 'phox') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?php esc_html_e('Server Info', 'phox') ?></td>
                        <td><?php x_wdes()->wdes_get_text ($this->data['server']) ; ?></td>
                    </tr>
                    <tr>
                        <td><?php esc_html_e('MySQL Version', 'phox') ?></td>
                        <td><?php x_wdes()->wdes_get_text ($this->data['mysql']) ; ?></td>
                    </tr>
                    <tr>
                        <td><?php esc_html_e('PHP Version', 'phox') ?></td>
                        <?php if($this->status['php']): ?>
                            <td>
                                <span class="fas fa-check-circle"></span> <?php echo PHP_VERSION ?>
                            </td>
                        <?php else: ?>
                            <td>
                                <span class="fas fa-close"></span> <?php echo PHP_VERSION ?>
                                <br />
                                <span><?php esc_html_e('WordPress requires PHP version 7 or greater.', 'phox') ?> <a target="_blank" href="https://wordpress.org/about/requirements/"><?php esc_html_e('Learn more', 'phox') ?></a></span>
                            </td>
                        <?php endif; ?>
                    </tr>

                    <tr>
                        <td><?php esc_html_e( 'PHP Memory Limit', 'phox' ); ?></td>
                        <?php if( $this->status['memory_limit'] ): ?>
                            <td>
                                <span class="fas fa-check-circle"></span><?php echo size_format( $this->data['memory_limit'] ); ?>
                            </td>
                        <?php else: ?>

                            <?php if( $this->data['memory_limit'] < 134217728 ): ?>
                                <td>
                                    <span class="fas fa-close"></span><?php echo size_format( $this->data['memory_limit'] ); ?>
                                    <span><?php esc_html_e('Minimum', 'phox') ?> <strong><?php esc_html_e('128 MB', 'phox') ?></strong><?php esc_html_e(' is required, ', 'phox') ?><strong><?php esc_html_e('256 MB', 'phox') ?></strong><?php esc_html_e(' is recommended.', 'phox') ?> </span>
                                </td>
                            <?php else: ?>


                                <td>
                                    <span class="fas fa-info"></span><?php echo size_format( $this->data['memory_limit'] ); ?>
                                    <span><?php esc_html_e('Current memory limit is OK, however', 'phox') ?> <strong><?php esc_html_e('256 MB', 'phox') ?></strong><?php esc_html_e(' is recommended.', 'phox') ?></span>

                                </td>


                            <?php endif; ?>

                        <?php endif; ?>
                    </tr>


                    <tr>
                        <td><?php esc_html_e( 'PHP Time Limit', 'phox' ); ?></td>
                        <?php if( $this->status['time_limit'] ): ?>

                            <td>
                                <span class="fas fa-check-circle"></span> <?php x_wdes()->wdes_get_text ($this->data['time_limit']); ?>
                            </td>

                        <?php else: ?>

                            <?php if( $this->data['time_limit'] < 60 ): ?>

                                <td>
                                    <span class="fas fa-close"></span> <?php x_wdes()->wdes_get_text($this->data['time_limit']) ; ?>
                                    <span><?php esc_html_e('Minimum', 'phox') ?> <strong><?php esc_html_e('60', 'phox') ?></strong><?php esc_html_e(' is required,', 'phox') ?> <strong><?php esc_html_e('180', 'phox') ?></strong><?php esc_html_e('is recommended.', 'phox') ?> </span>
                                </td>

                            <?php else: ?>

                                <td>
                                    <span class="fas fa-info"></span><?php x_wdes()->wdes_get_text ($this->data['time_limit']); ?>
                                    <span><?php esc_html_e('Current time limit is OK, however', 'phox') ?><strong><?php esc_html_e('180', 'phox') ?></strong><?php esc_html_e(' is recommended. ', 'phox') ?></span>
                                </td>

                            <?php endif; ?>

                        <?php endif; ?>
                    </tr>

                    <tr>
                        <td><?php esc_html_e( 'PHP Max Input Vars', 'phox' ); ?></td>
                        <?php if( $this->status['max_input_vars'] ): ?>
                            <td>
                                <span class="fas fa-check-circle"></span><?php x_wdes()->wdes_get_text ($this->data['max_input_vars']); ?>
                            </td>
                        <?php else: ?>
                            <td>
                                <span class="fas fa-close"></span><?php x_wdes()->wdes_get_text ($this->data['max_input_vars']); ?>
                                <span><?php esc_html_e(' Minimum 5000 is required ', 'phox') ?></span>
                            </td>
                        <?php endif; ?>
                    </tr>

                    <tr>
                        <td>
                            <?php esc_html_e( 'Upload Max File Size', 'phox' ); ?>
                        </td>
                        <?php if( $this->status['upload_max_filesize'] ): ?>
                            <td>
                                <span class="fas fa-check-circle"></span><?php x_wdes()->wdes_get_text ( size_format( $this->data['upload_max_filesize'] ) ); ?>
                            </td>
                        <?php else: ?>
                            <td>
                                <span class="fas fa-close"></span><?php x_wdes()->wdes_get_text ( size_format( $this->data['upload_max_filesize'] ) ); ?>
                                <span><?php esc_html_e(' 8mb is required ', 'phox') ?></span>
                            </td>
                        <?php endif; ?>
                    </tr>

                    <tr>
                        <td>
                            <?php esc_html_e( 'Post Max Size', 'phox' ); ?>
                        </td>
                        <?php if( $this->status['post_max_size'] ): ?>
                            <td>
                                <span class="fas fa-check-circle"></span><?php x_wdes()->wdes_get_text ( size_format( $this->data['post_max_size'] ) ); ?>
                            </td>
                        <?php else: ?>
                            <td>
                                <span class="fas fa-close"></span><?php x_wdes()->wdes_get_text ( size_format( $this->data['post_max_size'] ) ); ?>
                                <span><?php esc_html_e(' 8mb is required ', 'phox') ?></span>
                            </td>
                        <?php endif; ?>
                    </tr>

                    <tr>
                        <td>
                            <?php esc_html_e( 'cURL', 'phox' ); ?>
                        </td>
                        <td>
                            <?php if( $this->status['curl'] ): ?>

                                <span class="fas fa-check-circle"></span>

                            <?php else: ?>

                                <span class="fas fa-close"></span>
                                <span><?php esc_html_e('Your server does not have ', 'phox') ?><strong><?php esc_html_e('cURL', 'phox') ?></strong><?php esc_html_e('enabled. Please contact your hosting provider.', 'phox') ?></span>

                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php esc_html_e( 'fsockopen/cUrl', 'phox' ); ?></td>
                        <td>
		                    <?php if( $this->status['fsockopen'] ): ?>
                                <span class="fas fa-check-circle"></span>
		                    <?php else: ?>
                                <span class="fas fa-close"></span>
		                    <?php endif; ?>
                        </td>
                    </tr>

                    <tr>
                        <td><?php esc_html_e( 'Whois Server', 'phox' ); ?></td>
                        <td>
		                    <?php if( $this->status['whois_server'] ): ?>
                                <span class="fas fa-check-circle"></span>
		                    <?php else: ?>
                                <span class="fas fa-close"></span>
		                    <?php endif; ?>
                        </td>
                    </tr>

                    <tr>
                        <td><?php esc_html_e( 'ICU Version', 'phox' ); ?></td>
                        <td>
		                    <?php if( $this->status['icu_version'] ): ?>
                                <span class="fas fa-check-circle"></span>
                                <span><?php esc_html_e('Version: '. INTL_ICU_VERSION , 'phox'); ?></span>
		                    <?php else: ?>
                                <span class="fas fa-close"></span>
			                    <?php if(defined('INTL_ICU_VERSION')): ?>
                                    <span><?php esc_html_e('We required Version 4.6 or higher, you need to contact your hosting provider, your version now is'. INTL_ICU_VERSION .'', 'phox'); ?></span>
			                    <?php endif; ?>
		                    <?php endif; ?>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <!-- WordPress Environment -->
                <table class="table table-responsive wdes-table-welcome-page">
                    <thead>
                    <tr>
                        <th><?php esc_html_e('WordPress Environment', 'phox') ?></th>
                        <th><?php esc_html_e('Value', 'phox') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?php esc_html_e( 'Home URL', 'phox' ); ?></td>
                        <td><?php x_wdes()->wdes_get_text ($this->data['home']); ?></td>
                    </tr>
                    <tr>
                        <td><?php esc_html_e( 'Site URL', 'phox' ); ?></td>
                        <?php if( $this->status['siteurl'] ): ?>
                            <td><?php x_wdes()->wdes_get_text ($this->data['siteurl']); ?></td>
                        <?php else: ?>
                            <td>
                                <span class="fas fa-close"></span>
                                <?php x_wdes()->wdes_get_text ($this->data['siteurl']); ?>
                                <span><?php esc_html_e('Home URL host must be the same as Site URL host.', 'phox') ?></span>
                            </td>
                        <?php endif; ?>

                    </tr>
                    <tr>
                        <td><?php esc_html_e( 'WP Version', 'phox' ); ?></td>
                        <td>
                            <?php if( $this->status['wp_version'] ): ?>
                                <span class="fas fa-check-circle"></span>
                                <?php x_wdes()->wdes_get_text ($this->data['wp_version']); ?>
                            <?php else: ?>
                                <span class="fas fa-close"></span>
                                <?php x_wdes()->wdes_get_text ($this->data['wp_version']); ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php esc_html_e( 'WP Multisite', 'phox' ); ?></td>
                        <td>
                            <?php if( $this->status['multisite'] ): ?>
                                <span><?php esc_html_e('Yes', 'phox'); ?></span>
                            <?php else: ?>
                                <span><?php esc_html_e('No', 'phox'); ?></span>
                            <?php endif; ?>
                        </td>
                    </tr>

                    <tr>
                        <td><?php esc_html_e( 'WP Debug', 'phox' ); ?></td>
                        <td>
                            <?php if( $this->status['debug'] ): ?>
                                <span><?php esc_html_e('Yes', 'phox'); ?></span>
                            <?php else: ?>
                                <span><?php esc_html_e('No', 'phox'); ?></span>
                            <?php endif; ?>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>