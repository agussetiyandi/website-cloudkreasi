<?php
if( ! defined( 'ABSPATH' ) ){
    exit; //Exit if accessed directly
}

?>

    <div class="home-theme-wdes">

        <!-- Intro -->
        <div class="row">
            <div class="intro-msg">
                <div class="title">
                    <h2><?php esc_html_e( 'Welcome to Phox!', 'phox' ); ?></h2>
                    <P><?php esc_html_e( 'To use the theme in best way, we suggest importing the demo first.', 'phox' ); ?></P>
                </div>
                <div class="logo">
                    <?php $logo_png = WDES_OPTIONS_URI.'img/logo.png'; ?>
                    <?php echo '<img src="'. esc_url($logo_png) .'" alt="'.esc_attr__('Phox', 'phox').'">';?>
                    <h2><?php echo esc_html( 'Version '. WDES_THEME_VERSION ); ?></h2>
                </div>
            </div>
        </div>

        <!-- Tabs -->
        <div class="row">
            <ul class="nav nav-pills mb-3 wdes-nav-tabs-w" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" id="pills-verify-tab" data-toggle="pill" href="#pills-verify" role="tab" aria-controls="pills-verify"
                        aria-selected="false">
                        <span class="fas fa-check-circle"></span><?php esc_html_e( 'Verify', 'phox' ); ?> </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-support-tab" data-toggle="pill" href="#pills-support" role="tab" aria-controls="pills-support"
                        aria-selected="true">
                        <span class="fas fa-life-ring"></span><?php esc_html_e( 'Support', 'phox' ); ?> </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" id="pills-system-tab" data-toggle="pill" href="#pills-system" role="tab" aria-controls="pills-system"
                        aria-selected="false">
                        <span class="fas fa-exclamation-circle"></span><?php esc_html_e( 'System Status', 'phox' ); ?></a>
                </li>

                <?php $tgm = $GLOBALS['tgmpa']; ?>
                <?php if( ! $tgm->is_tgmpa_complete() ): ?>
                <li class="nav-item">
                    <a class="nav-link" id="pills-plugins-tab"  href="admin.php?page=wdes-plugins">
                        <span class="fas fa-cog"></span><?php esc_html_e( 'Required Plugins', 'phox' ); ?>
                    </a>
                </li>
                <?php endif; ?>

                <?php if( class_exists( 'OCDI_Plugin' ) ): ?>
                <li class="nav-item">
                    <a class="nav-link" id="pills-plugins-tab"  href="themes.php?page=wdes-demo-import">
                        <span class="fas fa-cog"></span><?php esc_html_e( 'Demo Import', 'phox' ); ?> </a>
                </li>
                <?php endif; ?>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <!-- Verify -->
                <div class="tab-pane fade" id="pills-verify" role="tabpanel" aria-labelledby="pills-verify-tab">
                    <div class="content-bg-tab-x">
                        <div class="title-in">
                            <?php if( class_exists( 'Envato_Market' ) ) : ?>
                                <h2><?php esc_html_e( 'Please Activate The Theme To Receive All The Updates', 'phox' ); ?></h2>
                            <?php else: ?>
                                <h2><span class="fas fa-check-circle"></span><?php esc_html_e( 'Verify Phox', 'phox' ); ?> </h2>
                                <hr>
                                <p><?php esc_html_e( 'Please Install', 'phox' ); ?> <b><?php esc_html_e( 'Envato Market Plugin', 'phox' ); ?></b></p>
                                <div class="register-api">
                                    <a href="admin.php?page=wdes-plugins"><?php esc_html_e( 'Register', 'phox' ); ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <!-- Support -->
                <div class="tab-pane fade" id="pills-support" role="tabpanel" aria-labelledby="pills-support-tab">
                    <div class="content-bg-tab-x">
                        <div class="title-in">
                            <h2>
                                <span class="fas fa-life-ring"></span><?php esc_html_e( 'Support Center', 'phox' ); ?> </h2>
                            <hr>
                            <p><?php esc_html_e( 'Below are all the resources we offer in our support center.', 'phox' ); ?></p>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <!-- Docs -->
                                <div class="block-docs">
                                    <a target="_blank" href="https://phox.whmcsdes.com/docs/">
                                        <i class="far fa-file"></i>
                                        <h2><?php esc_html_e( 'Documentation', 'phox' ); ?></h2>
                                        <p><?php esc_html_e( 'This is the place to go to reference different aspects of the theme.', 'phox' ); ?></p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <!-- Docs -->
                                <div class="block-docs">
                                    <a target="_blank" href="https://www.whmcsdes.com/ca/submitticket.php">
                                        <i class="fas fa-headphones"></i>
                                        <h2><?php esc_html_e( 'Ask our experts', 'phox' ); ?></h2>
                                        <p><?php esc_html_e( 'Need one-to-one assistance? Get in touch with our Support team.', 'phox' ); ?></p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <!-- Docs -->
                                <div class="block-docs">
                                    <a target="_blank" href="http://themeforest.net/user/whmcsdes/">
                                        <i class="fas fa-comments"></i>
                                        <h2><?php esc_html_e( 'Comments', 'phox' ); ?></h2>
                                        <p><?php esc_html_e( 'You can use ThemeForest Comments to ask about anything.', 'phox' ); ?></p>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- System -->
                <?php include_once WDES_ADMIN_DIR . '/templates-parts/status.php'; ?>
            </div>
        </div>

    </div>