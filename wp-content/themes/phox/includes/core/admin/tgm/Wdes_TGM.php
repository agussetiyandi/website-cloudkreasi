<?php

namespace Phox\core\admin\tgm;


class Wdes_TGM {

    public $plugins = array(

	    array(
		    'name'               => 'Phox Host',
		    'slug'               => 'phox-host',
		    'source'             => WDES_INC_DIR . '/core/admin/tgm/plugins/phox-host.zip',
		    'required'           => true,
		    'version'            => '1.6.9'
	    ),

        array(
            'name'		  => 'Elementor',
            'slug'     	=> 'elementor',
            'required' 	=> true,
        ),

        array(
            'name'     	=> 'Contact Form 7',
            'slug'     	=> 'contact-form-7',
            'required' 	=> true,
        ),

        array(
            'name'     	=> 'One Click Demo Import',
            'slug'     	=> 'one-click-demo-import',
            'required' 	=> true,

        ),

        array(
            'name'     	=> 'WHMCS Bridge',
            'slug'     	=> 'whmcs-bridge',
            'required' 	=> true,
        ),

        array(
            'name'               => 'Envato Market',
            'slug'               => 'envato-market',
            'source'             => 'https://envato.github.io/wp-envato-market/dist/envato-market.zip',
            'required'           => true,
            'version'            => '2.0.1',
            'external_url'       => 'https://envato.com/market-plugin/',
        )

    );

    /**
     * Constructor
     */
    public function __construct(){

        if( class_exists( 'TGM_Plugin_Activation' ) ){
            return false;
        }

        include_once 'class-tgm-plugin-activation.php';


        // TGMPA registraton and configuration
        add_action( 'tgmpa_register', array( $this, 'tgmpa_register' ) );


	    $this->plugins = $this->get_premium_plugin();


    }

    /**
     * TGMPA register action
     */
    public function tgmpa_register(){

        $config = array(

            'id'           	=> 'wdes-tgmpa',        		// Unique ID for hashing notices for multiple instances of TGMPA.
            'menu'         	=> 'wdes-plugins', 				// Menu slug.
            'parent_slug'  	=> 'phox',				// Parent menu slug.
            'capability'   	=> 'edit_theme_options',    	// Capability needed to view plugin install page, should be a capability associated with the parent menu used.
            'has_notices'  	=> true,                    	// Show admin notices or not.
            'dismissable'  	=> true,                    	// If false, a user cannot dismiss the nag message.
            'is_automatic'	=> false,                   	// Automatically activate plugins after installation or not.
            'strings'      	=> array(
                'page_title'                      	=> esc_html__( 'Install Plugins', 'phox' ),
                'menu_title'                     	  => esc_html__( 'Install Plugins', 'phox' ),
                'installing'                      	=> esc_html__( 'Installing Plugin: %s', 'phox' ), // %s = plugin name.
                'oops'                            	=> esc_html__( 'Something went wrong with the plugin server.', 'phox' ),
                'notice_can_install_required'     	=> _n_noop( 'The Phox theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'phox' ),
                'notice_can_install_recommended'  	=> _n_noop( 'The Phox theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'phox' ),
                'notice_cannot_install'           	=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'phox' ),
                'install_link'                    	=> _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'phox' ),
                'return'                          	=> esc_html__( 'Return to Required Plugins Installer', 'phox' ),
                'plugin_activated'                	=> esc_html__( 'Plugin activated successfully.', 'phox' ),
                'complete'                        	=> esc_html__( 'All plugins installed and activated successfully. %s', 'phox' ), // %s = dashboard link.
                'nag_type'                        	=> 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
            ),

        );

        tgmpa( $this->plugins, $config );

    }

    public function get_premium_plugin(){

    	$plugins = get_site_transient( 'wdes_plugin' );
    	if( ! $plugins ){
    		$plugins = $this->update_premium_plugin();
	    }


	    if( ! $plugins ){
    		return $this->plugins;
	    }
	    return array_merge( $this->plugins, $plugins );

    }

    public function update_premium_plugin(){

    	if( get_site_transient( 'wdes_update_plugin' ) ) {
		    return false;
	    }

	    set_site_transient( 'wdes_update_plugin', 1, HOUR_IN_SECONDS );

    	$args = [
    		'user-agent'  => 'WordPress/'. get_bloginfo('version') .'; '.network_site_url(),
		    'timeout'     => 30,
	    ];

    	$response = wp_remote_get( 'https://whmcsdes.com/phoxplugin/plugins.php?version='.WDES_THEME_VERSION, $args );

    	if( is_wp_error( $response ) ){
    		return $response ;
	    }

	    $date = json_decode( wp_remote_retrieve_body( $response ), true );

	    $plugins = $date;

	    if( ! $plugins ) {
	    	return false;
	    }

	    set_site_transient( 'wdes_plugin', $plugins, HOUR_IN_SECONDS );

		return $plugins;

    }

}
new Wdes_TGM();


