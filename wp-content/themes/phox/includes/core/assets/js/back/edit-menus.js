/**
 * Wdes backend menu extension
 *
 * @author WHMCSDES
 */

(function ($, w) {
  "use strict";

  var WdesMegaMenu = function () {
    this.navMenuList = w.wpNavMenu.menuList;

    this.updataitems();
    this.navMenuList.on("sortstop", this.update.bind(this));

    var superRegisterChange = wpNavMenu.addItemToMenu,
      that = this;
    wpNavMenu.addItemToMenu = function (menuItem, procesMethod, callback) {
      if (callback) {
        var _callback = callback;
        callback = function () {
          _callback();
          that.updataitems(true);
        };
      }

      that.updataitems();
      return superRegisterChange.call(this, menuItem, procesMethod, callback);
    };
  };

  //initialize
  var instance;
  WdesMegaMenu.init = function () {
    if (instance) {
      return instance;
    } else {
      return (instance = new WdesMegaMenu());
    }
  };

  var proto = WdesMegaMenu.prototype;

  proto.updataitems = function (newItems) {
    this.navMenuList.find(".menu-item").each(
      function (index, item) {
        var $item = $(item);
        if (newItems) {
          if (!$item.data("notNew")) {
            this.menuitem.apply(this, arguments);
            this.iconPickerInit(item);
            $item.data("notNew", true);
          }
        } else {
          this.iconPickerInit(item);
          this.menuitem.apply(this, arguments);
        }
      }.bind(this)
    );
  };

  proto.update = function (event, data) {
    var $item = data.item;
    setTimeout(
      function () {
        this.menuitem(0, $item, true);
      }.bind(this),
      10
    );
  };

  proto.menuitem = function (index, item, checkChildren, values) {
    var $item = $(item),
      depth,
      parentType;

    depth = parseInt($item[0].className.match(/-depth-(\d+)/)[1]);

    if (depth > 0) {
      parentType = $item
        .prevAll(".menu-item-depth-" + (depth - 1))
        .first()
        .data("type");
    }

    $item
      .find("p[class*=wdes-mega-setting]")
      .each(this.checkOption.bind(this, depth));

    $item.find(".wdes-mega-setting-mega_widgets").css("display", "none");
    $item.find(".wdes-mega-setting-sec_text").css("display", "none");

    $item.data("type", "classic");

    switch (depth) {
      case 0:
        var $megaCheckbox = $item.find(
            '.wdes-mega-setting-mega  input[type="checkbox"]'
          ),
          $megaType = $item.find(".wdes-mega-setting-mega_type  select");

        if ($megaCheckbox.length) {
          if (!$megaCheckbox.data("eventAdded")) {
            $megaCheckbox
              .on(
                "change",
                function () {
                  this.menuitem(0, $item, true);
                }.bind(this)
              )
              .data("eventAdded", true);
          }

          if ($megaType.length) {
            values = $megaType[0].value;
            if (!$megaType.data("eventAdded")) {
              $megaType
                .on(
                  "change",
                  function () {
                    this.menuitem(0, $item, true, $megaType[0].value);
                  }.bind(this)
                )
                .data("eventAdded", true);
            }
          }

          if ($megaCheckbox[0].checked) {
            $item.find(".wdes-mega-setting-mega_type").css("display", "");
            $item.find(".wdes-mega-setting-col_num").css("display", "none");
            $item
              .find(".wdes-mega-setting-mega_template")
              .css("display", "none");
            $item.data("type", "mega");

            if (values === "template") {
              $item.find(".wdes-mega-setting-mega_template").css("display", "");
              $item.find(".wdes-mega-setting-col_num").css("display", "none");
              $item.data("type", "template");
            } else if (values === "with_widget") {
              $item.find(".wdes-mega-setting-col_num").css("display", "");
              $item
                .find(".wdes-mega-setting-mega_template")
                .css("display", "none");
              $item.data("type", "widget");
            } else {
              $item.find(".wdes-mega-setting-col_num").css("display", "");
              $item
                .find(".wdes-mega-setting-mega_template")
                .css("display", "none");
            }
          } else {
            $item.find(".wdes-mega-setting-mega_type").css("display", "none");
            $item.find(".wdes-mega-setting-col_num").css("display", "none");
            $item
              .find(".wdes-mega-setting-mega_template")
              .css("display", "none");
          }
        }
        break;
      case 1:
        if (parentType === "widget") {
          $item.find(".wdes-mega-setting-mega_widgets").css("display", "");
        }

        if (parentType === "mega") {
          $item.find(".wdes-mega-setting-sec_text").css("display", "");
        }
        break;
    }

    if (checkChildren) {
      $.each($item.childMenuItems(), this.menuitem.bind(this));
    }

    menu_color_picker(item);
  };

  proto.checkOption = function (depth, index, options) {
    var $option = $(options),
      minDepth = $option.data("depth").min,
      maxDepth = $option.data("depth").max;
    $option.css(
      "display",
      depth <= maxDepth && depth >= minDepth ? "" : "none"
    );
  };

  proto.iconPickerInit = function (target) {
    $(target)
      .find(
        "> .menu-item-settings > .wdes-mega-setting-icon > label > .wdes-iconpicker-input"
      )
      .iconpicker({
        title: false, // Popover title (optional) only if specified in the template
        selected: false, // use this value as the current item and ignore the original
        defaultValue: false, // use this value as the current item if input or element value is empty
        placement: "bottomLeft", // (has some issues with auto and CSS). auto, top, bottom, left, right
        collision: "none", // If true, the popover will be repositioned to another position when collapses with the window borders
        animation: true, // fade in/out on show/hide ?
        //hide iconpicker automatically when a value is picked. it is ignored if mustAccept is not false and the accept button is visible
        hideOnSelect: false,
        showFooter: false,
        searchInFooter: false, // If true, the search will be added to the footer instead of the title
        mustAccept: false, // only applicable when there's an iconpicker-btn-accept button in the popover footer
        selectedCustomClass: "bg-primary", // Appends this class when to the selected item
        inputSearch: false, // use the input as a search box too?
        container: false, //  Appends the popover to a specific element. If not set, the selected element or element parent is used
        component: ".input-group-addon,.iconpicker-component", // children component jQuery selector or object, relative to the container element
        // Plugin templates:
        templates: {
          popover:
            '<div class="iconpicker-popover popover">' +
            '<div class="popover-title"></div><div class="popover-content"></div></div>',
          footer: '<div class="popover-footer"></div>',
          buttons:
            '<button class="iconpicker-btn iconpicker-btn-cancel btn btn-default btn-sm">Cancel</button>' +
            ' <button class="iconpicker-btn iconpicker-btn-accept btn btn-primary btn-sm">Accept</button>',
          search:
            '<input type="search" class="form-control iconpicker-search" placeholder="Type to filter" />',
          iconpicker:
            '<div class="iconpicker"><div class="iconpicker-items"></div></div>',
          iconpickerItem:
            '<a role="button" href="javascript:;" class="iconpicker-item"><i></i></a>',
        },
      });
  };

  //init after Dom get ready
  $(function () {
    WdesMegaMenu.init();
  });
})(jQuery, window);
