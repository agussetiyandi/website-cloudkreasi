<?php
namespace Phox\core;

use Phox\helpers;

/**
 * Head Function
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */
class WDES_Head {
	/**
	 * Instance.
	 *
	 * @var Wdes_Head
	 */
	protected static $_instance = null;

	/**
	 * Get Wdes_Head instance .
	 *
	 * @return Wdes_Head
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

	}

	/**
	 * Constructor.
	 */
	public function __construct() {

		// Enqueue Styles and Script and Icons.
		add_action( 'wp_enqueue_scripts', array( $this, 'wdes_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'wdes_scripts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'wdes_icons' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'wdes_g_fonts' ) );
		add_action( 'enqueue_block_editor_assets', array( $this, 'phox_gutenberg_styles' ) );
		
		// IE fix.
		add_action( 'wp_enqueue_scripts', array( $this, 'wdes_fix_ie' ) );

		// Dynamic Style.
		add_action( 'wp_head', array( $this, 'wdes_style_dynamic' ) );

		// Seo.
		add_action( 'wdes_seo', array( $this, 'wdes_seo' ) );

		// Custom File.
		add_action( 'wp_footer', array( $this, 'wdes_script_custom' ), 100 );
		add_action( 'wp_head', array( $this, 'wdes_style_custom' ) );
		add_action('wp_head', array( $this, 'wdes_styles_custom_font' ));

		$this->wdes_content_width();

	}

	/**
	 * SEO
	 */
	public function wdes_seo() {
		if ( wdes_opts_get( 'use-seo-fields' ) ) {

			// description.
			if ( wdes_opts_get( 'use-seo-fields' ) && get_post_meta( x_wdes()->post_ID(), 'general-meta-description', true ) ) {
				echo '<meta name="description" content ="' . stripslashes( get_post_meta( wdes_post_ID(), 'general-meta-description', true ) ) . '" />' . "\n";
			} elseif ( wdes_opts_get( 'meta-description' ) ) {
				echo '<meta name="description" content ="' . stripslashes( wdes_opts_get( 'meta-description' ) ) . '" />' . "\n";
			}

			// keywords.
			if ( wdes_opts_get( 'use-seo-fields' ) && get_post_meta( x_wdes()->post_ID(), 'general-meta-keyword', true ) ) {
				echo '<meta name="keywords" content ="' . stripslashes( get_post_meta( wdes_post_ID(), 'general-meta-keyword', true ) ) . '" />' . "\n";
			} elseif ( wdes_opts_get( 'meta-keyword' ) ) {
				echo '<meta name="keywords" content ="' . stripslashes( wdes_opts_get( 'meta-keyword' ) ) . '" />' . "\n";
			}
		}

	}


	/**
	 * Styles.
	 */
	public function wdes_styles() {
		// vendor Styles.
		wp_enqueue_style( 'bootstrap', WDES_ASSETS_URI . '/css/bootstrap.min.css', false, WDES_THEME_VERSION );

		// General Styles.
		wp_enqueue_style( 'wdes-style', WDES_ASSETS_URI . '/css/style.css', false, WDES_THEME_VERSION );
		wp_enqueue_style( 'wdes-responsive', WDES_ASSETS_URI . '/css/responsive.css', false, WDES_THEME_VERSION );

		// Dynamic Style.
		//Fix bug with elementor
		wp_add_inline_style( 'wdes-style', $this->wdes_style_dynamic() );

	}

	/**
	 * Script.
	 */
	public function wdes_scripts() {

		// vendor Scripts.
		wp_enqueue_script( 'popper', WDES_ASSETS_URI . '/js/popper.min.js', array( 'jquery' ), WDES_THEME_VERSION, true );

		wp_enqueue_script( 'ammap', WDES_ASSETS_URI . '/js/ammap.js', array( 'jquery' ), WDES_THEME_VERSION, true );
		wp_enqueue_script( 'worldLow', WDES_ASSETS_URI . '/js/worldLow.js', array( 'jquery' ), WDES_THEME_VERSION, true );
		wp_enqueue_script( 'jquery-plugin', WDES_ASSETS_URI . '/js/jquery.plugin.js', array( 'jquery' ), WDES_THEME_VERSION, true );
		wp_enqueue_script( 'jquery-countdown', WDES_ASSETS_URI . '/js/jquery.countdown.js', array( 'jquery' ), WDES_THEME_VERSION, true );
		//Fix bridge menu bug
		if( ! helpers::is_bridge_default_page( get_the_ID() ) ){
			wp_enqueue_script( 'bootstrap', WDES_ASSETS_URI . '/js/bootstrap.min.js', array( 'jquery' ), WDES_THEME_VERSION, true );
		}
		wp_enqueue_script( 'wdes-custom-script', WDES_ASSETS_URI . '/js/custom-script.js', array( 'jquery' ), WDES_THEME_VERSION, true );
		// General Scripts.
		wp_enqueue_script( 'wdes-plugins', WDES_ASSETS_URI . '/js/plugins.js', array( 'jquery' ), WDES_THEME_VERSION, true );

		//Chat platforms user data
		$this->chat_localize_script();


		// Comment replay js.
		if ( is_singular() && comments_open() ) {
			wp_enqueue_script( 'comment-reply' );
		}

		// Google Map.
		$wdes_google_api = wdes_opts_get( 'google_maps_api_key' );
		if ( isset( $wdes_google_api ) && ! empty( $wdes_google_api ) ) {
			wp_enqueue_script( 'wdes-map', esc_url( set_url_scheme('https://maps.googleapis.com/maps/api/js?key='.$wdes_google_api) ), null, null, TRUE );
		}

		// Coming soon.
		if ( wdes_opts_get( 'comingsoon-redirect' ) == true ) {

			$script_data = $this->wdes_comingsoon_script();
			wp_add_inline_script( 'wdes-plugins', $script_data );

		}

	}

	/**
	 * Icon.
	 */
	public function wdes_icons() {
		wp_enqueue_style( 'elementor-icons-shared-0-css', WDES_ASSETS_URI . '/css/all.min.css', false, WDES_THEME_VERSION );
	}

	/**
	 * Fonts
	 */
	public function wdes_g_fonts() {

		$fonts_url = '';

		/* Translators: If there are characters in your language that are not
			* supported by Open Sans, translate this to 'off'. Do not translate
			* into your own language.
			*/
		$open_sans = _x( 'on', 'Open Sans font: on or off', 'phox' );

		/* Translators: If there are characters in your language that are not
		 * supported by Poppins, translate this to 'off'. Do not translate
		 * into your own language.
		 */
		$poppins = _x( 'on', 'Poppins font: on or off', 'phox' );

		/* Translators: If there are characters in your language that are not
		 * supported by Karla, translate this to 'off'. Do not translate
		 * into your own language.
		 */
		$karla = _x( 'on', 'Karla font: on or off', 'phox' );

		/* Translators: If there are characters in your language that are not
		 * supported by Montserrat, translate this to 'off'. Do not translate
		 * into your own language.
		 */
		$montserrat = _x( 'on', 'Montserrat font: on or off', 'phox' );


		$font_families = array();

		if ( 'off' !== $open_sans ) {
			$font_families[] = 'Open Sans:wght@300;400;600;700';
		}

		if ( 'off' !== $poppins ) {
			$font_families[] = 'Poppins:wght@200;400;600;700';
		}



		// Panel Fonts

		$main_menu = wdes_opts_get('typography-main-menu');

		if( ! in_array(  $main_menu['fonts'].':wght@'.$main_menu['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $main_menu['fonts'].':wght@'.$main_menu['weight-font'];
		}

		$sub_menu = wdes_opts_get('typography-sub-menu');

		if( ! in_array(  $sub_menu['fonts'].':wght@'.$sub_menu['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $sub_menu['fonts'].':wght@'.$sub_menu['weight-font'];
		}

		$mega_menu = wdes_opts_get('typography-mega-menu');

		if( ! in_array(  $mega_menu['fonts'].':wght@'.$mega_menu['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $mega_menu['fonts'].':wght@'.$mega_menu['weight-font'];
		}

		$blog_bread = wdes_opts_get('typo_blog_bread_title');

		if( ! in_array(  $blog_bread['fonts'].':wght@'.$blog_bread['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $blog_bread['fonts'].':wght@'.$blog_bread['weight-font'];
		}

		$blog_t = wdes_opts_get('typography-blog-t');

		if( ! in_array(  $blog_t['fonts'].':wght@'.$blog_t['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $blog_t['fonts'].':wght@'.$blog_t['weight-font'];
		}

		$blog_t_s = wdes_opts_get('typography-blog-t-s');

		if( ! in_array(  $blog_t_s['fonts'].':wght@'.$blog_t_s['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $blog_t_s['fonts'].':wght@'.$blog_t_s['weight-font'];
		}

		$body_p = wdes_opts_get('typography-body-p');

		if( ! in_array(  $body_p['fonts'].':wght@'.$body_p['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $body_p['fonts'].':wght@'.$body_p['weight-font'];
		}

		$body_blockquote = wdes_opts_get('typography-body-blockquote');

		if( ! in_array(  $body_blockquote['fonts'].':wght@'.$body_blockquote['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $body_blockquote['fonts'].':wght@'.$body_blockquote['weight-font'];
		}

		$body_links = wdes_opts_get('typography-body-links');

		if( ! in_array(  $body_links['fonts'].':wght@'.$body_links['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $body_links['fonts'].':wght@'.$body_links['weight-font'];
		}

		$h1 = wdes_opts_get('heading-h1');

		if( ! in_array(  $h1['fonts'].':wght@'.$h1['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $h1['fonts'].':wght@'.$h1['weight-font'];
		}

		$h2 = wdes_opts_get('heading-h2');

		if( ! in_array(  $h2['fonts'].':wght@'.$h2['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $h2['fonts'].':wght@'.$h2['weight-font'];
		}

		$h3 = wdes_opts_get('heading-h3');

		if( ! in_array(  $h3['fonts'].':wght@'.$h3['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $h3['fonts'].':wght@'.$h3['weight-font'];
		}

		$h4 = wdes_opts_get('heading-h4');

		if( ! in_array(  $h4['fonts'].':wght@'.$h4['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $h4['fonts'].':wght@'.$h4['weight-font'];
		}

		$h5 = wdes_opts_get('heading-h5');

		if( ! in_array(  $h5['fonts'].':wght@'.$h5['weight-font'], $font_families) && !empty($main_menu) ){
			$font_families[] = $h5['fonts'].':wght@'.$h5['weight-font'];
		}

		$h6 = wdes_opts_get('heading-h6');

		if( ! in_array(  $h6['fonts'].':wght@'.$h6['weight-font'], $font_families) && !empty($main_menu) ){

			$font_families[] = $h6['fonts'].':wght@'.$h6['weight-font'];
		}


		$query_args = array(
			'family' => implode( '&family=', $font_families ) ,
			'display' => urlencode( 'swap' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css2' );


		wp_enqueue_style( 'google-fonts', esc_url_raw( $fonts_url ), array(), null);

	}

	/**
	 * Load Custom Fonts
	 */
	public function wdes_styles_custom_font () {

		if ($font_custom = wdes_opts_get('font-custom')) {

			$font_custom_woff  = wdes_opts_get('font-custom-woff');
			$font_custom_woff2 = wdes_opts_get('font-custom-woff2');
			$font_custom_ttf   = wdes_opts_get('font-custom-ttf');
			$font_custom_otf   = wdes_opts_get('font-custom-otf');
			$font_custom_svg   = wdes_opts_get('font-custom-svg');

			echo '<!-- style | custom font -->'."\n";
			echo '<style id="wdes-dnmc-font-css">'."\n";
				echo '@font-face{';
					echo 'font-family:"'. $font_custom .'";';
					echo 'src:';
					if ($font_custom_woff2) {
						echo 'url("'. $font_custom_woff2 .'") format("woff2")';
					}

					if ($font_custom_woff && $font_custom_ttf && $font_custom_woff2 && $font_custom_svg && $font_custom_otf) {
						echo ',';
					}

					if ($font_custom_woff) {
						echo 'url("'. $font_custom_woff .'") format("woff")';
					}

					if ($font_custom_woff && $font_custom_ttf && $font_custom_woff2 && $font_custom_svg && $font_custom_otf) {
						echo ',';
					}

					if ($font_custom_ttf) {
						echo 'url("'. $font_custom_ttf .'") format("truetype")';
					}

					if ($font_custom_woff && $font_custom_ttf && $font_custom_woff2 && $font_custom_svg && $font_custom_otf) {
						echo ',';
					}

					if ($font_custom_otf) {
						echo 'url("'. $font_custom_otf .'") format("opentype")';
					}

					if ($font_custom_woff && $font_custom_ttf && $font_custom_woff2 && $font_custom_svg && $font_custom_otf) {
						echo ',';
					}

					if ($font_custom_svg) {
						echo 'url("'. $font_custom_svg .'") format("svg")';
					}
					echo ';';
					echo 'font-weight:normal;';
					echo 'font-style:normal';
				echo '}'."\n";
			echo '</style>'."\n";

		}

		if ($font_custom_2 = wdes_opts_get('font-custom-2')) {

			$font_custom_2_woff  = wdes_opts_get('font-custom-2-woff');
			$font_custom_2_woff2 = wdes_opts_get('font-custom-2-woff2');
			$font_custom_2_ttf   = wdes_opts_get('font-custom-2-ttf');
			$font_custom_2_otf   = wdes_opts_get('font-custom-2-otf');
			$font_custom_2_svg   = wdes_opts_get('font-custom-2-svg');

			echo '<!-- style | custom font -->'."\n";
			echo '<style id="wdes-dnmc-font-2-css">'."\n";
				echo '@font-face{';
					echo 'font-family:"'. $font_custom_2 .'";';
					echo 'src:';

					if ($font_custom_2_woff) {
						echo 'url("'. $font_custom_woff2 .'") format("woff2")';
					}

					if ($font_custom_2_woff && $font_custom_2_ttf && $font_custom_2_woff2 && $font_custom_2_svg && $font_custom_2_otf) {
						echo ',';
					}

					if ($font_custom_2_woff2) {
						echo 'url("'. $font_custom_woff .'") format("woff")';
					}

					if ($font_custom_2_woff && $font_custom_2_ttf && $font_custom_2_woff2 && $font_custom_2_svg && $font_custom_2_otf) {
						echo ',';
					}

					if ($font_custom_2_ttf) {
						echo 'url("'. $font_custom_2_ttf .'") format("truetype")';
					}

					if ($font_custom_2_woff && $font_custom_2_ttf && $font_custom_2_woff2 && $font_custom_2_svg && $font_custom_2_otf) {
						echo ',';
					}

					if ($font_custom_2_otf) {
						echo 'url("'. $font_custom_2_otf .'") format("opentype")';
					}

					if ($font_custom_2_woff && $font_custom_2_ttf && $font_custom_2_woff2 && $font_custom_2_svg && $font_custom_2_otf) {
						echo ',';
					}

					if ($font_custom_2_svg) {
						echo 'url("'. $font_custom_svg .'") format("svg")';
					}

					echo ';';
					echo 'font-weight:normal;';
					echo 'font-style:normal';
				echo '}'."\n";
			echo '</style>'."\n";

		}
	}

	/**
	* Enqueue WordPress theme styles within Gutenberg.
	*/
	function phox_gutenberg_styles() {
		// Load the theme styles within Gutenberg.
		wp_enqueue_style( 'bootstrap', WDES_ASSETS_URI . '/css/gutenberg.css', false, WDES_THEME_VERSION, 'all' );
	}
	
	/**
	 * Fix IE
	 */
	public function wdes_fix_ie() {
		global $is_IE;

		if ( ! $is_IE ) {
			return '';
		}

		wp_enqueue_script( 'html5shiv', 'https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js', array( 'jquery' ), WDES_THEME_VERSION, true );
		wp_enqueue_script( 'bootstrap-respond', 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js', array( 'jquery' ), WDES_THEME_VERSION, true );

	}

	/**
	 * Chat Platforms
	 */

	public function chat_localize_script ( ) {

			$platform = wdes_opts_get('available-platform');
			$intercom = wdes_opts_get('platform-api-intercom') ;
			$tawk = wdes_opts_get('platform-api-tawk') ;

			//check the type of platform
			if( $platform === 'intercom' ) {

					//InterCom
					if ( ! empty( $intercom ) ) {

						wp_localize_script( 'wdes-plugins', 'intercomUser', array( 'app_id' => $intercom ) );

					}

			}elseif ( $platform === 'tawk' ) {

					//Tawk
					if ( !empty( $tawk ) ) {

						wp_localize_script('wdes-plugins', 'tawkUser', array('chatLink' => $tawk ) );

					}

			}else {

					return false;

			}


	}

	/**
	 * Coming Soon script.
	 */
	public function wdes_comingsoon_script() {

		$output          = 'jQuery( document ).ready(function() {';
			$output       .= 'var austDay = new Date("'. wdes_opts_get( 'comingsoon-counter', '' ) .'");';
				$output     .= 'jQuery("#defaultCountdown").countdown({';
					$output .= 'until: austDay';
				$output     .= '});';
		$output         .= '});';
		return $output;
	}

	/**
	 * Style dynamic.
	 */
	public function wdes_style_dynamic() {

		ob_start();

		include_once WDES_INC_DIR . '/core/style.php';

		$css = ob_get_contents();

		ob_get_clean();

		return $this->wdes_minify_css( $css );

	}

	/**
	 * Minify Css
	 *
	 * @param string $css The all css that will minifying
	 *
	 * @return mixed
	 */
	public function wdes_minify_css( $css ) {

		// remove traling semicolon of selector's last property.
		$css = preg_replace( '/;[\s\r\n\t]*?}[\s\r\n\t]*/ims', "}\r\n", $css );

		// remove any whitespace between semicolon and property-name.
		$css = preg_replace( '/;[\s\r\n\t]*?([\r\n]?[^\s\r\n\t])/ims', ';$1', $css );

		// remove any whitespace surrounding property-colon.
		$css = preg_replace( '/[\s\r\n\t]*:[\s\r\n\t]*?([^\s\r\n\t])/ims', ':$1', $css );

		// remove any whitespace surrounding selector-comma.
		$css = preg_replace( '/[\s\r\n\t]*,[\s\r\n\t]*?([^\s\r\n\t])/ims', ',$1', $css );

		// remove any whitespace surrounding opening parenthesis.
		$css = preg_replace( '/[\s\r\n\t]*{[\s\r\n\t]*?([^\s\r\n\t])/ims', '{$1', $css );

		// remove any whitespace between numbers and units.
		$css = preg_replace( '/([\d\.]+)[\s\r\n\t]+(px|em|pt|%)/ims', '$1$2', $css );

		// shorten zero-values.
		$css = preg_replace( '/([^\d\.]0)(px|em|pt|%)/ims', '$1', $css );

		// constrain multiple whitespaces.
		$css = preg_replace( '/\p{Zs}+/ims', ' ', $css );

		// remove newlines.
		$css = str_replace( array( "\r\n", "\r", "\n" ), '', $css );

		return $css;
	}

	/**
	 * Custom Script.
	 */
	public function wdes_script_custom() {

		$custom_js = wdes_opts_get( 'custom-js' );
		if ( $custom_js ) {
			echo '<!-- script | custom js -->' . "\n";
			echo '<script id="wdes-dnmc-custom-js">' . "\n";
				echo '//<![CDATA[' . "\n";
					x_wdes()->wdes_get_text( $custom_js . "\n" );
				echo '//]]>' . "\n";
			echo '</script>' . "\n";
		}

	}

	/**
	 * Custom Style.
	 */
	public function wdes_style_custom() {

		$custom_css = wdes_opts_get( 'custom-css' );

		if ( $custom_css ) {
			echo '<!-- style | custom css -->' . "\n";
			echo '<style id="wdes-dnmc-custom-css">' . "\n";
				echo esc_attr( $custom_css . "\n" );
			echo '</style>' . "\n";
		}

	}

	/**
	 * Content Width
	 */
	public function wdes_content_width() {

		if ( ! isset( $content_width ) ) {
			$content_width = '%100';
		}

		return $content_width;

	}

}

