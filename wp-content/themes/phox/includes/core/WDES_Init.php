<?php
namespace Phox\core;

/**
 * Initialize and Register Function
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */
class WDES_Init {
	/**
	 * Instance .
	 *
	 * @var WDES_Init
	 */
	protected static $_instance = null;

	/**
	 * Coming soon option .
	 *
	 * @var WDES_Init
	 */
	protected $coming_soon = null;

	/**
	 * Get WDES_Init instance .
	 *
	 * @return WDES_Init
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

	}

	/**
	 * Constructor.
	 */
	public function __construct() {

		$this->wdes_theme_support();
		$this->wdes_register_menu();

		// Add sidebars.
		add_action( 'widgets_init', array( $this, 'wdes_register_sidebars' ) );


		$this->wdes_html_content();

		$this->coming_soon = wdes_opts_get( 'comingsoon-redirect' );

		//render coming soon page
		if ( $this->coming_soon ) {

			if( ! current_user_can('administrator') ){

				add_action( 'template_redirect', [$this, 'render_comingsoon_page' ] );
			}


		}

		add_filter( 'theme_auto_update_setting_template', [$this, 'auto_update_setting_template'] );

		add_filter( 'upload_mimes', [ $this, 'mime_types' ] );


	}

	/**
	 * Theme support.
	 */
	public function wdes_theme_support() {

		// Add theme support
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'custom-logo', [ 'height' => 31 ]);

		//add image size
		add_image_size( 'wdes-image-widget', 150, 150, true );
		add_image_size( 'wdes-image-small', 220, 150, true );
		add_image_size( 'wdes-image-large', 390, 250, true );
		add_image_size( 'wdes-image-post',  780, 405, true );
		add_image_size( 'wdes-image-grid',  780, 500, true );
		add_image_size( 'wdes-image-full',  1170, 610,true );


	}

	/**
	 * Register Menu.
	 */
	public function wdes_register_menu() {
		register_nav_menu( 'main-menu', esc_html__( 'Primary Menu', 'phox' ) );
		register_nav_menu( 'header-secondary', esc_html__( 'Secondary Navigation', 'phox' ) );
		register_nav_menu( 'footer', esc_html__( 'Footer Navigation', 'phox' ) );
	}

	/**
	 * Register Sidebars.
	 */
	public function wdes_register_sidebars() {

		// Main sidebar.
		register_sidebar(
			array(
				'name'          => esc_html__( 'Sidebar', 'phox' ),
				'id'            => 'sidebar-main',
				'description'   => esc_html__( 'The main Sidebar that will show on blog', 'phox' ),
				'before_widget' => '<div id="%1$s" class="%2$s block-sidebar-function">',
				'after_widget'  => '</div>',
				'before_title'  => '<div class="wid-title"><h2>',
				'after_title'   => '</h2></div>',

			)
		);

		// Left sidebar.
		register_sidebar(
			array(
				'name'          => esc_html__( 'Left Sidebar', 'phox' ),
				'id'            => 'sidebar-left',
				'description'   => esc_html__( 'The Left Sidebar that you can use in page or blog', 'phox' ),
				'before_widget' => '<div id="%1$s" class="%2$s block-sidebar-function">',
				'after_widget'  => '</div>',
				'before_title'  => '<div class="wid-title"><h2>',
				'after_title'   => '</h2></div>',

			)
		);

		// Right sidebar.
		register_sidebar(
			array(
				'name'          => esc_html__( 'Right Sidebar', 'phox' ),
				'id'            => 'sidebar-right',
				'description'   => esc_html__( 'The Left Sidebar that you can use in page or blog', 'phox' ),
				'before_widget' => '<div id="%1$s" class="%2$s block-sidebar-function">',
				'after_widget'  => '</div>',
				'before_title'  => '<div class="wid-title"><h2>',
				'after_title'   => '</h2></div>',

			)
		);

		// Footer areas
		for ( $i = 1; $i <= 4; $i++ ) {
			register_sidebar(
				array(
					'name'          => esc_html__( 'Footer', 'phox' ) . '| No.' . $i,
					'id'            => 'footer-widget-' . $i,
					'description'   => esc_html__( 'Appears in the Footer section of the site', 'phox' ),
					'before_widget' => '<div id="%1$s" class="widget-footer %2$s" >',
					'after_widget'  => '</div>',
					'before_title'  => '<div class="wid-title"><h2>',
					'after_title'   => '</h2></div>',

				)
			);
		}

		// WooCommerce.
		if( x_wdes()->is_woocommerce_activated() ) {
			register_sidebar(
				array(
					'name'          => esc_html__( 'Shop Woocommerce', 'phox' ),
					'id'            => 'sidebar-shop-woocommerce',
					'description'   => esc_html__( 'WooCommerce widgets area', 'phox' ),
					'before_widget' => '<div id="%1$s" class="%2$s block-sidebar-function">',
					'after_widget'  => '</div>',
					'before_title'  => '<div class="wid-title"><h2>',
					'after_title'   => '</h2></div>',

				)
			);
		}


	}


	/**
	 * Html content.
	 */
	public function wdes_html_content() {

		add_action( 'wdes_after_container', [ \Phox\helpers::instance(), 'wdes_after_container' ], true );
		add_action( 'wdes_before_container', [ \Phox\helpers::instance(), 'wdes_before_container' ], true );

		// Archive.
		add_action( 'wdes_before_blog', [ \Phox\helpers::instance(), 'wdes_before_blog' ], true );

	}

	/**
	 * Upload mimes
	 */
	public function wdes_upload_mimes( $existing_mimes = array() ) {

		$existing_mimes['svg'] = 'image/svg+xml';
		return $existing_mimes;

	}

	public function remove_css_js_ver( $src ) {
		if ( strpos( $src, '?ver=' ) ) {
			$src = remove_query_arg( 'ver', $src );
		}
		return $src;
	}

	/**
	 * Render Coming Soon Page
	 *
	 * it will fire when comingsoon option is turn on from admin panel
	 * when this function run it will return template to all visitor except admin
	 *
	 * @since 1.3.6
	 * @return void
	 */
	public function render_comingsoon_page() {

		x_wdes()->wdes_get_tp( 'posts/coming', 'soon' );

		exit;

	}

	/**
	 * Admin Bar Menu
	 *
	 * add notice in admin bar for make admin know the coming soon option is active
	 * and only admin is can view the site
	 *
	 * it will stop to use it because themes must not add any entries to the admin bar
	 * and we will change 404 page way in theme
	 *
	 * @since 1.3.6
	 * @deprecated 1.4.3
	 * @return bool
	 */
	public function admin_bar_404_notice($str){

		global $wp_admin_bar;
		$option = $this->coming_soon;

		if( is_null( $option )  ) {

				return false;

		}

		$mag = '';

		if( $option ) {

			$mag = esc_html__('Coming Soon Mode Active', 'phox');

		}

		$wp_admin_bar->add_menu( [
			'id'	 		=> 'wdes-comingsoon-notice',
			'href' 		=> admin_url(),
			'parent'	=> 'top-secondary',
			'title'		=> $mag,
			'meta'		=> [ 'class' => 'wdes-comingsoon-active']
		] );

	}

	/**
	 * Disable Auto Update
	 *
	 * Stop wordpress to try auto update theme
	 *
	 * @since 1.7.1
	 * @return string
	 */
	function auto_update_setting_template( $template ) {
		$text = __( 'Auto-updates are not available for this theme.', 'phox' );

		return "<# if ( [ 'my-theme', 'phox' ].includes( data.id ) ) { #>
        <p>$text</p>
        <# } else { #>
        $template
        <# } #>";
	}

	/**
	 * Allow uploading font file types.
	 *
	 * @param array $mimes The mime types allowed.
	 * @access public
	 */
	public function mime_types( $mimes ) {

		$mimes['woff2'] = 'font/woff2';
		$mimes['woff']  = 'font/woff';
		$mimes['ttf']   = 'font/ttf';
		$mimes['otf']   = 'font/otf';
		$mimes['svg']   = 'font/svg';

		return $mimes;
	}



}

