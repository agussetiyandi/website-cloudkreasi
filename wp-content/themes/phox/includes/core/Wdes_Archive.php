<?php
namespace Phox\core;
use Phox_Host\Share_Button;

/**
 * Wdes_Archive
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */
class Wdes_Archive {

	/**
	 * Blog post content.
	 */
	public function blog_post_content() {

		if ( is_single() ) {

			$author = wdes_opts_get( 'single-post-author' );
			$is_date_active = wdes_opts_get( 'single-post-time' );
			$is_category_active = wdes_opts_get( 'single-post-category' );

			$this->post_author( $author, 'post' );

			$this->post_categories($is_category_active);

			$this->post_date( $is_date_active );

			$this->post_comment_count();


		}elseif ( is_page() ){

			return false ;

		} else {

			$author = wdes_opts_get( 'blog-author' );
			$date   = wdes_opts_get( 'blog-time' );

			$this->post_author( $author, 'blog' );
			$this->post_date( $date );
			$this->post_categories();
			$this->post_comment_count();

		}

	}

	/**
	 * Blog title Background
	 */
	public function blog_title_background(){

		$style = false;

		if( is_single() || is_page() ) {

			$feature_img = get_the_post_thumbnail_url();

			if( ! empty( $feature_img ) ){

				$style ='style="background-image: url('.$feature_img.');"';

			}


		}

		return $style;

	}

	/**
	 * Blog Title Class & Style
	 */

	public function blog_class_style(){

		$class = 'blog-title-top';

		$style = $this->blog_title_background();

		if( is_single() || is_page() ) {

			$class = 'blog-title-top single-feature-img';

		}

		return $result = 'class="'.$class.'" ' . $style;

	}

	/**
	 * Blog post head.
	 */
	public function blog_post_head() {

		if ( is_single() || is_page() ) {

			$is_image_active    = wdes_opts_get( 'single-post-image' );

			if ( is_null( $is_image_active ) ) {

				$this->post_image( $is_image_active );

			}

		} else {

			$image    = wdes_opts_get( 'blog-feature-img' );

			$category = wdes_opts_get( 'blog-category' );

			$this->post_image( $image );

			$this->post_category_label( $category );

		}

	}

	/**
	 * Post Content Navigation
	 */
	public function post_content_nav(){

		$this->post_author( null , 'post' );

		$this->post_categories();

		$this->post_date( null );

		$this->post_comment_count();


	}

	/**
	 * Post layout
	 *
	 * @return string
	 */
	public function posts_layout() {

		$layout = 'classic';
		$class  = 'col-sm-12 col-md-12 col-lg-6';

		if ( ! empty( $layout ) ) {

			if ( 'classic' === $layout ) {

				$class = 'article-item-box';

			}
		}

		return $class;
	}

	/**
	 * Blog Title.
	 *
	 * @param string $default The default data.
	 *
	 * @return mixed|null|string
	 */
	public function blog_title( $default = '' ) {

		$title  = wdes_opts_get( 'blog-page-title' );
		$result = $default;

		if ( ! empty( $title ) ) {

			$result = wdes_opts_get( 'blog-page-title' );

		}

		x_wdes()->wdes_get_text( $result );

	}

	/**
	 * Blog Description.
	 *
	 * @param string $default The default data.
	 *
	 * @return mixed|null|string
	 */
	public function blog_description( $default = '' ) {

		$title  = wdes_opts_get( 'blog-page-title' );
		$result = $default;

		if ( ! empty( $title ) ) {

			$result = wdes_opts_get( 'blog-page-description' ) ;

		}

		x_wdes()->wdes_get_text( $result );

	}

	/**
	 * Single Share Button
	 *
	 * Check if plugin is run and then fire the function
	 *
	 */
	public function single_share_button(){

		if( class_exists('Phox_Host\Share_Button' ) ) {

			$is_social_active = wdes_opts_get( 'single-socailshare' );

			Share_Button::post_share_social($is_social_active);

		}

	}


	/**
	 * Post image.
	 *
	 * @param string $image The Image.
	 */
	public function post_image( $image ) {

		if ( is_null( $image ) ) {

			if(is_single()){

				echo x_wdes()->wdes_thumbnail_post( 'post-large' );

			}else{

				echo x_wdes()->wdes_thumbnail_post( 'wdes-image-full' );

			}



		}

	}

	/**
	 * Post data.
	 *
	 * @param bool $date check if can show data or not.
	 */
	public function post_date( $date ) {

		if ( is_null( $date ) ) {

			$calendar_svg = WDES_ASSETS_URI.'/img/icons/blog-calendar.svg';

			echo '<div class="art-block-sub">';
				echo '<img class="icon-blog-outer" src="'.esc_url($calendar_svg).'">';
				echo '<span>' . get_the_date('M j, Y ') . '</span>';
			echo '</div>';
		}

	}

	/**
	 * Post author
	 *
	 * @param bool $author check if can show author or not.
	 * @param string $type which post_type will use this function
	 *
	 * @return void
	 */
	public function post_author( $author , $type ) {
		$author_id = get_the_author_meta( 'ID' );
		$option_image= ( $type === 'blog' ) ? wdes_opts_get('blog-feature-img') : wdes_opts_get('single-post-image') ;

		/**
		 * Fix the category icon style when the author is hide
		 * @since 1.3.4
		 */
		if( x_wdes()->wdes_is_thumbnail() && is_null($option_image)  ){

			$class_thumbnail = 'author-box';

		}else {

			$class_thumbnail = 'author-unbox';

		}

		if ( is_null( $author ) ) {
				$this->author_template( $author_id, $class_thumbnail );
		}
	}

	/**
	 * Author html template
	 *
	 * @param int $author_id get the author id
	 * @param string $thumbnail_class sit the class that will use when use thumbnail for post
	 * @since 1.3.4
	 *
	 * @return void
	 */
	public function author_template ( $author_id,  $thumbnail_class ) {

		//var that will use in template
		$class = 'art-block-sub ' . $thumbnail_class ;
		$author_post_url = esc_url( get_author_posts_url( $author_id ) );
		$author_avater = get_avatar( get_the_author_meta( 'user_email', $author_id ), 45 );
		$author_name = esc_html( get_the_author() );

		//html
		echo '<div class=" '. $class .' ">';
			echo '<a class="art-post-author" href="' . $author_post_url . '">'. $author_avater .'</a>';
			echo '<a class="art-post-author" href="' . $author_post_url . '"> ' . $author_name  . ' </a>';
		echo '</div>';

	}

	/**
	 * Post categories.
	 */
	public function post_categories($category = null){

		if(is_null($category)){

			if(!empty(x_wdes()->wdes_categories_link())){
				$category_svg = WDES_ASSETS_URI.'/img/icons/blog-category.svg';
				echo '<div class="art-block-sub"><img class="icon-blog-outer" src="'. esc_url($category_svg) .'">'. x_wdes()->wdes_categories_link().'</div>';
			}

		}

	}

	/**
	 * Post category Label.
	 *
	 * @param bool $category check if can show author or not.
	 */
	public function post_category_label( $category ) {

		if ( ! is_null( $category ) ) {

			echo x_wdes()->wdes_get_cat_label();

		}

	}

	public function post_comment_count(){

		$comment_svg = WDES_ASSETS_URI.'/img/icons/blog-comment.svg';

		echo '<div class="art-block-sub"><img class="icon-blog-outer" src="'. esc_url($comment_svg) .'"><a href="'. esc_url( get_comments_link() ) .'">'.get_comments_number_text('0', '1', '%').'</a></div>';


	}


	public function blog_board_img($size){

		$image = get_the_post_thumbnail_url(null, $size);
		$default_image = WDES_ASSETS_URI.'/img/no-photo.png';
		$background = ! empty( $image ) ? 'url('. $image .')' : 'url('. $default_image .')' ;

		return esc_attr('background-image: '.$background);

	}

	/**
	 * Board small box.
	 */
	public function blog_board_small_box($single_post) {

		#Import variables into the current symbol table from an $single_post
		extract($single_post);

		#html template
			echo '<div class="block-art sm-block">';
				echo '<a href="' . esc_url($pin_post_link) . '">';

					echo '<div class="post-img-outer-view" '. $pin_post_image .'></div>';
					echo '<div class="art-shadow"></div>';

					echo '<div class="post-info-inner-pin-details">';
						echo '<div class="post-date-pin">';
							echo '<span class="far fa-clock"></span>';
							echo '<span>' . esc_html($pin_post_date) . '</span>';
						echo '</div>';

						echo '<h2 class="title-art"> ' . esc_html($pin_post_title) . ' </h2>';
						echo '<p class="pin-posts-item-description">'.esc_html($pin_post_caption).'</p>';

					echo '</div>';

				echo '</a>';

		echo '</div>';

	}

	/**
	 * Blog board big box.
	 */
	public function blog_board_big_box($single_post) {

		#Import variables into the current symbol table from an $single_post
		extract($single_post);

		#html template
		echo '<div class="block-art top-post">';

			echo '<a href="' . esc_url($pin_post_link) . '">';

				echo '<div class="post-img-outer-view" '. $pin_post_image .'></div>';
				echo '<div class="art-shadow"></div>';

				echo '<div class="post-info-inner-pin-details">';
					echo '<div class="post-date-pin">';
						echo '<span class="far fa-clock"></span>';
						echo '<span>' . esc_html($pin_post_date) . '</span>';
					echo '</div>';

					echo '<h2 class="title-art"> ' . esc_html($pin_post_title) . ' </h2>';
					echo '<p class="pin-posts-item-description">'.esc_html($pin_post_caption).'</p>';

				echo '</div>';

			echo '</a>';

		echo '</div>';

	}

	/**
	 * Layout right blog board.
	 *
	 * @param int $count get the count.
	 */
	public function layout_right_blog_board( $limit_posts ) {

		foreach ($limit_posts as $key => $data) {

			if ( 0 === $key ) {

				$this->blog_board_big_box( $data );

			}
		}

		echo '<div class="area-sm-blocks custom-dir-sm-blocks">';
			foreach ($limit_posts as $key => $single_post) {
				if ( $key > 0 ) {

					$this->blog_board_small_box( $single_post );

				}
			}
		echo '</div>';

	}


	/**
	 * Layout Left blog board.
	 *
	 * @param (array) $count get the count.
	 */
	public function layout_left_blog_board( $limit_posts ) {

		foreach ($limit_posts as $key => $data) {

			if ( 0 === $key ) {

				$this->blog_board_big_box( $data );

			}
		}

		echo '<div class="area-sm-blocks">';
			foreach ($limit_posts as $key => $single_post) {
				if ( $key > 0 ) {

					$this->blog_board_small_box( $single_post );

				}
			}
		echo '</div>';
	}

	/**
	 * Get blog board posts
	 * Filter all post that active the pin post option
	 *
	 * @return array pin_post_items it is all posts that run pin post with data like(title, data, etc ...)
	 */

	public function get_blog_board_posts(){

		$pin_post_custom_id = 0;
		$pin_post_items = array();

		# Pin Post custom query
		$args = array( 'post_type' => 'post', 'posts_per_page' => -1 );
		$blog_board_query = new \WP_Query( $args );

		#check if have posts in the system
		if ( $blog_board_query->have_posts() ) {

			# start the post loop.
			while ( $blog_board_query->have_posts() ) {

				$blog_board_query->the_post();
				$pin_post_id = get_the_ID();

				#if this post is pin post or not when need to check the pin post meta value
				if ( x_wdes()->wdes_get_postdata( $pin_post_id ) ) {

					#Get the background image & set number of word in caption
					$post_img = 'style="' . $this->blog_board_img( 'wdes-image-grid' ) . '"';
					$number_word = 14;

					# Add the Pin Post Data
					$pin_post_items [ $pin_post_custom_id ]['pin_post_title']   = x_wdes()->wdes_get_title();
					$pin_post_items [ $pin_post_custom_id ]['pin_post_image']   = $post_img;
					$pin_post_items [ $pin_post_custom_id ]['pin_post_link']    = get_permalink();
					$pin_post_items [ $pin_post_custom_id ]['pin_post_date']    = get_the_date();
					$pin_post_items [ $pin_post_custom_id ]['pin_post_caption'] = x_wdes()->wdes_get_excerpt($number_word);

					#increase the pin post id
					$pin_post_custom_id++;

				}
			}

			# reset the post data.
			wp_reset_postdata();
		}

		return $pin_post_items;
	}

	/**
	 * Blog board
	 * Show the final result in html template
	 *
	 * @param string $layout The layout.
	 */
	public function blog_board( $layout = 'right' ) {

		$posts_items = $this->get_blog_board_posts();

		#html template & if there no post remove it
		if( isset( $posts_items[0] ) ) {
			echo '<div class="container">';
				echo '<div class="row">';
					echo '<div class="top-articles">';

						#if the $posts_item is array
						if(is_array($posts_items)){

							//Todo we can limit the post from wp query without get all post and limit it
							#limit all posts to last 5
							$limit_posts = array_slice($posts_items, 0 , 5);

							//Todo remove all layout and stile the left only
							#select the layout and run the function
							if ( 'left' === $layout ) {

								$this->layout_left_blog_board( $limit_posts );

							} else {

								$this->layout_right_blog_board( $limit_posts );

							}

						}
					echo '</div>';
				echo '</div>';
			echo '</div>';
		}

	}


}
