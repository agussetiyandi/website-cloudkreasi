<?php
namespace Phox\core;

use \WC;

/**
 * Header
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */
class Wdes_Header {
	/**
	 * Class .
	 *
	 * @var Wdes_Header
	 */
	private $class = '';

	/**
	 * Template Id
	 *
	 * @var Wdes_Header
	 */
	private $temp_id = 0;

	protected static $_instance = null;


	public static function instance()
	{
		if ( is_null( self::$_instance ) )
		{
			self::$_instance = new self;
		}

		return self::$_instance;
	}


	/**
	 * Layout.
	 */
	public function Layout() {
		if ( wdes_opts_get( 'layout' ) === 'boxed' ) {
			$this->class = 'layout-box';
		} else {
			$this->class = 'layout-wide';
		}
		return $this->class;
	}

	/**
	 * Pattern.
	 *
	 * @return string
	 */
	public function pattern() {

		switch ( wdes_opts_get( 'background_patterns' ) ) {
			case 'pattern1':
				$style = 'img/layout/patterns/1.png';
				break;

			case 'pattern2':
				$style = 'img/layout/patterns/2.png';
				break;

			case 'pattern3':
				$style = 'img/layout/patterns/3.png';
				break;

			case 'pattern4':
				$style = 'img/layout/patterns/4.png';
				break;

			case 'pattern5':
				$style = 'img/layout/patterns/5.png';
				break;

			case 'pattern6':
				$style = 'img/layout/patterns/6.png';
				break;

			case 'pattern7':
				$style = 'img/layout/patterns/7.png';
				break;

			case 'pattern8':
				$style = 'img/layout/patterns/8.png';
				break;

			case 'pattern9':
				$style = 'img/layout/patterns/9.png';
				break;

			case 'pattern10':
				$style = 'img/layout/patterns/10.png';
				break;

			default:
				$style = '';

		}

		 return $style;

	}

	/**
	 * Get pattern.
	 *
	 * @return string
	 */
	public function get_pattern() {

		$pattern = $this->pattern();

		if ( ! empty( $pattern ) ) {

			$result = 'style="background-image: url(' . WDES_OPTIONS_URI . $pattern . ')"';

		} else {

			$result = '';

		}

		return $result;

	}

	/**
	 * Logo.
	 *
	 * @return string
	 */
	public function logo() {
		$image_logo   = wdes_opts_get( 'logo' );
		$text_logo    = wdes_opts_get( 'text-logo' );
		$retina_image = wdes_opts_get( 'retinal-logo' );

		if ( ! empty( $image_logo ) ) {;

			$result = '<img class="logo" src="' . esc_url($image_logo) . '" alt="' . esc_attr( get_bloginfo() ) . '" />';

			if ( ! empty( $retina_image ) ) {
				$result .= '<img class="logo logo-main" src="' . esc_url($retina_image) . '" alt="' . esc_attr(get_bloginfo() ) . '" />';
			}

		} elseif ( ! empty( $text_logo ) ) {

			$result = '<h2 class="text-logo">' . esc_html($text_logo) . '</h2>';

		} elseif ( has_custom_logo() ) {

			$result = get_custom_logo();

        } else {
			$logo_s_png = WDES_ASSETS_URI . '/img/logo.png';
			$logo_x2_png = WDES_ASSETS_URI . '/img/logo@2x.png';

			$result = '<img class="logo" src="' . esc_url($logo_s_png) . '' . '" alt="'. esc_attr(get_bloginfo() ) . '" />';
			$result .= '<img class="logo logo-main" src="' . esc_url($logo_x2_png) . '" alt="' . esc_attr(get_bloginfo() ) . '" />';
        }

		return $result;

	}

	/**
	 * Favicon.
	 *
	 * @return string
	 */
	public function favicon() {
		$favicon = wdes_opts_get( 'favicon' );

		if ( ! empty( $favicon ) ) {

			$result = $favicon;

		} else {

			$result = WDES_THEME_URI . '/favicon.ico';

		}

		$output = '<link rel="shortcut icon" href="' . $result . '" type="image/x-icon">';

		return $output;

	}

	/**
	 * Menu icons
	 *
	 * @return bool|string
	 */
	public function menu_icons() {
		$data = array(
			'login'  => array(
				'check' => wdes_opts_get( 'topbar-login' ),
				'url'   => wdes_opts_get( 'topbar-login-url' ),
			),
			'search' => array(
				'check' => wdes_opts_get( 'topbar-search' ),
				'url'   => '',
			),
			'cart'   => array(
				'check' => wdes_opts_get( 'topbar-cart' ),
				'url'   => wdes_opts_get( 'topbar-cart-url' ),
			)
		);

		if ( ! empty( $data  ) || defined( 'ICL_LANGUAGE_CODE' ) ) {
			$result      = '<div class="clientarea-shortcuts">';

				//custom direction
				if( ! empty(  $data ) ){

					$result .= $this->menu_icon_block( $data );

				}

				//Language switcher wpml
				if( defined( 'ICL_LANGUAGE_CODE' ) ){

					$result .= $this->language_switcher();

				}
			$result     .= '</div>';

			return $result;
		}

		return false;

	}

	/**
	 * Show Woo Widget cart in all case
	 *
	 * @since 1.5.9
	 */
	public function woo_widget_cart_is_hidden(){
		return false;
	}

	/**
	 * Cart Fragments
	 * Ensure cart contents update when products are added to the cart via AJAX
	 *
	 * @param  array $fragments Fragments to refresh via AJAX.
	 * @return array            Fragments to refresh via AJAX
	 */
	function cart_link_fragment( $fragments ) {
		global $woocommerce;

		ob_start();
		$this->woo_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}

	/**
	 * Cart Link
	 * Displayed a link to the cart including the number of items present and the cart total
	 *
	 * @return string $content
	 * @since  1.5.9
	 */
	public function woo_cart_link() {
	    ob_start();
		?>
			<a role="button" id="wdesDropDownWooCart" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="cart-contents dropdown-toggle" href="#" title="<?php esc_attr_e( 'View your shopping cart', 'phox' ); ?>">
				<?php /* translators: %d: number of items in cart */ ?>
				<span class="count"><?php echo wp_kses_data( sprintf( '%d',WC()->cart->get_cart_contents_count()  ) ); ?></span> <span id="<?php echo esc_attr('icon')?>" class="<?php echo esc_attr('ico-icon-cart')?>"></span>
			</a>
		<?php

        $content = ob_get_contents();
		ob_end_clean();

        echo $content ;
	}

	/**
	 * Display Header Cart
	 *
	 * @since  1.5.9
	 * @uses  is_woocommerce_activated()  check if WooCommerce is activated
	 * @return string $content
	 */
	public function woo_cart() {
        if ( is_cart() ) {
            $class = 'current-menu-item';
        } else {
            $class = '';
        }
        ob_start();
        ?>
        <div id="site-header-cart" class="wdes-menu-shortcut-woo-cart dropdown">
            <?php $this->woo_cart_link(); ?>
            <div class="dropdown-menu <?php echo $class; ?>" aria-labelledby="wdesDropDownWooCart">
                <?php apply_filters( 'woocommerce_widget_cart_is_hidden', false ); ?>
                <?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
            </div>
        </div>
        <?php

        $content = ob_get_contents();
        ob_end_clean();

        return $content ;
	}


	/**
	 * Menu Icon Block
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	public function menu_icon_block( $item ) {
		$output = '';

		foreach ( $item as $key => $value ) {

			if ( $key === 'login' ) {

				$icon = 'ico-icon-user';

			} elseif ( $key === 'cart' ) {

				$icon = 'ico-icon-cart';

			} else {

				$icon = 'ico-icon-search';

			}

			extract( $value );

			if ( $check == true ) {

				if ( $key === 'search' ) {
					$href = '';
					$id   = 'search-button';
				} elseif ( $key === 'login' ) {

					$href = 'href="' . esc_url( $url ) . '"';
					$id   = 'login';

				} else {

					$href = 'href="' . esc_url( $url ) . '"';
					$id   = 'cart';

				}

				if( $key === 'cart' && x_wdes()->is_woocommerce_activated() ) {

					$output  .= $this->woo_cart();

				}else{

					$output .= '<a ' . $href . ' data-toggle="tooltip" data-placement="bottom" title="' . esc_attr($key) . '" >';
					$output .= '<span id="' . esc_attr($id) . '" class="' . $icon . '"></span>';
					$output .= '</a>';

				}

				if ($key === 'search'){

					$output  .= '<div class="wdes-fullscreen-search-overlay">';
                        $output  .= '<a class="wdes-fullscreen-close">';
                            $output  .= '<i class="fas fa-times"></i>';
                        $output  .= '</a>';
                        $output  .= get_search_form( false );
					$output  .= '</div>';

				}

			}
		}
		return $output;

	}

	/**
	 * Sidebar layout show.
	 *
	 * @return mixed|null|string
	 */
	public function sdiebar_layout_show() {
		$page_layout   = wdes_opts_get( 'sidebar-page-layout' );
		$single_layout = wdes_opts_get( 'sidebar-single-layout' );

		if ( is_single() ) {

			if ( ! empty( $single_layout ) ) {

				$result = $single_layout;

			} else {

				$result = 'no';

			}
		} else {

			if ( ! empty( $page_layout ) ) {

				$result = $page_layout;

			} else {

				$result = 'no';

			}
		}

		return $result;

	}

	/**
	 * Sidebar Name
	 *
	 * @param string $layout
	 *
	 * @return array|mixed|null|string
	 */
	public function sidebar_name( $layout = 'both' ) {
		$single_name_one = wdes_opts_get( 'sidebar-single-sidebar-one' );
		$single_name_two = wdes_opts_get( 'sidebar-single-sidebar-two' );

		$page_name_one = wdes_opts_get( 'sidebar-page-sidebar-one' );
		$page_name_two = wdes_opts_get( 'sidebar-page-sidebar-two' );

		$is_sidebar_active = wdes_opts_get( 'single-post-sidebars' );


		if ( is_single() ) {

			// Single.
			if ( is_null( $is_sidebar_active ) ) {

				if ( $layout === 'side' ) {

					if ( ! empty( $single_name_one ) ) {

						$result = $single_name_one;

					} else {

						$result = 'main';

					}
				} else {

					$result = array();

					if ( ! empty( $single_name_one ) ) {

						$result [] = $single_name_one;

					} else {

						$result = 'main';

					}

					if ( ! empty( $single_name_two ) ) {

						$result [] = $single_name_two;

					} else {

						$result = 'main';

					}
				}
			}
		} else {

			// Archive.
			if ( $layout === 'side' ) {

				if ( ! empty( $page_name_one ) ) {

					$result = $page_name_one;

				} else {

					$result = 'main';

				}
			} else {

				$result = array();

				if ( ! empty( $page_name_one ) ) {

					$result [] = $page_name_one;

				} else {

					$result = 'main';

				}

				if ( ! empty( $page_name_two ) ) {

					$result [] = $page_name_two;

				} else {

					$result = 'main';

				}
			}
		}

		if( ! isset( $result ) ){

			$result = '' ;

		}

		return $result;

	}

	/**
	 * Add perloaders overlay div when its options is enabled
	 *
	 * @since 1.3.3
	 */
	public function perloader_body_overlay() {
		$output = '';

		if(wdes_opts_get('preloader') == true ){

			$output .= '<div class="wdes-loading">';
				$output .= '<span class="loader-wdes loader-double"></span>';
			$output .= '</div>';

		}

		x_wdes()->wdes_get_text ( $output );

	}

	/**
	 * Top Header Section
	 *
	 * @since 1.4.2
	 */

	public function top_header_section (){

        if( ! wdes_opts_get('show-topheader') ) return false;

        ?>
        <!-- Top Header -->
        <div class="wdes-interface-menu">
            <div class="container">
                <div class="row">
                    <?php
                        x_wdes()->wdes_get_tp('header/header', 'top');
                    ?>
                </div>
            </div>
        </div>
        <?php

	}

	/**
	 * Main Header Section
	 *
	 * @since 1.4.2
	 */
	public function main_header_section(){

        $sticky_class =  (wdes_opts_get( 'sticky-header' )) ? 'alternate-sticky-header' : '';
		?>

        <header id="Top_bar" class="<?php echo esc_attr($sticky_class)?>" >

			<?php x_wdes()->wdes_get_tp( 'header/header', 'main' ); ?>
		</header>

		<?php

	}

	/**
	 * Header toolbar WPML language switcher
	 *
	 * @since 1.4.0
	 *
	 */
	public function language_switcher() {

		$languages = icl_get_languages ('skip_missing=0&orderby=id') ;
		$output		 = '';
		$lang_icon_image = WDES_ASSETS_URI . '/img/icons/language_switcher.svg' ;

		if( is_array( $languages ) ){
			$output .= '<div class="dropdown language-switcher-wdes">';
				$output .= '<button class="dropdown-toggle nav-link" type="button" id="languageSwitcher" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img src="' . esc_url($lang_icon_image) . '" alt="' . esc_attr__( 'Language Switcher', 'phox' ) . '" data-toggle="tooltip" data-placement="bottom" title="Languages"> </button>';
				$output .= '<ul class="dropdown-menu custom dropdown-menu-right" aria-labelledby="languageSwitcher">';

				foreach ($languages as $lang) {

					$output .= '<li class="language_'. esc_attr( $lang['language_code'] ) . '">';
						$output .= '<a href="' . esc_url( $lang['url'] ) . '">';
							$output .= '<img title="' . esc_attr( $lang['native_name'] ) . '" src="' . esc_url( $lang['country_flag_url'] ) . '" />';
							$output .= '<p>' . esc_html( $lang['native_name'] ) . '</p>';
						$output .= '</a>';
					$output .= '</li>';

				}

				$output .= '</ul>';
			$output .= '</div>';

			return $output;

		}

		return false;

	}

	/**
	 * Get Header builder ID
	 * if option website header is have value
	 *
	 * @since 2.0.0
	 * @return false|int
	 */
	public function get_header_builder_id(){

		$this->temp_id = (int) wdes_opts_get( 'website_header', 'default' );

		if ( ! $this->temp_id > 0 ){

			return false;

		}

		return $this->temp_id;

	}

	/**
	 * Get Header builder Template
	 * after check the id it get header template
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function get_header_template(){

		\Phox\helpers::get_builder_content_for_display($this->temp_id);

	}


}