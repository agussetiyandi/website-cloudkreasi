<?php
namespace Phox\core;
/**
 * Hooks
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */
class Wdes_Hooks {

	protected static $_instance = null;

	public static function instance() {

		if ( is_null( self::$_instance ) )
		{
			self::$_instance = new self;
		}

		return self::$_instance;

	}

	/**
	 * Construct
	 *
	 * @since 1.4.2
	 */
	public function fire() {

		$this->header_after_body_open ();
		$this->footer_before_body_close ();
		$this->add_to_cart_fragments();

	}

	/**
	 * Header After Body Open
	 *
	 * @see wp_body_open (it is required after version 5.2)
	 * @see perloader_body_overlay
	 * @see top_header_section
	 * @see main_header_section
	 * @since 1.4.2
	 */
	public function header_after_body_open () {

		$inst_header = Wdes_Header::instance();

		add_action( 'wdes_header_after_body_open', [$inst_header, 'perloader_body_overlay'], 1 );

		if( $inst_header->get_header_builder_id() ){

			add_action( 'wdes_header_after_body_open', [$inst_header, 'get_header_template'], 4 );

		}else{

			add_action( 'wdes_header_after_body_open', [$inst_header, 'top_header_section'], 4 );

			add_action( 'wdes_header_after_body_open', [$inst_header, 'main_header_section'], 4 );

		}


	}

	/**
	 * Cart fragment
	 *
	 * @see cart_link_fragment()
	 * @see woo_widget_cart_is_hidden()
	 *  @since 1.5.9
	 */
	public function add_to_cart_fragments(){

		$inst_header = Wdes_Header::instance();

		if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '2.3', '>=' ) ) {
			add_filter( 'woocommerce_add_to_cart_fragments',  [$inst_header, 'cart_link_fragment']);
		} else {
			add_filter( 'add_to_cart_fragments', [$inst_header, 'cart_link_fragment']);
		}

		add_filter('woocommerce_widget_cart_is_hidden',[$inst_header, 'woo_widget_cart_is_hidden'],10, 3);


	}

	/**
	 * Footer Before Body Close
	 *
	 * @see site_footer
	 * @since 1.4.2
	 */

	public function footer_before_body_close () {

		$inst_footer = Wdes_Footer::instance();

		if( $inst_footer->get_footer_builder_id() ) {

			add_action( 'wdes_the_footer', [$inst_footer, 'get_footer_template']);

		}else {

			add_action('wdes_the_footer', [$inst_footer, 'site_footer']);

		}

		add_action('wdes_footer_before_body_close', [$inst_footer, 'goto_top_but']);

	}


}
