/* ==============================================
/*  Color
/* ============================================= */

/* =======================
/*  Header
/* ====================== */


<?php
    $header_bg = wdes_opts_get('color-header-bg');
    if ( is_array( $header_bg ) ):
?>
header {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($header_bg['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($header_bg['color-two']) ; ?> 100%);
    border-bottom: 1px solid <?php wdes_opts_show('color-header-bg-border-bottom'); ?>;
}
<?php endif; ?>

<?php
    $header_bg_sticky = wdes_opts_get('color-header-sticky-background');
    if ( is_array( $header_bg_sticky ) ):
?>
.sticky-header {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($header_bg_sticky['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($header_bg_sticky['color-two']) ; ?> 100%);
    border-bottom: 1px solid <?php wdes_opts_show('color-header-sticky-border-bottom'); ?>;
}
<?php endif; ?>

.wdes-interface-menu #wdes-menu-top-header ul.menu li a {
    color: <?php wdes_opts_show('color-header-top-header-link-clr'); ?>;
}

.wdes-interface-menu #wdes-menu-top-header ul.menu li a:hover {
    color: <?php wdes_opts_show('color-header-top-header-link-clr-hvr'); ?>;
}

.clientarea-shortcuts a span {
    color: <?php wdes_opts_show('color-header-top-bar-shortcuts-icon-clr'); ?>;
}

.clientarea-shortcuts a span:hover {
    color: <?php wdes_opts_show('color-header-top-bar-shortcuts-icon-clr-hvr'); ?>;
}

.clientarea-shortcuts {
  border-left: 1px solid <?php wdes_opts_show('color-header-bg-shortcut-border'); ?>;
}

.s-area span,
.social li a {
    color: <?php wdes_opts_show('color-header-top-bar-r-clr'); ?>;
}

.social li a:hover  {
    color: <?php wdes_opts_show('color-header-top-bar-r-clr-hvr'); ?>;
}
<?php
    $topbar_bg = wdes_opts_get('color-header-sub-bg');
    if( is_array( $topbar_bg ) ):
?>
.wdes-interface-menu {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($topbar_bg['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($topbar_bg['color-two']) ; ?> 100%);
    border-bottom: 1px solid <?php wdes_opts_show('color-header-top-bar-border-b'); ?>;
}
<?php endif; ?>

.wdes-menu-navbar ul.default-drop > li > a {
    color: <?php wdes_opts_show('color-header-menu-clr'); ?>;
}

.wdes-menu-navbar ul.default-drop > li > a:hover {
    color: <?php wdes_opts_show('color-header-menu-clr-hvr'); ?>;
}

.wdes-menu-navbar .default-drop > li.current-menu-item > a {
    color: <?php wdes_opts_show('color-header-menu-clr-active'); ?> !important;
}

.wdes-menu-navbar ul.default-drop > li > ul.dropdown-content.wdes-global-style > li a {
    color: <?php wdes_opts_show('color-header-sub-menu-clr'); ?>;
}

.wdes-menu-navbar ul.default-drop > li > ul.dropdown-content.wdes-global-style > li a:hover {
    color: <?php wdes_opts_show('color-header-sub-menu-clr-hvr'); ?>;
}

.logo {
    height: <?php wdes_opts_show('height-logo'); ?>px !important;
}

@media only screen and (max-width: 960px) and (min-width: 160px) {
    .menu-d-mob {
        background-color: <?php wdes_opts_show('color-header-dropdown-menu'); ?>;
    }
}

.wdes-mob-btn .icon-menu .line {
    background-color: <?php wdes_opts_show('color-header-menu-switch-btn'); ?>;
}

.wdes-menu-is-active .icon-menu .line-1,
.wdes-menu-is-active .icon-menu .line-3 {
    background-color: <?php wdes_opts_show('color-header-menu-switch-btn-inside'); ?>;
}

@media (max-width: 960px) and (min-width: 160px) {
    .wdes-menu-navbar ul.default-drop > li > a {
        color: <?php wdes_opts_show('color-header-overlay-menu-link'); ?> !important;
    }
    .wdes-menu-navbar ul.default-drop > li > a:hover {
        color: <?php wdes_opts_show('color-header-overlay-menu-link-hvr'); ?> !important;
    }
    .wdes-menu-navbar ul.default-drop > li.current-menu-item > a {
        color: <?php wdes_opts_show('color-header-overlay-menu-link-active'); ?> !important;
    }
    .wdes-menu-navbar ul.default-drop > li {
        border-bottom: 1px solid <?php wdes_opts_show('color-header-overlay-menu-border'); ?>;
    }
    .wdes-menu-navbar ul.default-drop > li > ul.dropdown-content.wdes-global-style > li a {
        color: <?php wdes_opts_show('color-header-overlay-sub-menu-link'); ?>;
    }
    .wdes-menu-navbar ul.default-drop > li > ul.dropdown-content.wdes-global-style > li a:hover {
        color: <?php wdes_opts_show('color-header-overlay-sub-menu-link-hvr'); ?>;
    }
}

.wdes-menu-navbar .mega-w.dropdown-content li.heading > a {
    color: <?php wdes_opts_show('color-header-menu-mega-heading'); ?>;
}

.wdes-menu-navbar ul.default-drop > li ul.dropdown-content li.heading:hover > a {
    color: <?php wdes_opts_show('color-header-menu-mega-heading-hvr'); ?>;
}

.wdes-menu-navbar ul.default-drop > li > ul.dropdown-content > li > a {
    border-bottom: 1px solid <?php wdes_opts_show('color-header-sub-menu-clr-border'); ?>;
}

.wdes-menu-navbar .mega-w.dropdown-content li.heading > a span.divider-heading-mega {
    background: <?php wdes_opts_show('color-header-menu-mega-heading-border'); ?>;
}

.text-logo {
    font-size: <?php wdes_opts_show('text-logo-size'); ?>px;
}

/* =======================
/*  Headings
/* ====================== */

h1 {
    color: <?php wdes_opts_show('color-heading-h1'); ?>;
}

h2 {
    color: <?php wdes_opts_show('color-heading-h2'); ?>;
}

h3 {
    color: <?php wdes_opts_show('color-heading-h3'); ?>;
}

h4 {
    color: <?php wdes_opts_show('color-heading-h4'); ?>;
}

h5 {
    color: <?php wdes_opts_show('color-heading-h5'); ?>;
}

h6 {
    color: <?php wdes_opts_show('color-heading-h6'); ?>;
}

/* =======================
/*  Footer
/* ====================== */
<?php
    $footer_bg_sec = wdes_opts_get('color-footer-bg');
    if( is_array( $footer_bg_sec ) ):
?>
.footer {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($footer_bg_sec['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($footer_bg_sec['color-two']) ; ?> 100%);
}
<?php endif; ?>

.footer h1,
.footer h2,
.footer h3,
.footer h4,
.footer h5,
.footer h6,
.footer .widget_wdes_newsletter h2,
.footer .widget_rss h2 > a.rsswidget,
.widget-footer.widget_rss cite,
#wp-calendar tfoot td a {
    color: <?php wdes_opts_show('color-footer-heading'); ?> !important;
}

.footer ul li a {
    color: <?php wdes_opts_show('color-footer-link-clr'); ?>;
}

.footer ul li a:hover {
    color: <?php wdes_opts_show('color-footer-link-clr-hvr'); ?>;
}

.email-news {
    background: <?php wdes_opts_show('color-footer-new-letter-inp'); ?> !important;
    border: 1px solid <?php wdes_opts_show('color-footer-new-letter-inp-border'); ?> !important;
    text-transform: lowercase;
}

<?php
    $footer_news_letter = wdes_opts_get('color-footer-new-letter-sub');
    if( is_array( $footer_news_letter ) ):
?>
input.sub-news,
input.sub-news:hover {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($footer_news_letter['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($footer_news_letter['color-two']) ; ?> 100%) !important;
}
<?php endif; ?>

<?php
    $footer_copyright_bg = wdes_opts_get('color-footer-copyright-top-border');
    if( is_array($footer_copyright_bg) ):
?>
.copyrights-wdes-ft {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($footer_copyright_bg['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($footer_copyright_bg['color-two']) ; ?> 100%);
}
<?php endif; ?>

.footer-menu ul li a {
    color: <?php wdes_opts_show('color-footer-copyright-link-clr'); ?>;
}

.footer-menu ul li a:hover {
    color: <?php wdes_opts_show('color-footer-copyright-link-clr-hvr'); ?>;
}

.footer .copyrights-wdes-ft p.wdes-copyrights-text {
    color: <?php wdes_opts_show('color-footer-copyright-text'); ?>;
}

<?php $position = explode(';',wdes_opts_get('footer-bg-img-position')); ?>

.footer-bg {
    background: <?php wdes_opts_show('color-header-sub-menu-bg-hvr', '#29306c'); ?>;
    background-image: url(<?php x_wdes()->wdes_get_text (wdes_opts_get('footer-bg-img')); ?>);
    <?php if(is_array($position)){ ?>
        <?php if(isset($position[0])): ?>
            background-repeat:<?php x_wdes()->wdes_get_text ($position[0]) ?>;
        <?php endif; ?>
        <?php if(isset($position[1])): ?>
            background-position:<?php x_wdes()->wdes_get_text ($position[1]) ?>;
        <?php endif; ?>
    <?php } ?>
    background-size:<?php echo wdes_opts_get('footer-bg-img-size') ?>

}

.wdes-partners-company {
  border-top: 1px solid <?php wdes_opts_show('color-footer-copyright-border-top'); ?>;
  border-bottom: 1px solid <?php wdes_opts_show('color-footer-copyright-border-bottom'); ?>;
}

.wdes-partners-company span {
    color: <?php wdes_opts_show('color-footer-payment-icon-color'); ?>;
}

.wdes-partners-company span:hover {
    color: <?php wdes_opts_show('color-footer-payment-icon-color-hvr'); ?>;
}

#wdes-back-to-top {
    background-color: <?php wdes_opts_show('color-footer-backtotop-bg'); ?>;
    border: 1px solid <?php wdes_opts_show('color-footer-backtotop-border'); ?>;
}

#wdes-back-to-top:hover {
    background-color: <?php wdes_opts_show('color-footer-backtotop-bg-hvr'); ?>;
    border: 1px solid <?php wdes_opts_show('color-footer-backtotop-border-hvr'); ?>;
}

#wdes-back-to-top span {
    color: <?php wdes_opts_show('color-footer-backtotop-icon-clr'); ?>;
}

/* =======================
/*  ShortCodes
/* ====================== */

<?php
    $loading_bg = wdes_opts_get('color-shortcodes-loading-bg');
    if( is_array($loading_bg) ):
?>
.wdes-loading {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($loading_bg['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($loading_bg['color-two']) ; ?> 100%);
}
<?php endif; ?>

<?php
    $shortcode_bg_grd = wdes_opts_get('color-shortcodes-best-f-bg');
    if( is_array( $shortcode_bg_grd ) ):
?>
.best-feature .l-item-f {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($shortcode_bg_grd['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($shortcode_bg_grd['color-two']) ; ?> 100%);
}
<?php endif; ?>

.best-feature .r-item-f {
    background:  <?php wdes_opts_show('color-shortcodes-best-f-bg-r'); ?>;
}

<?php
    $shortcode_info_box_t = wdes_opts_get('color-shortcodes-info-box');
    if( is_array($shortcode_info_box_t) ):
?>
.block-comp span {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($shortcode_info_box_t['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($shortcode_info_box_t['color-two']) ; ?> 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
}
<?php endif; ?>

<?php
    $shortcode_general_text_g = wdes_opts_get('color-shortcodes-text-general');
    if( is_array($shortcode_general_text_g) ):
?>
.plan-icon, .reg-dom:hover, a.chat-n, .block-footer-mlyo ul li a:hover, .info-bio h4, .title-error-t, .t-sty-a.colored h2, ul.categories li a::before, ul.categories li a:hover {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($shortcode_general_text_g['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($shortcode_general_text_g['color-two']) ; ?> 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
}
<?php endif; ?>

<?php
    $shortcode_general_bg_g = wdes_opts_get('color-shortcodes-bg-general');
    if( is_array($shortcode_general_bg_g) ):
?>
.social-media, .user-sub, .l-border, .cat, a.getstarted-modernlyo, a.job-modernlyo {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($shortcode_general_bg_g['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($shortcode_general_bg_g['color-two']) ; ?> 100%);
}
<?php endif; ?>

<?php
    $shortcode_faq_bg = wdes_opts_get('color-shortcodes-faq_bg');
    if( is_array($shortcode_faq_bg) ):
?>
.title-faq-lv2 {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($shortcode_faq_bg['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($shortcode_faq_bg['color-two']) ; ?> 100%);
}
<?php endif; ?>

.wpcf7 input:hover, .wpcf7 textarea:hover {
    border: 2px solid <?php wdes_opts_show('color-shortcode-cf7-input-hvr'); ?>;
}

.wpcf7 input[type=submit] {
    background: <?php wdes_opts_show('color-shortcode-cf7-send-btn-bg'); ?>;
}
.wpcf7 input[type=submit]:hover {
    background: <?php wdes_opts_show('color-shortcode-cf7-send-btn-bg-hvr'); ?>;
}

/* =======================
/*  Blog
/* ====================== */

<?php
    $blog_breadcrumb_bg = wdes_opts_get('color-blog-breadcrumb');
    if ( is_array( $blog_breadcrumb_bg ) ):
?>
.parent-img-post-inner {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($blog_breadcrumb_bg['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($blog_breadcrumb_bg['color-two']) ; ?> 100%);
}
<?php endif; ?>

.parent-title-archive .main-page-title-in h2 {
    color: <?php wdes_opts_show('color-blog-blog-breadcrumb-title'); ?>;
}

.parent-title-archive p {
    color: <?php wdes_opts_show('color-blog-blog-breadcrumb-description'); ?>;
}

ul.wdes-breadcrumb-area li a {
    color: <?php wdes_opts_show('color-blog-blog-breadcrumb-link'); ?>;
}

ul.wdes-breadcrumb-area li {
    color: <?php wdes_opts_show('color-blog-blog-breadcrumb-link-active'); ?>;
}

ul.wdes-breadcrumb-area li:after {
    color: <?php wdes_opts_show('color-blog-blog-breadcrumb-link-separator'); ?>;
}

.classic-post-view a.title-art-sub {
    color: <?php wdes_opts_show('color-blog-post-title'); ?>;
}

.classic-post-view a.title-art-sub:hover {
    color: <?php wdes_opts_show('color-blog-post-title-hover'); ?>;
}

.text-area a.read-more-btn {
    color: <?php wdes_opts_show('color-blog-post-more-btn'); ?>;
    border: 2px solid <?php wdes_opts_show('color-blog-post-more-btn-border'); ?>;
}

.text-area a.read-more-btn:hover {
    background: <?php wdes_opts_show('color-blog-post-more-btn-bg-hvr'); ?>;
    color: <?php wdes_opts_show('color-blog-post-more-btn-hvr'); ?>;
    border: 2px solid <?php wdes_opts_show('color-blog-post-more-btn-border-hvr'); ?>;
}

.sidebar-area .wid-title h2, .sidebar-area .wid-title a {
    color: <?php wdes_opts_show('color-blog-widget-title'); ?>;
}

.block-sidebar-function ul li a:hover {
    color: <?php wdes_opts_show('color-blog-widget-link'); ?>;
}

.block-sidebar-function.widget_search #wdes-fullscreen-searchform button[type=submit],
.elementor-widget-container #wdes-fullscreen-searchform button[type=submit],
.elementor-element #wdes-fullscreen-searchform button[type=submit] {
    background: <?php wdes_opts_show('color-blog-search-widget-send'); ?> !important;
}

.posts li a {
    color: <?php wdes_opts_show('color-blog-widget-posts-title-clr'); ?> !important;
}

.posts li a:hover {
    color: <?php wdes_opts_show('color-blog-widget-posts-title-clr-hvr'); ?> !important;
}

.art-block-sub a[rel=tag]:hover {
    background: <?php wdes_opts_show('color-blog-tags-widget-tag'); ?>;
}

.body-article h1, .body-article h2, .body-article h3, .body-article h4, .body-article h5, .body-article h6 {
    color: <?php wdes_opts_show('color-blog-post-heading-main'); ?>;
}

a.related-title {
    color: <?php wdes_opts_show('color-blog-related-posts-title'); ?>;
}

a.related-title:hover {
    color: <?php wdes_opts_show('color-blog-related-posts-title-hvr'); ?>;
}

.tag-cloud-link:hover {
    background: <?php wdes_opts_show('color-blog-tags-widget-tag-item'); ?>;
}

.comment-form #comment:hover, .comment-form #author:hover, .comment-form #email:hover, .comment-form #url:hover, .comment-form #comment:focus, .comment-form #author:focus, .comment-form #email:focus, .comment-form #url:focus {
    border: 1px solid <?php wdes_opts_show('color-blog-comment-input-hvr'); ?>;
}
.comment-form .submit {
    background: <?php wdes_opts_show('color-blog-comment-input-send'); ?>;
}
.comment-form .submit:hover {
    background: <?php wdes_opts_show('color-blog-comment-input-send-hvr'); ?>;
}

.comments-blog .wid-title h2 {
    color: <?php wdes_opts_show('color-blog-comments-title'); ?> !important;
}

a.comment-reply-link {
    background: <?php wdes_opts_show('color-blog-comment-reply-link'); ?>;
    border: 2px solid <?php wdes_opts_show('color-blog-comment-reply-link-border'); ?>;
}

a.user-rep {
    color: <?php wdes_opts_show('color-blog-comment-user'); ?> !important;
}

h3.comment-reply-title {
    color: <?php wdes_opts_show('color-blog-reply-title'); ?> !important;
}

p.logged-in-as a {
    color: <?php wdes_opts_show('color-blog-reply-logged'); ?>;
}

.widget-footer #wp-calendar thead th {
    background: <?php wdes_opts_show('color-blog-footer-widget-calendar-head'); ?>;
}

.widget-footer table td {
    background: <?php wdes_opts_show('color-blog-footer-widget-calendar-body'); ?>;
}

.widget-footer input[type=text], .widget-footer input[type=password], .widget-footer input[type=email], .widget-footer input[type=url], .widget-footer input[type=date], .widget-footer input[type=number], .widget-footer input[type=tel], .widget-footer input[type=file], .widget-footer textarea, .widget-footer select {
    background: <?php wdes_opts_show('color-blog-footer-widget-input'); ?>;
    border: 1px solid <?php wdes_opts_show('color-blog-footer-widget-input-border'); ?>;
}

.widget-footer a.tag-cloud-link {
    background: <?php wdes_opts_show('color-blog-footer-widget-tags'); ?>;
    color: <?php wdes_opts_show('color-tags-text'); ?>;
}

.widget-footer a.tag-cloud-link:hover {
    background: <?php wdes_opts_show('color-blog-footer-widget-tags-hvr'); ?>;
    color: <?php wdes_opts_show('color-tags-text-hvr'); ?>;
}

.widget-footer #wdes-fullscreen-search-input {
    background: <?php wdes_opts_show('color-blog-footer-widget-search-btn'); ?>;
    border: 1px solid <?php wdes_opts_show('color-blog-footer-widget-search-btn-border'); ?>;
}

.widget-footer i.fas.fa-search.fullscreen-search-icon {
    color: <?php wdes_opts_show('color-footer-search-clr-btn'); ?>;
}

.widget-footer.widget_search button[type="submit"] {
    background: <?php wdes_opts_show('color-footer-search-bg-btn'); ?> !important;
}

.func-hr2 {
    background: <?php wdes_opts_show('color-footer-heading-border'); ?>;
}

.footer .company-info-block span,
.footer .company-info-block a,
.footer .about p {
    color: <?php wdes_opts_show('color-footer-aboutus-widget'); ?> !important;
}

.footer .company-info-block:nth-child(2) a::before,
.footer .company-info-block:nth-child(3) span::before {
    color: <?php wdes_opts_show('color-footer-aboutus-widget-icons'); ?>;
}

.footer li,
.footer input[type='text'],
.footer input[type='password'],
.footer input[type='email'],
.footer input[type='url'],
.footer input[type='date'],
.footer input[type='number'],
.footer input[type='tel'],
.footer input[type='file'],
.footer textarea,
.footer select,
.widget-footer #wp-calendar caption,
.widget-footer #wdes-fullscreen-search-input::placeholder,
.footer.footer-dark a.rsswidget {
    color: <?php wdes_opts_show('color-footer-text-clr'); ?> !important;
}

.footer p {
    color: <?php wdes_opts_show('color-footer-text-clr'); ?>;
}

.widget-footer #wp-calendar tbody td,
.widget-footer #wp-calendar thead th,
.widget-footer #wp-calendar tfoot td {
    border: 1px solid <?php wdes_opts_show('color-blog-footer-widget-calendar-body-border'); ?>;
}

.widget-footer #wp-calendar thead th,
.footer-dark #wp-calendar tfoot td a,
.widget-footer #wp-calendar tbody td {
    color: <?php wdes_opts_show('color-blog-footer-widget-calendar-body-text'); ?>;
}

#wp-calendar #today {
    background: <?php wdes_opts_show('color-blog-footer-widget-calendar-highlighted-bg'); ?>;
    border: 1px solid <?php wdes_opts_show('color-blog-footer-widget-calendar-highlighted-bg'); ?>;
    color: <?php wdes_opts_show('color-blog-footer-widget-calendar-highlighted-text'); ?>;
}

<?php if( x_wdes()->is_woocommerce_activated() ) : ?>

/* =======================
/*  WooCommerce
/* ====================== */

.wdes-menu-shortcut-woo-cart a.cart-contents span.count {
    background-color: <?php wdes_opts_show('color-woocommerce-btn-count'); ?>;
}
.wdes-menu-shortcut-woo-cart .dropdown-menu .woocommerce-mini-cart__buttons a.wc-forward {
    background-color: <?php wdes_opts_show('color-woocommerce-btn-view-cart'); ?>;
}
.wdes-menu-shortcut-woo-cart .dropdown-menu .woocommerce-mini-cart__buttons a.wc-forward:hover {
    background-color: <?php wdes_opts_show('color-woocommerce-btn-view-cart-hvr'); ?>;
}
.wdes-menu-shortcut-woo-cart .dropdown-menu .woocommerce-mini-cart__buttons a.checkout {
    background-color: <?php wdes_opts_show('color-woocommerce-btn-checkout'); ?>;
}
.wdes-menu-shortcut-woo-cart .dropdown-menu .woocommerce-mini-cart__buttons a.checkout:hover {
    background-color: <?php wdes_opts_show('color-woocommerce-btn-checkout-hvr'); ?>;
}

<?php
    $woo_bread_bg = wdes_opts_get('color-woocommerce-bread_bg');
    if ( is_array( $woo_bread_bg ) ):
?>
.woocommerce .wdes-bread {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($woo_bread_bg['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($woo_bread_bg['color-two']) ; ?> 100%);
}
<?php endif; ?>

.woocommerce ul.products li.product:hover {
    border: 1px solid <?php wdes_opts_show('color-woocommerce-product-h-brd'); ?>;
}

.woocommerce ul.products a.button.product_type_simple.add_to_cart_button, .woocommerce ul.products a.button.product_type_variable.add_to_cart_button, .woocommerce ul.products a.button.product_type_grouped, .woocommerce ul.products a.button.product_type_external {
    background: <?php wdes_opts_show('color-woocommerce-product-btn'); ?>;
}

.woocommerce ul.products li.product span.onsale {
    background: <?php wdes_opts_show('color-woocommerce-product-sale'); ?>;
}

.woocommerce ul.products li.product h2.woocommerce-loop-product__title {
    color: <?php wdes_opts_show('color-woocommerce-product-title'); ?>;
}

.woocommerce .sidebar-area .wid-title h2, .woocommerce .sidebar-area .wid-title a {
    color: <?php wdes_opts_show('color-woocommerce-widget-title'); ?>;
}

.woocommerce a.button.wc-forward,
.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
    background: <?php wdes_opts_show('color-woocommerce-widget-cart'); ?>;
}

.woocommerce a.button.wc-forward:hover,
.woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover {
    background: <?php wdes_opts_show('color-woocommerce-widget-cart-hvr'); ?>;
}

.woocommerce .widget_price_filter .ui-slider .ui-slider-range, .woocommerce .widget_price_filter .ui-slider .ui-slider-handle {
    background-color: <?php wdes_opts_show('color-woocommerce-widget-filter'); ?>;
}

.woocommerce section.related.products h2,
.woocommerce div.product .product_title,
.woocommerce h2.woocommerce-Reviews-title,
.woocommerce #review_form #respond .comment-reply-title {
    color: <?php wdes_opts_show('color-woocommerce-product-page-title'); ?>;
}

.woocommerce div.product p.price, .woocommerce div.product span.price,
.woocommerce .sku_wrapper, .woocommerce .posted_in, .woocommerce .tagged_as {
    color: <?php wdes_opts_show('color-woocommerce-product-page-feature'); ?>;
}

.woocommerce button.single_add_to_cart_button.button.alt,
.woocommerce .woocommerce-notices-wrapper .woocommerce-message,
.woocommerce #respond input#submit.alt:hover, .woocommerce #respond input#submit.alt:focus, .woocommerce a.button.alt:hover, .woocommerce a.button.alt:focus, .woocommerce button.button.alt:hover, .woocommerce button.button.alt:focus, .woocommerce input.button.alt:hover, .woocommerce input.button.alt:focus {
    background-color: <?php wdes_opts_show('color-woocommerce-product-page-btn'); ?>;
}

.woocommerce div.product .woocommerce-tabs ul.tabs li.active a {
    border: 1px solid <?php wdes_opts_show('color-woocommerce-product-tabs'); ?>;
    background: <?php wdes_opts_show('color-woocommerce-product-tabs'); ?>;
}

.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt {
    background: <?php wdes_opts_show('color-woocommerce-product-order-bg'); ?>;
}

.woocommerce .woocommerce-product-search button {
    background: <?php wdes_opts_show('color-woocommerce-search-bg'); ?>;
}

.woocommerce header.woocommerce-Address-title.title a.edit,
.woocommerce .woocommerce-MyAccount-navigation ul .is-active a {
    color: <?php wdes_opts_show('color-woocommerce-profile-link'); ?>;
}

<?php endif; ?>

.woocommerce nav.woocommerce-pagination ul li a {
    background: <?php wdes_opts_show('color-woocommerce-pagination-bg'); ?>;
}

.woocommerce nav.woocommerce-pagination ul li a {
    color: <?php wdes_opts_show('color-woocommerce-pagination-color'); ?>;
}

.woocommerce nav.woocommerce-pagination ul li span.page-numbers.current,
.woocommerce nav.woocommerce-pagination ul li a:hover {
    background: <?php wdes_opts_show('color-woocommerce-pagination-bg-active'); ?>;
}

.woocommerce nav.woocommerce-pagination ul li span.page-numbers.current,
.woocommerce nav.woocommerce-pagination ul li a:hover {
    color: <?php wdes_opts_show('color-woocommerce-pagination-color-active'); ?>;
}

/* =======================
/*  WHMCS Bridge
/* ====================== */

<?php
    $color_bridge_bg = wdes_opts_get('color-bridge-bg');
    if( is_array( $color_bridge_bg ) ):
?>
div#bridge section#main-menu,
div#bridge section#home-banner,
div#bridge section#main-menu .navbar-main {
    background: linear-gradient(-90deg, <?php x_wdes()->wdes_get_text ($color_bridge_bg['color-one']); ?> 0%, <?php x_wdes()->wdes_get_text ($color_bridge_bg['color-two']) ; ?> 100%);
}
<?php endif; ?>

div#bridge section#home-banner .btn.search {
    background: <?php wdes_opts_show('color-bridge-search'); ?>;
}

div#bridge section#home-banner .btn.transfer {
    background: <?php wdes_opts_show('color-bridge-transfer'); ?> !important;
}

div#bridge .home-shortcuts {
    background: <?php wdes_opts_show('color-bridge-home-bg'); ?>;
}

div#bridge .home-shortcuts li,
div#bridge .home-shortcuts li:first-child {
    border-color: <?php wdes_opts_show('color-bridge-home-sc-c'); ?>;
}

div#bridge div#twitterFeedOutput {
    border-left: 4px solid <?php wdes_opts_show('color-bridge-twitter-border'); ?>;
}

div#bridge .panel-sidebar .panel-heading {
    background: <?php wdes_opts_show('color-bridge-sidebar-head'); ?> !important;
}

div#bridge .panel-sidebar a.list-group-item.active, div#bridge .panel-sidebar a.list-group-item.active:focus, div#bridge .panel-sidebar a.list-group-item.active:hover {
    background: <?php wdes_opts_show('color-bridge-sidebar-active-tab'); ?> !important;
    border-color: <?php wdes_opts_show('color-bridge-sidebar-active-tab'); ?> !important;
}

div#bridge .btn-primary {
    background: <?php wdes_opts_show('color-bridge-btn-bg'); ?>;
    border-color: <?php wdes_opts_show('color-bridge-btn-bg'); ?>;
}

/* =======================
/*  404
/* ====================== */

.error-p{
background : <?php wdes_opts_show('page404-background'); ?> url(<?php wdes_opts_show('page404-background-img') ?>);
}

.title-error h2 {
    color: <?php wdes_opts_show('page404-main');?>
}

.title-error p {
color: <?php echo wdes_opts_show('page404-desc');?>
}

/* =======================
/*  Coming Soon
/* ====================== */

.coming{
background : <?php wdes_opts_show('comingsoon-background', '#204056'); ?> url(<?php wdes_opts_show('comingsoon-background-img') ?>);
}

.com-title {
color: <?php wdes_opts_show('comingsoon-entry', '#fff');?>
}

/* ==============================================
/*  Typography
/* ============================================= */

/* =======================
/*  Menu
/* ====================== */

<?php $typo_menu_link = wdes_opts_get('typography-main-menu'); ?>
.wdes-menu-navbar ul.default-drop > li > a {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_menu_link['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_menu_link['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_menu_link['size']); ?> px;
}

<?php $typo_sub_menu_link = wdes_opts_get('typography-sub-menu'); ?>
.wdes-menu-navbar ul.default-drop > li > ul.dropdown-content > li > a {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_sub_menu_link['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_sub_menu_link['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_sub_menu_link['size']); ?> px;
}

<?php $typo_mega_menu_link = wdes_opts_get('typography-mega-menu'); ?>
.wdes-menu-navbar .mega-w.dropdown-content li.heading > a {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_mega_menu_link['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_mega_menu_link['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_mega_menu_link['size']); ?> px;
}

/* =======================
/*  Blog
/* ====================== */

<?php $typo_blog_bread_title = wdes_opts_get('typo_blog_bread_title'); ?>
.parent-title-archive h2 {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_blog_bread_title['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_blog_bread_title['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_blog_bread_title['size']); ?>px;
}

<?php $typo_blog_t = wdes_opts_get('typography-blog-t'); ?>
.classic-post-view a.title-art-sub {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_blog_t['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_blog_t['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_blog_t['size']); ?>px;
}

<?php $typo_blog_t_s = wdes_opts_get('typography-blog-t-s'); ?>
.inner-item-art .title-art-sub {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_blog_t_s['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_blog_t_s['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_blog_t_s['size']); ?>px;
}

/* =======================
/*  Body
/* ====================== */

<?php $typo_typo_p = wdes_opts_get('typography-body-p'); ?>
p {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_typo_p['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_typo_p['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_typo_p['size']); ?>px;
}

<?php $typo_typo_blockquote = wdes_opts_get('typography-body-blockquote'); ?>
blockquote {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_typo_blockquote['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_typo_blockquote['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_typo_blockquote['size']); ?>px;
}

<?php $typo_typo_links = wdes_opts_get('typography-body-links'); ?>
a {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_typo_links['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_typo_links['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_typo_links['size']); ?>px;
}

/* =======================
/*  Headings
/* ====================== */

<?php $typo_heading_h1 = wdes_opts_get('heading-h1'); ?>
h1 {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_heading_h1['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_heading_h1['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_heading_h1['size']); ?>px;
}

<?php $typo_heading_h2 = wdes_opts_get('heading-h2'); ?>
h2 {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_heading_h2['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_heading_h2['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_heading_h2['size']); ?>px;
}

<?php $typo_heading_h3 = wdes_opts_get('heading-h3'); ?>
h3 {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_heading_h3['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_heading_h3['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_heading_h3['size']); ?>px;
}

<?php $typo_heading_h4 = wdes_opts_get('heading-h4'); ?>
h4 {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_heading_h4['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_heading_h4['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_heading_h4['size']); ?>px;
}

<?php $typo_heading_h5 = wdes_opts_get('heading-h5'); ?>
h5 {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_heading_h5['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_heading_h5['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_heading_h5['size']); ?>px;
}

<?php $typo_heading_h6 = wdes_opts_get('heading-h6'); ?>
h6 {
    font-family: "<?php x_wdes()->wdes_get_text ($typo_heading_h6['fonts']); ?>";
    font-weight: <?php x_wdes()->wdes_get_text ($typo_heading_h6['weight-font']); ?>;
    font-size: <?php x_wdes()->wdes_get_text ($typo_heading_h6['size']); ?>px;

}

<?php if( empty( wdes_opts_get( 'preloader' ) ) ): ?>
html,body{
    overflow:auto
}
<?php endif; ?>