<?php
namespace Phox\core;

use Phox\core\menu\Wdes_Walker_Nav_Secondary_Menu;

/**
 * Footer
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */
class Wdes_Footer {
	/**
     * All classes
     *
	 * @var $classes.
	 */
	public $classes = [];

	/**
	 * Template Id
	 *
	 * @var Wdes_Header
	 */
	private $temp_id = 0;

	protected static $_instance = null;


	public static function instance()
	{
		if ( is_null( self::$_instance ) )
		{
			self::$_instance = new self;
		}

		return self::$_instance;
	}

	/**
	 * Constructor.
	 */
	function __construct() {

		$this->image_background();

	}

	/**
	 * Footer Company.
	 */
	public function footer_company() {

	    $footer_display = wdes_opts_get( 'footer-display' );
		$copyright = wdes_opts_get( 'footer-copyright' );
		$copyright_text = ! empty( $copyright ) ? str_replace( [ '{{Y}}', '{{sitename}}' ], [ date_i18n( 'Y' ), get_bloginfo( 'name' ) ], $copyright  )  : '' ;
        if( ! isset( $footer_display ) ){
		?>
		<div class="copyrights-wdes-ft">
			<div class="container">
                <?php if( $copyright_text ): ?>
				    <p class="wdes-copyrights-text"> <?php echo esc_html( $copyright_text ); ?> </p>
                <?php endif; ?>
				<div class="sc-links-ft">
					<?php
                        wp_nav_menu(
                            [
                                'container_id'    => 'menu-footer-nav',
                                'container_class' => 'footer-menu',
                                'theme_location'  => 'footer',
                                'depth'           => 1,
                                'fallback_cb'     => 'Phox\core\menu\Wdes_Walker_Nav_Menu::fallback',
                                'walker'          => new Wdes_Walker_Nav_Secondary_Menu()
                            ]
                        );
					?>
				</div>
			</div>
		</div>
		<?php
        }
	}


	/**
     * Image BG.
     *
	 * @return string
	 */
	public function image_background() {
		$footer_bg = wdes_opts_get( 'footer-bg-img' );

		if ( ! empty( $footer_bg ) ) {

			$class = 'footer-bg';

		} else {

			$class = '';

		}

		return $this->classes[] = $class;

	}

	/**
	 * Site Footer Block
	 *
	 * @since 1.4.2
	 */
      public function site_footer () {

            ob_start();
            ?>

            <footer class="<?php array_map(function ($class){ echo esc_attr ($class.' '); },wdes_footer()->classes); ?>footer">
              <?php x_wdes()->wdes_get_tp('footer/footer', 'main') ?>

              <!-- Partners -->
              <?php x_wdes()->wdes_get_tp('footer/footer', 'payments') ?>

              <?php $this->footer_company(); ?>
            </footer>

            <?php

            $output = ob_get_contents();
            ob_end_clean();

            x_wdes()->wdes_get_text( $output ) ;

      }

	/**
	 * Go to top Button
     * Add go to top button to footer.php
     *
     * @since 1.6.1
	 */
      public function goto_top_but(){
          $show_backto_top_btn = wdes_opts_get('backto-btn-display');
          $position_backto_top_btn = wdes_opts_get('backto-btn-position', 'right');
          if($show_backto_top_btn){
              echo '<button id="wdes-back-to-top" class="wdes-btn-align-'.$position_backto_top_btn.'"><span class="fas fa-arrow-up"></span></button>';
          }

      }
	/**
	 * Get Footer builder ID
	 * if option website footer is have value
	 *
	 * @since 2.0.0
	 * @return false|int
	 */
	public function get_footer_builder_id(){

		$this->temp_id = (int) wdes_opts_get( 'website_footer', 'default' );

		if ( ! $this->temp_id > 0 ){

			return false;

		}

		return $this->temp_id;

	}

	/**
	 * Get Footer builder Template
	 * after check the id it get footer template
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function get_footer_template(){

		\Phox\helpers::get_builder_content_for_display($this->temp_id);

	}


}
