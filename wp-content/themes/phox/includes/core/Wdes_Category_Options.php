<?php
namespace Phox\core;

/**
 * Category Options
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */
class Wdes_Category_Options {
	/**
	 * Instance .
	 *
	 * @var Wdes_Category_Options
	 */
	protected static $_instance = null;

	/**
	 * Get Category_Options instance .
	 *
	 * @return Wdes_Category_Options
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;

	}

	/**
	 * Constructor.
	 */
	public function __construct() {
		add_action( 'category_edit_form', array( $this, 'wdes_custom_category_options' ) );
		add_action( 'edited_category', array( $this, 'wdes_save_category_options' ) );
	}

	/**
	 * Custom category options.
	 *
	 * @param int $id  The category Id.
	 */
	public function wdes_custom_category_options( $id ) {
			$value = x_wdes()->wdes_get_category_option( $id->term_id );
		?>
		<tr class="form-field term-color-wrap">
			<th scope="row"><label for="wdes-color"><?php esc_attr_e( 'Color Label', 'phox'); ?></label></th>
			<td>
				<input type="color" id="wdes-color" name="wdes_cat_color" value="<?php x_wdes()->wdes_get_text( $value ); ?>" />
			</td>
		</tr>
		<?php
	}

	/**
	 * Save category options.
	 *
	 * @param int $cat_id The category Id.
	 */
	public function wdes_save_category_options( $cat_id ) {
		if ( ! empty( $_POST['wdes_cat_color'] ) ) {

			$wdes_cats_options = get_option( 'wdes_cats_options' );

			$data = sanitize_text_field($_POST['wdes_cat_color']);

			$wdes_cats_options[ $cat_id ] = $data;
			update_option( 'wdes_cats_options', $wdes_cats_options, 'yes' );

		}

	}
}
