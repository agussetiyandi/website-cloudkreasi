<?php
namespace Phox\core\menu;

use \Elementor\Plugin;

/**
 * Register New Fields In Nav Menu
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */

/**
 * Wdes_Menu_Register_Fields
 *
 * Main class for adding custom menu item fields in admin area
 *
 * @since 1.5.0
 */
class Wdes_Menu_Register_Fields {
	/**
     * Instance
     *
	 * @var null
	 */
	protected static $_instance = null;

	/**
     * Instance
     *
	 * @return null|Wdes_Menu_Register_Fields
	 */
	public static function instance()
	{
		if ( is_null( self::$_instance ) )
		{
			self::$_instance = new self;
		}

		return self::$_instance;
	}

	/**
     * Menu items Options
     *
	 * @var array
	 */
	private $menu_item_fileds = [];

	/**
     * All Elementor Templates
     *
	 * @var array
	 */
	private $elementor_templates = [];

	private $data = [];

	/**
	 * Menu_Fields constructor.
	 */
	public function __construct()
	{

	    $this->get_elementor_templates();

	    //add custom fields
        $this->menu_item_fileds = [
	        'mega' => [
		        'label' => esc_html__( 'Mega Menu', 'phox' ),
		        'type'  => 'checkbox',
		        'default' => '0'
	        ],
            'mega_type' => [
                'label' => esc_html__('Mega Menu Type', 'phox'),
                'type'  => 'select',
                'options' => [
	                'normal'        => esc_html__('Normal Mega Menu', 'phox'),
	                'with_widget'   => esc_html__('Mega Menu With Widget ', 'phox'),
	                'template'   => esc_html__('Mega Menu With Template', 'phox'),
                ],
                'default' => 'normal'
            ],
	        'mega_template' => [
		        'label' => esc_html__('Select Elementor Template', 'phox'),
		        'type'  => 'select',
		        'options' => $this->elementor_templates,
		        'default' => ''
	        ],
	        'col_num' => [
		        'label' => esc_html__( 'Mega Menu Number of Columns', 'phox' ),
		        'type'  => 'select',
                'options' => [
                  1 => 1,
                  2 => 2,
                  3 => 3,
                  4 => 4,
                  5 => 5,
                  6 => 6,
                ],
		        'default' => '2'
	        ],
            'icon' => [
                'label' => esc_html( 'Menu Item Icon' ),
                'type'  => 'icon',
                'default' => '',
                'depth' => [ 'min' => 0, 'max' => 100 ]
            ],
	        'icon_color' => [
		        'label' => esc_html( 'Icon Color' ),
		        'type'  => 'color',
		        'default' => '',
		        'depth' => [ 'min' => 0, 'max' => 100 ]
	        ],
	        'icon_align' => [
		        'label' => esc_html__('Icon Alignment', 'phox'),
		        'type'  => 'select',
		        'options' => [
                    'auto'  => esc_html__('Auto', 'phox'),
                    'left'  => esc_html__('Left', 'phox'),
                    'right'  => esc_html__('Right', 'phox'),
                ],
		        'default' => '',
		        'depth' => [ 'min' => 0, 'max' => 100 ]
	        ],
	        'item_label' =>[
		        'label' => esc_html__('Label', 'phox'),
		        'type'  => 'text',
		        'depth' => [ 'min' => 0, 'max' => 100 ]
	        ],
	        'label_color' => [
		        'label' => esc_html( 'Label Color' ),
		        'type'  => 'color',
		        'default' => '',
		        'depth' => [ 'min' => 0, 'max' => 100 ]
	        ],
	        'label_bg_color' => [
		        'label' => esc_html( 'Label Background Color' ),
		        'type'  => 'color',
		        'default' => '',
		        'depth' => [ 'min' => 0, 'max' => 100 ]
	        ],
            'css_class' =>[
	            'label' => esc_html__('Add Custom Class', 'phox'),
	            'type'  => 'text',
	            'depth' => [ 'min' => 0, 'max' => 100 ]
            ],
	        'sec_text' =>[
		        'label' => esc_html__('Secondary Text', 'phox'),
		        'type'  => 'textarea',
		        'depth' => [ 'min' => 1, 'max' => 100 ]
	        ],
            'mega_widgets' => [
		        'label' => esc_html__('Select Mega Menu Sidebar', 'phox'),
		        'type'  => 'sidebars',
		        'default' => '0',
                'depth' => [ 'min' => 1, 'max' => 1 ]
	        ],
        ];

        //add extra fields to menu item in backend menu editor
        add_filter( 'wp_setup_nav_menu_item', [ $this, 'add_custom_nav_item_fields' ] );
		//Edit Menus Page
		add_filter('wp_edit_nav_menu_walker',  [$this, 'wdes_custom_nav_edit_walker'], 10, 2);
		//The Custom Menu
		add_action('wp_nav_menu_item_custom_fields', [$this, 'wdes_custom_fields'], 10, 4);
		//Save The custom Fields
		add_action('wp_update_nav_menu_item', [$this, 'wdes_custom_save'],10 ,3);
		//register css and js for edit menu page
        add_action( 'admin_menu', [$this, 'enqueue_edit_menu'] );
	}

	/**
	 * Add custom fields to menu item object
	 */
	public function add_custom_nav_item_fields( $menu_item ){

	    foreach ( $this->menu_item_fileds as $filed_id => $filed_info ){

		    $menu_item->{$filed_id} = get_post_meta( $menu_item->ID, '_menu_item_' . $filed_id, true );

        }

        return $menu_item;

    }

	/**
	 * Wdes Custom Fields
	 *
	 * @param $item_id
	 * @param $item
	 *
	 */
	public function wdes_custom_fields($item_id, $item, $depth, $args)
	{

	    global $wp_registered_sidebars;

	    foreach ( $this->menu_item_fileds as $field_id => $field_info ){

            if( isset($field_info['depth']) ){
	            $data_depth = 'data-depth=' . json_encode([ 'min' => $field_info['depth']['min'], 'max' =>$field_info['depth']['max'] ]) . ' ';
            }else{
	            $data_depth = 'data-depth=' . json_encode([ 'min' => 0, 'max' =>0 ]) . '';
            }


	        switch ( $field_info['type'] ) {

                case 'checkbox':
                ?>
                <p class="wdes-menu-element field-<?php echo esc_attr( $field_id ); ?> description-wide wdes-mega-setting-field-<?php echo esc_attr( $field_info['type'] )?> wdes-mega-setting-<?php echo esc_attr( $field_id )?>" <?php echo esc_attr($data_depth) ?>>
                    <label for="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e($field_info['label'], 'phox') ?><br>
                        <input type="checkbox" id="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>" name="menu-item-<?php echo esc_attr($field_id); ?>[<?php echo esc_attr($item_id) ?>]" <?php checked($item->{$field_id}, '1') ?>>
                    </label>
                </p>
                <?php
                break;

		        case 'select':

			        ?>
                    <p class="wdes-menu-element field-<?php echo esc_attr( $field_id ); ?> description wdes-mega-setting-field-<?php echo esc_attr( $field_info['type'] )?> wdes-mega-setting-<?php echo esc_attr( $field_id )?>" <?php echo esc_attr($data_depth) ?>>
                        <label for="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>">
					        <?php esc_html_e($field_info['label'], 'phox') ?><br>
                        </label>
                        <select id="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>" name="menu-item-<?php echo esc_attr($field_id); ?>[<?php echo esc_attr($item_id) ?>]" >
                            <?php
                            if( is_array( $field_info['options'] ) ){
	                            foreach ( $field_info['options'] as $option_id => $option_value  ){
                                    ?>
                                    <option value="<?php echo esc_attr($option_id) ?>" <?php selected( $item->{$field_id}, $option_id, true ); ?> > <?php echo esc_html( $option_value ) ?>  </option>
                                    <?php
	                            }
                            }
                            ?>
                        </select>
                    </p>
			        <?php
			        break;

		        case 'sidebars':
			        ?>
                    <p class="wdes-menu-element field-<?php echo esc_attr( $field_id ); ?> description wdes-mega-setting-field-<?php echo esc_attr( $field_info['type'] )?> wdes-mega-setting-<?php echo esc_attr( $field_id )?>" <?php echo esc_attr($data_depth) ?>>
                        <label for="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>">
					        <?php esc_html_e($field_info['label'], 'phox') ?><br>
                        </label>
                        <select id="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>" name="menu-item-<?php echo esc_attr($field_id); ?>[<?php echo esc_attr($item_id) ?>]" >
                            <option value="0" <?php selected( $item->{$field_id}, 0, true ); ?> > <?php echo esc_html__( 'Select Widget Area', 'phox' ); ?>  </option>
					        <?php
					        if( is_array( $wp_registered_sidebars ) ){
						        foreach ( $wp_registered_sidebars as $sidebar  ){
							        ?>
                                    <option value="<?php echo esc_attr($sidebar['id']) ?>" <?php selected( $item->{$field_id}, $sidebar['id'], true ); ?> > <?php echo esc_html( $sidebar['name'] ); ?>  </option>
							        <?php
						        }
					        }
					        ?>
                        </select>
                    </p>
			        <?php
			        break;

                case 'icon':
                    ?>

                    <p class="wdes-menu-element description description-wide wdes-mega-setting-field-<?php echo esc_attr( $field_info['type'] )?> wdes-mega-setting-<?php echo esc_attr( $field_id )?>" <?php echo esc_attr($data_depth) ?>>
                        <label for="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>">
			                <?php esc_html_e($field_info['label'], 'phox') ?><br>
                            <input type="text" id="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>" name="menu-item-<?php echo esc_attr($field_id); ?>[<?php echo esc_attr($item_id) ?>]" data-placement="bottomRight" class="widefat edit-menu-item-<?php echo esc_attr( $field_id )?> form-control icp icp-auto iconpicker-element wdes-iconpicker-input" value="<?php echo esc_html($item->{$field_id}); ?>">
                            <span class="input-group-addon"><i class="<?php echo esc_html($item->{$field_id}); ?>"></i></span>
                        </label>
                    </p>

                    <?php
                    break;

                case 'color':
	                ?>
                    <p class="wdes-menu-element description description-wide wdes-mega-setting-field-<?php echo esc_attr( $field_info['type'] )?> wdes-mega-setting-<?php echo esc_attr( $field_id )?>" <?php echo esc_attr($data_depth) ?>>
                        <label for="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>">
	                        <?php esc_html_e($field_info['label'], 'phox') ?><br>
                        </label>
                        <input type="text" id="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-<?php echo esc_attr( $field_id )?> wdes-color-selector" name="menu-item-<?php echo esc_attr($field_id); ?>[<?php echo esc_attr($item_id) ?>]" value="<?php echo esc_html($item->{$field_id}); ?>">

                    </p>

	                <?php
                    break;

		        case 'textarea':
			        ?>

                    <p class="wdes-menu-element description description-wide wdes-mega-setting-field-<?php echo esc_attr( $field_info['type'] )?> wdes-mega-setting-<?php echo esc_attr( $field_id )?>" <?php echo esc_attr($data_depth) ?>>
                        <label for="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>">
					        <?php esc_html_e($field_info['label'], 'phox') ?><br>
                            <textarea id="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-<?php echo esc_attr( $field_id )?>" name="menu-item-<?php echo esc_attr($field_id); ?>[<?php echo esc_attr($item_id) ?>]" cols="30" rows="10"><?php echo esc_html($item->{$field_id}); ?></textarea>
                        </label>
                    </p>

                    <?php
			        break;
                case 'text':
                default:
                ?>

                <p class="wdes-menu-element description description-wide wdes-mega-setting-field-<?php echo esc_attr( $field_info['type'] )?> wdes-mega-setting-<?php echo esc_attr( $field_id )?>" <?php echo esc_attr($data_depth) ?>>
                    <label for="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>">
				        <?php esc_html_e($field_info['label'], 'phox') ?><br>
                        <input type="text" id="edit-menu-item-<?php echo esc_attr($field_id); ?>-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-<?php echo esc_attr( $field_id )?>" name="menu-item-<?php echo esc_attr($field_id); ?>[<?php echo esc_attr($item_id) ?>]" value="<?php echo esc_html($item->{$field_id}); ?>">
                    </label>
                </p>

                <?php
                break;

            }

        }

	}


	/**
	 * Wdes Custom Save
	 *
	 * @param $menu_id
	 * @param $menu_item_db_id
	 * @param $args
	 */
	public function wdes_custom_save($menu_id, $menu_item_db_id, $args){

	    foreach ( $this->menu_item_fileds as $field_id => $field_info ){

		    if( 'checkbox' == $field_info['type'] ){

		        if( ! isset( $_POST['menu-item-'. $field_id][ $menu_item_db_id ] ) ){

			        $_POST['menu-item-'. $field_id][ $menu_item_db_id ] = '0';

                }else{

			        $_POST['menu-item-'. $field_id][ $menu_item_db_id ] = '1';

                }

		    } elseif( ! isset( $_POST['menu-item-'. $field_id][ $menu_item_db_id ] ) ){

		        $_POST['menu-item-'. $field_id][ $menu_item_db_id ] = '';

            }

		    update_post_meta( $menu_item_db_id, '_menu_item_'. $field_id, $_POST['menu-item-'. $field_id][ $menu_item_db_id ] );

        }


	}

	/**
	 * Wdes Custom Nav Edit Walker
	 *
	 * @param $walker
	 * @param $menu_id
	 * @return string
	 */
	public function wdes_custom_nav_edit_walker($walker, $menu_id)
	{
		return 'Phox\core\menu\Wdes_Menu_Edit';
	}

	/**
     * Load specific asset files on edit menu page
     */
    public function enqueue_edit_menu(){
        global $pagenow;

        if( 'nav-menus.php' == $pagenow ){

	        wp_enqueue_style( 'fontawesome5', WDES_INC_URI . '/core/assets/css/back/lib/fontawesome5.css', null, '1.0' );
	        wp_enqueue_style( 'iconpickercss', WDES_INC_URI . '/core/assets/css/back/lib/fontawesome-iconpicker.min.css', null, '1.0' );
	        wp_enqueue_style( 'wdes-editmenuscss', WDES_INC_URI . '/core/assets/css/back/edit-menus.css', null, '1.0' );
	        wp_enqueue_script( 'iconpickerjs', WDES_INC_URI . '/core/assets/js/back/lib/fontawesome-iconpicker.min.js', null, '1.0' );
	        wp_enqueue_script( 'wdes-editmenusjs', WDES_INC_URI . '/core/assets/js/back/edit-menus.js', ['nav-menu'], '1.0' );
	        //Color picker
	        wp_enqueue_style('wp-color-picker');
	        wp_enqueue_script( 'wdes-menu-color-picker', WDES_INC_URI . '/core/assets/js/back/color-picker.js', array('jquery', 'jquery-ui-sortable', 'jquery-ui-draggable', 'wp-color-picker'), false, true );

        }



    }

    /**
     * Load Elementor Templates
     *
     * Get all saved template to use in the menu
     * Test with elementor 2.6.1
     *
     */
    public function get_elementor_templates(){

        if( did_action( 'elementor/loaded' ) ){

	        $args = [
		        'post_type' => 'elementor_library',
                'posts_per_page' => -1
	        ];

	        $elementor_query = new \WP_Query($args);

	        if( $elementor_query->have_posts() ){
		        foreach ($elementor_query->posts as $key => $value ){

			        $this->elementor_templates['te-'.$value->ID] = $value->post_title ;

		        }
	        }

	        wp_reset_postdata();

        }else{

	        $this->elementor_templates = [ 'no' => 'No Template' ];

        }


        $default_template = [ '' => 'Select Template' ];

	    $this->elementor_templates = array_merge($this->elementor_templates, $default_template);

    }
    /**
     * Get All Sidebar Name
     */
    public function get_sidebars_names(){

	    $sidebars_names = [];

	    $get_sidebars = get_option( 'sidebars_widgets', array() );

	    foreach (wp_get_sidebars_widgets() as $name => $value){
		    $replace_name = str_replace('-',' ',$name);
		    $sidebars_names [$name]= ucfirst($replace_name);
	    }

	    $sidebars_names ['wp_inactive_widgets'] = 'Select SideBar';

	    return $sidebars_names;

    }

}