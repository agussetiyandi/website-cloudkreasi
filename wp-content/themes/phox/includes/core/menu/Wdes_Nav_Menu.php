<?php
namespace Phox\core\menu;
/**
 * Main Class That Control Menu
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */


/**
 * Wdes Nav Menu
 *
 * Main class that will add configuration for extending WordPress Menu
 *
 * @since 1.5.0
 */
class Wdes_Nav_Menu{

	/**
	 * Instance .
	 *
	 * @var $_instance
	 */
	protected static $_instance = null;


	/**
	 * Get Wdes_Nav_Menu instance .
	 *
	 * @return Wdes_Nav_Menu
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

	}

	function __construct() {

		if( ! is_admin() ){

			add_filter( 'wp_nav_menu_args', [ $this, 'wdes_wp_nav_menu' ], 9,1 );

		}

	}

	/**
	 * Wdes Wp Nav Menu
	 *
	 * Modifies thw walker class and default args for menu
	 *
	 * @return array $args
	 */
	public function wdes_wp_nav_menu( $args ) {

		if( $args['fallback_cb'] !== 'wp_page_menu' ) {
			return $args;
		}

		$args['container'] = 'nav';

		$default_menu_theme_location = ['main-menu'];


		if( in_array( $args['theme_location'], $default_menu_theme_location ) ){

			$args['menu_class'] = 'default-drop responsive-menu-show';
			$args['fallback_cb'] = 'Phox\core\menu\Wdes_Walker_Nav_Menu::fallback';
			$args['walker']      = new Wdes_Walker_Nav_Menu();

		}


		$mobile_button = '<button class="wdes-mob-btn wdes-btn-mob-toggle"> <div class="icon-menu"> <span class="line line-1"></span> <span class="line line-2"></span> <span class="line line-3"></span></div></button>';

		$args['items_wrap'] = $mobile_button. '<div class="menu-d-mob"> <ul id="%1$s" class="%2$s" >' . '%3$s' .'</ul><div></div>';

		return $args;

	}

}