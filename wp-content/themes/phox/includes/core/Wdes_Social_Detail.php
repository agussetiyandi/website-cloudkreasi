<?php
namespace Phox\core;

use Phox\core\menu\Wdes_Walker_Nav_Secondary_Menu;

/**
 * Social
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */
class Wdes_Social_Detail {

	/**
	 * Constructor.
	 */
	public function __construct() {

		add_action( 'wdes_top_company_box', array( $this, 'Detail_Box' ) );
		add_action( 'wdes_bottom_company_box', array( $this, 'Company_Card' ) );
		add_action( 'wdes_header_social', array( $this, 'Header_Social' ), 10, 2 );

	}

	/**
	 * Detail Box.
	 */
	public function Detail_Box() {
		wp_nav_menu([
		   'container_id'     => 'wdes-menu-top-header',
           'container_class'  => 'mega-top-header',
           'theme_location'   => 'header-secondary',
           'depth'            =>  1 ,
           'fallback_cb'      => 'Phox\core\menu\Wdes_Walker_Nav_Menu::fallback',
           'walker'          => new Wdes_Walker_Nav_Secondary_Menu()
        ]);

	}

	/**
     * Detail box block.
     *
	 * @param $option
	 */
	public function Detail_Box_Block( $option ) {
		$item    = wdes_opts_get( $option );
		$icon    = '';
		$element = '';

		if ( $option === 'info-email' ) {

			$icon    = 'email.svg';
			$element = '<a href="mailto:' . $item . '">' . $item . '</a>';

		} elseif ( $option === 'info-phone' ) {

			$icon    = 'phone.svg';
			$element = '<span>' . $item . '</span>';

		} elseif ( $option === 'info-location' ) {

			$icon    = 'fa-map-marker';
			$element = '<span>' . $item . '</span>';

		} else {

			$icon    = 'chat.svg';
			$element = '<a target="_blank" href="' . $item . '">' . esc_html__( 'Live Chat', 'phox' ) . '</a>';

		}

		if ( ! empty( $item ) ) {

		    $icon_svg = WDES_ASSETS_URI . '/img/icons/' . $icon;

			$output = '<div class="block-sc">';

				$output .= '<img src="' . esc_url($icon_svg) .'" alt="'.esc_attr__('phone', 'phox').'">';

				$output .= $element;

			$output .= '</div>';

		} else {

			$output = '';

		}

		x_wdes()->wdes_get_text( $output );

	}

	/**
	 * Company Card
	 */
	public function Company_Card() {

		$details = array( 'info-location', 'info-email', 'info-phone', 'info-live-chat' );

		if($this->Company_Card_item_Check($details) == true ){

            ?>

            <div class="contact">

                <?php array_map( array( $this, 'Company_Card_Block' ), $details ); ?>

            </div>

            <?php
        }
	}

	/**
	 * Check Company Item
	 */

	public function Company_Card_item_Check($data){

        $result = false;

	    foreach ($data as $key){

            $item = wdes_opts_get( $key );

            if(!empty($item) ){

                $result = true;

            }

        }

        return $result;


    }

	/**
     * Company card block.
     *
	 * @param $option
	 */
	public function Company_Card_Block( $option ) {
		$item = wdes_opts_get( $option );

		if ( $option === 'info-email' ) {

			$icon    = 'fa-envelope fa-fw';
			$element = '<a href="mailto:' . $item . '">' . $item . '</a>';
			$class   = 'mail';

		} elseif ( $option === 'info-phone' ) {

			$icon    = 'fa-phone fa-fw';
			$element = '<span>' . $item . '</span>';
			$class   = 'phone';

		} elseif ( $option === 'info-location' ) {

			$icon    = 'fa-map-marker fa-fw';
			$element = '<span>' . $item . '</span>';
			$class   = 'map';

		} else {

			$icon    = 'fa-headphones fa-fw';
			$element = '<a target="_blank" href="' . $item . '">' . esc_html__( 'Live Chat', 'phox' ) . '</a>';
			$class   = 'l-chat';

		}

		if ( ! empty( $item ) ) {

			$output = '<div class="' . $class . '">';

				$output .= '<span class="fa ' . $icon . ' cont"></span>';

				$output .= $element;

			$output .= '</div>';

		} else {

          $output = '';

		}

		x_wdes()->wdes_get_text( $output );

	}

	/**
     * Social media.
     *
	 * @return array
	 */
	public static function Social_Media() {
		$social = array(
			array(
				'val'  => wdes_opts_get( 'social-media-facebook', '' ),
				'icon' => 'facebook',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-twitter', '' ),
				'icon' => 'twitter',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-google+', '' ),
				'icon' => 'google-plus ',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-skype', '' ),
				'icon' => 'skype',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-youtube', '' ),
				'icon' => 'youtube',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-vimeo', '' ),
				'icon' => 'vimeo',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-instagram', '' ),
				'icon' => 'instagram',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-pinterest', '' ),
				'icon' => 'pinterest',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-dribbble', '' ),
				'icon' => 'dribbble',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-behance', '' ),
				'icon' => 'behance',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-flickr ', '' ),
				'icon' => 'flickr',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-tumblr', '' ),
				'icon' => 'tumblr',
			),
			array(
				'val'  => wdes_opts_get( 'social-media-linkedin', '' ),
				'icon' => 'linkedin',
			),
		);

		return $social;

	}

	/**
     * Header social.
     *
	 * @param $ul_class
	 * @param $li_class
	 */
	public function Header_Social( $ul_class, $li_class ) {

		$all_networks = self::Social_Media();

		echo '<ul class="' . $ul_class . '">';
		foreach ( $all_networks as $item ) {
			if ( ! empty( $item['val'] ) ) {
				echo '<li>';
					echo '<a  class="fab fa-' . $item['icon'] . ' ' . $li_class . '" href="' . esc_url($item['val']) . '" target="_blank"></a>';
				echo '</li>';

			}
		}

		echo '</ul>';
	}

}
