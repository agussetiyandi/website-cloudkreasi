<?php
namespace Phox\import;

/**
 * Demo Import
 * This configuration file that will use one click import plugin
 * to show the demo in it
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */


if( !defined('ABSPATH') ) {
	exit;
}

Class Demo_Import {
	/**
	 * Instance .
	 *
	 * @var Demo_Import
	 */
	protected static $_instance = null;

	/**
	 * Get demo Import instance .
	 *
	 * @return Demo_Import
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;

	}

	public $demo ;

	public function __construct() {

		if( ! is_admin() ){
				return;
		}

		$this->demo = [

			new demo\Primary_Demo ,
			new demo\Modern_Demo  ,
			new demo\Super_Demo   ,
			new demo\Startup_Demo,
			new demo\Saas_Demo,
			new demo\WoHost_Demo,
			new demo\Classy_Demo,
			new demo\Shiny_Demo,
			new demo\Go_Demo,
			new demo\Fancy_Demo

		];

		add_filter('ocdi/plugin_page_setup', array($this, 'plugin_page_setup'), 30 );
		add_filter('ocdi/import_files', array( $this, 'set_import_files' ) );
		add_action('ocdi/after_import', array($this, 'after_import') );
		add_action('ocdi/before_content_import_execution', array($this, 'before_import'), 10, 3);
		add_action('ocdi/time_for_one_ajax_call', array($this, 'change_time_of_single_ajax_call') );
		add_action('ocdi/after_content_import_execution', [$this, 'get_info_about_import'],10,4);
		add_filter('ocdi/before_widgets_import_data', [$this, 'widgets_import'], 10, 4);
		add_filter('ocdi/register_plugins', [$this, 'register_plugins']);
	}

	public function plugin_page_setup($page){

		//show in theme menu
		$page['menu_title']		= 'Demo Import';
		$page['capability']		= 'edit_theme_options';
		$page['menu_slug']		= 'wdes-demo-import';

		return $page;

	}

	public function after_import($selected_import){
		$locations = get_theme_mod('nav_menu_locations');
		$menus = wp_get_nav_menus();

		if( $menus ) {
			foreach ($menus as $menu ) {

				if( $menu->name == 'Menu 1' || $menu->name == 'startup' || $menu->name == 'wohost-main' || $menu->name == 'classy-main' ) {
					$locations['main-menu'] = $menu->term_id;
				}

				if( $menu->name == 'startup-top' || $menu->name == 'host-header' || $menu->name == 'wohost-top' || $menu->name == 'classy-top' ) {
					$locations['header-secondary'] = $menu->term_id;
				}

				if( $menu->name == 'startup-foot' || $menu->name == 'host-footer' || $menu->name == 'wohost-foot' || $menu->name == 'classy-foot' ) {
					$locations['footer'] = $menu->term_id;
				}

			}

			set_theme_mod('nav_menu_locations', $locations);

			$front_page_id = get_page_by_title('Home');
			$blog_page_id  = get_page_by_title('Blog');

			update_option('show_on_front', 'page');
			update_option('page_on_front', $front_page_id->ID);
			update_option('page_for_posts', $blog_page_id->ID);

			do_action( sprintf( 'wdes/demo-import/%s/after-import', $selected_import['demo_id'] ), $selected_import );


		}


	}

	public function set_import_files(){

		return apply_filters( 'wdes/demo-import/files', [], $this );

	}

	public function before_import( $selected_import_files, $import_files, $selected_index ) {

		$demo = is_array( $import_files ) && isset( $import_files[ $selected_index ] ) ? $import_files[ $selected_index ] : false;
		if ( ! is_array( $demo ) || empty( $demo['demo_id'] ) ) {
			return;
		}

	}

	/**
	 * Get info about import
	 *
	 * set information about import file to use in widget_import function
	 *
	 * @param $selected_import_files
	 * @param $import_files
	 * @param $selected_index
	 */
	public function get_info_about_import($selected_import_files, $import_files, $selected_index){
		$demo = is_array( $import_files ) && isset( $import_files[ $selected_index ] ) ? $import_files[ $selected_index ] : false;

		$this->select_imp = $demo;
	}

	/**
	 * Before Widget Import
	 * check all links in widget and replace it
	 * it work with all demos
	 *
	 * @since 1.5.7
	 * @param $import_files
	 * @return mixed
	 */
	public function widgets_import($import_files){
		$import_files = apply_filters( sprintf( 'wdes/demo-import/%s/before_widgets_import_data', $this->select_imp['demo_id'] ), $import_files );
		return $import_files;
	}



	/**
	 * Change Time Of Single Ajax Call
	 * it increase time to 180 second to solve the problem with low server
	 *
	 * @since 1.4.4
	 * @return int
	 */
	public function change_time_of_single_ajax_call() {

		return 180;

	}

	/**
	 * register the require plugins
	 * this plugins if not install the one click import must tell use to must install it to can use the demo
	 *
	 * @since 1.7.4
	 * @return array
	 */
	public function register_plugins(){

		$demo_id = isset($_GET['import']) ? $_GET['import'] : false;

		$require_plugins = [
			[
				'name'        => esc_html__( 'Phox Host', 'phox' ),
				'description' => esc_html__( 'The Core Plugin for Phox', 'phox' ),
				'slug'        => 'phox-host',
				'required'    => true,
				'preselected' => true,
				'source'      => WDES_INC_DIR . '/core/admin/tgm/plugins/phox-host.zip'
			],[
				'name'        => esc_html__( 'Elementor', 'phox' ),
				'description' => esc_html__( 'The Elementor Website Builder has it all: drag and drop page builder, pixel perfect design, mobile responsive editing, and more. Get started now!', 'phox' ),
				'slug'        => 'elementor',
				'required'    => true,
				'preselected' => true,
			],
			[
				'name'        => esc_html__( 'Contact Form 7', 'phox' ),
				'description' => esc_html__( 'Just another contact form plugin. Simple but flexible.', 'phox' ),
				'slug'        => 'contact-form-7',
				'required'    => true,
				'preselected' => true,
			],

		];

		if($demo_id !== '3' && $demo_id !== '4'){

			$host_plugins =[
				'name'        => esc_html__( 'WHMCS Bridge', 'phox' ),
				'description' => esc_html__( 'WHMCS Bridge is a plugin that integrates the powerful WHMCS support and billing software with WordPress.', 'phox' ),
				'slug'        => 'whmcs-bridge',
				'required'    => true,
				'preselected' => true,
			];

			$require_plugins[] = $host_plugins;

		}

		if($demo_id == '3' || $demo_id == '5'){

			$args = [
				'user-agent'  => 'WordPress/'. get_bloginfo('version') .'; '.network_site_url(),
				'timeout'     => 30,
			];

			$response = wp_remote_get( 'https://whmcsdes.com/phoxplugin/plugins.php?version='.WDES_THEME_VERSION, $args );

			if( is_wp_error( $response ) ){
				return $response ;
			}

			$slider_plugins = json_decode( wp_remote_retrieve_body( $response ), true );

			if( is_array( $slider_plugins ) ){
				$slider_plugins[0]['description'] = esc_html__( 'Slider Revolution - Premium responsive slider.', 'phox' );

				$slider_plugins[0]['preselected'] = true;

				$require_plugins[] = $slider_plugins[0];
			}

		}

		return $require_plugins;

	}

}
