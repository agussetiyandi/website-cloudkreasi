<?php
namespace Phox\import\demo;

/**
 * Classy Demo
 * This is seventh demo for the theme
 * the primary color #6a62fe
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */

class Classy_Demo extends Demo {


	public function config() {

		return [
			'demo_id'                  => 'classy',
			'import_file_name'         => 'Classy Demo',
			'import_file_url'          => 'https://www.whmcsdes.com/ca/dl.php?type=d&id=73',
			'import_widget_file_url'   => 'https://whmcsdes.com/phoxdemo/classy/classy_wordpress-widget.wie',
			'preview_url'              => 'https://phox.whmcsdes.com/demos/classy',
			'import_preview_image_url' => 'https://whmcsdes.com/phoxdemo/classy/classy.jpg',
			'update_media_url'         => true
		];

	}

	public function theme_option() {

		$theme_options_json = wp_remote_retrieve_body( wp_remote_get( 'https://whmcsdes.com/phoxdemo/classy/classy_demo-2021-07-08.json' ) );

		return json_decode( $theme_options_json, true );
	}

	/**
	 * Before Widget Import
	 * check all links in widget and replace it
	 *
	 * @since 1.6.5
	 * @param $import_files
	 * @return mixed
	 */
	public function widgets_import($import_files){

		$site_url = get_site_url();
		$replace_wie_url = str_replace('https://phox.whmcsdes.com/classy', $site_url, $import_files->{'footer-widget-1'}->{'widget_wdes_about-1'}->{'image'});
		$import_files->{'footer-widget-1'}->{'widget_wdes_about-1'}->{'image'} = $replace_wie_url;

		return $import_files ;

	}

}