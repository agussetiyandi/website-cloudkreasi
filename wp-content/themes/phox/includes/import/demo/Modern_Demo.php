<?php
namespace Phox\import\demo;

/**
 * Modern Demo
 * This is first demo for the theme
 * the primary color #1f266a
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */

class Modern_Demo extends Demo {


	public function config() {

		return [
			'demo_id'                  => 'modern-demo',
			'import_file_name'         => 'Modern Demo',
			'import_file_url'          => 'https://www.whmcsdes.com/ca/dl.php?type=d&id=68',
			'import_widget_file_url'   => 'https://whmcsdes.com/phoxdemo/primary/primary_wordpress-widget.wie',
			'preview_url'              => 'https://phox.whmcsdes.com/demos/modern',
			'import_preview_image_url' => 'https://whmcsdes.com/phoxdemo/modern/modern.jpg',
			'update_media_url'         => true
		];

	}

	public function theme_option() {

		$theme_options_json = wp_remote_retrieve_body( wp_remote_get( 'https://whmcsdes.com/phoxdemo/modern/modern_demo-2021-07-08.json' ) );

		return json_decode( $theme_options_json, true );
	}

	/**
	 * Before Widget Import
	 * check all links in widget and replace it
	 *
	 * @since 1.5.7
	 * @param $import_files
	 * @return mixed
	 */
	public function widgets_import($import_files){

		$site_url = get_site_url();
		$replace_wie_url = str_replace('https://phox.whmcsdes.com', $site_url, $import_files->{'footer-widget-4'}->{'widget_wdes_about-1'}->{'image'});
		$import_files->{'footer-widget-4'}->{'widget_wdes_about-1'}->{'image'} = $replace_wie_url;

		return $import_files ;

	}

}







