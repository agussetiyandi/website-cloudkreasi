<?php
namespace Phox\import\demo;

/**
 * Super Demo
 * This is third demo for the theme
 * the primary color #29156b
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */

class Super_Demo extends Demo {


	public function config() {

		return [
			'demo_id'                  => 'super-demo',
			'import_file_name'         => 'Super Demo',
			'import_file_url'          => 'https://www.whmcsdes.com/ca/dl.php?type=d&id=69',
			'import_widget_file_url'   => 'https://whmcsdes.com/phoxdemo/primary/primary_wordpress-widget.wie',
			'preview_url'              => 'https://phox.whmcsdes.com/demos/super',
			'import_preview_image_url' => 'https://whmcsdes.com/phoxdemo/super/super.jpg',
			'update_media_url'         => true
		];

	}

	public function theme_option() {

		$theme_options_json = wp_remote_retrieve_body( wp_remote_get( 'https://whmcsdes.com/phoxdemo/super/super_demo-2021-07-08.json' ) );

		return json_decode( $theme_options_json, true );
	}

	/**
	 * Before Widget Import
	 * check all links in widget and replace it
	 *
	 * @since 1.5.7
	 * @param $import_files
	 * @return mixed
	 */
	public function widgets_import($import_files){
		$site_url = get_site_url();

		$replace_wie_url = str_replace('https://phox.whmcsdes.com', $site_url, $import_files->{'footer-widget-4'}->{'widget_wdes_about-1'}->{'image'});
		$replace_wie_img = str_replace('fox.png', 'fox-1.png', $replace_wie_url);
		$import_files->{'footer-widget-4'}->{'widget_wdes_about-1'}->{'image'} = $replace_wie_img;
		return $import_files ;

	}

}







