<?php
namespace Phox\import\demo;

/**
 * Shiny Demo
 * This is eighth demo for the theme
 * the primary color #0A2540
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */

class Shiny_Demo extends Demo {


	public function config() {

		return [
			'demo_id'                  => 'shiny',
			'import_file_name'         => 'Shiny Demo',
			'import_file_url'          => 'https://www.whmcsdes.com/ca/dl.php?type=d&id=78',
			'import_widget_file_url'   => 'https://whmcsdes.com/phoxdemo/shiny/shiny_wordpress-widget.wie',
			'preview_url'              => 'https://phox.whmcsdes.com/demos/shiny',
			'import_preview_image_url' => 'https://whmcsdes.com/phoxdemo/shiny/shiny.jpg',
			'update_media_url'         => true
		];

	}

	public function theme_option() {

		$theme_options_json = wp_remote_retrieve_body( wp_remote_get( 'https://whmcsdes.com/phoxdemo/shiny/shiny_demo-2021-07-08.json' ) );

		return json_decode( $theme_options_json, true );
	}

	/**
	 * Before Widget Import
	 * check all links in widget and replace it
	 *
	 * @since 1.7.2
	 * @param $import_files
	 * @return mixed
	 */
	public function widgets_import($import_files){

		$site_url = get_site_url();
		$replace_wie_url = str_replace('https://phox.whmcsdes.com/shiny', $site_url, $import_files->{'footer-widget-1'}->{'widget_wdes_about-2'}->{'image'});
		$import_files->{'footer-widget-1'}->{'widget_wdes_about-2'}->{'image'} = $replace_wie_url;

		return $import_files ;

	}

}