<?php
namespace Phox\import\demo;

/**
 * Fancy Demo
 * This is tenth demo for the theme
 * the primary color #13c1e9
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */

class Fancy_Demo extends Demo {


	public function config() {

		return [
			'demo_id'                  => 'fancy',
			'import_file_name'         => 'Fancy Demo',
			'import_file_url'          => 'https://www.whmcsdes.com/ca/dl.php?type=d&id=80',
			'import_widget_file_url'   => 'https://whmcsdes.com/phoxdemo/fancy/fancy_wordpress-widget.wie',
			'preview_url'              => 'https://phox.whmcsdes.com/demos/fancy',
			'import_preview_image_url' => 'https://whmcsdes.com/phoxdemo/fancy/fancy.jpg',
			'update_media_url'         => true
		];

	}

	public function theme_option() {

		$theme_options_json = wp_remote_retrieve_body( wp_remote_get( 'https://whmcsdes.com/phoxdemo/fancy/fancy_demo-2021-10-02.json' ) );

		return json_decode( $theme_options_json, true );
	}

	/**
	 * Before Widget Import
	 * check all links in widget and replace it
	 *
	 * @since 2.0.4
	 * @param $import_files
	 * @return mixed
	 */
	public function widgets_import($import_files){

		$site_url = get_site_url();
		$replace_wie_url = str_replace('https://phox.whmcsdes.com/go', $site_url, $import_files->{'footer-widget-1'}->{'widget_wdes_about-1'}->{'image'});
		$import_files->{'footer-widget-1'}->{'widget_wdes_about-1'}->{'image'} = $replace_wie_url;

		return $import_files ;

	}

}