<?php
namespace Phox\import\demo;

/**
 * WoHost Demo
 * This is sixth demo for the theme
 * the primary color #39c36e
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */

class WoHost_Demo extends Demo {


	public function config() {

		return [
			'demo_id'                  => 'wohost',
			'import_file_name'         => 'WoHost Demo',
			'import_file_url'          => 'https://www.whmcsdes.com/ca/dl.php?type=d&id=72',
			'import_widget_file_url'   => 'https://whmcsdes.com/phoxdemo/wohost/wohost_wordpress-widget.wie',
			'preview_url'              => 'https://phox.whmcsdes.com/demos/wohost',
			'import_preview_image_url' => 'https://whmcsdes.com/phoxdemo/wohost/wohost.jpg',
			'revslider'                => [ 'wehost' ],
			'update_media_url'         => true
		];

	}

	public function theme_option() {

		$theme_options_json = wp_remote_retrieve_body( wp_remote_get( 'https://whmcsdes.com/phoxdemo/wohost/wohost_demo-2021-07-08.json' ) );

		return json_decode( $theme_options_json, true );
	}

	/**
	 * Before Widget Import
	 * check all links in widget and replace it
	 *
	 * @since 1.5.9
	 * @param $import_files
	 * @return mixed
	 */
	public function widgets_import($import_files){

		$site_url = get_site_url();
		$replace_wie_url = str_replace('https://phox.whmcsdes.com/wohost', $site_url, $import_files->{'footer-widget-1'}->{'widget_wdes_about-3'}->{'image'});
		$import_files->{'footer-widget-1'}->{'widget_wdes_about-3'}->{'image'} = $replace_wie_url;

		return $import_files ;

	}

}







