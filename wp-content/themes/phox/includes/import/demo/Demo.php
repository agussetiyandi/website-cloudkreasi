<?php
namespace Phox\import\demo;

/**
 * Demo
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */


if( !defined('ABSPATH') ) {
	exit;
}

abstract class Demo {

	public $config;

	public function __construct() {

		$this->config = $this->config();

		add_filter('wdes/demo-import/files', array( $this, 'register_demo' ));
		add_action( 'wdes/demo-import/'.$this->config['demo_id'].'/after-import', array( $this, 'set_theme_options' ) );
		add_action( 'wdes/demo-import/'.$this->config['demo_id'].'/after-import', array( $this, 'import_slider' ) );
		add_action( 'wdes/demo-import/'.$this->config['demo_id'].'/after-import', array( $this, 'replace_demo_url' ) );
		add_filter( 'wdes/demo-import/'.$this->config['demo_id'].'/before_widgets_import_data', array( $this, 'widgets_import' ) );

	}

	abstract public function config();
	abstract public function theme_option();

	public function register_demo( $demo ) {

		$demo[] = $this->config;
		return $demo ;

	}

	public function set_theme_options() {

		$options = $this->theme_option();

		if ( (array) $options ) {

			update_option('phox', $options );

		}


	}

	/**
	 * Import Slider
	 *
	 * Use to import RevSlider to the demo
	 *
	 * @return bool|\WP_Error
	 */
	public function import_slider(){

		$downloader = new \OCDI\Downloader();
		$slider_import_start_time = date('Y-m-d__H-i-s') ;


		$sliders = [] ;
		$demo_args = $this->config ;
		$demo_path = 'https://whmcsdes.com/phoxdemo/'.$demo_args['demo_id'];

		if ( ! class_exists( 'RevSliderSlider' ) ){
			return false;
		}

		if( ! is_callable( [ 'RevSliderSlider', 'importSliderFromPost' ] ) ){
			return new \WP_Error( 'rev_update', 'Revolution Slider is outdated . Please <a href="admin.php?page=wdes-plugins">Update Plugin</a>.' );
		}


		$revslider = new \RevSliderSlider();


		if ( isset( $demo_args['revslider'] ) ){

			if( is_array( $demo_args['revslider'] ) ){

				foreach ( $demo_args['revslider'] as $slider ){

					$sliders[] = $slider . '.zip' ;

				}

			}else{

				$sliders[] = $demo_args['revslider'] . '.zip';

			}

		} else {

			return false ;

		}

		foreach ( $sliders as $slider ) {

			$content_filename = apply_filters( 'wdes/downloaded_content_file_prefix', $slider . '_' ) . $slider_import_start_time . apply_filters( 'wdes/downloaded_content_file_suffix_and_file_extension', '.zip' );
			$file_uri = wp_normalize_path( $demo_path . DIRECTORY_SEPARATOR . $slider );
			$downloader_file = $downloader->download_file( $file_uri, $content_filename );

			ob_start();
			$revslider->importSliderFromPost( true, false, $downloader_file);
			ob_clean();
			ob_end_clean();

		}

		return true;


	}

	/**
	 * Replace Url
	 *
	 * This is use to remove all default url from the elementor editor
	 *
	 * @since 1.5.7
	 */
	public function replace_demo_url(){

		$site_url = get_site_url();
		$demo_args = $this->config ;
		$demo_url = $demo_args['preview_url'];

		if ( ! defined( 'ELEMENTOR__FILE__' ) ){
			return false;
		}

		if( ! is_callable( ['Elementor\Utils','replace_urls'] ) ){
			return new \WP_Error( 'ُElementor', 'Elementor is outdated . Please <a href="admin.php?page=wdes-plugins">Update Plugin</a>.' );
		}

		$elementorUtils = new \Elementor\Utils ;


		if (  isset($demo_args['update_media_url']) && isset( $demo_url ) && isset($site_url) ){

			//fix new directory demos
			$demo_url = str_replace( '/demos', '',$demo_url );

			$elementorUtils-> replace_urls($demo_url, $site_url);

		}

	}

	/**
	 * Before Widget Import
	 * check all links in widget and replace it
	 * it work with all demos
	 *
	 * @since 1.5.7
	 * @param $import_files
	 * @return mixed
	 */
	public function widgets_import($import_files){
		return $import_files;
	}


}



