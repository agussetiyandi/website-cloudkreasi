<?php
namespace Phox\import\demo;

/**
 * Startup Demo
 * This is fourth demo for the theme
 * the primary color #29156b
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */

class Startup_Demo extends Demo {


	public function config() {

		return [
			'demo_id'                  => 'startup',
			'import_file_name'         => 'Startup Demo',
			'import_file_url'          => 'https://www.whmcsdes.com/ca/dl.php?type=d&id=71',
			'import_widget_file_url'   => 'https://whmcsdes.com/phoxdemo/startup/startup_wordpress-widget.wie',
			'preview_url'              => 'https://phox.whmcsdes.com/demos/startup',
			'import_preview_image_url' => 'https://whmcsdes.com/phoxdemo/startup/startup.jpg',
			'revslider'                => [ 'showcase', 'agency', 'builders', 'email', 'youtube-hero' ],
			'update_media_url'         => true
		];

	}

	public function theme_option() {

		$theme_options_json = wp_remote_retrieve_body( wp_remote_get( 'https://whmcsdes.com/phoxdemo/startup/startup_demo-2021-07-08.json' ) );

		return json_decode( $theme_options_json, true );
	}

	/**
	 * Before Widget Import
	 * check all links in widget and replace it
	 *
	 * @since 1.5.7
	 * @param $import_files
	 * @return mixed
	 */
	public function widgets_import($import_files){

		$site_url = get_site_url();
		$replace_wie_url = str_replace('https://phox.whmcsdes.com/startup', $site_url, $import_files->{'footer-widget-1'}->{'widget_wdes_about-2'}->{'image'});
		$import_files->{'footer-widget-1'}->{'widget_wdes_about-2'}->{'image'} = $replace_wie_url;

		return $import_files ;

	}

}







