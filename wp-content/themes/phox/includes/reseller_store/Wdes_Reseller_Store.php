<?php
/**
 * Reseller Store Class
 *
 * @package Phox\reseller_store
 * @author WHMCSdes
 * @since  2.0.1
 * @link https://whmcsdes.com
 */


namespace Phox\reseller_store;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} //Exit if accessed directly

/**
 * The Reseller_Store Integration class
 */
class Wdes_Reseller_Store{

	/**
	 * Check if page is archive
	 *
	 * @since 2.0.1
	 * @var bool
	 */
	private $is_archive = false;

	/**
	 * Check if it is singular page
	 *
	 * @since 2.0.1
	 * @var bool
	 */
	private $is_singular = false;

	/**
	 * Check if it is category page
	 *
	 * @since 2.0.1
	 * @var bool
	 */
	private $is_cat = false;

	/**
	 * Check if it is tag page
	 *
	 * @since 2.0.1
	 * @var bool
	 */
	private $is_tag = false;

	/**
	 * Constructor.
	 *
	 * Setup Reseller_Store
	 * @see load_custom_templates
	 * @see body_class
	 * @since 2.0.1
	 */
	public function __construct() {

		add_action( 'template_include', [ $this ,'load_custom_templates'] );
		add_filter( 'body_class', [$this ,'body_class'] );

	}





	/**
	 * Load Custom templates
	 *
	 * @param string $template Template to load.
	 * @since 2.0.1
	 * @return string
	 */
	public function load_custom_templates($template){

		//Define property
		$this->is_archive = is_post_type_archive( 'reseller_product' );
		$this->is_singular = is_singular( 'reseller_product' );
		$this->is_cat = is_tax( 'reseller_product_category' );
		$this->is_tag = is_tax( 'reseller_product_tag' );


		if ( is_embed() ) {
			return $template;
		}

		if( $this->is_archive ) {

			$template = x_wdes()->wdes_get_tp( 'reseller_product/archive', 'products');


		}

		if( $this->is_singular ){

			$template = x_wdes()->wdes_get_tp( 'reseller_product/single', 'product');

		}

		if( $this->is_cat ){

			$template = x_wdes()->wdes_get_tp( 'reseller_product/taxonomy', 'product-cat');

		}

		if( $this->is_tag ){

			$template = x_wdes()->wdes_get_tp( 'reseller_product/taxonomy', 'product-tag');

		}


		return $template;

	}

	/**
	 * Add Custom classes to body
	 *
	 * @param array $classes An array of body class names.
	 * @since 2.0.1
	 * @return array
	 */
	public function body_class($classes){

		$include = array
		(
			'godaddy-reseller-products'  ,
			'woocommerce'
		);

		foreach ( $include as $class )
		{
			if ( $this->is_archive || $this->is_singular || $this->is_cat || $this->is_tag ) $classes[ $class ] = $class;
		}

		return $classes;

	}

}