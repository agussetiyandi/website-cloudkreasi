<?php
/**
 * Reseller Store Template Class
 *
 * @package Phox\reseller_store
 * @author WHMCSdes
 * @since  2.0.1
 * @link https://whmcsdes.com
 */

namespace Phox\reseller_store;

use Phox\helpers;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} //Exit if accessed directly

/**
 * Create Reseller Store Template
 */
abstract class Wdes_Reseller_Store_Template {

	/**
	 * Output the start of the page wrapper.
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::wrappers
	 */
	public function output_content_wrapper() {

		print ( '<div class="wdes-bread">' );

		do_action( 'wdes_rs_header_archive' );

		print ( '</div>' );

		print ( '<div class="wrapper-wdes">' );
		print ( '<div class="container"> ' );
		print ( '<div class="row"> ' );

	}

	/**
	 * Close the theme wrapper
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::wrappers
	 */
	public function output_content_wrapper_end() {

		do_action( 'wdes_rs_sidebar' );
		echo '</div>';
		echo '</div>';
		echo '</div>';

	}


	/**
	 * Breadcrumb
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::header_archive
	 */
	public function breadcrumb() {

		x_wdes()->wdes_breadcrumb();

	}

	/**
	 * Sidebar
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::sidebar
	 */
	public function create_sidebar() {

		if ( is_active_sidebar( 'sidebar-left' ) ) {

			echo '<div class="sidebar-area">';

			dynamic_sidebar( 'sidebar-left' );

			echo '</div>';

		}

	}

	/**
	 * Page title
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::header_archive
	 */
	public function page_title() {

		echo '<h1 class="rs-products-header__title page-title">';
		esc_html_e( 'Products', 'phox' );
		echo ' </h1>';

	}

	/**
	 * Open the shop loop
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::shop_loop
	 */
	public function shop_loop_start() {

		$col_class = ( is_active_sidebar( 'sidebar-left' ) ) ? 'custom-width-post' : '';

		printf ( '<div class="wide-posts-block %s">', $col_class );

		print( '<ul class="products">');

	}

	/**
	 * Close the shop loop
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::shop_loop
	 */
	public function shop_loop_end() {

		print ( '</ul>' );
		print ( '</div>' );

	}

	/**
	 * Output Pagination
	 *
	 * @since  2.0.1
	 * @see  helpers::wdes_posts_pagination
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::output_pagination
	 */
	public function output_pagination() {

		print ( '<div class="clearfix"></div>' );

		x_wdes()->wdes_posts_pagination();

	}

	/**
	 * Insert the opening anchor tag for products in the loop.
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::template_loop_product_link
	 */
	public function template_loop_product_link_open() {

		$link = get_the_permalink();

		echo '<a href="' . esc_url( $link ) . '" class="wdes-rs-LoopProduct-link wdes-rs-loop-product-link">';
	}

	/**
	 * Insert the closing anchor tag for products in the loop.
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::template_loop_product_link
	 */
	public function template_loop_product_link_close() {
		echo '</a>';
	}

	/**
	 * Get the add to cart template for the loop.
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::template_loop_product_link
	 */
	public function template_loop_add_to_cart() {

		global $post;

		$redirect = ! ( (bool) rstore_get_product_meta( $post->ID, 'skip_cart_redirect' ) );

		echo rstore_add_to_cart_form( $post->ID, false, null, null, $redirect );

	}

	/**
	 * Get the product thumbnail for the loop.
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::shop_loop_item_title
	 * @return string
	 */
	public function template_loop_product_thumbnail() {

		return get_the_post_thumbnail();

	}

	/**
	 * Get the product price for the loop.
	 *
	 * @since  2.0.1
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::shop_loop_item_title
	 */
	public function template_loop_price() {

		global $post;

		$price = rstore_price( $post->ID );

		printf( '<span class="price">%s</span>', $price );

	}

	/**
	 * Show the product title in the product loop. By default this is an H2.
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::shop_loop_item_title
	 */
	public function template_loop_product_title() {

		echo '<h2 class="' . esc_attr( apply_filters( 'wdes_rs_product_loop_title_classes', 'wdes-rs-loop-product-title woocommerce-loop-product__title' ) ) . '">' . get_the_title() . '</h2>';

	}

	/**
	 * Show the product image.
	 * @see  template_loop_product_thumbnail
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::single_product_summary_wrappers
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::shop_loop_item_title
	 */
	public function show_product_image() {

		$thumbnail_html = $this->template_loop_product_thumbnail();

		print( '<div class="wdes-rs-product-gallery-wrapper" >' );

		if ( ! empty( $thumbnail_html ) ) {

			echo $thumbnail_html;

		} else {

			print( '<div class="wdes-rs-product-gallery-image-placeholder">' );

			printf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( WDES_ASSETS_URI . '/img/no-photo.png' ), esc_html__( 'Awaiting product image', 'phox' ) );

			print( '</div>' );

		}

		print( '</div>' );

	}

	/**
	 * Show the product content.
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::single_product_summary_wrappers
	 */
	public function show_product_description() {

		remove_filter( 'the_content', 'rstore_append_add_to_cart_form' );

		$heading = apply_filters( 'wdes_rs_product_description_heading', __( 'Description', 'phox' ) );

		if ( $heading ) {
			printf( '<h2>%s</h2>', $heading );
		}

		the_content();


	}

	/**
	 * Show the product title.
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::single_product_summary
	 */
	public function show_product_title() {

		the_title( '<h1 class="product_title entry-title">', '</h1>' );

	}

	/**
	 * Show the product price.
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::single_product_summary
	 */
	public function show_product_price() {

		global $post;

		$price = rstore_price( $post->ID );

		printf( '<p class="price">%s</p>', $price );

	}

	/**
	 * Show the product meta.
	 * @uses \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks::single_product_summary
	 */
	public function show_product_meta() {

		global $post;

		print( '<div class="product_meta">' );

		do_action( 'wdes-rc_product_meta_start' );

		echo get_the_term_list( $post->ID, 'reseller_product_category', '<span class="posted_in">' . esc_html__( 'Category:', 'phox' ) . ' ', ', ', '</span>' );

		do_action( 'wdes-rc_product_meta_end' );

		print( '</div>' );

	}


}