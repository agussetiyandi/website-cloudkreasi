<?php
/**
 * Reseller Store Template Hooks Class
 *
 * @package Phox\Reseller Store
 * @author WHMCSdes
 * @since  2.0.1
 * @link https://whmcsdes.com
 */

namespace Phox\reseller_store;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} //Exit if accessed directly

/**
 * Create Reseller Store Template Hooks
 */
class Wdes_Reseller_Store_Template_Hooks extends Wdes_Reseller_Store_Template{

    /**
     * Constructor.
     *
     * @since 2.0.1
     */
    public function __construct() {

        /**
         * @see wrappers
         */
        $this->wrappers();

        /**
         * @see sidebar
         */
        $this->sidebar();

        /**
         * @see header_archive
         */
        $this->header_archive();

        /**
         * @see shop_loop
         */
        $this->shop_loop();

        /**
         * @see pagination
         */
        $this->pagination();

        /**
         * @see template_loop_product_link
         */
        $this->template_loop_product_link();

        /**
         * @see shop_loop_item_title
         */
        $this->shop_loop_item_title();

        /**
         * @see single_product_summary_wrappers
         */
        $this->single_product_summary_wrappers();

        /**
         * @see single_product_summary
         */
        $this->single_product_summary();

    }

    /**
     * Wrappers
     *
     * @see output_content_wrapper
     * @see output_content_wrapper_end
     * @since 2.0.1
     */
    private function wrappers() {

        add_action ( 'wdes_rs_before_main_content', [ $this, 'output_content_wrapper' ], 10);
        add_action ( 'wdes_rs_after_main_content', [ $this, 'output_content_wrapper_end' ], 10);

    }

    /**
     * Sidebar
     *
     * @see create_sidebar
     * @since 2.0.1
     */
    private function sidebar(){

        add_action( 'wdes_rs_sidebar', [ $this, 'create_sidebar' ], 10 );

    }

    /**
     * Header Archive
     *
     * @see page_title
     * @see breadcrumb
     *
     * @since 2.0.1
     */
    private function header_archive() {

        /**
         * Title
         */
        add_action('wdes_rs_header_archive', [ $this, 'page_title' ], 10 );

        /**
         * Breadcrumb
         */
        add_action('wdes_rs_header_archive', [ $this, 'breadcrumb' ], 30 );

    }

    /**
     * Shop Loop
     *
     * @see shop_loop_start
     * @see shop_loop_end
     * @since 2.0.1
     */
    private function shop_loop() {

        add_action ( 'wdes_rs_before_shop_loop', [ $this, 'shop_loop_start' ]);
        add_action ( 'wdes_rs_after_shop_loop', [ $this, 'shop_loop_end' ]);

    }

    /**
     * Pagination
     *
     * @see output_pagination
     * @since 2.0.1
     */
    private function pagination(){

        add_action ( 'wdes_rs_pagination', [ $this, 'output_pagination' ]);

    }

    /**
     * Template loop product link
     *
     * @see template_loop_product_link_open
     * @see template_loop_product_link_close
     * @see template_loop_add_to_cart
     * @since 2.0.1
     */
    private function template_loop_product_link(){

        add_action ( 'wdes_rs_before_shop_loop_item', [ $this, 'template_loop_product_link_open' ]);
        add_action ( 'wdes_rs_after_shop_loop_item', [ $this, 'template_loop_product_link_close' ], 5);
        add_action ( 'wdes_rs_after_shop_loop_item', [ $this, 'template_loop_add_to_cart' ], 10);

    }

    /**
     * Shop loop items before & after title and title
     *
     * @see show_product_image
     * @see template_loop_price
     * @see template_loop_product_title
     * @since 2.0.1
     */
    private function shop_loop_item_title(){

        add_action ( 'wdes_rs_before_shop_loop_item_title', [ $this, 'show_product_image' ]);
        add_action ( 'wdes_rs_after_shop_loop_item_title', [ $this, 'template_loop_price' ]);
        add_action ( 'wdes_rs_shop_loop_item_title', [ $this, 'template_loop_product_title' ]);


    }

    /**
     * Single product summary wrappers
     *
     * @see show_product_image
     * @see show_product_description
     * @since 2.0.1
     */
    private function single_product_summary_wrappers(){

        add_action ( 'wdes_rs_before_single_product_summary', [ $this, 'show_product_image' ]);
        add_action ( 'wdes_rs_after_single_product_summary', [ $this, 'show_product_description' ]);

    }

    /**
     * Single product summary
     *
     * @see show_product_title
     * @see show_product_price
     * @see template_loop_add_to_cart
     * @see show_product_meta
     * @since 2.0.1
     */
    private function single_product_summary(){

        add_action ( 'wdes_rs_single_product_summary', [ $this, 'show_product_title' ], 5);
        add_action ( 'wdes_rs_single_product_summary', [ $this, 'show_product_price' ], 10);
        add_action ( 'wdes_rs_single_product_summary', [ $this, 'template_loop_add_to_cart' ], 20);
        add_action ( 'wdes_rs_single_product_summary', [ $this, 'show_product_meta' ], 30);

    }





}