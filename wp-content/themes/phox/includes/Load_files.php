<?php
/**
 * Load all Theme files
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */


use Phox\core\admin\panel\wdes_options\Wdes_Control;


if(is_admin()){

	/* WDES Panel */
	Phox\core\admin\panel\Wdes_Panel::instance();

	/* TGM Plugin */
	new Phox\core\admin\tgm\Wdes_TGM();

	/* Import Demo */
	Phox\import\Demo_Import::instance();

}

/* Wdes Control */
$init_admin_options = new Phox\core\admin\panel\wdes_options\Wdes_Options();
add_action( 'wp_loaded', [ $init_admin_options, 'fire' ] );



//Load Menu
Phox\core\menu\Wdes_Menu_Register_Fields::instance();
Phox\core\menu\Wdes_Nav_Menu::instance();


//Load Elementor
require_once (WDES_INC_DIR .'/elementor/elementor.php');


/* Head Function */
Phox\core\WDES_Head::instance();


/* Helpers */
function x_wdes () {
	return Phox\helpers::instance();
}


/* Init */
Phox\core\WDES_Init::instance();

/* Header Function */
function wdes_header(){
	return new Phox\core\Wdes_Header();
}

/* Footer Function */
function wdes_footer(){
	return new Phox\core\Wdes_Footer();
}

/* Archive Function */
function wdes_archive(){
	return new Phox\core\Wdes_Archive();
}


/* Social Details */
new Phox\core\Wdes_Social_Detail();


/* Category options */
Phox\core\Wdes_Category_Options::instance();

/* Hooks */
$init_hooks = Phox\core\Wdes_Hooks::instance();
add_action( 'wp_loaded', [ $init_hooks, 'fire' ] );


/* Get Admin Options */
function wdes_opts_get($opts_name, $default = null) {

	return Wdes_Control::get($opts_name, $default);

}

/* Show Admin Options */
function wdes_opts_show($opts_name, $default = null) {

	Wdes_Control::show($opts_name, $default);

}

/* WooCommerce */
if( class_exists( 'WooCommerce' ) ){

	new Phox\woocommerce\Wdes_WooCommerce();
	new Phox\woocommerce\Wdes_WooCommerce_Template_Hooks();

}

/* Reseller Store */
if( class_exists( 'Reseller_Store\Plugin' ) ){

	new Phox\reseller_store\Wdes_Reseller_Store();
	new \Phox\reseller_store\Wdes_Reseller_Store_Template_Hooks();

}
