<?php
/**
 * WooCommerce Class
 *
 * @package Phox\woocommerce
 * @author WHMCSdes
 * @since  1.4.0
 * @link https://whmcsdes.com
 */

namespace Phox\woocommerce;

/**
 * The WooCommerce Integration class
 */
class Wdes_WooCommerce {

	/**
	 * Constructor.
	 *
	 * Setup WooCommerce
	 *
	 * @since 1.4.0
	 */
	public function __construct() {

		add_action( 'after_setup_theme', [ $this, 'setup']);
		add_filter('woocommerce_output_related_products_args', [ $this, 'related_products_args' ]);


		//Integration
		add_action( 'wdes_woocommerce_setup', [ $this, 'setup_integrations' ] );
		add_filter('woocommerce_enqueue_styles', [ $this, 'enqueue_styles' ]);

	}

	/**
	 * Setup
	 *
	 * Sets up theme defaults and registers support for various Woocommerce features.
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function setup() {
			add_theme_support(
					'woocommerce', apply_filters(
							'wdes_woocommerce_args', array(
									'single_image_width'		=> 416,
									'thumbnail_image_width'	=> 324,
									'product_grid'					=> array(
											'default_columns'	=> 2,
											'default_rows'		=> 4,
											'min_columns'			=> 1,
											'max_columns'			=> 6,
											'min_rows'				=> 1,

									)
							)
					)
			);

			add_theme_support('wc-product-gallery-zoom');
			add_theme_support('wc-product-gallery-lightbox');
			add_theme_support('wc-product-gallery-slider');

			//wdes_woocommerce setup
			do_action( 'wdes_woocommerce_setup' )	;

	}

	public function setup_integrations() {

		//Disable the tabs Heading
		add_filter('woocommerce_product_description_heading', '__return_false' );
		add_filter('woocommerce_product_additional_information_heading', '__return_false' );
		add_filter('woocommerce_show_page_title', '__return_false');

	}

	/**
	 * Related Products Args
	 *
	 * @param array $args related products args
	 * @since 1.4.0
	 * @return array $args related products args
	 */
	public function related_products_args($args) {

		$args = apply_filters(
			'wdes_related_products_args', [
				'posts_per_page' => 2,
				'columns'				 => 2,
			]
		);

		return $args;

	}

	/**
	 * Enqueue Css for this theme
	 *
	 * @param  array $styles Array of registered style
	 * @since 1.4.0
	 * @return array
	 */
	public function enqueue_styles( $styles ) {
		
		$styles['wdes-woocommerce'] = array (
			'src'			=>  WDES_ASSETS_URI . '/css/components/woocommerce.css',
			'deps'		=> '',
			'version'	=> WDES_THEME_VERSION,
			'media'		=> 'all'
		);

		return $styles;

	}

}