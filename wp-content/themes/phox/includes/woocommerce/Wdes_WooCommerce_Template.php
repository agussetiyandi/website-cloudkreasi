<?php
/**
 * WooCommerce Template Class
 *
 * @package Phox\woocommerce
 * @author WHMCSdes
 * @since  1.4.0
 * @link https://whmcsdes.com
 */


namespace Phox\woocommerce;

/**
 * Create WooCommerce Template
 */
class Wdes_WooCommerce_Template {

	/**
	 * Open the theme wrapper
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function content_wrapper_start() {

		echo '<div class="wdes-bread">';

			do_action('wdesWoo_header_archive');

		echo '</div>';

		echo '<div class="wrapper-wdes">';
		echo '<div class="container"> ';
		echo '<div class="row"> ';

	}

	/**
	 * Close the theme wrapper
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function content_wrapper_end() {

		do_action( 'wdesWoo_sidebar' );
		echo '</div>';
		echo '</div>';
		echo '</div>';

	}

	/**
	 * Open the shop loop
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function shop_loop_start() {
		
		$col_class = ( is_active_sidebar( 'sidebar-shop-woocommerce' ) ) ? 'custom-width-post' : '';

		printf ( '<div class="wide-posts-block %s">', $col_class );

	}

	/**
	 * Close the shop loop
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function shop_loop_end() {

		echo '</div>';

	}

	/**
	 * Get woocommerce sidebar
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function wdesWoo_get_sidebar() {
 
		if ( is_active_sidebar( 'sidebar-shop-woocommerce' ) ) {

			echo '<div class="sidebar-area">';

			dynamic_sidebar( 'sidebar-shop-woocommerce' );

			echo '</div>';

		}

	}

	/**
	 * Get woocommerce breadcrumb
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function wdesWoo_get_breadcrumb($args = []) {

			$args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
				'delimiter'   => '&nbsp;&#47;&nbsp;',
				'wrap_before' => '<nav class="woocommerce-breadcrumb">',
				'wrap_after'  => '</nav>',
				'before'      => '',
				'after'       => '',
				'home'        => _x( 'Home', 'breadcrumb', 'phox' ),
			) ) );

			$breadcrumbs = new \WC_Breadcrumb();

			if ( ! empty( $args['home'] ) ) {
				$breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
			}

			$args['breadcrumb'] = $breadcrumbs->generate();

			/**
			 * WooCommerce Breadcrumb hook
			 *
			 * @hooked WC_Structured_Data::generate_breadcrumblist_data() - 10
			 */
			do_action( 'woocommerce_breadcrumb', $breadcrumbs, $args );

			wc_get_template( 'global/breadcrumb.php', $args );
	}

	/**
	 * Get woocommerce page title
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function wdesWoo_get_page_title () {

			echo '<h1 class="woocommerce-products-header__title page-title">';
					woocommerce_page_title() ;
			echo ' </h1>';

	}

	/**
	 * Get woocommerce taxonomy archive description
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function wdesWoo_taxonomy_archive_description() {

			if ( is_product_taxonomy() && 0 === absint( get_query_var( 'paged' ) ) ) {
					$term = get_queried_object();

					if ( $term && ! empty( $term->description ) ) {
						echo '<div class="term-description">' . wc_format_content( $term->description ) . '</div>'; // WPCS: XSS ok.
					}
			}

	}

	/**
	 * Get woocommerce product archive description
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function wdesWoo_product_archive_description() {

			// Don't display the description on search results page.
			if ( is_search() ) {
					return;
			}

			if ( is_post_type_archive( 'product' ) && in_array( absint( get_query_var( 'paged' ) ), array( 0, 1 ), true ) ) {
					$shop_page = get_post( wc_get_page_id( 'shop' ) );
					if ( $shop_page ) {
							$description = wc_format_content( $shop_page->post_content );
							if ( $description ) {
									echo '<div class="page-description">' . $description . '</div>'; // WPCS: XSS ok.
							}
					}
			}

	}

	/**
	 * Open the Single Product
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function single_product_start_loop() {

		echo '<div class="wide-posts-block custom-width-post">';

	}

	/**
	 * Close the Single Product
	 *
	 * @since 1.4.0
	 * @return void
	 */
	public function single_product_end_loop() {

		echo '</div>';

	}

	/**
	 * Default loop columns on product archives
	 *
	 * @since 1.4.0
	 * @return int
	 */
	public function loop_columns() {

		return 2;

	}



}