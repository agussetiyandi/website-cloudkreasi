<?php
/**
 * WooCommerce Template Hooks Class
 *
 * @package Phox\woocommerce
 * @author WHMCSdes
 * @since  1.4.0
 * @link https://whmcsdes.com
 */

namespace Phox\woocommerce;


class Wdes_WooCommerce_Template_Hooks extends Wdes_WooCommerce_Template {

	/**
	 * Constructor.
	 *
	 * @since 1.4.0
	 */
	public function __construct() {

		$this->sidebar();
		$this->columns_filter();

		$this->remove_archive_description();
		$this->header_archive();

		$this->wrappers();
		$this->remove_wrappers();

		$this->shop_loop();
		$this->single_product_loop();

		$this->remove_shop_loop();

	}

	/**
	 * Sidebar
	 * change the default sidebar
	 *
	 * @since 1.4.0
	 */
	private function sidebar(){

		add_action( 'wdesWoo_sidebar', [ $this, 'wdesWoo_get_sidebar' ], 10 );

	}

	/**
	 * Header Archive
	 * add new header and resort the element in header
	 *
	 * @since 1.4.0
	 */
	private function header_archive() {

		//page title
		add_action('wdesWoo_header_archive', [ $this, 'wdesWoo_get_page_title' ], 10 );

		//archive description
		add_action( 'wdesWoo_header_archive', [ $this, 'wdesWoo_taxonomy_archive_description' ], 20 );
		add_action( 'wdesWoo_header_archive', [ $this, 'wdesWoo_product_archive_description' ], 20 );

		//breadcrumb
		add_action('wdesWoo_header_archive', [ $this, 'wdesWoo_get_breadcrumb' ], 30 );

	}

	/**
	 * Remove Archive Description
	 * remove some items form archive description
	 *
	 * @since 1.4.0
	 */
	private function remove_archive_description() {

		remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description');
		remove_action('woocommerce_archive_description', 'woocommerce_product_archive_description');

	}

	/**
	 * Wrappers
	 *
	 * @see content_wrapper_start
	 * @see content_wrapper_end
	 * @since 1.4.0
	 */
	private function wrappers() {

		add_action ( 'woocommerce_before_main_content', [ $this, 'content_wrapper_start' ]);
		add_action ( 'woocommerce_after_main_content', [ $this, 'content_wrapper_end' ]);

	}

	/**
	 * Remove Wrappers
	 * remove items before and after content
	 *
	 * @since 1.4.0
	 */
	private function remove_wrappers() {

		remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper' );
		remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end' );

	}

	/**
	 * Shop Loop
	 *
	 * @see shop_loop_start
	 * @see shop_loop_end
	 * @since 1.4.0
	 */
	private function shop_loop() {

		add_action ( 'woocommerce_before_shop_loop', [ $this, 'shop_loop_start' ]);
		add_action ( 'woocommerce_after_shop_loop', [ $this, 'shop_loop_end' ]);

	}

	/**
	 * Single Product Loop
	 *
	 * @see shop_loop_start
	 * @see shop_loop_end
	 * @since 1.4.0
	 */
	private function single_product_loop() {

		add_action ( 'woocommerce_before_single_product', [ $this, 'shop_loop_start' ]);
		add_action ( 'woocommerce_after_single_product', [ $this, 'shop_loop_end' ]);

	}

	/**
	 * Remove Shop Loop
	 * remove breadcrumb and sidebar
	 *
	 * @since 1.4.0
	 */
	private function remove_shop_loop() {

		remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
		remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

	}

	/**
	 * Columns Loop
	 * default number of column
	 *
	 * @see loop_columns
	 * @since 1.4.0
	 */
	private function columns_filter () {

		add_filter( 'loop_shop_columns', [ $this, 'loop_columns' ],99, 1);

	}


}