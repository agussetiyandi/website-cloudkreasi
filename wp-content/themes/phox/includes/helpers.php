<?php
namespace Phox;

/**
 * Helpers Functions
 *
 * @package Phox
 * @author WHMCSdes
 * @link https://whmcsdes.com
 */
class helpers {
	/**
	 * helpers .
	 *
	 * @var helpers
	 */
	protected static $_instance = null;

	/**
	 * Get helpers instance .
	 *
	 * @return helpers
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;

	}

	/**
	 * Escape the text .
	 *
	 * it will help to escape the string that will have html tags
	 * Update to allow form tags and script tags .
	 *
	 * @since 1.0.0
	 */
	static public function wdes_get_text( $text ) {

		global $allowedposttags;

		$tags = $allowedposttags;

		//Support Form
		$tags['form'] = array(
			'action'         => true,
			'method'         => true,
			'name'           => true,
			'id'			 => true,
			'class'			 => true

		);

		//Support Input
		$tags['input'] = array(
			'type'         => true,
			'name'         => true,
			'class'        => true,
			'placeholder'  => true,
			'id'		   => true,
			'value'		   => true
		);

		//Support Select
		$tags['select'] = array(
			'id'         => true,
			'class'        => true,
			'onchange'  => true
		);

		//Support Option
		$tags['option'] = array(
			'id'         => true,
			'class'        => true,
			'value'         => true
		);

		//Support Style
		$tags['style'] = array(
			'type'      => true,
			'media'     => true
		);

		//Support Script
		$tags['script'] = array(
			'type'         => true
		);

		echo wp_kses( $text,  $tags );

	}
	/**
	 * Validate the variable as boolean
	 *
	 * Change any type of data to true or false
	 * this is will use with Elementor settings because it did not return boolean it return string
	 * or any case like that
	 *
	 * @since 1.4.5
	 * @return boolean $var
	 */
	static public function check_var_true ($var) {

		if( is_bool($var) || is_numeric($var) ) {
			return (bool) $var;
		}

		if( is_string( $var ) ) {

			$var = strtolower( $var );
			if( in_array( $var, array( 'yes', 'on', 'true', 'checked' ) ) ) {
				return true;
			}

			return false;

		}

	}

	/**
	 * Get the template part.
	 */
	public function wdes_get_tp( $slug, $name = '' ) {
		get_template_part( 'template-parts/' . $slug, $name );
	}

	/**
	 * Get widget block.
	 */
	public function wdes_get_widget_block( $templ, $data = [] ) {

		if ( ! locate_template( "/includes/elementor/render-html/{$templ}.php" ) ) {
			return;
		}

		require locate_template( "/includes/elementor/render-html/{$templ}.php" );
	}

	/**
	 * Get Elementor template
	 */
	public static function  get_builder_content_for_display( $template_id ){

		if ( ! is_int( $template_id ) ) return false;

		self::wdes_get_text( \Elementor\Plugin::$instance->frontend->get_builder_content_for_display( $template_id, true ) );

	}

	/**
	 * array Merge
	 */
	public function wdes_array_merge( $defaults, $options ) {
		return array_replace_recursive( $defaults, $options );
	}

	/**
	 * Before container.
	 */
	public function wdes_before_container() {

		print ( '<div class="container-fluid"><div class="row">' );

	}

	/**
	 * After container.
	 */
	public function wdes_after_container() {

		print ( '</div></div>' );

	}
	/**
	 * Grid System
	 */
	public function wdes_sys_col_grid($type){

		if($type === 'archive'){

			$get_option = wdes_opts_get('sidebar-page-layout','full');

		}else{

			$get_option = wdes_opts_get('sidebar-single-layout', 'full');

		}

		$output = 'wide-posts-block';

		if($get_option === 'right' || $get_option === 'left' ){

			$output .= ' custom-width-post';

		}elseif ( $get_option === 'both'){

			$output .= ' custom-boxed-width';

		}

		return $output;


	}

	/**
	 * before Blog
	 */
	public function wdes_before_blog() {

		x_wdes()->wdes_get_tp( 'posts/archive', 'title' );


		if ( ! is_archive() && ! is_search() && get_query_var( 'paged' ) === 1 || get_query_var( 'paged' ) === 0 ) {


			if(is_null(wdes_opts_get('blog-pin-post-show')) && is_home() ){
				wdes_archive()->blog_board( wdes_opts_get( 'blog-pin-post' ) );
			}

		}

	}

	/**
	 * Category Label.
	 */
	public function wdes_get_cat_label($layout = 'color') {

		$layout ='classic';

		if ( empty(get_the_post_thumbnail()) && $layout ) {
			$class_cat = 'cat-img-post';
		} else {
			$class_cat = '';
		}
		$cat_list = get_the_category();
		$output   = '';
		foreach ( $cat_list as $cat_item ) {

			$cat_color = $this->wdes_cat_label_color( $cat_item->cat_ID );

			$cat_link = get_category_link( $cat_item->cat_ID );
			if($layout === 'color'){
				$output  .= '<a class="art-format custom-bg-p1 '. $class_cat .'" href="' . esc_url( $cat_link ) . '" style="background:' . $cat_color . ';">' . $cat_item->cat_name . '</a>';
			}else{
				$output  .= '<li><a href="' . esc_url( $cat_link ) . '" >' . $cat_item->cat_name . '</a></li>';
			}


			break;
		}

		return $output;

	}



	/**
	 * Category Label color.
	 */
	public function wdes_cat_label_color( $cat_id ) {

		if ( ! is_null( $this->wdes_get_category_option( $cat_id ) ) && ! empty( $this->wdes_get_category_option( $cat_id ) ) ) {
			$label_color = $this->wdes_get_category_option( $cat_id );

			return $label_color;
		}

		return '#3897F0';
	}


	/**
	 * categories link
	 */
	public function wdes_categories_link(){

		$cat_list = get_the_category();
		$output   = '';
		foreach ( $cat_list as $cat_item ) {

			$cat_link = get_category_link( $cat_item->cat_ID );
			$output  .= '<a href="' . esc_url( $cat_link ). '" >' . $cat_item->cat_name . '</a>';
			$output  .= ' ';


		}

		return $output;

	}

	/**
	 * Get tags.
	 */
	public function wdes_get_tags( $tag_list, $class ) {
		if ( ! empty( $tag_list ) ) {
			$output      = '<div class="' . $class . '">';
				$output	.= '<span class="tags-title">'. esc_html__('Tags:', 'phox') .'</span>';
				$output .= $tag_list;
			$output     .= '</div>';
		} else {
			$output = '';
		}

		return $output;
	}

	/**
	 * Posts pagination.
	 */
	public function wdes_posts_pagination() {

		global $wp_query;
		$total = $wp_query->max_num_pages;
		$big   = 999999999; // need an unlikely integer.
		if ( $total > 1 ) {

			if ( ! $current_page = get_query_var( 'paged' ) ) {
				$current_page = 1;
			}
			if ( get_option( 'permalink_structure' ) ) {
				$format = 'page/%#%/';
			} else {
				$format = '&paged=%#%';
			}
			$pages = paginate_links(
				array(
					'base'     => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format'   => $format,
					'current'  => max( 1, get_query_var( 'paged' ) ),
					'total'    => $total,
					'mid_size' => 3,
					'type'     => 'array',

				)
			);

			if ( is_array( $pages ) ) {
				$paged = ( get_query_var( 'paged' ) === 0 ) ? 1 : get_query_var( 'paged' );
				echo '<ul class="pagination">';
				foreach ( $pages as $page ) {
					$page = str_replace( 'page-numbers', 'page-number', $page );
					echo "<li>$page</li>";
				}
				echo '</ul>';
			}
		}
	}

	/**
	 *Breadcrumbs
	 */
	function wdes_breadcrumb(){

		global $post;

		$output = '';

		$after_salah = '<li class="wdes-breadcrumb-title-active">'. get_the_title( $post->id ) . '</li>';

		if( is_home() ) {

			$after_salah = '<li class="wdes-breadcrumb-title-active">'. esc_html__('Blog', 'phox') . '</li>';

		} elseif ( is_tag() ) {

			$after_salah = '<li class="wdes-breadcrumb-title-active">'. single_tag_title( '', false ) . '</li>';

		} elseif ( is_category() ) {

			$after_salah = '';
			$category    = get_query_var( 'cat' );
			$category    = get_category( $category );

			if ( $category->parent !== 0 ) {

				$parent_categories = array_reverse( get_ancestors( $category->cat_ID,
					'category' ) );

				foreach ( $parent_categories as $parent_category ) {

					$after_salah .= '<li class="wdes-breadcrumb-title-active"><a href="'. esc_url( get_category_link( $parent_category ) ) . '">'. get_cat_name( $parent_category ) . '</a></li>';

				}

			}

			$after_salah .= '<li class="wdes-breadcrumb-title-active">'. single_cat_title( '', false ) . '</li>';

		} elseif ( is_search() ) {

			$after_salah = '<li class="wdes-breadcrumb-title-active">'. esc_html__( 'Search', 'phox' ) . '</li>';

		} elseif ( is_author() ) {

			$after_salah = '<li class="wdes-breadcrumb-title-active">'. ucfirst( get_the_author() ) . '</li>';

		} elseif ( is_day() ) {

			$after_salah = '<li><a href="' . esc_url( get_month_link( get_the_time( 'Y' ),get_the_time( 'm' ) ) ) . '">' . get_the_date( 'Y' ) . '</a></li>';
			$after_salah .= '<li class="wdes-breadcrumb-title-active">'. get_the_date( 'F' ) . '</li>';


		} elseif ( is_month() ) {

			$after_salah = '<li><a href="' . esc_url (get_year_link( get_the_time( 'Y' ) ) ). '">' . get_the_date( 'Y' ) . '</a></li>';
			$after_salah .= '<li class="wdes-breadcrumb-title-active">'. get_the_date( 'm' ) . '</li>';


		} elseif ( is_year() ) {

			$after_salah = '<li class="wdes-breadcrumb-title-active">'. get_the_date( 'Y' ) . '</li>';


		} elseif ( is_single() && ! is_attachment() ) {

			$after_salah = '';


			if ( 'post' == get_post_type() ) {

				$category = get_the_category( get_the_ID() );
				if( !empty($category) ){
					if ( $category[0]->parent !== 0 ) {

						$parent_categories
							= array_reverse( get_ancestors( $category[0]->cat_ID,
							'category' ) );
	
						foreach ( $parent_categories as $parent_category ) {
	
							$after_salah .= '<li class="wdes-breadcrumb-title-active"><a href="'. esc_url (get_category_link( $parent_category ) ) . '">'. get_cat_name( $parent_category ) . '</a></li>';
	
						}
	
					}
					$after_salah .= '<li class="wdes-breadcrumb-title-active"><a href="'. esc_url( get_category_link( $category[0]->term_id ) ). '">'. $category[0]->name . '</a></li>';
				}
				

			} else {

				$taxonomies = get_object_taxonomies( get_post(), 'object' );

				if ( ! empty( $taxonomies ) && is_array( $taxonomies ) ) {

					foreach ( $taxonomies as $taxonomy ) {

						if ( $taxonomy->hierarchical ) {

							$taxonomy_name = $taxonomy->name;
							break;

						}

					}

				}

				if ( ! empty( $taxonomy_name ) ) {

					$custom_terms = get_the_terms( get_post(), $taxonomy_name );

					if ( ! empty( $custom_terms ) && ! is_wp_error( $custom_terms ) ) {

						if ( ! is_null( get_category( $custom_terms[0]->term_id ) ) ) {

							if ( $custom_terms[0]->parent !== 0 ) {

								$parent_categories= array_reverse( get_ancestors( $custom_terms[0]->term_id,'category' ) );

								foreach ( $parent_categories as $parent_category ) {

									$after_salah .= '<li class="wdes-breadcrumb-title-active"><a href="'. esc_url( get_category_link( $parent_category ) ). '">' . get_cat_name( $parent_category ). '</a></li>';

								}

							}

						} else {

							if ( $custom_terms[0]->parent !== 0 ) {

								$parent_categories
									= array_reverse( get_ancestors( $custom_terms[0]->term_id,
									$custom_terms[0]->taxonomy ) );

								foreach ( $parent_categories as $parent_category ) {

									$term_item   = get_term( $parent_category,
										$custom_terms[0]->taxonomy );
									$after_salah .= '<li class="wdes-breadcrumb-title-active"><a href="'. esc_url( get_term_link( $parent_category ) ) . '">'. $term_item->name . '</a></li>';

								}

							}

						}

						$after_salah .= '<li class="wdes-breadcrumb-title-active"><a href="'. esc_url( get_term_link( $custom_terms[0]->term_id ) ). '">' . $custom_terms[0]->name . '</a></li>';

					}

				}

			}

			$after_salah .= '<li class="wdes-breadcrumb-title-active">'. get_the_title() . '</li>';

		} elseif ( is_attachment() ) {

			$after_salah = '';

			if ( ! empty( $post->post_parent ) ) {
				$parent      = get_post( $post->post_parent );
				$after_salah .= '<li class="wdes-breadcrumb-title-active"><a href="'. esc_url( get_permalink( $parent ) ) . '">' . $parent->post_name. '</a></li>';
			}

			$after_salah .= '<li class="wdes-breadcrumb-title-active">'. get_the_title() . '</li>';


		} elseif ( is_tax() ) {

			$taxonomy = get_query_var( 'taxonomy' );
			$name     = get_query_var( 'term' );

			if ( $taxonomy == 'portfolio_category' ) {
				$after_salah = '<li class="wdes-breadcrumb-title-active">' . $name. '</li>';
			}


		} elseif ( is_archive() ) {
			$after_salah = '<li class="wdes-breadcrumb-title-active">'. post_type_archive_title( '', false ) . '</li>';
		} elseif ( is_page() && get_post_taxonomies() ) {

			$after_salah = '<li class="wdes-breadcrumb-title-active">'. single_cat_title( '', false ) . '</li>';

		} elseif ( is_page() && $post->post_parent ) {

			$after_salah = '<li class="wdes-breadcrumb-title-active">'. get_the_title( $post->id ) . '</li>';

		}

		$output = '<ul class="wdes-breadcrumb-area">';
		$output .= '<li><a href="' . esc_url( home_url( '/' ) )
							 . '"><span class="fas fa-home home-icon"></span>'
							 . esc_html__( 'Home', 'phox' ) . '</a></li>';
		$output .= $after_salah;
		$output .= '</ul>';

		$this->wdes_get_text( $output );


	}

	/**
	 * Content nav.
	 */
	public function wdes_content_nav() {

		$prev_single_post = get_previous_post();
		$next_single_post = get_next_post();
		$title_no_image_next = '';
		$title_no_image_prev = '';

		echo '<div class="clearfix"></div>';
		if(!empty($prev_single_post) || !empty($next_single_post)){
			echo '<div class="row-elements-box">';
				echo '<div class="related-posts-area">';
				if ( ! empty( $prev_single_post ) ) {
					echo '<div class="article-rel">';
						echo '<a href="' . esc_url( get_the_permalink( $prev_single_post->ID ) ) . '">';
					if ( ! empty( get_the_post_thumbnail_url( $prev_single_post->ID, 'small' ) ) ) {
						echo ' <div class="thumb-box-wrap" style="background-image:url('.get_the_post_thumbnail_url( $prev_single_post->ID, 'small' ).')"><span class="fas fa-angle-left arrow-bg-prev"></span></div>';
					}else{
						echo '<div class="nav-without-thumb"><span class="fas fa-angle-left"></span><h2>'. esc_html__('Previous', 'phox') .'</h2></div>';
						$title_no_image_prev = 'title-no-image-prev';
					}
						echo '</a>';
						if ( ! empty( get_the_title( $prev_single_post->ID ) ) ) {
							echo '<a class="related-title '.$title_no_image_prev.'" href="' . esc_url( get_the_permalink( $prev_single_post->ID ) ) . '">' . get_the_title( $prev_single_post->ID ) . '</a>';
						} else {
							echo '<a class="related-title '.$title_no_image_prev.'" href="' . esc_url( get_the_permalink( $prev_single_post->ID ) ) . '"> ' . esc_html__( 'No Title', 'phox' ) . ' </a>';
						}
					echo '</div>';
				}

				if ( ! empty( $next_single_post ) ) {
					echo '<div class="article-rel">';
						echo '<a href="' . esc_url( get_the_permalink( $next_single_post->ID ) ) . '">';
							if ( ! empty( get_the_post_thumbnail_url( $next_single_post->ID, 'small' ) ) ) {
								echo ' <div class="thumb-box-wrap" style="background-image:url('.get_the_post_thumbnail_url( $next_single_post->ID, 'small' ).')""><span class="fas fa-angle-right arrow-bg-prev"></span></div>';
							}else{
								echo '<div class="nav-without-thumb dir-next-post"><h2>'. esc_html__('Next', 'phox') .'</h2><span class="fas fa-angle-right"></span></div>';
								$title_no_image_next = 'title-no-image-next';
							}
						echo '</a>';
							if ( ! empty( get_the_title( $next_single_post->ID ) ) ) {
								echo '<a class="related-title '.$title_no_image_next.'" href="' . esc_url( get_the_permalink( $next_single_post->ID ) ) . '">' . get_the_title( $next_single_post->ID ) . '</a>';
							} else {
								echo '<a class="related-title '.$title_no_image_next.'" href="' . esc_url( get_the_permalink( $next_single_post->ID ) ) . '">' . esc_html__( 'No Title', 'phox' ) . '</a>';
							}
					echo '</div>';
				}
				echo '</div>';
			echo '</div>';
		}
	}


	/**
	 * Get Post Url
	 */
	public function wdes_get_post_uri(){

		return get_permalink($this->post_ID());

	}

	/**
	 * Check is post have thumbnail
	 */

	public function wdes_is_thumbnail(){

		if ( !empty(get_the_post_thumbnail()) ) {

			return true;

		}

		return false;

	}

	/**
	 * Thumbnail Post.
	 */
	public function wdes_thumbnail_post( $thumb ) {

		if ( is_front_page() || is_home() || is_archive() || is_search() ) {
			$class_thumb = 'class="img-post-wrap"';
		} else {
			$class_thumb = '';
		}
		
		if ( ! empty( $thumb ) && !empty(get_the_post_thumbnail()) ) {

			$output      = '<div ' . $class_thumb . ' >';

			$output     .= $this->wdes_get_thumbnail_link($thumb);

			$output     .= '</div>';

		} else {

			$output = '';

		}

		return $output;
	}


	public function wdes_get_thumbnail_link($thumb) {

		$output = '';

		$output .= '<a href="'. esc_url( $this->wdes_get_post_uri() ) .'">';

		$output .=  get_the_post_thumbnail( null, $thumb, '' );

		$output .= '</a>';

		return $output;
	}


	/**
	 * Get all categories.
	 */
	public function wdes_get_all_categories( $limit_val = 0 ) {
		$limit_val = intval( $limit_val );
		if ( ! is_int( $limit_val ) ) {
			return '';
		}
		$categories = array();

		$limit = 0;

		$get_categories = get_categories( 'hide_empty=0' );

		if ( ! empty( $get_categories ) && is_array( $get_categories ) ) {
			foreach ( $get_categories as $category ) {
				$categories [ $category->cat_ID ] = $category->cat_name;

				$limit++;
				if ( $limit === $limit_val ) {
					break;
				}
			}
		}

		return $categories;
	}

	/**
	 * Get Category.
	 */
	public function wdes_get_category_option( $cat_id = 0 ) {
		if ( is_category() && empty( $cat_id ) ) {
			$cat_id = get_query_var( 'cat' );
		}

		if ( empty( $cat_id ) ) {
			return false;
		}

		$categories_options = get_option( 'wdes_cats_options' );

		if ( ! empty( $categories_options[ $cat_id ] ) ) {
			return $categories_options[ $cat_id ];
		}

	}

	/**
	 * Get Post Data.
	 */
	public function wdes_get_postdata( $post_id ) {
		$categories_options = get_option( 'wdes_posts_options' );

		if ( ! empty( $categories_options[ $post_id ] ) ) {
			if ( $categories_options[ $post_id ] === 'on' ) {
				return true;
			}
		}

		return false;

	}

	/**
	 * Get Excerpt.
	 */
	public function wdes_get_excerpt($restricted_number = 0) {

		if( ! is_int($restricted_number) ) {
			return false ;
		}

		add_filter( 'excerpt_length', array( $this, 'wdes_excerpt_max_length' ) );

		$excerpt = get_the_excerpt();
		$num     = ( $restricted_number === 0 ) ? wdes_opts_get( 'blog-page-excerpt', 30 ) : $restricted_number;

		return wp_trim_words( $excerpt, $num, '...' );

	}

	/**
	 * Get Max Length.
	 */
	public function wdes_excerpt_max_length() {

		return 200;

	}

	/**
	 * The excerpt.
	 */
	public function wdes_the_excerpt() {

		echo esc_html( $this->wdes_get_excerpt() );

	}

	/**
	 * Post multi pages.
	 */
	public function wdes_post_multi_pages() {

		$args = array(
			'before'         => '<ul class="post-pagination">',
			'after'          => '</ul>',
			'link_before'    => '<li>',
			'link_after'     => '</li>',
			'next_or_number' => 'number',

		);

		wp_link_pages( $args );
	}

	/**
	 * Elementor is run on this post.
	 */
	public function wdes_is_buider_edit_mode( $post_id ) {
		return ! ! get_post_meta( $post_id, '_elementor_edit_mode', true );
	}

	/**
	 * Post Id.
	 */
	public function post_ID() {
		global $post;
		$post_ID = false;

		if ( ! is_404() ) {

			$post_ID = get_the_ID();

		}
		return $post_ID;
	}

	/**
	 * The bridge Page Id.
	 */
	public function get_bridge_page_id( $type = 'string' ) {

		$bridge_page_id = get_option( 'cc_whmcs_bridge_pages' );

		if ( isset( $bridge_page_id ) && defined('WHMCS_BRIDGE') ) {

			if ( $type === 'int' ) {
				return intval( $bridge_page_id );
			}
			return $bridge_page_id;

		} else {

			return false;

		}

	}

	/**
	 * Bridge page exists.
	 */
	public function wdes_bridge_page_exists() {
		if ( $this->get_bridge_page_id() ) {

			return true;

		}

		return false;
	}

	/**
	 * Check page is WHMCS bridge
	 */
	public static function is_bridge_default_page( $page_id ){
		$ispage = false;

		if( defined( 'WHMCS_BRIDGE' ) ){

			$ispage = cc_whmcs_bridge_default_page($page_id);
		}

		return $ispage;

	}



	/**
	 * Bridge Checker.
	 */
	public function wdes_bridge_checker() {

		if ( $this->wdes_bridge_page_exists() ) {

			$output = get_permalink( $this->get_bridge_page_id() ) . '?ccce=domainchecker';

			return $output;

		}

		return false;

	}

	/**
	 * Domain Action Url.
	 */
	public function wdes_domain_action_url( $default, $type ) {

		if ( $this->wdes_bridge_checker() ) {

			$output = $this->wdes_bridge_checker();

		} else {

			if ( ! empty( $default ) ) {

				if( $type === 'custom' ){

					$output = $default;

				}else{
					$output = trim($default, '/').'/domainchecker.php' ;
				}


			} else {

				$output = '#';

			}
		}

		return $output;
	}

	/**
	 * Get title.
	 */
	public function wdes_get_title($id=null) {

		if ( ! empty( get_the_title($id) ) ) {

			$output = get_the_title($id);

		} else {

			$output = esc_html__( 'No Title', 'phox' );

		}

		return $output;

	}

	/**
	 * Get Currency symbol
	 *
	 * it will use in elementor element to change the currency select to only symbol
	 *
	 * @param $symbol_name : the currency that will select
	 *
	 * @return mixed|string : symbol
	 */
	public function wdes_get_currency_symbol( $symbol_name ) {
		$symbols = [
			'dollar' => '&#36;',
			'euro' => '&#128;',
			'franc' => '&#8355;',
			'pound' => '&#163;',
			'ruble' => '&#8381;',
			'shekel' => '&#8362;',
			'baht' => '&#3647;',
			'yen' => '&#165;',
			'won' => '&#8361;',
			'guilder' => '&fnof;',
			'peso' => '&#8369;',
			'peseta' => '&#8359',
			'lira' => '&#8356;',
			'rupee' => '&#8360;',
			'indian_rupee' => '&#8377;',
			'real' => 'R$',
			'krona' => 'kr',
		];

		return isset( $symbols[ $symbol_name ] ) ? $symbols[ $symbol_name ] : '';
	}
	/**
	 * Query WooCommerce activation
	 *
	 * @since 1.5.9
	 */
	function is_woocommerce_activated() {
		return class_exists( 'WooCommerce' ) ? true : false;
	}

}

