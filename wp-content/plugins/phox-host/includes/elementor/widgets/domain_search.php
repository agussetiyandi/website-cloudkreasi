<?php
namespace Phox_Host\Elementor\Widgets;

if ( ! defined( 'ABSPATH' ) ) {
    exit; //Exit if assessed directly
}

use Phox_Host\Elementor\Base\Base_Widget;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Typography;
use Elementor\Repeater;

/**
 * Domain Search widget.
 *
 * Tabs widget that will display in Phox category
 *
 * @package Elementor\Widgets
 * @since 1.7.5
 */
class Domain_Search extends Base_Widget {

    /**
     * Get Widget name
     *
     * Retrieve Domain Search widget name
     *
     * @return string Widget name
     * @since  1.7.5
     * @access public
     *
     */
    public function get_name() {
        return 'wdes-forms-widget';
    }

    /**
     * Get Widget title
     *
     * Retrieve Domain Search widget title
     *
     * @return string Widget title
     * @since  1.7.5
     * @access public
     *
     */
    public function get_title() {
        return __( 'Domain Search', 'phox-host' );
    }

    /**
     * Get Widget icon
     *
     * Retrieve Domain Search widget icon
     *
     * @return string Widget icon
     * @since  1.7.5
     * @access public
     *
     */
    public function get_icon() {
        return 'wdes-widget-elementor wdes-widget-domain';
    }


    /**
     * Get Widget keywords
     *
     * Retrieve the list of keywords the widget belongs to.
     *
     * @return array widget keywords.
     * @since  1.7.5
     * @access public
     *
     */
    public function get_keywords() {
        return [ 'domain', 'search', 'form' ];
    }

    /**
     * Get script dependencies.
     *
     * Retrieve the list of script dependencies the element requires.
     *
     * @since 1.4.3
     * @access public
     *
     * @return array Element scripts dependencies.
     */

    public function get_script_depends() {
        return [ 'wdes-psl' ];
    }

    /**
     * Register Tabs widget controls
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since  1.4.0
     * @access protected
     */
    protected function register_controls() {
        $css_scheme = [
             'input'              => 'input.reg-dom',
             'main-area'          => '.domain-search-area-main .sea-dom',
             'ext-block'          => '.extensions-block .block',
             'result-box'         => '.domain-results-box .wdes-result-domain-box',
             'result-dom'         => '.domain-results-box .wdes-result-domain-box .results-wdes-dom',
             'result-pur'         => '.domain-results-box .wdes-result-domain-box .inner-block-result-item input.wdes-purchase-btn',
             'result-aval'        => '.domain-results-box .wdes-result-domain-box .inner-block-result-item .wdes-available-dom',
             'result-token'       => '.domain-results-box .wdes-result-domain-box .inner-block-result-item .wdes-btn-token',
             'suggestion'         => '#wdes-dc-more-options',
             'suggestion-items'   => '#wdes-dc-more-options ul li'
        ];
        $this->start_controls_section(
            'the_domain_controls',
            ['label' => esc_html__('Form Content', 'phox-host'), ]
        );

        $this->add_control(
            'the_placeholder_input',
            [
                'label' => esc_html__('Search Input Placeholder', 'phox-host'),
                'type'  => Controls_Manager::TEXT,
                'default' => esc_html__('example.com', 'phox-host')
            ]
        );

        $extensions = new Repeater();


        $extensions->add_control(
            'extension_domain',
            [
                'label' => esc_html__('Extension', 'phox-host'),
                'type'  => Controls_Manager::TEXT,
                'default'   => esc_html__('Extension', 'phox-host'),
            ]
        );

	    $extensions->add_control(
		    'upload_ext_img',
		    [
			    'label'     => esc_html__('Extension Logo', 'phox-host'),
			    'type'      => Controls_Manager::SWITCHER,
			    'label_on'     => esc_html__( 'Yes', 'phox-host' ),
			    'label_off'    => esc_html__( 'No', 'phox-host' ),
			    'return_value' => 'yes'
		    ]
	    );

        $extensions->add_control(
            'extension_domain_img',
            [
                'label' => esc_html__('Extension', 'phox-host'),
                'type'  => Controls_Manager::MEDIA,
                'condition' => [
                    'upload_ext_img' => 'yes',
                ],
            ]
        );

        $extensions->add_control(
            'extension_price',
            [
                'label' => esc_html__('Price', 'phox-host'),
                'type'  => Controls_Manager::NUMBER,
                'default'   => esc_html__('5', 'phox-host')
            ]
        );

        $extensions->add_control(
            'currency_symbol',
            [
                'label' => esc_html__( 'Currency Symbol', 'phox-host' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '' => __( 'None', 'phox-host' ),
                    'dollar' => '&#36; ' . esc_html__( 'Dollar', 'phox-host' ),
                    'euro' => '&#128; ' . esc_html__( 'Euro', 'phox-host' ),
                    'baht' => '&#3647; ' . esc_html__( 'Baht', 'phox-host' ),
                    'franc' => '&#8355; ' . esc_html__( 'Franc', 'phox-host' ),
                    'guilder' => '&fnof; ' . esc_html__( 'Guilder', 'phox-host' ),
                    'krona' => 'kr ' . esc_html__( 'Krona', 'phox-host' ),
                    'lira' => '&#8356; ' . esc_html__( 'Lira', 'phox-host' ),
                    'peseta' => '&#8359 ' . esc_html__( 'Peseta', 'phox-host' ),
                    'peso' => '&#8369; ' . esc_html__( 'Peso', 'phox-host' ),
                    'pound' => '&#163; ' . esc_html__( 'Pound Sterling', 'phox-host' ),
                    'real' => 'R$ ' . esc_html__( 'Real', 'phox-host' ),
                    'ruble' => '&#8381; ' . esc_html__( 'Ruble', 'phox-host' ),
                    'rupee' => '&#8360; ' . esc_html__( 'Rupee', 'phox-host' ),
                    'indian_rupee' => '&#8377; ' . esc_html__( 'Rupee (Indian)', 'phox-host' ),
                    'shekel' => '&#8362; ' . esc_html__( 'Shekel', 'phox-host' ),
                    'yen' => '&#165; ' . esc_html__( 'Yen/Yuan', 'phox-host' ),
                    'won' => '&#8361; ' . esc_html__( 'Won', 'phox-host' ),
                    'custom' => esc_html__( 'Custom', 'phox-host' ),
                ],
                'default' => 'dollar',
            ]
        );

        $extensions->add_control(
            'currency_symbol_custom',
            [
                'label' => esc_html__( 'Custom Symbol', 'phox-host' ),
                'type' => Controls_Manager::TEXT,
                'condition' => [
                    'currency_symbol' => 'custom',
                ],
            ]
        );

        $extensions->add_control(
            'currency_symbol_location',
            [
                'label' => esc_html__( 'Symbol Location', 'phox-host' ),
                'type' => Controls_Manager::SELECT,
                'options'	=> [
                    'left' 	=> 'Left',
                    'right' => 'Right'
                ],
                'default'	=> 'left'
            ]
        );

        $extensions->add_control(
            'show_ext',
            [
                'label' => esc_html__( 'Show Extension', 'phox-host' ),
                'type' => Controls_Manager::SWITCHER ,
                'default'	=> 'yes'
            ]
        );

        $extensions->add_control(
            'add_suggestion_list',
            [
                'label' => esc_html__( 'Add To Suggestions List', 'phox-host' ),
                'type' => Controls_Manager::SWITCHER ,
                'default'	=> 'yes'
            ]
        );

        $this->add_control(
            'the_extensions',
            [
                'label'  => esc_html__('Extensions', 'phox-host'),
                'type'   => Controls_Manager::REPEATER,
                'fields' => $extensions->get_controls(),
                'default' =>[
                    [
                       'extension_domain' => '.com'
                    ],
                    [
                        'extension_domain' => '.net'
                    ]
                ],
                'title_field' => '{{{extension_domain}}}',
            ]
        );


        $this->add_control(
            'the_check_button_text',
            [
                'label' => esc_html__('Button Text', 'phox-host'),
                'type'  => Controls_Manager::TEXT,
                'default' => esc_html__( 'Check', 'phox-host' ),
            ]
        );

        $this->add_control(
            'the_url_type',
            [
                'label' => esc_html__('URL Type', 'phox-host'),
                'type'  => Controls_Manager::SELECT,
                'options' => [
                    'whmcs'  => esc_html__( 'WHMCS URL', 'phox-host' ),
                    'custom' => esc_html__( 'Custom URL', 'phox-host' ),
                ],
                'default' => 'whmcs',

            ]
        );

        $this->add_control(
            'the_whmcs_url',
            [
                'label' => esc_html__('WHMCS URL', 'phox-host'),
                'type'  => Controls_Manager::URL,
                'condition' => [
                    'the_url_type' => 'whmcs'
                ]
            ]
        );

        $this->add_control(
            'the_custom_url',
            [
                'label' => esc_html__('Custom URL', 'phox-host'),
                'type'  => Controls_Manager::URL,
                'condition' => [
                    'the_url_type' => 'custom'
                ]
            ]
        );

        $this->add_control(
            'the_form_method',
            [
                'label'   => esc_html__('Form Method', 'phox-host'),
                'type'    => Controls_Manager::SELECT,
                'options' => [
                    'post' => esc_html__('Post', 'phox-host'),
                    'get' => esc_html__('Get', 'phox-host')
                ],
                'default' => 'post',
                'condition' => [
                    'the_url_type' => 'custom'
                ]
            ]
        );

        $this->add_control(
            'the_whois_button',
            [
                'label' => esc_html__('Whois Button', 'phox-host'),
                'type'  => Controls_Manager::SWITCHER,
                'condition' => [
	                'the_lookup_provider_id' => 'lup-1'
                ]
            ]
        );

        $this->end_controls_section();

        /**
         * Layouts Section
         * It will deprecate
         */

        $this->start_controls_section(
            'the_form_layout',
            [
                'label'	=> esc_html__('Layouts', 'phox-host'),
                'condition' => [
                    'the_extension_layouts' => [ 'l-2'],
                ]

            ]
        );

        $this->add_control(
            'the_extension_layouts',
            [
                'label' => esc_html__('Extension Layouts', 'phox-host'),
                'type'      => Controls_Manager::SELECT,
                'default'   => 'l-1',
                'options'   => [
                    'l-1' => esc_html__('Layout 1', 'phox-host'),
                    'l-2' => esc_html__('Layout 2', 'phox-host')
                ],
                'multiple'  => false
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'the_general_setting',
            [
                'label'	=> esc_html__('General Settings', 'phox-host'),

            ]
        );

	    $this->add_control(
		    'the_lookup_provider_id',
		    [
			    'label' => esc_html__('Lookup Provider', 'phox-host'),
			    'type'      => Controls_Manager::SELECT,
			    'default'   => 'lup-1',
			    'options'   => [
				    'lup-1' => esc_html__('Standard', 'phox-host'),
				    'lup-2' => esc_html__('Godaddy', 'phox-host'),
			    ],
			    'multiple'  => false,
			    'description' => esc_html__('If you select Godaddy you must add api key/secret on admin panel first')
		    ]
	    );

	    $this->add_control(
		    'general_suggestion_section_display',
		    [
			    'label'     => esc_html__('Active Suggestion Sections', 'phox-host'),
			    'type'      => Controls_Manager::SWITCHER,
			    'label_on'     => esc_html__( 'Yes', 'phox-host' ),
			    'label_off'    => esc_html__( 'No', 'phox-host' ),
			    'return_value' => 'yes',
                'default'      => 'yes'
		    ]
	    );

        $this->add_control(
            'general_suggestion_section_title',
            [
                'label' => esc_html__('Suggestion Section Title', 'phox-host'),
                'type'  => Controls_Manager::TEXT,
                'default' => esc_html__( 'More Options', 'phox-host' ),
                'condition' => [
	                'general_suggestion_section_display' => 'yes',
                ]

            ]
        );


        $this->end_controls_section();

        /*-- Style Tab --*/

        $this->start_controls_section(
            'the_plan_layout_one_style',
            [
                'label'	=> esc_html__('Form', 'phox-host'),
                'tab'		=> Controls_Manager::TAB_STYLE,

            ]
        );

        //Search Button
        $this->add_control(
            'the_heading_search_button_form',
            [
                'label'	=> esc_html__('Search Button', 'phox-host'),
                'type'	=> Controls_Manager::HEADING,
            ]
        );

        $this->add_control(
            'the_search_button_background_form',
            [
                'label'	=> esc_html__('Background', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'default' => '#274961',
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['input']	=>	'background: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'the_search_button_border_width_form',
            [
                'label'	=> esc_html__('Border Width', 'phox-host'),
                'type'	=> Controls_Manager::SLIDER,
                'default'	=> [
                    'size'	=> 1
                ],
                'range'	=>	[
                    'px'	=>	[
                        'min'	=> 0,
                        'max'	=> 10,
                    ]
                ],
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['input']	=>	'border-width: {{SIZE}}{{UNIT}} !important;'
                ]
            ]
        );

        $this->add_control(
            'the_search_button_border_color_form',
            [
                'label'	=> esc_html__('Border Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'default' => '#274961',
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['input']	=>	'border-color: {{VALUE}} !important;'
                ]
            ]
        );

	    $this->add_control(
		    'the_search_button_border_radius',
		    [
			    'label'	=> esc_html__('Border Radius', 'phox-host'),
			    'type'	=> Controls_Manager::DIMENSIONS,
			    'size_units' => array( 'px', '%' ),
			    'selectors'	=>	[
				    '{{WRAPPER}} '.$css_scheme['input'] => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
			    ]
		    ]
	    );

        //Search Hover
        $this->add_control(
            'the_heading_search_hover_form',
            [
                'label'	=> esc_html__('Search Button Hover', 'phox-host'),
                'type'	=> Controls_Manager::HEADING,
                'separator'	=> 'before'
            ]
        );

        $this->add_control(
            'the_search_button_background_hover_form',
            [
                'label'	=> esc_html__('Background', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'default' => '#c51e3a',
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['input'].':hover'	=>	'background: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'the_search_button_border_width_hover_form',
            [
                'label'	=> esc_html__('Border Width', 'phox-host'),
                'type'	=> Controls_Manager::SLIDER,
                'default'	=> [
                    'size'	=> 1
                ],
                'range'	=>	[
                    'px'	=>	[
                        'min'	=> 0,
                        'max'	=> 10,
                    ]
                ],
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['input'].':hover'	=>	'border-width: {{SIZE}}{{UNIT}} !important;'
                ]
            ]
        );

        $this->add_control(
            'the_search_button_border_color_hover_form',
            [
                'label'	=> esc_html__('Border Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'default' => '#c51e3a',
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['input'].':hover'	=>	'border-color: {{VALUE}} !important;'
                ]
            ]
        );

        //Search Field
        $this->add_control(
            'the_heading_search_field_form',
            [
                'label'	=> esc_html__('Search Field', 'phox-host'),
                'type'	=> Controls_Manager::HEADING,
                'separator'	=> 'before'
            ]
        );

        $this->add_control(
            'the_search_field_form_background',
            [
                'label'	=> esc_html__('Background', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['main-area']	=>	'background: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'the_search_field_form_text_color',
            [
                'label'	=> esc_html__('Text Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['main-area']	=>	'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'the_search_field_form_placeholder_color',
            [
                'label'	=> esc_html__('placeholder Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['main-area'].'::placeholder'=>	'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'the_search_field_form_shadow_box',
                'selector' => '{{WRAPPER}} '.$css_scheme['main-area']
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'the_search_field_form_border',
                'selector' => '{{WRAPPER}} '.$css_scheme['main-area'],
	            'fields_options' =>[
		            'border' => [
			            'default' => 'solid',
		            ],
		            'width' => [
			            'default' => [
				            'top' => '1',
				            'right' => '1',
				            'bottom' => '1',
				            'left' => '1'
			            ],
		            ],
		            'color' => [
			            'default' => '#eee',
		            ]
                ]
            ]
        );

        $this->add_control(
            'the_search_field_form_border_radius',
            [
                'label'	=> esc_html__('Border Radius', 'phox-host'),
                'type'	=> Controls_Manager::SLIDER,
                'default'	=> [
                    'size'	=> 2
                ],
                'range'	=>	[
                    'px'	=>	[
                        'min'	=> 0,
                        'max'	=> 100,
                    ]
                ],
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['main-area']	=>	'border-radius: {{SIZE}}{{UNIT}} !important;'
                ]
            ]
        );

        //TLDS
        $this->add_control(
            'the_heading_tlds_form',
            [
                'label'	=> esc_html__('Extension', 'phox-host'),
                'type'	=> Controls_Manager::HEADING,
                'separator'	=> 'before'
            ]
        );

	    $this->add_control(
		    'the_tlds_ext_inline',
		    [
			    'label'     => esc_html__('Extension Inline', 'phox-host'),
			    'type'      => Controls_Manager::SWITCHER,
			    'label_on'     => esc_html__( 'Yes', 'phox-host' ),
			    'label_off'    => esc_html__( 'No', 'phox-host' ),
			    'return_value' => 'yes'
		    ]
	    );

        $this->add_control(
            'the_tlds_ext_background_form',
            [
                'label'	=> esc_html__('Extension Background', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['ext-block'].' .domain-ext-name'	=>	'background: {{VALUE}};'
                ]
            ]
        );

	    $this->add_control(
		    'the_tlds_ext_text_color_form',
		    [
			    'label'	=> esc_html__('Extension Text Color', 'phox-host'),
			    'type'	=> Controls_Manager::COLOR,
			    'selectors'	=>	[
				    '{{WRAPPER}} '.$css_scheme['ext-block'].' .domain-ext-name'	=>	'color: {{VALUE}};'
			    ]
		    ]
	    );

	    $this->add_control(
		    'the_tlds_ext_img_height_form',
		    [
			    'label'	=> esc_html__('Extension Logo Height', 'phox-host'),
			    'type'	=> Controls_Manager::NUMBER,
			    'min'   => 1,
			    'max'   => 100,
			    'default' => 30,
			    'selectors'	=>	[
				    '{{WRAPPER}} '.$css_scheme['ext-block'].' .domain-ext-img'	=>	'height: {{VALUE}}px;'
			    ]
		    ]
	    );

	    $this->add_responsive_control(
		    'the_tlds_ext_padding_form',
		    [
			    'label'      => esc_html__( 'Extension Padding', 'phox-host' ),
			    'type'       => Controls_Manager::DIMENSIONS,
			    'size_units' => [ 'px', '%', 'em' ],
			    'selectors'  => [
				    '{{WRAPPER}} '.$css_scheme['ext-block'].' .domain-ext-img' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				    '{{WRAPPER}} '.$css_scheme['ext-block'].' .domain-ext-name' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
			    ],
		    ]
	    );

        $this->add_control(
            'the_tlds_price_background_form',
            [
                'label'	=> esc_html__('Price Background', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['ext-block'].' .price-extention-domain-wdes'	=>	'background: {{VALUE}};'
                ]
            ]
        );

	    $this->add_control(
		    'the_tlds_price_text_color_form',
		    [
			    'label'	=> esc_html__('Price Color', 'phox-host'),
			    'type'	=> Controls_Manager::COLOR,
			    'selectors'	=>	[
				    '{{WRAPPER}} '.$css_scheme['ext-block'].' .price-extention-domain-wdes'	=>	'color: {{VALUE}};'
			    ]
		    ]
	    );

	    $this->add_responsive_control(
		    'the_tlds_price_text_padding_form',
		    [
			    'label'      => esc_html__( 'Price Padding', 'phox-host' ),
			    'type'       => Controls_Manager::DIMENSIONS,
			    'size_units' => [ 'px', '%', 'em' ],
			    'selectors'  => [
				    '{{WRAPPER}} '.$css_scheme['ext-block'].' .price-extention-domain-wdes' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
			    ],
		    ]
	    );

        $this->end_controls_section();

        $this->start_controls_section(
            'the_plan_layout_results',
            [
                'label'	=> esc_html__('Results', 'phox-host'),
                'tab'		=> Controls_Manager::TAB_STYLE,

            ]
        );

        //general
        $this->add_control(
            'the_forms_results_general',
            [
                'label'	=> esc_html__('General', 'phox-host'),
                'type'	=> Controls_Manager::HEADING,
                'separator'	=> 'before'
            ]
        );

        $this->add_control(
            'the_forms_results_general_background',
            [
                'label'	=> esc_html__('Background Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-box']	=>	'background-color: {{VALUE}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            array(
                'name'        => 'the_forms_results_general_border',
                'label'       => esc_html__( 'Border', 'phox-host' ),
                'placeholder' => '1px',
                'default'     => '1px',
                'selector'    => '{{WRAPPER}} '.$css_scheme['result-box'],
            )
        );

        $this->add_control(
            'the_forms_results_general_border_radius',
            [
                'label'	=> esc_html__('Border Radius', 'phox-host'),
                'type'	=> Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-box'] => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        //text
        $this->add_control(
            'the_forms_results_text',
            [
                'label'	=> esc_html__('Text', 'phox-host'),
                'type'	=> Controls_Manager::HEADING,
                'separator'	=> 'before'
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'the_forms_results_text_typo',
                'scheme'   => Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} '.$css_scheme['result-dom'],
            ]
        );

        $this->add_control(
            'the_forms_results_text_typo_color',
            [
                'label'	=> esc_html__('Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-dom']	=>	'color: {{VALUE}} !important;'
                ]
            ]
        );

        //button
        $this->add_control(
            'the_forms_results_button',
            [
                'label'	=> esc_html__('Button', 'phox-host'),
                'type'	=> Controls_Manager::HEADING,
                'separator'	=> 'before'
            ]
        );

        $this->start_controls_tabs( 'the_forms_results_button_tabs' );

        $this->start_controls_tab(
            'available',
            [
                'label' => esc_html__( 'Available', 'phox-host' ),
            ]
        );

        $this->add_control(
            'the_forms_results_button_available_background',
            [
                'label'	=> esc_html__('Background Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-pur']	=>	'background-color: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'the_forms_results_button_available_hover_background',
            [
                'label'	=> esc_html__('Hover Background Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-pur'].':hover'	=>	'background-color: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'the_forms_results_button_available_text_color',
            [
                'label'	=> esc_html__('Text Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-pur']	=>	'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name'     => 'the_forms_results_button_available_text_typo',
                'scheme'   => Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} '.$css_scheme['result-pur'],
            )
        );

        $this->add_control(
            'the_forms_results_button_available_border_radius',
            [
                'label'	=> esc_html__('Border Radius', 'phox-host'),
                'type'	=> Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-pur']	=> 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $this->add_control(
            'the_forms_results_button_available_icon',
            [
                'label'	=> esc_html__('Icon', 'phox-host'),
                'type'	=> Controls_Manager::HEADING,
                'separator'	=> 'before'
            ]
        );

        $this->add_control(
            'the_forms_results_button_available_icon_color',
            [
                'label'	=> esc_html__('Icon Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-aval']	=>	'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'the_forms_results_button_available_icon_x_position',
            [
                'label'	=> esc_html__('X position', 'phox-host'),
                'type'	=> Controls_Manager::NUMBER,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-aval']	=>	'left: {{VALUE}}px;'
                ]
            ]
        );

        $this->add_control(
            'the_forms_results_button_available_icon_y_position',
            [
                'label'	=> esc_html__('Y position', 'phox-host'),
                'type'	=> Controls_Manager::NUMBER,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-aval']	=>	'top: {{VALUE}}px;'
                ]
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'unavailable',
            [
                'label' => esc_html__( 'Unavailable', 'phox-host' ),
            ]
        );

        $this->add_control(
            'the_forms_results_button_unavailable_background',
            [
                'label'	=> esc_html__('Background Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-token']	=>	'background-color: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'the_forms_results_button_unavailable_hover_background',
            [
                'label'	=> esc_html__('Hover Background Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-token'].':hover'	=>	'background-color: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'the_forms_results_button_unavailable_text_color',
            [
                'label'	=> esc_html__('Text Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-token']	=>	'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            array(
                'name'     => 'the_forms_results_button_unavailable_text_typo',
                'scheme'   => Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} '.$css_scheme['result-token'],
            )
        );

        $this->add_control(
            'the_forms_results_button_unavailable_border_radius',
            [
                'label'	=> esc_html__('Border Radius', 'phox-host'),
                'type'	=> Controls_Manager::DIMENSIONS,
                'size_units' => array( 'px', '%' ),
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-token']	=> 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ]
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        $this->start_controls_section(
            'the_plan_layout_spinner',
            [
                'label'	=> esc_html__('Spinner', 'phox-host'),
                'tab'		=> Controls_Manager::TAB_STYLE,

            ]
        );

        $this->add_control(
            'the_forms_spinner',
            [
                'label'	=> esc_html__('Spinner Color', 'phox-host'),
                'type'	=> Controls_Manager::COLOR,
                'selectors'	=>	[
                    '{{WRAPPER}} '.$css_scheme['result-box'].' .fa-circle-notch'	=>	'color: {{VALUE}};'
                ]
            ]
        );

	    $this->end_controls_section();

	    $this->start_controls_section(
		    'suggestions_section_style',
		    [
			    'label'	=> esc_html__('Suggestions', 'phox-host'),
			    'tab'		=> Controls_Manager::TAB_STYLE,

		    ]
	    );

	    $this->add_control(
		    'suggestions_section_main_heading',
		    [
			    'label'	=> esc_html__('Main Heading', 'phox-host'),
			    'type'	=> Controls_Manager::HEADING,
		    ]
	    );

	    $this->add_control(
		    'suggestions_section_main_heading_color',
		    [
			    'label'	=> esc_html__('Color', 'phox-host'),
			    'type'	=> Controls_Manager::COLOR,
			    'selectors'	=>	[
				    '{{WRAPPER}} '.$css_scheme['suggestion'].' > h2'  =>	'color: {{VALUE}};'
			    ]
		    ]
	    );

	    $this->add_group_control(
		    Group_Control_Typography::get_type(),
		    [
			    'name'     => 'suggestions_section_main_heading_typo',
			    'scheme'   => Typography::TYPOGRAPHY_1,
			    'selector' => '{{WRAPPER}} ' . $css_scheme['suggestion'] . ' > h2',
		    ]
	    );

	    $this->add_control(
		    'suggestions_section_items',
		    [
			    'label'	=> esc_html__('Items Text', 'phox-host'),
			    'type'	=> Controls_Manager::HEADING,
			    'separator'	=> 'before'
		    ]
	    );

	    $this->add_control(
		    'suggestions_section_item_text_color',
		    [
			    'label'	=> esc_html__('Color', 'phox-host'),
			    'type'	=> Controls_Manager::COLOR,
			    'selectors'	=>	[
				    '{{WRAPPER}} '.$css_scheme['suggestion-items'].' > h5'  =>	'color: {{VALUE}};'
			    ]
		    ]
	    );

	    $this->add_control(
		    'suggestions_section_item_text_border_color',
		    [
			    'label'	=> esc_html__('Border Color', 'phox-host'),
			    'type'	=> Controls_Manager::COLOR,
			    'selectors'	=>	[
				    '{{WRAPPER}} '.$css_scheme['suggestion-items'] =>	'border-color: {{VALUE}} !important;'
			    ]
		    ]
	    );

	    $this->add_group_control(
		    Group_Control_Typography::get_type(),
		    [
			    'name'     => 'suggestions_section_item_text_typo',
			    'scheme'   => Typography::TYPOGRAPHY_1,
			    'selector' => '{{WRAPPER}} ' . $css_scheme['suggestion-items'].' > h5',
		    ]
	    );

	    $this->add_control(
		    'suggestions_section_items_price',
		    [
			    'label'	=> esc_html__('Items Price', 'phox-host'),
			    'type'	=> Controls_Manager::HEADING,
			    'separator'	=> 'before'
		    ]
	    );

	    $this->add_control(
		    'suggestions_section_item_price_color',
		    [
			    'label'	=> esc_html__('Color', 'phox-host'),
			    'type'	=> Controls_Manager::COLOR,
			    'selectors'	=>	[
				    '{{WRAPPER}} '.$css_scheme['suggestion-items'].' > span'  =>	'color: {{VALUE}};'
			    ]
		    ]
	    );

	    $this->add_group_control(
		    Group_Control_Typography::get_type(),
		    [
			    'name'     => 'suggestions_section_item_price_typo',
			    'scheme'   => Typography::TYPOGRAPHY_1,
			    'selector' => '{{WRAPPER}} ' . $css_scheme['suggestion-items'] . ' > span',
		    ]
	    );

	    $this->add_control(
		    'suggestions_section_items_spinner',
		    [
			    'label'	=> esc_html__('Spinner', 'phox-host'),
			    'type'	=> Controls_Manager::HEADING,
			    'separator'	=> 'before'
		    ]
	    );

	    $this->add_control(
		    'suggestions_section_item_spinner_color',
		    [
			    'label'	=> esc_html__('Color', 'phox-host'),
			    'type'	=> Controls_Manager::COLOR,
			    'selectors'	=>	[
				    '{{WRAPPER}} '.$css_scheme['suggestion'].' .spinner i'  =>	'color: {{VALUE}};'
			    ]
		    ]
	    );

	    $this->end_controls_section();
    }

    /**
     * Render
     */
    protected function render() {
        $settings = $this->get_settings_for_display();

        if($settings['the_url_type'] === 'custom'){
            $url = $settings['the_custom_url']['url'];
            $method = $settings['the_form_method'];
        }else{
            $url = $settings['the_whmcs_url']['url'];
            $method = 'post';
        }

        $method = (x_wdes()->wdes_bridge_checker())?'post': $method;

        $domain_exts = [];

        ?>
        <div class="domain-element-wdes">
            <div id="wdes-domain-search">
                <form id="wdes-domain-form" class="domain-search-area-main" method="<?php echo esc_attr($method)?>" action="<?php echo esc_html(x_wdes()->wdes_domain_action_url( $url, $settings['the_url_type'] ))?>" data-setting="<?php esc_attr_e(json_encode(['whois_button' => $settings['the_whois_button']])) ?>">
                    <?php do_action('wdes_domain_verify_code'); ?>

                    <input id="wdes-domain" class="sea-dom" type="search" name="domain" placeholder="<?php echo esc_attr(($settings['the_placeholder_input']) ? $settings['the_placeholder_input'] : '') ; ?>">
                    <?php  wp_nonce_field('check-domain-nonce', 'security', true, true); ?>
                    <input type="hidden" id="wdes_lup" name="wdes_lup" value="<?php echo esc_html($settings['the_lookup_provider_id']); ?>">

                    <input id="wdes-search" class="reg-dom" type="submit" value="<?php echo esc_attr( ! empty( $settings['the_check_button_text'] ) ? $settings['the_check_button_text'] : 'Check' )?>">

                    <div id="dc-error-message-invalid" class="d-none">
                        <i class="fas fa-exclamation-circle"></i>
                        <p class="text-h-warning"><?php esc_html_e('Invalid domain name', 'phox-host'); ?></p>
                    </div>

                    <div class="extensions-block">
		                <?php foreach ($settings['the_extensions'] as $item): ?>
			                <?php if( $settings['the_extension_layouts'] === 'l-1' || is_null($settings['the_extension_layouts']) ): ?>
				                <?php
                                    if( $item['currency_symbol'] == 'custom' ){

                                        $symbol = $item['currency_symbol_custom'];


                                    }else {

                                        $symbol = x_wdes()->wdes_get_currency_symbol( $item['currency_symbol'] );

                                    }

				                ?>
                                <?php if(!empty($item['show_ext'])): ?>
                                    <a href="#" class="block <?php echo ($settings['the_tlds_ext_inline']) ? esc_attr('inline-block') : ''; ?>">
                                        <?php if( ! empty($item['extension_domain'])): ?>

                                            <?php if($item['upload_ext_img'] === 'yes'): ?>
                                                <?php if(!empty($item['extension_domain_img']['url'])): ?>
                                                    <img class="domain-ext-img" src="<?php echo esc_url($item['extension_domain_img']['url'])?>" alt="tld">
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <?php if( !empty($item['extension_domain']) ): ?>
                                                    <span class="domain-ext-name"><?php echo esc_html($item['extension_domain']); ?></span>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <div class="price-extention-domain-wdes">
                                                <?php if( $item['currency_symbol_location'] == 'left' ) : ?>
                                                    <span class="dollar-sign-wdes">
                                                <?php echo esc_html($symbol); ?>
                                            </span>
                                                    <span class="direct-price-extention-domain-wdes"><?php echo esc_html($item['extension_price']); ?></span>
                                                <?php else : ?>
                                                    <span class="direct-price-extention-domain-wdes"><?php echo esc_html($item['extension_price']); ?></span>
                                                    <span class="dollar-sign-wdes">
                                                <?php echo esc_html($symbol); ?>
                                            </span>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                    </a>
                                <?php endif; ?>
				                <?php

                                    if ( !empty($item['add_suggestion_list'] ) && ! empty($settings['general_suggestion_section_display']) ) {
                                        $remove_dot = preg_replace('/\./', '', $item['extension_domain'], 1);
                                        $domain_ext = preg_replace('/[0-9]/', '', $remove_dot);
                                        $domain_exts [] = [ 'tld' => 'domain-' . $domain_ext, 'price' => $symbol.$item['extension_price'] ];
                                    }

				                ?>
			                <?php else: ?>

                                <div class="ext-dom">
					                <?php if(!empty($item['extension_domain'])): ?>
                                        <span class="ext"><?php echo esc_html($item['extension_domain']) ; ?></span>
                                        <span class="ext-p">
                                    <?php
                                    if( $item['currency_symbol_location'] == 'left' ) {
	                                    if( $item['currency_symbol'] == 'custom' ){

		                                    echo esc_html( $item['currency_symbol_custom'] );

	                                    }else {

		                                    echo esc_html( x_wdes()->wdes_get_currency_symbol( $item['currency_symbol'] ) );

	                                    };

	                                    echo esc_html($item['extension_price']);
                                    }else{
	                                    echo esc_html($item['extension_price']);

	                                    if( $item['currency_symbol'] == 'custom' ){

		                                    echo esc_html( $item['currency_symbol_custom'] );

	                                    }else {

		                                    echo esc_html( x_wdes()->wdes_get_currency_symbol( $item['currency_symbol'] ) );

	                                    };

                                    }

                                    ?>
						        </span>
					                <?php endif; ?>
                                </div>
			                <?php endif; ?>
		                <?php endforeach; ?>
                    </div>
                    <div id="preloader-resource" data-priority-tlds='<?php echo json_encode($domain_exts); ?>'></div>
                    <!-- Result section-->
                    <div id="wdes-domain-results" class="domain-results-box">
                        <!-- Modal -->
                        <div class="modal fade" id="wdesDomainWhois" tabindex="-1" role="dialog" aria-labelledby="wdesDomainWhoisLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="wdesDomainWhoisLabel"><?php esc_html_e('Domain Whois Lookup', 'phox-host') ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--more option section-->
                    <div id="wdes-dc-more-options" class="d-none"> <h2> <?php echo esc_attr( ! empty( $settings['general_suggestion_section_title'] ) ? $settings['general_suggestion_section_title'] : 'More Options' )?> </h2> </div>
                </form>
            </div>

        </div>
        <?php

    }

}