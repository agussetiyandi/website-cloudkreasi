<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_cloudkreasi' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!7$vCoMdiRW7ct6RMcXkYWNZ-X$lg%_/s(vOc!G,.mK=Sp?L}5k[nzzMC=Fx]:p#' );
define( 'SECURE_AUTH_KEY',  '{:JfFQf$Z0i``&&/;r/W76iVFCFm5kCxq|MiotU?Kf@U}hUD)5|+-:WxVUlLQOPj' );
define( 'LOGGED_IN_KEY',    'bh0A7~ycJ$.a>q}f)7qg3V(lkvR..dE{^2mc_,%m!g,49O`Pm!.Qd;sZ2v KII]+' );
define( 'NONCE_KEY',        '|%#fCPLb.5~d:4H |7A[!OAjp~6]E.-yA4Msg6Gv32FcU_oeIk-$hDkbJDq<vH[=' );
define( 'AUTH_SALT',        'f6P{ZLC?<LO kc2PN9s0K?=C(C^f(|><*DWub@S<19EkW>WUxB[QPM,7! C{z W$' );
define( 'SECURE_AUTH_SALT', '`T$5wwHqlD9pWfb%@-miw&7ynB}H^jLP,|b_?Pq>C^+>kfvR[YGRXMM-{9$F7)2w' );
define( 'LOGGED_IN_SALT',   'OK1@ o7:WvK(>^0z1i;DCm <n/a ~Y*vdJ#oR<y@gRaI9#Td6ndQ*<ZyUK/&va${' );
define( 'NONCE_SALT',       '!<i#/EMn[B4S<K{Z3:W}5iT9$0s:{`Rp$Ufp:F6w-F1nb#.gf/p;p=*{KN@Y9hwL' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ck_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
